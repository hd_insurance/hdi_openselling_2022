@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Sellings.ExtenActionHdi.SyncDataCore.component.formSearch')

    {{--list data---}}
    @include('Sellings.ExtenActionHdi.SyncDataCore.component.listData')

@stop
