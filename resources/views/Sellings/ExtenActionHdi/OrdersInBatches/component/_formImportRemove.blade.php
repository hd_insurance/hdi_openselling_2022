<div style="position: relative">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>

    <div id="divDetailItem">
        <div class="card-header">
            Import hủy danh sách đơn
            <div class="btn-actions-pane-right">
                <button type="button"  class="btn color_hdi" onclick="jqueryCommon.hideContentRightPage()" title="{{viewLanguage('Close')}}">&nbsp;&nbsp;<i class="pe-7s-close fa-3x"></i>&nbsp;&nbsp;</button>
                &nbsp;&nbsp;&nbsp;
            </div>
        </div>
        <div class="div-infor-right marginT20">
            <div class="main-card mb-3">
                <div class="card-body paddingTop-unset">
                    <form id="form_import_remove" enctype="multipart/form-data">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-9">
                                    <label for="user_group">Chương trình bảo hiểm</label> <span class="red"> (*)</span>
                                    <div id="select_programme" class="">
                                        <select class="input-sm chosen-select w-100" name="p_programme_code" id="p_programme_code" onchange="ajaxGetInforProgramme(1,'{{$urlActionFunction}}');">
                                            {!! $optionProgrammes !!}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row marginT20">
                                <div class="col-lg-12">
                                    <label for="user_group"><b id="title_create_order"></b></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-lg-3">
                                    <label for="user_group">Số phụ lục hợp đồng</label>
                                    <input type="text" class="form-control input-sm" id="p_contract_no" name="p_contract_no" placeholder="Số phụ lục hợp đồng">
                                </div>
                                <div class="col-lg-3">
                                    <label for="user_email">File danh sách Import</label>
                                    <label title="{{viewLanguage('Upload danh sách')}}" for="inputFileExcelImport" class="w-100 btn-transition btn btn-outline-success">
                                        <input type="file" name="inputFileExcelImport" id="inputFileExcelImport" style="display:none">
                                        <i class="fa fa-share-square"></i> {{viewLanguage('Upload danh sách(xlsx,xls)')}}
                                    </label>
                                </div>
                            </div>
                            <div class="row form-group marginT15">
                                <input name="data_infor_program" id="data_infor_program" type="hidden">
                                <div class="col-lg-3">
                                    <button class="w-100 mb-2 mr-2 btn btn-danger" id="submitImportRemove" type="button" > <i class="fa fa-check"></i> {{viewLanguage('Hủy đơn')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Nội dung form Edit Other by ajax--}}
    <div id="content-other-right"></div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var config = {
            '.chosen-select'           : {width: "100%"},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $("#submitImportRemove").click(function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            // Get form
            submitAjaxFormMultipartImport('form_import_remove','submitImportRemove','{{$urlPostFormImport}}')
        });
    });
    function submitAjaxFormMultipartImport(form_id,btnSubmit,urlAjax){
        var chose_program_id = $('#p_programme_code').val();
        if(chose_program_id > 0){
            var form = $('#'+form_id)[0];
            // Create an FormData object

            var data = new FormData(form);
            //data chương trình
            var data_infor_program = $('#data_infor_program').val();
            data.append("data_infor_program", data_infor_program);
            data.append("p_type_import", 2);

            // disabled the submit button
            //$("#"+btnSubmit).prop("disabled", true);
            $('#loader').show();
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: urlAjax,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (res) {
                    $('#loader').hide();
                    if(res.success == 1){
                        jqueryCommon.showMsg('success', 'Đã cấp đơn thành công');
                        window.location.href = res.urlIndex;
                    }else {
                        jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.message);
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            });
        }else {
            jqueryCommon.showMsg('error', '', 'Thông báo lỗi', 'Chưa chọn Chương trình để cấp đơn');
        }

    }

    function ajaxGetInforProgramme(type_edit,url){
        var p_programme_code = 0
        if(type_edit == 1){
            p_programme_code = $('#p_programme_code').val();
        }else {
            p_programme_code = 0;
        }
        var dataOption = $('#dataOption').val();
        var _token = $('input[name="_token"]').val();
        $('#loader').show();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                '_token': _token,
                'programme_id': p_programme_code,
                'dataOption': dataOption,
                'divShowId': 'data_infor_programme',
                'templateView': '_inforProgramme',
                'functionAction': 'ajaxGetInforProgramme'
            },
            success: function (res) {
                $('#loader').hide();
                if (res.success == 1) {
                    $('#data_infor_programme').html(res.html);

                    //block cấp đơn
                    if(res.dataCreate.inforProgram.CONTRACT_NO != ''){
                        $('#p_contract_no').val(res.dataCreate.inforProgram.CONTRACT_NO);
                        $('#p_contract_no').prop('readonly', true);
                    }
                    $('#data_infor_program').val(JSON.stringify(res.dataCreate.inforProgram));
                    $('#title_create_order').html(res.dataCreate.title_create_order);
                } else {
                    jqueryCommon.showMsgError(res.success, res.message);
                }
            }
        });
    }

</script>
