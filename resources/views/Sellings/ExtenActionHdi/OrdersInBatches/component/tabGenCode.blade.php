<?php
$formName = 'creat_order';
?>
<div class="ibox">
    <form id="form_gen_code" enctype="multipart/form-data">
        <input name="_token" type="hidden" value="{{ csrf_token() }}">
        <div class="row form-group">
            <div class="col-lg-5">
                <label for="user_group">Khách hàng</label> <span class="red"> (*)</span>
                <select class="input-sm chosen-select w-100" name="p_org_seller" id="p_org_seller">
                    {!! $optionOrg !!}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="user_group">Sản phẩm</label> <span class="red"> (*)</span>
                <select class="input-sm chosen-select w-100" name="p_product" id="p_product">
                    {!! $optionProducts !!}
                </select>
            </div>

            <div class="col-lg-2">
                <label for="user_group">Upload file excel</label>
                <label title="{{viewLanguage('Upload file excel')}}" for="inputFileExcel" class="w-100 btn-transition btn btn-outline-success">
                    <input type="file" name="inputFileExcel" id="inputFileExcel" style="display:none">
                    <i class="fa fa-share-square"></i> {{viewLanguage('Upload (xlsx.xls)')}}
                </label>
            </div>
            <div class="col-lg-2 marginT30">
                <button class="w-100 mb-2 mr-2 btn btn-success" id="submitGenCode" type="button" > <i class="fa fa-check"></i> {{viewLanguage('Gen GCN')}}</button>
            </div>
        </div>

        <div class="row form-group">
            <div class=" col-lg-3">
                <label for="user_email">Từ ngày tạo</label>
                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_from_date" id="p_from_date" @if(isset($search['p_from_date']))value="{{$search['p_from_date']}}"@endif>
                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
            </div>
            <div class=" col-lg-3">
                <label for="user_email">Đến ngày tạo</label>
                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_to_date" id="p_to_date" @if(isset($search['p_to_date']))value="{{$search['p_to_date']}}"@endif >
                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
            </div>
            <div class="col-lg-3 marginT30">
                <input type="hidden" id="div_show" name="div_show" value="table_show_ajax">
                <button class="btn btn-primary" type="button" name="submit" value="1" onclick="jqueryCommon.searchAjaxWithForm('form_gen_code','{{$urlSearchAjax}}')"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
            </div>
        </div>
    </form>
</div>

<div class="main-card mb-3 card" id="table_show_ajax"></div>

<script type="text/javascript">
    $(document).ready(function () {
        jqueryCommon.pagingAjaxWithForm('form_gen_code','{{$urlSearchAjax}}');

        $("#submitGenCode").click(function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            // Get form
            submitAjaxFormMultipart('form_gen_code','submitGenCode','{{$urlPostGenCode}}')
        });
    });

    function submitAjaxFormMultipart(form_id,btnSubmit,urlAjax){
        var p_org_seller = $('#p_org_seller').val();
        var p_product = $('#p_product').val();
        if(p_org_seller != '' && p_product != ''){
            var form = $('#'+form_id)[0];
            var data = new FormData(form);

            // disabled the submit button
            $("#"+btnSubmit).prop("disabled", true);
            $('#loader').show();
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: urlAjax,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (res) {
                    $('#loader').hide();
                    if(res.success == 1){
                        jqueryCommon.showMsg('success', 'Đã cấp đơn thành công');
                        //location.reload();
                    }else {
                        jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.msg);
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            });
        }else {
            jqueryCommon.showMsg('error', '', 'Thông báo lỗi', 'Chưa chọn Chương trình để cấp đơn');
        }

    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        var config = {
            '.chosen-select'           : {width: "100%"},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });

</script>




