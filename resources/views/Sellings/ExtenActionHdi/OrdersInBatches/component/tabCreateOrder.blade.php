<?php
$formName = 'creat_order';
?>
<div class="ibox">
    {{----Block 1----}}
    @include('Sellings.ExtenActionHdi.OrdersInBatches.component._blockCrearOrder1')

    {{----Block 2----}}
    @include('Sellings.ExtenActionHdi.OrdersInBatches.component._blockCrearOrder2')

</div>


<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        var config = {
            '.chosen-select'           : {width: "100%"},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });

</script>




