<input name="_token" type="hidden" value="{{ csrf_token() }}">
<input name="dataOption" id="dataOption" type="hidden" @if(isset($dataOption))value="{{ json_encode($dataOption)}}"@endif>
<input name="dataInforCreatOrder" id="dataInforCreatOrder" type="hidden" @if(isset($dataInfor))value="{{ json_encode($dataInfor)}}"@endif>

<form id="form_programme{{$isEdit}}" enctype="multipart/form-data">
    <input name="_token" type="hidden" value="{{ csrf_token() }}">
<div class="form-group form-infor-common">
    <div class="row">
        <div class="col-lg-9">

            <div id="select_programme" class="">
                <label for="user_group">Chọn chương trình bảo hiểm</label> <span class="red"> (*)</span>
                <select class="input-sm chosen-select w-100" name="p_programme_code" id="p_programme_code{{$isEdit}}" onchange="ajaxGetInforProgramme(1,{{$isEdit}},'{{$urlActionFunction}}');">
                    {!! $optionProgrammes !!}
                </select>
            </div>
            <div id="input_programme" class="display-none-block">
                <label for="user_group">Nhập tên chương trình bảo hiểm</label> <span class="red"> (*)</span>
                <input type="text" class="form-control input-sm" id="p_programme_name" name="p_programme_name" placeholder="Tên chương trình bảo hiểm">
            </div>
        </div>
        @if($isEdit ==1)
        <div class="col-lg-3 marginT40">
            <input type="hidden" id="check_create_programme" name="check_create_programme" value="0">
            <input type="checkbox" class="custom-checkbox float-left" onchange="jqueryCommon.changeRadio('check_create_programme'); showHideAddProgramme();">
            <label for="check_create_programme" class="float-left marginL10" id="show_text_edit">{{viewLanguage('Cập nhật')}}</label>
        </div>
        @endif
    </div>

    <div class="row marginT10" id="data_infor_programme{{$isEdit}}">
        {{--@include('Sellings.ExtenActionHdi.OrdersInBatches.component._inforProgramme')--}}
    </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        //showHideAddProgramme();
    });

    function showHideAddProgramme(){
        var value_defaul = $("#check_create_programme").val();//0 edit: 1; thêm mới
        if(value_defaul == 1){//thêm mới
            $("#input_programme").removeClass("display-none-block");
            $("#select_programme").addClass("display-none-block");
            $("#show_text_edit").html('Thêm mới');
        }else {
            $("#select_programme").removeClass("display-none-block");
            $("#input_programme").addClass("display-none-block");
            $("#show_text_edit").html('Cập nhật');
        }
        ajaxGetInforProgramme(0,{{$isEdit}},'{{$urlActionFunction}}');
    }

    function ajaxGetInforProgramme(type_edit2,isEditProgram,url){
        var p_programme_code = -1;
        var type_edit = $("#check_create_programme").val();//0 thêm mới: 1; eddit
        if(type_edit == 1){
            p_programme_code = 0;
        }else {
            p_programme_code = $('#p_programme_code'+isEditProgram).val();
        }
        var dataOption = $('#dataOption').val();
        var divShowId = 'data_infor_programme'+isEditProgram;
        var _token = $('input[name="_token"]').val();

        $('#'+divShowId).html('');
        $('#loader').show();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                '_token': _token,
                'programme_id': p_programme_code,
                'dataOption': dataOption,
                'isEditProgram': isEditProgram,
                'divShowId': divShowId,
                'templateView': '_inforProgramme',
                'functionAction': 'ajaxGetInforProgramme'
            },
            success: function (res) {
                $('#loader').hide();
                $('#'+divShowId).html('');
                if (res.success == 1) {
                    $('#'+divShowId).html(res.html);

                    //block cấp đơn
                    if(res.dataCreate.inforProgram.NUM_OF_GEN == 1){
                        $('#p_contract_addendum').val(res.dataCreate.inforProgram.CONTRACT_NO);
                        $('#p_contract_addendum').prop('readonly', true);
                    }

                    $('#lancapdon').html('Lần cấp '+res.dataCreate.inforProgram.NUM_OF_GEN);
                    $('#p_lancapdon').val(res.dataCreate.inforProgram.NUM_OF_GEN);
                    $('#title_create_order').html(res.dataCreate.title_create_order);
                } else {
                    jqueryCommon.showMsgError(res.success, res.message);
                }
            }
        });
    }

    function ajaxChangeParamPack(url){
        var p_product = $('#p_product').val();
        var p_org_buyer = $('#p_org_buyer').val();

        if(p_product.trim() != ''){
            var dataInforCreatOrder = $('#dataInforCreatOrder').val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url,
                data: {
                    '_token': _token,
                    'p_product': p_product,
                    'p_org_buyer': p_org_buyer,
                    'dataInforCreatOrder': dataInforCreatOrder,
                    'divShowId': 'table_list_packs',
                    'templateView': '_table_list_packs',
                    'functionAction': 'ajaxChangeParamPack'
                },
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        $('#table_list_packs').html(res.html);
                    } else {
                        jqueryCommon.showMsgError(res.success, res.message);
                    }
                }
            });
        }else {
            $('#table_list_packs').html('');
            $('#p_package_obj').val('');
        }
    }

    function ajaxAddInforPacks(url){
        var p_product = $('#p_product').val();
        var p_org_buyer = $('#p_org_buyer').val();
        var p_package_obj = $('#p_package_obj').val();

        if(p_product.trim() == ''){
            jqueryCommon.showMsgError(0, 'Bạn chưa chọn Sản phẩm cho gói');
            return false;
        }
        /*if(p_org_buyer.trim() == ''){
            jqueryCommon.showMsgError(0, 'Bạn chưa chọn Khách hàng cho gói');
            return false;
        }*/
        var dataInforCreatOrder = $('#dataInforCreatOrder').val();
        var _token = $('input[name="_token"]').val();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                '_token': _token,
                'p_product': p_product,
                'p_org_buyer': p_org_buyer,
                'p_package_obj': p_package_obj,
                'dataInforCreatOrder': dataInforCreatOrder,
                'titlePopup': 'Cập nhật gói cho chương trình',
                'divShowId': 'sys_show_infor',
                'templateView': '_popupInforPacks',
                'functionAction': 'ajaxAddInforPacks'
            },
            success: function (res) {
                $('#loader').hide();
                if (res.success == 1) {
                    $('#sys_showPopupCommon').modal('show');
                    $(".modal-dialog").addClass("modal-lg");
                    $('#sys_show_infor').html(res.html);
                } else {
                    jqueryCommon.showMsgError(res.success, res.message);
                }
            }
        });
    }
</script>
