<div class="card-body">
    @if(isset($data) && !empty($data))
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="3%" class="text-center middle">{{viewLanguage('STT')}}</th>
                    <th width="10%" class="text-center middle">{{viewLanguage('Số GCN')}}</th>

                    <th width="15%" class="text-center middle">{{viewLanguage('Tên khách hàng')}}</th>
                    <th width="12%" class="text-center middle">{{viewLanguage('Sản phẩm')}}</th>
                    <th width="10%" class="text-center middle">{{viewLanguage('Gói')}}</th>

                    <th width="10%" class="text-center middle">{{viewLanguage('Người tạo')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-left middle">@if(isset($item->CERTIFICATE_NO)){{$item->CERTIFICATE_NO}}@endif</td>

                        <td class="text-left middle">@if(isset($item->NAME)){{$item->NAME}}@endif</td>
                        <td class="text-left middle">
                            @if(isset($item->PRODUCT_CODE)){{$item->PRODUCT_CODE}}<br/>@endif
                            @if(isset($item->PRODUCT_NAME)){{$item->PRODUCT_NAME}}@endif
                        </td>
                        <td class="text-left middle">
                            @if(isset($item->PACK_CODE)){{$item->PACK_CODE}}<br/>@endif
                            @if(isset($item->PACK_NAME)){{$item->PACK_NAME}}@endif
                        </td>
                        <td class="text-left middle">
                            @if(isset($item->CREATE_BY)){{$item->CREATE_BY}}<br/>@endif
                            @if(isset($item->CREATE_DATE)){{$item->CREATE_DATE}}@endif
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
<script type="text/javascript">
    $(document).ready(function(){
        jqueryCommon.pagingAjaxWithForm('form_gen_code','{{$urlSearchAjax}}');
    });
</script>