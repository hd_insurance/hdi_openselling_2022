@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Sellings.ExtenActionHdi.OrdersInBatches.component.formSearch')

    {{--list data---}}
    @include('Sellings.ExtenActionHdi.OrdersInBatches.component.listData')

@stop
