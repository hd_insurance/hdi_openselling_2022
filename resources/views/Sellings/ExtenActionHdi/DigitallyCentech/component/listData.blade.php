<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Tìm kiếm')}}</h5>
        <div class="ibox-tools marginDownT6">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-10">
                <label for="user_email">Số hợp đồng/Số GCN</label>
                <input type="text" class="form-control input-sm" id="p_search" name="p_search" placeholder="1;2;3;" @if(isset($search['p_search']))value="{{$search['p_search']}}"@endif>
            </div>
            <div class="col-lg-2 marginT30">
                <button class="btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="col-lg-4 marginT20" id="show_button_approval_order">
        @if($is_root || $permission_view)
        <button class="btn btn-light" type="button" name="approval_order" id="approval_order" value="0" onclick="clickApprovalOrderList('{{$urlCancelOrder}}')"><i class="fa fa-check"></i> {{viewLanguage('Hủy ký số')}}</button>
        @endif
    </div>
    <div class="card-body">
        @if($data && sizeof($data) > 0)
            <div class="row">
                <div class="col-lg-4 text-left">
                    <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif</h5>
                </div>
                <div class="col-lg-8 text-right">
                    @if($total >0)
                        <button class="btn-transition btn btn-outline-warning btn-search-right marginDownT15 display-none-block" type="submit" name="submit" value="2" title="Xuất excel"><i class="fa fa-file-excel"></i></button>
                    @endif
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                        <tr class="table-background-header">
                            <th width="3%" class="text-center middle"><input type="checkbox" class="check" id="checkAllOrder"></th>
                            <th width="10%" class="text-center middle">{{viewLanguage('Batch id')}}</th>
                            <th width="10%" class="text-center middle">{{viewLanguage('User code')}}</th>
                            <th width="10%" class="text-center middle">{{viewLanguage('Mẫu giấy')}}</th>

                            <th width="8%" class="text-center middle">{{viewLanguage('Mã file ký')}}</th>
                            <th width="10%" class="text-center middle">{{viewLanguage('Sản phẩm')}}</th>
                            <th width="10%" class="text-center middle">{{viewLanguage('Mã tổ chức')}}</th>
                            <th width="8%" class="text-center middle">{{viewLanguage('Xem File')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $item)
                        <tr>
                            <td class="text-center middle">
                                <input class="check" type="checkbox" @if(isset($item->BATCH_ID)) name="checkItems[]"@else disabled @endif value="@if(isset($item->BATCH_ID)){{$item->BATCH_ID}}@endif" data-temp-code="{{$item->TEMP_CODE}}" data-file-code="{{$item->FILE_CODE}}" data-product-code="{{$item->PRODUCT_CODE}}" data-org-code="{{$item->ORG_CODE}}" onchange="changeColorButton();">
                            </td>
                            <td class="text-left middle"> @if(isset($item->BATCH_ID)){{$item->BATCH_ID}}@endif</td>
                            <td class="text-left middle"> @if(isset($item->USER_CODE)){{$item->USER_CODE}}@endif</td>
                            <td class="text-left middle"> @if(isset($item->TEMP_CODE)){{$item->TEMP_CODE}}@endif</td>
                            <td class="text-left middle"> @if(isset($item->FILE_CODE)){{$item->FILE_CODE}}@endif</td>

                            <td class="text-left middle"> @if(isset($item->PRODUCT_CODE)){{$item->PRODUCT_CODE}}@endif</td>
                            <td class="text-left middle"> @if(isset($item->ORG_CODE)){{$item->ORG_CODE}}@endif</td>
                            <td class="text-left middle">
                                @if(isset($item->FILE_SIGNED))
                                    <a href="{{$item->FILE_SIGNED}}" target="_blank" title="Xem file">Xem file</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="paging_simple_numbers">
                {{--{!! $paging !!}--}}
            </div>
        @else
            <div class="alert">
                Không có dữ liệu
            </div>
        @endif
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        $("#checkAllOrder").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
            changeColorButton();
        });
    });

    function changeColorButton(){
        var changeColor = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                changeColor = 1;
            }
        });
        if(changeColor == 1){
            //$('#show_button_approval_order').removeClass("display-none-block");
            $("#approval_order").addClass("btn-danger");
            $("#approval_order").removeClass("btn-light");
        }else {
            //$('#show_button_approval_order').addClass("display-none-block");
            $("#approval_order").removeClass("btn-danger");
            $("#approval_order").addClass("btn-light");
        }
    }
    function clickApprovalOrderList(url_ajax){
        var dataId = [];
        var dataTempCode = [];
        var dataOrgCode = [];
        var dataProductCode = [];
        var dataFileCode = [];
        var i = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                dataId[i] = $(this).val();
                dataTempCode[i] = $(this).attr('data-temp-code');
                dataOrgCode[i] = $(this).attr('data-org-code');
                dataProductCode[i] = $(this).attr('data-product-code');
                dataFileCode[i] = $(this).attr('data-file-code');
                i++;
            }
        });
        if (dataId.length == 0) {
            alert('Bạn chưa chọn đơn để thao tác.');
            return false;
        }
        var _token = $('input[name="_token"]').val();
        var msg = 'Bạn có muốn HỦY các đơn này?';
        jqueryCommon.isConfirm(msg).then((confirmed) => {
            $('#loader').show();
            $.ajax({
                type: "post",
                url: url_ajax,
                data: {dataId: dataId, _token: _token, dataTempCode: dataTempCode, dataOrgCode: dataOrgCode, dataProductCode: dataProductCode, dataFileCode: dataFileCode},
                dataType: 'json',
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        jqueryCommon.showMsg('success',res.message);
                        window.location.reload();
                    } else {
                        jqueryCommon.showMsgError(res.success,res.message);
                    }
                }
            });
        });
    }

</script>



