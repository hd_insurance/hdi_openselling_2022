@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
    {{--Search---}}
    {{--@include('Sellings.PaymentContract.component.formSearch')--}}

    {{--list data---}}
    @include('Sellings.ExtenActionHdi.DigitallyCentech.component.listData')
    {{ Form::close() }}
@stop
