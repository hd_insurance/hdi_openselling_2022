<div class="div-parent-background">
    <div class="div-background">
        <div class="div-block-right">
            <a href="javascript:void(0);" onclick="jqueryCommon.hideContentRightPageLayout();" class="btn-close-search-list btn btn-default" title="{{viewLanguage('Đóng lại')}}">
                <i class="pe-7s-close fa-w-16 fa-3x"></i>
            </a>
            {{-- Nội dung form search--}}
            <form id="form_create_order" enctype="multipart/form-data">
            {{--<form method="POST" action="{{$urlCreateDigitallySigned}}" enctype="multipart/form-data">--}}
                {{ csrf_field() }}
            <div class="content-search-page" >
                <h3 class="themeoptions-heading">Tạo ký số</h3>
                <div class="ibox-content">
                    <div class="row">
                        <div class="form-group col-lg-12">
                            <label for="depart_name">{{viewLanguage('Số chứng từ')}}</label>
                            <input type="text" class="form-control input-sm" id="FILE_REFF" name="FILE_REFF">
                        </div>
                        <div class="form-group col-lg-12">
                            <label for="depart_name">{{viewLanguage('Tọa độ ký số dưới nội dung')}}</label>
                            <input type="text" class="form-control input-sm" id="TEXT_CA" name="TEXT_CA" value="Ngày cấp" readonly>
                        </div>

                        <div class="form-group col-lg-12 form-file">
                            <label for="depart_name">{{viewLanguage('Upload file')}}</label><span class="red"> *(pdf)</span><br>
                            <input type="file" id="inputFile" name="inputFile" >
                        </div>
                        <hr>
                        <div class="form-group col-lg-12">
                            @if($is_root || $permission_view)
                                <button class="mb-2 mr-2 btn-icon btn btn-success submitFormItem" id="submitCreateOrder" type="submit" ><i class="fa fa-search"></i> {{viewLanguage('Tạo ký số')}}</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#submitCreateOrder").click(function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            // Get form
            submitAjaxFormMultipart('form_create_order','submitCreateOrder','{{$urlCreateDigitallySigned}}')
        });
    });
    function submitAjaxFormMultipart(form_id,btnSubmit,urlAjax){
        var formPrograme = $(document.forms[form_id]);
        var inputFile = formPrograme.find('#inputFile').val();
        var TEXT_CA = formPrograme.find('#TEXT_CA').val();
        var FILE_REFF = formPrograme.find('#FILE_REFF').val();

        //if(chose_program_id > 0){
            if(FILE_REFF.length <= 0){
                jqueryCommon.showMsg('error', '', 'Thông báo lỗi', 'Bạn chưa nhập Số chứng từ');
                return;
            }
            if(TEXT_CA.length <= 0){
                jqueryCommon.showMsg('error', '', 'Thông báo lỗi', 'Bạn chưa nhập Tọa độ ký số dưới nội dung');
                return;
            }
            if(inputFile.length <= 0){
                jqueryCommon.showMsg('error', '', 'Thông báo lỗi', 'Bạn chưa upload file ký');
                return;
            }

            var title = "Bạn chắc chắn ký số cho file này";
            jqueryCommon.isConfirm(title).then(() => {
                var form = $('#'+form_id)[0];
                // Create an FormData object
                var data = new FormData(form);

                // disabled the submit button
                $("#"+btnSubmit).prop("disabled", true);
                $('#loader').show();
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: urlAjax,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (res) {
                        $('#loader').hide();
                        if(res.success == 1){
                            jqueryCommon.showMsg('success', 'Đã cấp đơn thành công');
                            window.location.href = res.urlIndex;
                        }else {
                            jqueryCommon.showMsg('error', '', 'Thông báo lỗi', 'Cấp đơn lỗi');
                        }
                        $("#"+btnSubmit).prop("disabled", false);
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });
            });
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.form-file input').each(function () {
            $this = jQuery(this);
            $this.on('change', function() {
                var fsize = $this[0].files[0].size,
                    ftype = $this[0].files[0].type,
                    fname = $this[0].files[0].name,
                    fextension = fname.substring(fname.lastIndexOf('.')+1);
                validExtensions = ["pdf"];
                if ($.inArray(fextension, validExtensions) == -1){
                    alert("Up file .pdf! Hãy up lại");
                    this.value = "";
                    return false;
                }else{
                    if(fsize > 3145728){/*1048576-1MB(You can change the size as you want)*/
                        alert("File quá lớn, dung lương cho phép < 3MB");
                        this.value = "";
                        return false;
                    }
                    return true;
                }

            });
        });
    });
</script>
