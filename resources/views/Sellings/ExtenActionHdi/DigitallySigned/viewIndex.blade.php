@extends('Layouts.BaseAdmin.indexHDI')
@section('content')

    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

        {{--Search---}}
     @include('Sellings.ExtenActionHdi.DigitallySigned.component.formDigitallySigned')
    {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
        {{--list data---}}
        @include('Sellings.ExtenActionHdi.DigitallySigned.component.listDigitallySigned')
    {{ Form::close() }}
@stop
