<div class="modal-content" id="popupHistoryTimeLine" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_popupHistoryTimeLine">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body" style="padding-top: -20px!important;">
            <div class="form-group">
                <div class="row">
                    @if(!empty($dataHistoryUpdate))
                    <div class="card-body" style="margin-top: -35px!important; height: 500px; overflow: scroll; overflow-x:hidden ">
                        <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column marginL5">
                            @foreach ($dataHistoryUpdate as $date_timeLine => $item_timeLine)
                                <div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                        <span class="vertical-timeline-element-date" style="left: -110px">
                                            <h5 class="card-title">{{$date_timeLine}}</h5>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <?php
                                $arrColor = [
                                    1=>'badge-success',
                                    2=>'badge-warning',
                                    3=>'badge-danger',
                                    4=>'badge-primary',
                                    5=>'badge-info',
                                    6=>'badge-dark'];
                                $key_color = 1;
                                ?>
                                @foreach ($item_timeLine as $key_hour => $arr_timeLine)
                                    <div class="vertical-timeline-item vertical-timeline-element">
                                        <div>
                                            <span class="vertical-timeline-element-icon bounce-in">
                                                <i class="badge badge-dot badge-dot-xl {{$arrColor[$key_color]}}"> </i>
                                            </span>
                                            <div class="vertical-timeline-element-content bounce-in">
                                                <p>{{$arr_timeLine['CREATE_BY']}}</p>
                                                <table class="table table-bordered table-hover">
                                                    <thead class="thin-border-bottom">
                                                    <tr class="table-background-header">
                                                        <th width="2%" class="text-center middle">STT</th>
                                                        <th width="28%" class="text-center middle">Thông tin</th>

                                                        <th width="35%" class="text-center middle">Giá trị cũ</th>
                                                        <th width="35%" class="text-center middle">Giá trị mới</th>
                                                    </tr>
                                                    </thead>
                                                    @foreach ($arr_timeLine['HISTORY_UPDATE'] as $key_item => $valu_timeLine)
                                                    <tbody>
                                                        <td class="text-center middle">{{$key_item+1}}</td>
                                                        <td class="text-left middle">{{$valu_timeLine->DESCRIPTION}}</td>
                                                        <td class="text-left middle">{{$valu_timeLine->OLD_VALUE}}</td>
                                                        <td class="text-left middle">{{$valu_timeLine->NEW_VALUE}}</td>
                                                    </tbody>
                                                    @endforeach
                                                </table>
                                                <span class="vertical-timeline-element-date" style="left: -64px">{{$key_hour}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        $key_color = $key_color + 1;
                                        if($key_color == 7)
                                        $key_color=1;
                                    ?>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                    @else
                        Chưa có dữ liệu
                    @endif
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                <i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});
    });
</script>
