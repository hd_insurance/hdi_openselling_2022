<div>
    <div class="formInforItem" >
        <div class="">
            @if(!empty($listTimeLine))
                <?php
                    $arrStatusShow = [STATUS_CLAIM_DYCB,STATUS_CLAIM_DYCC,STATUS_CLAIM_TCCB,STATUS_CLAIM_TCCC];
                ?>
                @foreach ($listTimeLine as $key => $item)
                    @if(in_array($item->WORK_ID,$arrStatusShow))
                        <div class="form-group form-infor-detail paddingT10">
                            <div class="row form-group">
                                <div class="col-lg-12">
                                    <b>@if(isset($item->STAFF_CODE)){{$item->STAFF_CODE}}@endif @if(isset($item->STAFF_NAME)) - {{$item->STAFF_NAME}}@endif</b>
                                    @if(isset($item->WORK_NAME))( {{$item->WORK_NAME}} )@endif
                                    <span class="marginL10"><i class="font_10">@if(isset($item->PROCESSING_DATE)){{$item->PROCESSING_DATE}}@endif</i></span>
                                </div>
                                <div class="col-lg-12 marginT10">
                                    @if(isset($item->CONTENT)){!! $item->CONTENT !!}@endif
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //
    });
</script>