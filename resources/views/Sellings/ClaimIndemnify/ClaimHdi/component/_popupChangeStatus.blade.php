<div class="modal-content" id="{{$formNameOther}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="0">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="paramSearch" name="paramSearch" value="{{json_encode($paramSearch)}}">
        <input type="hidden" id="dataClaim" name="dataClaim" value="{{json_encode($dataClaim)}}">
        <input type="hidden" id="dataItem" name="dataItem" value="{{json_encode($dataItem)}}">
        <input type="hidden" id="listBoiThuong" name="listBoiThuong" value="{{json_encode($listBoiThuong)}}">
        <input type="hidden" id="listBoiThuongChose" name="listBoiThuongChose" value="{{json_encode($listBoiThuongChose)}}">
        <input type="hidden" id="listFilesAdd" name="listFilesAdd" value="{{json_encode($listFilesAdd)}}">
        <input type="hidden" id="arrLisDocument" name="arrLisDocument" value="{{json_encode($arrLisDocument)}}">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body paddingBottom-unset">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="NAME" class="text-right">{{viewLanguage('Trạng thái')}}</label> <span
                            class="red">(*)</span>
                        <select class="form-control input-sm" required id="CLAIM_STATUS_POPUP" name="CLAIM_STATUS" onchange="changeStatus(this);">
                            {!! $optionStatusPopup !!}
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label for="NAME" class="text-right">{{viewLanguage('Nhân viên xử lý')}}</label>
                        <span class="form-control">
                            {{$userAction['user_full_name']}}
                        </span>
                    </div>
                </div>
            </div>
            {{-----DYKH,TTBT-----}}
            @if(isset($listBoiThuong) && !empty($listBoiThuong))
                <div class="display-none-block" id="listboithuong">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6 display-none-block" id="dateboithuong">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày xử lý')}}</label>
                                <span class="red">(*)</span>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="PAY_DATE" id="PAY_DATE" @if(isset($dataClaim['PAY_DATE']) && trim($dataClaim['PAY_DATE']) !='')value="{{$dataClaim['PAY_DATE']}}" @else value="{{date('d/m/Y')}}" @endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                            <div class="col-lg-6 display-none-block" id="chose_add_claim">
                                <label for="NAME" class="text-right">{{viewLanguage('Chọn quyền lợi bồi thường')}}</label>
                                <select class="form-control input-sm" id="option_claim" name="option_claim" onchange="changeOptionClaim(this,'{{$urlAjaxGetData}}');">
                                    {!! $optionBoiThuong !!}
                                </select>
                            </div>

                            @if(isset($dataItem['REQUIRED_AMOUNT']) && $dataItem['REQUIRED_AMOUNT'] > 0)
                            <div class="col-lg-6">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Số tiền KH yêu cầu bồi thường')}}</label>
                                <span class="red">(*)</span>
                                <input type="text" name="REQUIRED_AMOUNT" id="REQUIRED_AMOUNT" value="{{numberFormat($dataItem['REQUIRED_AMOUNT'])}}" readonly class="form-control input-sm text-right">
                                <input type="hidden" name="tong_money_yeu_cau" id="tong_money_yeu_cau" value="{{$dataItem['REQUIRED_AMOUNT']}}">
                            </div>
                            @endif

                        </div>
                    </div>

                    <!---Phần mới--->
                    <div class="form-group" id="table_list_claim">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="NAME" class="text-right">{{viewLanguage('Số tiền HDI bồi thường')}}</label> <span class="red">(*)</span>
                                <table class="table table-bordered table-hover marginBottom-unset">
                                    <thead class="thin-border-bottom">
                                    <tr class="table-background-header">
                                        <th width="70%" class="text-left middle">{{viewLanguage('Quyền lợi bồi thường')}}</th>
                                        <th width="30%" class="text-center middle">{{viewLanguage('Số tiền bồi thường')}}</th>
                                    </tr>
                                    </thead>

                                    <tbody id="table_claim">
                                        @foreach ($listBoiThuongChose as $keybt => $itembt)
                                            <tr id="tr_claim_chose_{{$itembt['BEN_CODE']}}">
                                                <td class="text-left middle">
                                                    <a href="javascript:void(0);" style="color: red" title="{{viewLanguage('Bỏ quyền lợi bồi thường này')}}" onclick="removeOptionClaim('{{$itembt['BEN_CODE']}}');"><i class="pe-7s-trash fa-2x"></i></a>&nbsp;&nbsp;
                                                    {{$itembt['BEN_NAME']}}
                                                    <input type="hidden" id="name_ben_code_{{$itembt['BEN_CODE']}}" name="name_ben_code_{{$itembt['BEN_CODE']}}" value="{{$itembt['BEN_NAME']}}">
                                                </td>
                                                <td class="text-center middle">
                                                    {{--<input type="text" id="{{$itembt['BEN_CODE']}}" name="{{$itembt['BEN_CODE']}}" value="" class="form-control input-sm text-right input_money_boi_thuong" onchange="changeMoneyBoiThuong(this);">--}}
                                                    <input type="text" id="{{$itembt['BEN_CODE']}}" name="{{$itembt['BEN_CODE']}}" value="{{$itembt['AMOUNT']}}" onchange="changeMoneyBoiThuong(this);" class="form-control formatMoney input-sm text-right input_money_boi_thuong" data-v-max="999999999999999" data-v-min="0" data-a-sep="." data-a-dec="," data-a-sign=" " data-p-sign="s" placeholder="Số tiền bồi thường" >

                                                    <input type="hidden" class="input_money_bt" id="money_{{$itembt['BEN_CODE']}}" name="money_{{$itembt['BEN_CODE']}}" value="{{$itembt['AMOUNT']}}">
                                                    <input type="hidden" id="claim_chose_{{$itembt['BEN_CODE']}}" name="claim_chose[]" value="{{$itembt['BEN_CODE']}}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tr>
                                        <td class="text-right middle">Tổng số tiền bồi thường</td>
                                        <td class="text-center middle">
                                            <span id="show_money_boi_thuong" class="form-control input-sm text-right"></span>
                                            <input type="hidden" id="tong_money_tu_choi" name="tong_money_tu_choi" value="0">
                                            <input type="hidden" id="tong_money" name="tong_money" value="0">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            {{-----Danh sách list file bổ sung----}}
            <div class="form-group display-none-block"  id="blockListFilesAdd">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right">{{viewLanguage('Chọn loại giấy tờ bổ sung')}}</label>
                            <select class="form-control input-sm" id="TYPE_FILES_ADD" name="TYPE_FILES_ADD" onchange="changeOptionTypeFilesAdds(this,'{{$urlAjaxGetData}}');">
                                {!! $optionLisDocument !!}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label for="NAME" class="text-right">{{viewLanguage('Giấy tờ yêu cầu KH bổ sung')}}</label> <span class="red">(*)</span>
                        <table class="table table-bordered table-hover marginBottom-unset">
                            <thead class="thin-border-bottom">
                            <tr class="table-background-header">
                                <th width="5%" class="text-left middle">{{viewLanguage('STT')}}</th>
                                <th width="38%" class="text-left middle">{{viewLanguage('Giấy tờ cần bổ sung')}}</th>
                                <th width="38%" class="text-left middle">{{viewLanguage('Ghi chú')}}</th>
                                <th width="19%" class="text-center middle">{{viewLanguage('Loại')}}</th>
                            </tr>
                            </thead>

                            <tbody id="table_additional_files_claim">
                                @include('Sellings.ClaimIndemnify.ClaimHdi.component._tr_add_file_claim')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{-----Ghi chú----}}
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <label for="NAME" class="text-right control-label" id="label_note_status_claim">Ghi chú 222</label><span class="red">(*)</span>
                        <textarea type="text" class="form-control input-sm" required name="NOTE_STATUS" placeholder="" rows="2"></textarea>
                    </div>
                </div>
            </div>

            {{-----TCKH,TCBT----}}
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12 display-none-block" id="issendmail">
                        <input type="checkbox" class="custom-checkbox float-left" id="is_send_mail" name="is_send_mail"
                               onchange="changerRadio();">
                        <input type="hidden" id="IS_SEND_MAIL_ACTION" name="IS_SEND_MAIL_ACTION" value="0">
                        <label for="is_send_mail" class="float-left marginL10 red">**Gửi email cho người khai báo, người được bảo hiểm</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{--@if($is_root || $permission_edit || $permission_add)--}}
            <button type="button" class="btn btn-primary"
                onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlChangeProcess}}');"><i
                    class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            {{--@endif--}}
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i>
                {{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy'});
    jQuery('.formatMoney').autoNumeric('init');

    var valu_staus = $('#CLAIM_STATUS_POPUP').val();
    showInforBoiThuong(valu_staus);
    getMoneyBoiThuongDefaul(valu_staus);
});
function getMoneyBoiThuongDefaul(valu_staus){
    var tong_boi_thuong = 0;
    var tong_money_yeu_cau = $('#tong_money_yeu_cau').val();//1000000 KH yeeu caafu

    //từ chối thì bằng só tiền yêu cầu
    if(valu_staus == '{{STATUS_CLAIM_TCBT}}'|| valu_staus == '{{STATUS_CLAIM_TCKH}}' || valu_staus == '{{STATUS_CLAIM_TTBT}}'){
        $('.input_money_boi_thuong').each(function(){
            $(this).attr('disabled','disabled');
            var str_tong_money_yeu_cau = $(this).val();//1.000.000: moi
            var money_yc_boi_thuong = jqueryCommon.convertNumberFromStr(str_tong_money_yeu_cau);//1000000: moi
            tong_boi_thuong = parseInt(tong_boi_thuong) + money_yc_boi_thuong;
        });
        if(valu_staus == '{{STATUS_CLAIM_TCBT}}' || valu_staus == '{{STATUS_CLAIM_TCKH}}') {
            $('#tong_money_tu_choi').val(parseInt(0));
        }
    }else{
        $('.input_money_boi_thuong').each(function(){
            $(this).removeAttr('disabled');
            var str_tong_money_yeu_cau = $(this).val();//1.000.000: moi
            var money_yc_boi_thuong = jqueryCommon.convertNumberFromStr(str_tong_money_yeu_cau);//1000000: moi
            tong_boi_thuong = parseInt(tong_boi_thuong) + money_yc_boi_thuong;
            //tong_boi_thuong = parseInt(tong_boi_thuong) + parseInt($(this).val());
        });
        if(parseInt(tong_boi_thuong) > 0){
            var tong_money_tu_choi = parseInt(tong_money_yeu_cau) - parseInt(tong_boi_thuong);
            $('#tong_money_tu_choi').val(parseInt(tong_money_tu_choi));
            $('#tong_money').val(parseInt(tong_boi_thuong));
        }
    }
    if(parseInt(tong_boi_thuong) > 0){
        $('#show_money_boi_thuong').html(jqueryCommon.numberFormat(tong_boi_thuong, '.', '.'));
    }
}
function changeMoneyBoiThuong(obj) {
    var tong_money_yeu_cau = $('#tong_money_yeu_cau').val();//1000000 KH yeeu caafu
    var id_input = obj.id;
    var str_money_boi_thuong = $(obj).val();//1.000.000: moi
    var money_boi_thuong = jqueryCommon.convertNumberFromStr(str_money_boi_thuong);//1000000: moi

    var money_boi_thuong_cu = parseInt($('#money_'+id_input).val()); //cu
    var tong_money_tu_choi = $('#tong_money_tu_choi').val();//1000000
    var tong_money = $('#tong_money').val();//1000000
    if(money_boi_thuong > 0){
        if(tong_money == 0){
            tong_money_tu_choi = parseInt(tong_money_yeu_cau) - parseInt(money_boi_thuong);
            tong_money = parseInt(money_boi_thuong);
        }else {
            var tong_money_moi = parseInt(tong_money) - parseInt(money_boi_thuong_cu) + parseInt(money_boi_thuong);
            if(parseInt(tong_money_moi) < 0 ||parseInt(tong_money_moi) > parseInt(tong_money_yeu_cau) || parseInt(tong_money_moi) == parseInt(tong_money_yeu_cau)){
                tong_money_tu_choi = 0;
                tong_money_tu_choi = parseInt(tong_money_yeu_cau) - parseInt(tong_money_yeu_cau);
                tong_money = parseInt(tong_money_moi);
            }else {
                tong_money_tu_choi = parseInt(tong_money_yeu_cau) - parseInt(tong_money_moi);
                tong_money = parseInt(tong_money_moi);
            }
        }
        $('#show_money_boi_thuong').html(jqueryCommon.numberFormat(tong_money, '.', '.'));
        $('#tong_money_tu_choi').val(parseInt(tong_money_tu_choi));

        $('#tong_money').val(parseInt(tong_money));
        $('#money_'+id_input).val(parseInt(money_boi_thuong));
    }else {
        tong_money_tu_choi = parseInt(tong_money_tu_choi) + parseInt(money_boi_thuong_cu);
        tong_money = parseInt(tong_money_yeu_cau) - parseInt(tong_money_tu_choi);

        $('#show_money_boi_thuong').html(jqueryCommon.numberFormat(tong_money, '.', '.'));
        $('#tong_money_tu_choi').val(parseInt(tong_money_tu_choi));

        $('#tong_money').val(parseInt(tong_money));
        $('#money_'+id_input).val(parseInt(0));
    }
}

function changeOptionClaim(obj,url_action) {
    var valu_claim = $(obj).val();
    var listBoiThuong = $('#listBoiThuong').val();
    var _token = $('input[name="_token"]').val();

    var dataClaimChose = [];
    var i = 0;
    $("input[name*='claim_chose']").each(function () {
        var benCode = $(this).val();
        if(benCode.trim() != ''){
            dataClaimChose[i] = $(this).val();
        }
        i++;
    });
    $('#loaderPopup').show();
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: url_action,
        data: {
            '_token': _token,
            'valu_claim_chose': valu_claim,
            'listBoiThuong': listBoiThuong,
            'dataClaimChose': dataClaimChose,
            'functionAction': '_ajaxActionOther',
            'type': 'addInforClaim',
        },
        success: function (res) {
            $('#loaderPopup').hide();
            if (res.success == 1) {
                $('#table_claim').append(res.html);
            } else {
                jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.message);
            }
        }
    });
    if(valu_claim == 'KHAC'){
        $("#option_claim").val("");
    }
}
function removeOptionClaim(benCode) {
    var tong_money_yeu_cau = $('#tong_money_yeu_cau').val();
    var money_boi_thuong_cu = parseInt($('#money_'+benCode).val());
    var tong_money = $('#tong_money').val();//1000000

    var tong_money_sau = tong_money-money_boi_thuong_cu;
    $('#tong_money').val(parseInt(tong_money_sau));
    $('#show_money_boi_thuong').html(jqueryCommon.numberFormat(tong_money_sau, '.', '.'));
    var tong_money_tu_choi = tong_money_yeu_cau - tong_money_sau;
    $('#tong_money_tu_choi').val(tong_money_tu_choi);

    $('#tr_claim_chose_'+benCode).addClass("display-none-block");
    $('#claim_chose_'+benCode).val('');
    $('#money_'+benCode).val(0);
    $('#'+benCode).val(0);
}

function changeOptionTypeFilesAdds(obj,url_action) {
    var valu_claim = $(obj).val();
    var listFilesAdd = $('#listFilesAdd').val();
    var arrLisDocument = $('#arrLisDocument').val();
    var _token = $('input[name="_token"]').val();

    var dataFileExit = [];
    var i = 0;
    $("input[name*='file_key_add']").each(function () {
        var benCode = $(this).val();
        if(benCode.trim() != ''){
            dataFileExit[i] = $(this).val();
        }
        i++;
    });
    $('#loaderPopup').show();
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: url_action,
        data: {
            '_token': _token,
            'key_file_chose': valu_claim,
            'listFilesAdd': listFilesAdd,
            'arrLisDocument': arrLisDocument,
            'dataFileExit': dataFileExit,
            'functionAction': '_ajaxActionOther',
            'type': 'addFilesClaim',
        },
        success: function (res) {
            $('#loaderPopup').hide();
            if (res.success == 1) {
                $('#table_additional_files_claim').append(res.html);
            } else {
                jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.message);
            }
        }
    });
    if(valu_claim == 'KHAC'){
        $("#option_claim").val("");
    }
}
function removeOptionTypeFilesAdds(fileKey) {
    $('#tr_claim_files_add_chose_'+fileKey).addClass("display-none-block");
    $('#file_key_add_'+fileKey).val('');
}


function changeStatus(obj) {
    var valu_staus = $(obj).val();
    showInforBoiThuong(valu_staus);
}

function changeTypeFilesAdds(obj) {
    var valu_staus = $(obj).val();
    showInforBoiThuong(valu_staus);
}
function showInforBoiThuong(valu_staus) {
    //hiển thị label ghi chú
    if(valu_staus == '{{STATUS_CLAIM_DYCB}}' || valu_staus == '{{STATUS_CLAIM_TCCB}}' || valu_staus == '{{STATUS_CLAIM_TCBT}}'){
        $('#label_note_status_claim').html('Lý do từ chối bồi thường gửi đến khách hàng (nếu có) ');
    }else {
        $('#label_note_status_claim').html('Ghi chú ');
    }

    if(valu_staus == '{{STATUS_CLAIM_TCBT}}' || valu_staus == '{{STATUS_CLAIM_TCKH}}' || valu_staus == '{{STATUS_CLAIM_TTBT}}'){
        $('.input_money_boi_thuong').each(function(){
            $(this).attr('disabled','disabled');
        });
        if(valu_staus == '{{STATUS_CLAIM_TCBT}}' || valu_staus == '{{STATUS_CLAIM_TCKH}}') {
            $('#tong_money_tu_choi').val(0);
        }
    }else {
        $('.input_money_boi_thuong').each(function () {
            $(this).removeAttr('disabled');
        });
        getMoneyBoiThuongDefaul(valu_staus);
    }

    //thêm file bổ sung
    //Chờ bổ sung giấy tờ hoặc Chờ hoàn tất hồ sơ
    if(valu_staus == '{{STATUS_CLAIM_YCBS}}' || valu_staus == '{{STATUS_CLAIM_HTHS}}'){
        $("#blockListFilesAdd").removeClass('display-none-block');
    }else {
        $("#blockListFilesAdd").addClass('display-none-block');
    }

    //send email: TCKH,TCBT
    if (valu_staus == '{{STATUS_CLAIM_TTBT}}') {
        $("#dateboithuong").removeClass('display-none-block');
    }else {
        $("#dateboithuong").addClass('display-none-block');
    }
    //Phương án bồi thường: STATUS_CLAIM_DYCB: Đồng ý bồi thường chờ LĐ Ban duyệt
    if (valu_staus == '{{STATUS_CLAIM_DYCB}}') {
        $("#chose_add_claim").removeClass('display-none-block');
    }else {
        $("#chose_add_claim").addClass('display-none-block');
    }

    if (valu_staus == '{{STATUS_CLAIM_TTBT}}' || valu_staus == '{{STATUS_CLAIM_DYCB}}') {
        $("#listboithuong").removeClass('display-none-block');
    }else {
        $("#listboithuong").addClass('display-none-block');
    }

    //'TMBT','DYKH','TCKH','YCBS'
    if (valu_staus == '{{STATUS_CLAIM_TMBT}}' || valu_staus == '{{STATUS_CLAIM_DYKH}}'|| valu_staus == '{{STATUS_CLAIM_TCKH}}'|| valu_staus == '{{STATUS_CLAIM_YCBS}}') {
        $("#issendmail").removeClass('display-none-block');
    }
    else {
        $("#issendmail").addClass('display-none-block');
    }
}

function changerRadio() {
    var valua = $("#IS_SEND_MAIL_ACTION").val();
    if (valua == 1) {
        $("#IS_SEND_MAIL_ACTION").val(0);
    } else {
        $("#IS_SEND_MAIL_ACTION").val(1);
    }
}

</script>
