<div class="row form-group">
    <div class="col-lg-3">
        <label for="depart_name">{{viewLanguage('Số GCN/Hồ sơ BT')}}</label>
        <input type="text" class="form-control input-sm" id="p_certificate_no" name="p_certificate_no" @if(isset($search['p_certificate_no']))value="{{$search['p_certificate_no']}}"@endif>
    </div>
    <div class="col-lg-2">
        <label for="depart_name">{{viewLanguage('KH bồi thường')}}</label>
        <input type="text" class="form-control input-sm" id="p_ins_name" name="p_ins_name" @if(isset($search['p_ins_name']))value="{{$search['p_ins_name']}}"@endif>
    </div>
    <div class=" col-lg-3">
        <label for="user_group">Sản phẩm</label>
        <select  class="form-control input-sm chosen-select w-100" name="p_product_code" id="p_product_code">
            {!! $optionProduct !!}
        </select>
    </div>
    <div class=" col-lg-4">
        <label for="user_group">Đối tác</label>
        <select  class="form-control input-sm chosen-select w-100" name="p_org_code" id="p_org_code">
            {!! $optionOrg !!}
        </select>
    </div>
    <input type="hidden" id="p_key_search" name="p_key_search" value="">
    <input type="hidden" id="div_show" name="div_show" @if(isset($paramSearch['div_show']))value="{{$paramSearch['div_show']}}"@endif>
    <input type="hidden" id="template_out" name="template_out" @if(isset($paramSearch['template_out']))value="{{$paramSearch['template_out']}}"@endif>
    <input type="hidden" id="router_index" name="router_index" @if(isset($paramSearch['router_index']))value="{{$paramSearch['router_index']}}"@endif>
</div>
<div class="row form-group">
    <div class=" col-lg-3">
        <label for="user_group">Trạng thái</label><br>
        <select  class="form-control input-sm" name="p_status" id="p_status" multiple="">
            {!! $optionStatus !!}
        </select>
        <input type="hidden" id="p_str_status" name="p_str_status" value="">
    </div>
    <div class=" col-lg-3">
        <label for="user_group">Kênh tiếp nhận</label>
        <select  class="form-control input-sm chosen-select w-100" name="p_channel" id="p_channel">
            {!! $optionChannel !!}
        </select>
    </div>
    <div class="col-lg-2">
        <label for="user_email">Thời gian yêu cầu</label>
        <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_from_date" id="p_from_date" @if(isset($search['p_from_date']))value="{{$search['p_from_date']}}"@endif>
        <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
    </div>
    <div class="col-lg-2">
        <label for="user_email">đến</label>
        <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_to_date" id="p_to_date" @if(isset($search['p_to_date']))value="{{$search['p_to_date']}}"@endif >
        <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
    </div>
    <div class="col-lg-2 marginT30 text-right">
        <button class="btn btn-primary" type="button" name="submit" value="1" onclick="changeStatus(); jqueryCommon.searchAjaxWithForm('{{$formSeachIndex}}','{{$urlSearchAjax}}')"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
    </div>
</div>
