<div>
    <form id="form_{{$formName}}">
        <div class="formInforItem @if($objectId <= 0)display-none-block @endif">
            @include('Sellings.ClaimIndemnify.ClaimHdi.component._inforGeneral')
        </div>
    {{----Form Edit----}}
    <div class="formEditItem @if($objectId > 0)display-none-block @endif" >
        <div class="">
            <input type="hidden" id="objectId" name="objectId" value="{{$objectId}}">
            <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
            <input type="hidden" id="formName" name="formName" value="{{$formName}}">
            <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
            <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
            <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">
            {{ csrf_field() }}
        </div>
    </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".a_edit_block").on('click', function () {
            jqueryCommon.clickEditBlock(this);
        });

        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
</script>