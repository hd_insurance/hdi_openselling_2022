<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center middle">STT</th>
            <th width="10%" class="text-center middle">{{viewLanguage('Thao tác')}}</th>

            <th width="70%" class="text-center middle">{{viewLanguage('Loại tài liệu')}}</th>
            <th width="10%" class="text-center middle">{{viewLanguage('File')}}</th>
            <th width="8%" class="text-center middle">Đã nộp <br/>bản cứng</th>
        </tr>
        </thead>
        <tbody>

        @if(isset($listFileAttack) && !empty($listFileAttack))
            @foreach($listFileAttack as $key3 => $item3)
            <tr>
                <td class="text-left middle" colspan="5"><h5>{{$item3['GROUP_NAME']}}</h5></td>
            </tr>
            <?php
                $key_list_file = 1;
            ?>
            @foreach($item3['ARR_LIST'] as $file_key => $inforFiles)

                <tr>
                    <td class="text-center middle">{{$key_list_file}}</td>
                    <td class="text-center middle">
                        <!--<i class="fa fa-pencil-square-o fa-2x color_hdi" aria-hidden="true"></i>
                        <i class="pe-7s-trash fa-2x red" aria-hidden="true"></i>-->
                        <a href="javascript:void(0);"  class="color_warning" onclick="jqueryCommon.getDataByAjax(this);"  data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['funcAction'=>'getUploadFileClaim','file_key'=>$file_key,'relationship'=>isset($itemList['RELATIONSHIP'])?$itemList['RELATIONSHIP']:'','itemList'=>isset($itemList)?$itemList:[],'detailClaim'=>$data])}}" data-loading="2" data-form-name="addFormFilesAttack" data-show="0" data-div-show="content-page-right" title="{{viewLanguage('Upload file bổ sung')}}" data-method="post" data-objectId="">
                            <i class="fa fa-upload fa-2x" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-left middle">{{$inforFiles['GROUP_FILE_NAME']}}</td>
                    <td class="text-center middle">
                        <?php
                        $total_file = 0;
                        ?>
                        @foreach($inforFiles['ITEM_FILE'] as $file_key22 => $item_chi22)
                            @if(trim($item_chi22['FILE_ID']) != '')
                                    <?php $total_file ++; ?>
                            @endif
                        @endforeach
                        @if($total_file > 0)
                            <a href="javascript:void(0);"  onclick="jqueryCommon.getDataByAjax(this);"  data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['funcAction'=>'showListFileClaim','file_key'=>$file_key,'listFiles'=>$inforFiles['ITEM_FILE'],'itemList'=>isset($itemList)?$itemList:[],'detailClaim'=>$data])}}" data-loading="2" data-form-name="addFormFilesAttack" data-show="1" data-div-show="content-page-right" title="{{viewLanguage('Danh sách file: ')}}{{$inforFiles['GROUP_FILE_NAME']}}" data-method="post" data-objectId="">
                                Xem file (<b>{{$total_file}}</b>)
                            </a>
                        @endif
                    </td>
                    <td class="text-center middle"><input type="checkbox" value="{{$inforFiles['TYPE_OF_PAPER']}}" @if($inforFiles['TYPE_OF_PAPER'] == 1) checked @endif></td>
                </tr>
                <?php
                $key_list_file ++;
                ?>
            @endforeach
            @endforeach
        @endif
        </tbody>
    </table>
</div>
