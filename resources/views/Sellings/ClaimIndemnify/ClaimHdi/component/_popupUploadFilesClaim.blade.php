<div class="modal-content" id="{{$formName}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formName}}">
        <input type="hidden" id="objectId" name="objectId" value="0">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="url_action" name="url_action" value="{{$urlUpdateData}}">
        <input type="hidden" id="actionUpdate" name="actionUpdate" value="uploadFilesClaim">
        <input type="hidden" id="divShowDataSuccess" name="divShowDataSuccess" value="{{$tabOtherItem2}}">
        <input type="hidden" id="detailClaim" name="detailClaim" value="{{json_encode($detailClaim)}}">
        <input type="hidden" id="FILE_KEY" name="FILE_KEY" value="{{$file_key}}">
        <input type="hidden" id="RELATIONSHIP" name="RELATIONSHIP" value="{{$relationship}}">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body paddingBottom-unset">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6 marginT20">
                        <label title="{{viewLanguage('Chọn files bổ sung')}}" for="filesClaim" class="w-100 btn-transition btn btn-outline-success">
                            <input type="file" name="filesClaim[]" id="filesClaim" style="display:none" multiple>
                            <i class="fa fa-share-square"></i> {{viewLanguage('Chọn files bổ sung')}}
                        </label>
                    </div>
                    <div class="col-lg-6 marginT25">
                        <input type="checkbox" name="TYPE_OF_PAPER" id="TYPE_OF_PAPER">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Bản cứng')}}</label>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            {{--@if($is_root || $permission_edit || $permission_add)--}}
                <button class="btn btn-success" id="submitCreateOrder" type="button" > <i class="fa fa-check"></i> {{viewLanguage('Upload file bổ sung')}}</button>
            {{--@endif--}}
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i>
                {{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#submitCreateOrder").click(function (event) {
        //stop submit the form, we will post it manually.
        event.preventDefault();
        // Get form
        submitAjaxFormMultipart('form_{{$formName}}','submitCreateOrder','{{$urlUpdateData}}')
    });
});
function submitAjaxFormMultipart(form_id,btnSubmit,urlAjax){
    var form = $('#'+form_id)[0];
    var data = new FormData(form);
    $("#"+btnSubmit).prop("disabled", true);
    $('#loaderPopup').show();
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: urlAjax,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (res) {
            $('#loaderPopup').hide();
            if(res.success == 1){
                jqueryCommon.showMsg('success', 'Upload file bồi thường thành công');
                $('#sys_showPopupCommon').modal('hide');
                $('#' + res.divShowInfor).html(res.html);
            }else {
                $("#"+btnSubmit).prop("disabled", false);
                jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.message);
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
}

</script>
