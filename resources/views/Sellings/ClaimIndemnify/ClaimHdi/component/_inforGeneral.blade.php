
<div class="card-header">
    Thông tin chung
</div>
<div class="marginT15">
    <div class="form-group" style="position: relative">
        @if($isEdit == STATUS_INT_MOT && isset($data->STATUS) && !in_array($data->STATUS,$arrStatusNotEdit))
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
        @endif
        <h5><b>Thông tin người được bảo hiểm</b></h5>
        <div class="row form-group marginT5">
            <div class="col-lg-4">
                Số hợp đồng/GCN: <b class="showInforItem" data-field="CERTIFICATE_NO"></b>
            </div>
            <div class="col-lg-8">
                Sản phẩm bảo hiểm: <b class="showInforItem" data-field="PRODUCT_NAME"></b>
            </div>

            <div class="col-lg-4">
                Người được BH: <b class="showInforItem" data-field="NAME"></b>
            </div>
            <div class="col-lg-4">
                Ngày sinh: <b class="showInforItem" data-field="DOB"></b>
            </div>
            <div class="col-lg-4">
                Số điện thoại: <b class="showInforItem" data-field="PHONE"></b>
            </div>

            <div class="col-lg-4">
                CMND/CCCD/Hộ chiếu: <b class="showInforItem" data-field="IDCARD"></b>
            </div>
            <div class="col-lg-4">
                Email: <b class="showInforItem" data-field="EMAIL"></b>
            </div>
            <div class="col-lg-4">
                Quốc tịch: <b class="showInforItem" data-field="NATIONALITY"></b>
            </div>
            <div class="col-lg-12">
                Địa chỉ: <b class="showInforItem" data-field="ADDRESS_FULL"></b>
            </div>
        </div>

        <h5 class="marginT20"><b>Thông tin người liên hệ bồi thường</b></h5>
        <div class="row form-group marginT5">
            <div class="col-lg-4">
                Tên người liên hệ: <b class="showInforItem" data-field="REQUIRE_NAME"></b>
            </div>
            <div class="col-lg-4">
                Quan hệ với NĐBH: <b class="showInforItem">@if(isset($itemList['RELATIONSHIP_CODE']) && isset($arrMoiQuanHe[$itemList['RELATIONSHIP_CODE']])) {{$arrMoiQuanHe[$itemList['RELATIONSHIP_CODE']]}} @endif</b>
            </div>
            <div class="col-lg-4">
                CMND/CCCD/Hộ chiếu: <b class="showInforItem">@if(isset($itemList['IDCARD'])) {{$itemList['IDCARD']}} @endif</b>
            </div>

            <div class="col-lg-4">
                Phone: <b class="showInforItem">@if(isset($itemList['PHONE'])) {{$itemList['PHONE']}} @endif</b>
            </div>
            <div class="col-lg-4">
                Email: <b class="showInforItem">@if(isset($itemList['EMAIL'])) {{$itemList['EMAIL']}} @endif</b>
            </div>
        </div>

    </div>
</div>
