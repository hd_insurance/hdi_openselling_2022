@foreach ($listFilesAdd as $key_file => $value_files_add)
    <tr id="tr_claim_files_add_chose_{{$key_file}}">
        <td class="text-left middle">
            <a href="javascript:void(0);" style="color: red" title="{{viewLanguage('Bỏ file bổ sung này')}}" onclick="removeOptionTypeFilesAdds('{{$key_file}}');"><i class="pe-7s-trash fa-2x"></i></a>
            <input type="hidden" id="file_key_add_{{$key_file}}" name="file_key_add[]" value="{{$key_file}}">
            <input type="hidden" id="strDataFileExit" name="strDataFileExit" value="{{$strDataFileExit}}">
        </td>
        <td class="text-center middle">
            <input type="text" id="NAME_FILE_{{$key_file}}" name="NAME_FILE_{{$key_file}}" class="form-control input-sm" value="{{$value_files_add['NAME_FILE']}}" readonly>
        </td>
        <td class="text-center middle">
            <input type="text" id="NOTE_FILE_{{$key_file}}" name="NOTE_FILE_{{$key_file}}" class="form-control input-sm" value="{{$value_files_add['NOTE_FILE']}}">
        </td>
        <td class="text-center middle">
            <select class="form-control input-sm" id="TYPE_FILE_{{$key_file}}" name="TYPE_FILE_{{$key_file}}">
                @foreach($arrTypeFiles as $key_type=>$name_type)
                    <option value="{{$key_type}}" @if($value_files_add['TYPE_FILE'] == $key_type) selected @endif>{{$name_type}}</option>
                @endforeach
            </select>
        </td>
    </tr>
@endforeach
