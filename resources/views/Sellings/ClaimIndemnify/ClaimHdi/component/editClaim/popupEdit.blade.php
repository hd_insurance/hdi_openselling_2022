<div style="position: relative">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>

    <div id="divDetailItem">
        <div class="card-header">
            @if($objectId > 0)Cập nhật @else Thêm @endif hồ sơ bồi thường
            <div class="btn-actions-pane-right marginT10">
                @if($permission_view || $permission_edit)
                    @if($permission_edit)
                    <a href="javascript:void(0);" class="marginT10 mb-2 mr-2 btn-transition btn btn-outline-success detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-loading="2" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxActionOther" data-input="{{json_encode(['type'=>'getChangeStatus','functionAction'=>'','paramSearch'=>$paramSearch,'dataClaim'=>$data,'dataItem'=>$itemList,'listDuocBoiThuong'=>$listDuocBoiThuong,'listFileBoSung'=>$listFileBoSung,'arrEditStatusPopup'=>$arrEditStatusPopup])}}" data-show="1" data-show-id="" title="{{viewLanguage('Chuyển trạng thái: ')}}  @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                        <i class="fa fa-refresh"></i> {{viewLanguage('Chuyển trạng thái')}}
                    </a>
                    @endif
                    <a href="javascript:void(0);"  class="marginT10 mb-2 mr-2 btn-transition btn btn-outline-info detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-loading="2" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxActionOther" data-input="{{json_encode(['type'=>'getHistory','functionAction'=>'','dataClaim'=>$listTimeLine])}}" data-show="1" data-show-id="" title="{{viewLanguage('Lịch sử bồi thường: ')}}  @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                        <i class="fa fa-search"></i> {{viewLanguage('Lịch sử xử lý hồ sơ')}}
                    </a>
                    <a href="javascript:void(0);" class="marginT10 mb-2 mr-2 btn-transition btn btn-outline-success" onclick="jqueryCommon.getDataByAjax(this);"  data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['funcAction'=>'getHistoryUpdateClaim','dataItem'=>$itemList,'dataClaim'=>$data])}}" data-loading="2" data-form-name="formApproveSuccess" data-show="1" data-div-show="" title="{{viewLanguage('Lịch sử cập nhật: ')}}  @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                        <i class="fa fa-list"></i> {{viewLanguage('Lịch sử cập nhật')}}
                    </a>
                @endif
                <button type="button"  class="btn color_hdi" onclick="jqueryCommon.hideContentRightPage()" title="{{viewLanguage('Close')}}">&nbsp;&nbsp;<i class="pe-7s-close fa-3x"></i>&nbsp;&nbsp;</button>
            </div>
        </div>

        <div class="div-infor-right">
            <div class="main-card mb-3">
                <div class="card-body paddingTop-unset">
                    <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column" style="padding-top: 0px!important;">

                        {{---Block 1---}}
                        <div class="vertical-timeline-item vertical-timeline-element marginBottom-unset">
                            <span class="vertical-timeline-element-icon bounce-in icon-timeline timeline-active">1</span>
                            <div class="vertical-timeline-element-content bounce-in" id="formShowEditSuccess">
                                @include('Sellings.ClaimIndemnify.ClaimHdi.component.editClaim._editFormItem')
                            </div>
                        </div>

                        {{---Block 2---}}
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div>
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">2</span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    {{---tạo mới tổ chứ---}}
                                    @if($objectId <= 0)
                                        <div class="card-header"> Thông tin khác</div>
                                        <div class="marginT15">
                                            Bạn cần thêm người dùng trước khi phân quyền
                                        </div>
                                    @else
                                        <div class="listTabWithAjax">
                                            <div class="card-header card-header-tab-animation">
                                                <ul class="nav nav-justified">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link active" data-toggle="tab" href="#{{$tabOtherItem1}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem1}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItem" data-input="{{json_encode(['type'=>$tabOtherItem1,'item_id'=>0])}}" data-object-id="1">
                                                            <span><b>{{viewLanguage('Thông tin chi tiết bồi thường')}}</b></span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link" data-toggle="tab" href="#{{$tabOtherItem2}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem2}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItem" data-input="{{json_encode(['type'=>$tabOtherItem2,'item_id'=>0])}}" data-object-id="1">
                                                            <span><b>{{viewLanguage('Tài liệu đính kèm')}}</b></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content marginT10" >
                                                {{--Thông tin cá nhân---}}
                                                <div class="tab-pane tabs-animation fade show active" id="{{$tabOtherItem1}}" role="tabpanel">
                                                    @include('Sellings.ClaimIndemnify.ClaimHdi.component.editClaim._formInforClaim')
                                                </div>
                                                {{--Phân quyền theo nhóm---}}
                                                <div class="tab-pane tabs-animation fade" id="{{$tabOtherItem2}}" role="tabpanel">
                                                    @include('Sellings.ClaimIndemnify.ClaimHdi.component._tableFilesAttack')
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        //chi tiết banks
        $('.detailOtherCommon').dblclick(function () {
            jqueryCommon.ajaxGetData(this);
        });
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>