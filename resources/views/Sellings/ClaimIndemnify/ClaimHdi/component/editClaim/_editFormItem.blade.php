{{---ID > 0 và có thông tin data---}}
<form id="form_{{$formName}}">
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">
    @include('Sellings.ClaimIndemnify.ClaimHdi.component._inforGeneral')
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        Thông tin chung
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" value="{{$objectId}}">
        <input type="hidden" id="url_action" name="url_action" value="{{$urlUpdateData}}">
        <input type="hidden" id="actionUpdate" name="actionUpdate" value="updateContact">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
        <input type="hidden" id="itemList" name="itemList" value="{{json_encode($itemList)}}">

        <input type="hidden" id="isEdit" name="isEdit" value="{{$isEdit}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">
        {{ csrf_field() }}
        <div class="marginT15">
            <h5><b>Thông tin người được bảo hiểm</b></h5>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Số hợp đồng/ GCN')}} </label>
                        <div class="input-group">
                            <input type="text" class="form-control" readonly name="CERTIFICATE_NO" required id="form_{{$formName}}_CERTIFICATE_NO">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button"><i class="fa fa-search fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày yêu cầu')}}</label>
                        <input type="text" class="form-control input-sm" data-valid = "text" readonly name="CLAIM_DATE" required id="form_{{$formName}}_CLAIM_DATE">
                        <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                    </div>
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Kênh tiếp nhận')}}</label>
                        <input type="text" class="form-control input-sm"  readonly name="CHANNEL" required id="form_{{$formName}}_CHANNEL">
                    </div>
                </div>
            </div>

            <h5 class="marginT20"><b>Thông tin người liên hệ</b></h5>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Quan hệ với NĐBH')}}</label>
                        <select  class="form-control input-sm" name="RELATIONSHIP" id="RELATIONSHIP">
                            {!! $optionMoiQuanHe !!}
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Tên người liên hệ')}} </label>
                        <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100"  name="REQUIRE_NAME"  id="form_{{$formName}}_REQUIRE_NAME">
                    </div>
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('CMND/CCCD/Hộ chiếu')}} </label>
                        <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100"  name="IDCARD" id="IDCARD" @if(isset($itemList['IDCARD']) !='')value="{{$itemList['IDCARD']}}" @endif>
                    </div>
<!--                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày sinh')}}</label>
                        <input type="text" class="form-control input-sm input-date" data-valid = "text"  name="DOB" id="DOB" @if(isset($itemList['DOB']) !='')value="{{$itemList['DOB']}}" @endif>
                        <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                    </div>-->
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Số điện thoại')}} </label>
                        <input type="text" class="form-control input-sm" name="PHONE" id="PHONE" @if(isset($itemList['PHONE']) !='')value="{{$itemList['PHONE']}}" @endif>
                    </div>
                    <div class="col-lg-3">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Email')}} </label>
                        <input type="text" class="form-control input-sm" name="EMAIL" id="EMAIL" @if(isset($itemList['EMAIL']) !='')value="{{$itemList['EMAIL']}}" @endif>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        @if($is_root || $permission_add || $permission_edit)
                            <button type="button" class="btn btn-success submitFormItem @if($objectId > 0)display-none-block @endif" onclick="jqueryCommon.doSubmitForm();"><i class="pe-7s-diskette"></i> {{viewLanguage('Update')}}</button>
                            <button type="button" class="btn btn-secondary cancelUpdate display-none-block" onclick="jqueryCommon.cancelUpdateFormItem();"><i class="pe-7s-back"></i> {{viewLanguage('Hủy bỏ')}}</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');

        var config = {
            '.chosen-select'           : {width: "100%"},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });
</script>
