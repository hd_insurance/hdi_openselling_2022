<div>
    <form id="form_{{$formNameOther}}">
        {{ csrf_field() }}
        <div class="formEditItemInfor" >
            <div class="marginT15">

                {{---------Thông tin bồi thường mở rộng----------}}
                @if(isset($inforDetailExten) && !empty($inforDetailExten))
                    <div class="form-group form-infor-detail ">
                        <h4>Thông tin bồi thường mở rộng</h4>
                        <div class="row form-group">
                            @foreach($inforDetailExten as $key1 => $item1)
                                <div class="col-lg-4">
                                    {{$item1->INS_DESC}}: <b class="showInforItem" data-field="">{{$item1->INS_VALUE}}</b>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                {{---------Yêu cầu bồi thường----------}}
                @if(isset($desRequestClaim) && !empty($desRequestClaim))
                    @foreach($desRequestClaim as $key2 => $item2)
                        <div class="form-group form-infor-detail  marginT20">
                            <h4>{{$item2['BEN_NAME']}}</h4>
                            <div class="row form-group">
                                @foreach($item2['ARR_LIST'] as $key_item => $item_chi)
                                    <div class="col-lg-4">
                                        {{$item_chi['DEC_DESC']}}: <b class="showInforItem" data-field="">{{$item_chi['DEC_VALUE']}}</b>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @endif

                {{---------Chi tiết bồi thường----------}}
                @if(isset($itemList) && !empty($itemList))
                    <div class="form-group form-infor-detail  marginT20">
                        <h4>Khách hàng yêu cầu bồi thường</h4>
                        <div class="row">
                            <div class="col-lg-4">
                                Số tiền YC bồi thường: <b class="showInforItem" data-field="">{{numberFormat($itemList['REQUIRED_AMOUNT'])}} {{MONEY_VND}}</b>
                            </div>
                            <div class="col-lg-8">
                                Mối quan hệ NĐBH và người thụ hưởng: <b class="showInforItem" data-field="BENEFICIARY_TYPE_NAME">{{$itemList['RELATIONSHIP']}}</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                Người thụ hưởng: <b class="showInforItem" data-field="BENEFICIARY_NAME"></b>
                            </div>
                            <div class="col-lg-4">
                                CMND/CCCD/Hộ chiếu: <b class="showInforItem" data-field="BENEFICIARY_IDCARD"></b>
                            </div>
                            <div class="col-lg-4">
                                Số điện thoại: <b class="showInforItem" data-field="BENEFICIARY_PHONE"></b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                Hình thức nhận tiền: <b class="showInforItem" data-field="PAYMENT_NAME"></b>
                            </div>
                            <div class="col-lg-4">
                                Loại ngân hàng: <b class="showInforItem" data-field="BANK_TYPE"></b>
                            </div>
                            <div class="col-lg-4">
                                Ngân hàng: <b class="showInforItem" data-field="ACCOUNT_BANK"></b>
                            </div>
                            <div class="col-lg-4">
                                Số tài khoản: <b class="showInforItem" data-field="ACCOUNT_NO"></b>
                            </div>
                            <div class="col-lg-4">
                                Chủ tài khoản: <b class="showInforItem" data-field="ACCOUNT_NAME"></b>
                            </div>
                            @if(isset($data->INTERNATIONAL_BANK) && $data->INTERNATIONAL_BANK == 0)
                            <div class="col-lg-4">
                                Chi nhánh: <b class="showInforItem" data-field="BRANCH_BANK"></b>
                            </div>
                            @endif
                        </div>

                       @if(isset($data->INTERNATIONAL_BANK) && $data->INTERNATIONAL_BANK == 1)
                           <div class="row marginT10">
                                <div class="col-lg-4">
                                    Swift/BIC(1): <b class="showInforItem" data-field="SWIFT_CODE"></b>
                                </div>
                                <div class="col-lg-4">
                                    Địa chỉ ngân hàng: <b class="showInforItem" data-field="BANK_ADDRESS"></b>
                                </div>
                           </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    Ngân hàng trung gian: <b class="showInforItem" data-field="INTERMEDIATE_BANK"></b>
                                </div>
                                <div class="col-lg-4">
                                    Swift/BIC: <b class="showInforItem" data-field="INTER_SWIFT_CODE"></b>
                                </div>
                                <div class="col-lg-4">
                                    Địa chỉ chủ TK: <b class="showInforItem" data-field="BENEFICIARY_ADDRESS"></b>
                                </div>
                            </div>
                        @endif
                    </div>

                    <?php
                        $status_ketqua = isset($itemList['STATUS']) ? $itemList['STATUS'] : '';
                    ?>
                    @if(!in_array($status_ketqua,$arrStatusHideListBT))
                        <div class="form-group form-infor-detail marginT20">
                            <h4>Phương án xử lý bồi thường</h4>
                            <div class="row form-group">
                                <?php
                                    $amount = (isset($itemList['AMOUNT']) && $itemList['AMOUNT'] > 0)? $itemList['AMOUNT'] : 0;
                                ?>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        Kết quả: <b class="showInforItem" data-field="">
                                            @if(isset($arrStatus[$status_ketqua]))
                                                {{$arrStatus[$status_ketqua]}}
                                            @endif
                                        </b>
                                    </div>
                                </div>

                                @if(isset($listDuocBoiThuong) && !empty($listDuocBoiThuong))
                                    <div class="col-lg-10">
                                        <table class="table table-bordered table-hover marginBottom-unset">
                                            <thead class="thin-border-bottom">
                                            <tr class="table-background-header">
                                                <th width="3%" class="text-center middle">STT</th>
                                                <th width="70%" class="text-left middle">{{viewLanguage('Quyền lợi bồi thường')}}
                                                </th>
                                                <th width="27%" class="text-right middle">{{viewLanguage('Số tiền bồi thường')}}
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $tongTienBoiThuong = 0;
                                            ?>
                                            @foreach ($listDuocBoiThuong as $keybt => $itembt)
                                                <tr>
                                                    <td class="text-center middle">{{$keybt+1}}</td>
                                                    <td class="text-left middle">{{$itembt->BEN_NAME}}</td>
                                                    <td class="text-right middle">
                                                        <b @if($amount > 0)class="red" @endif >{{numberFormat($itembt->AMOUNT)}} {{MONEY_VND}}</b>
                                                    </td>
                                                </tr>
                                                <?php
                                                    $tongTienBoiThuong = $tongTienBoiThuong + $itembt->AMOUNT;
                                                ?>
                                            @endforeach
                                            <tr>
                                                <td class="text-right middle" colspan="2">Tổng bồi thường</td>
                                                <td class="text-right middle">
                                                    <b class="red">{{numberFormat($tongTienBoiThuong)}} {{MONEY_VND}}</b>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>

        {{----Form Edit----}}
        <div class="formEditItem22 @if($objectId > 0)display-none-block @endif" >
            <div class="clear1 marginT15">
                <input type="hidden" id="objectId" name="objectId" value="{{$objectId}}">
                <input type="hidden" id="url_action" name="url_action" value="">
                <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
                <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">

                <input type="hidden" id="{{$formNameOther}}USER_CODE" name="USER_CODE" value="">

                <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="">
                <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="">
                <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="">
                {{ csrf_field() }}
                <h4><b>Thông tin tổn thất</b></h4>
                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="NAME" class="text-right control-label"><b>{{viewLanguage('Khai báo tổn thất cho')}}</b></label>
                    </div>
                    <input type="hidden" id="check_show_type_bt" name="check_show_type_bt" value="1">
                    <div class="col-lg-1">
                        <div class="custom-radio custom-control">
                            <input type="radio" id="exampleCustomRadio" name="customRadio" class="custom-control-input" checked onclick="jqueryCommon.showHideAddBlock('check_show_type_bt','block_bt_tainan','block_bt_khambenh');">
                            <label class="custom-control-label" for="exampleCustomRadio"><b>Tai nạn</b></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="custom-radio custom-control">
                            <input type="radio" id="exampleCustomRadio2" name="customRadio" class="custom-control-input" onclick="jqueryCommon.showHideAddBlock('check_show_type_bt','block_bt_tainan','block_bt_khambenh');">
                            <label class="custom-control-label" for="exampleCustomRadio2"><b>Khám chữa bệnh</b></label>
                        </div>
                    </div>
                </div>

                <div>
                    {{---Thông tin bồi thường tai nạn--}}
                    <div id="block_bt_tainan" class="">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Mô tả tai nạn')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Giờ tai nạn')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày tai nạn')}} </label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="DATE_BIRTHDAY_CONTACT" id="DATE_BIRTHDAY_CONTACT" @if(isset($dataClaim['DATE_BIRTHDAY_CONTACT']) && trim($dataClaim['DATE_BIRTHDAY_CONTACT']) !='')value="{{$dataClaim['DATE_BIRTHDAY_CONTACT']}}" @endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                            <div class="col-lg-6">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Địa chỉ cụ thể')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Tỉnh - thành phố')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Quận - huyện')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Phường - xã')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="checkbox" id="exampleCustomCheckbox" checked class="">
                                <label class="" for="exampleCustomCheckbox"><b>Điều trị y tế do tai nạn</b></label>
                            </div>
                            <div class="col-lg-6">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Hậu quả tai nạn')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày nhập viện')}} </label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="DATE_BIRTHDAY_CONTACT" id="DATE_BIRTHDAY_CONTACT" @if(isset($dataClaim['DATE_BIRTHDAY_CONTACT']) && trim($dataClaim['DATE_BIRTHDAY_CONTACT']) !='')value="{{$dataClaim['DATE_BIRTHDAY_CONTACT']}}" @endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày xuất viện')}} </label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="DATE_BIRTHDAY_CONTACT" id="DATE_BIRTHDAY_CONTACT" @if(isset($dataClaim['DATE_BIRTHDAY_CONTACT']) && trim($dataClaim['DATE_BIRTHDAY_CONTACT']) !='')value="{{$dataClaim['DATE_BIRTHDAY_CONTACT']}}" @endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Tỉnh - thành phố')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Cơ sở y tế')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Cơ sở y tế khác')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                        </div>
                    </div>

                    {{---Thông tin bồi thường khám bênh--}}
                    <div id="block_bt_khambenh" class="display-none-block">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Chuẩn đoán bệnh')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày nhập viện')}} </label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="DATE_BIRTHDAY_CONTACT" id="DATE_BIRTHDAY_CONTACT" @if(isset($dataClaim['DATE_BIRTHDAY_CONTACT']) && trim($dataClaim['DATE_BIRTHDAY_CONTACT']) !='')value="{{$dataClaim['DATE_BIRTHDAY_CONTACT']}}" @endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày xuất viện')}} </label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="DATE_BIRTHDAY_CONTACT" id="DATE_BIRTHDAY_CONTACT" @if(isset($dataClaim['DATE_BIRTHDAY_CONTACT']) && trim($dataClaim['DATE_BIRTHDAY_CONTACT']) !='')value="{{$dataClaim['DATE_BIRTHDAY_CONTACT']}}" @endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Tỉnh - thành phố khám')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Cơ sở y tế')}}</label>
                                <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label for="NAME" class="text-right control-label">{{viewLanguage('Cơ sở y tế khác')}} </label>
                                <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <input type="checkbox" id="exampleCustomCheckbox" checked class="">
                            <label class="" for="exampleCustomCheckbox"><b>Người được bảo hiểm bị tử vong</b></label>
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Giờ tử vong')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày tử vong')}} </label>
                            <input type="text" class="form-control input-sm input-date" data-valid = "text" name="DATE_BIRTHDAY_CONTACT" id="DATE_BIRTHDAY_CONTACT" @if(isset($dataClaim['DATE_BIRTHDAY_CONTACT']) && trim($dataClaim['DATE_BIRTHDAY_CONTACT']) !='')value="{{$dataClaim['DATE_BIRTHDAY_CONTACT']}}" @endif>
                            <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                        </div>
                        <div class="col-lg-12 marginT10">
                            <input type="checkbox" id="exampleCustomCheckbox" checked class="">
                            <label class="" for="exampleCustomCheckbox"><b>Có phát sinh chi phí vận chuyển cấp cứu</b></label>
                        </div>
                    </div>

                    <h4><b>Thông tin yêu cầu bồi thường</b></h4>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Số tiền yêu cầu bồi thường')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Quan hệ NTH với NĐBH')}} </label>
                            <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Người thụ hưởng')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('CMND/CCCD/Hộ chiếu')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Số điện thoại')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Email')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Hình thức nhận tiền')}} </label>
                            <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Loại ngân hàng')}} </label>
                            <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Ngân hàng')}} </label>
                            <select  class="form-control input-sm" name="CAMPAIGN_CODE" required id="form_{{$formName}}_CAMPAIGN_CODE" >

                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Chi nhánh')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Số tài khoản')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Chủ tài khoản')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <input type="hidden" id="check_show_chuyenkhoan" name="check_show_chuyenkhoan" value="0">
                            <div id="block_chuyenkhoan_hiden" class=""></div>
                            <input type="checkbox" class="custom-checkbox float-left" id="is_success" name="is_success" onchange="jqueryCommon.showHideAddBlock('check_show_chuyenkhoan','block_chuyenkhoan','block_chuyenkhoan_hiden');">
                            <label for="is_success" class="float-left marginL10"><b>Chuyển tiền qua ngân hàng trung gian</b></label>
                        </div>
                    </div>
                    <div id="block_chuyenkhoan" class="form-group row display-none-block">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Ngân hàng trung gian')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('Swift code')}} </label>
                            <input type="text" class="form-control input-sm"  minlength="1" maxlength="4100" name="GIFT_CODE" id="form_{{$formName}}_GIFT_CODE">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-4">
                            <button class="btn btn-success" type="submit" name="submit" value="1"><i class="fa fa-save"></i> Lưu lại</button>
                            <button class="btn btn-secondary marginL50" type="reset" name="reset" value="0"><i class="fa fa-refresh"></i> Làm lại</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("form#form_{{$formNameOther}}").submit(function(e) {
        e.preventDefault();
        var dataForm = new FormData(this);
        var msg = 'Bạn có chắc chắc cập nhật thông tin này?';
        $('#loaderRight').show();
        $.ajax({
            type: 'POST',
            url: "{{URL::route('userSystem.ajaxUpdateUserRelation')}}",
            data: dataForm,
            success: function (res) {
                $('#loaderRight').hide();
                if (res.success == 1) {
                    jqueryCommon.showMsg('success',res.message);
                    $('#'+res.divShowAjax).html(res.html);
                } else {
                    jqueryCommon.showMsg('error','','Thông báo lỗi',res.message);
                }
            },
            contentType: false,
            processData: false,
            cache: false
        });
        return false;
    });

    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formNameOther}}');
    });

</script>
