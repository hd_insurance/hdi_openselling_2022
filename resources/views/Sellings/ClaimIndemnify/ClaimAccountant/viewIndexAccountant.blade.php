@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{ Form::open(array('method' => 'GET', 'role'=>'form','id'=>'formSeachIndex')) }}
    {{--Search---}}
    @include('Sellings.ClaimIndemnify.ClaimHdi.component.formSearch')

    {{--list data---}}
    @include('Sellings.ClaimIndemnify.ClaimAccountant.componentAccountant.listData')
    {{ Form::close() }}
@stop
