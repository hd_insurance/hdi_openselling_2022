<div style="position: relative">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>

    <div id="divDetailItem">
        <div class="card-header">
            Thông tin trình lãnh đạo Ban
            <div class="btn-actions-pane-right">
            <div class="marginT20 col-lg-12">
                @if($permission_view || $permission_approve)
                    @if(($permission_approve) && (isset($data->STATUS) && in_array($data->STATUS,[STATUS_CLAIM_DYCB,STATUS_CLAIM_TCCB])))
                        <a href="javascript:void(0);" class="mb-2 mr-2 btn-transition btn btn-outline-success" onclick="jqueryCommon.getDataByAjax(this);"  data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['funcAction'=>'getApproveClaim','actionApprove'=>'approveSuccessDepart','indexPage'=>$indexPage,'dataItem'=>$itemList,'detailClaim'=>$data,'paramSearch'=>$paramSearch])}}" data-loading="2" data-form-name="formApproveSuccess" data-show="0" data-div-show="" title="{{viewLanguage('Duyệt phương án bồi thường: ')}}  @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                           <i class="fa fa-check"></i> {{viewLanguage('Duyệt phương án')}}
                        </a>
                        <a href="javascript:void(0);" class="mb-2 mr-2 btn-transition btn btn-outline-warning" onclick="jqueryCommon.getDataByAjax(this);"  data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['funcAction'=>'getApproveClaim','actionApprove'=>'approveCancelDepart','indexPage'=>$indexPage,'dataItem'=>$itemList,'detailClaim'=>$data,'paramSearch'=>$paramSearch])}}" data-loading="2" data-form-name="formApproveSuccess" data-show="0" data-div-show="" title="{{viewLanguage('Từ chối phương án bồi thường: ')}}  @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                            <i class="fa fa-times"></i> {{viewLanguage('Từ chối phương án')}}
                        </a>
                    @endif
                    <a href="javascript:void(0);"  class="mb-2 mr-2 btn-transition btn btn-outline-info detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-loading="2" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxActionOther" data-input="{{json_encode(['type'=>'getHistory','functionAction'=>'','dataClaim'=>$listTimeLine])}}" data-show="1" data-show-id="" title="{{viewLanguage('Lịch sử bồi thường: ')}} @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                        <i class="fa fa-search"></i> {{viewLanguage('Lịch sử xử lý hồ sơ')}}
                    </a>
                    <a href="javascript:void(0);" class="mb-2 mr-2 btn-transition btn btn-outline-success" onclick="jqueryCommon.getDataByAjax(this);"  data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['funcAction'=>'getHistoryUpdateClaim','dataItem'=>$itemList,'dataClaim'=>$data])}}" data-loading="2" data-form-name="formApproveSuccess" data-show="1" data-div-show="" title="{{viewLanguage('Lịch sử cập nhật: ')}} @if(isset($data->CONTRACT_NO)){{$data->CONTRACT_NO}}@endif" data-method="post" data-objectId="">
                        <i class="fa fa-list"></i> {{viewLanguage('Lịch sử cập nhật')}}
                    </a>
                @endif
            </div>
            </div>
            <div class="btn-actions-pane-right">
                @include('Layouts.BaseAdmin.listButtonActionFormEdit')
            </div>
        </div>

        <div class="div-infor-right">
            <div class="main-card mb-3">
                <div class="card-body paddingTop-unset">
                    <ul class="nav nav-tabs nav-justified marginT15">
                        <li class="nav-item">
                            <a data-toggle="tab" href="#tab-eg11-0" class="active nav-link"><b>Hồ sơ bồi thường</b></a>
                        </li>
                        <li class="nav-item">
                            <a data-toggle="tab" href="#tab-eg11-1" class="nav-link"><b>Nội dung trình duyệt</b></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-eg11-0" role="tabpanel">
                            <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column" style="padding-top: 0px!important;">
                                {{---Block 1---}}
                                <div class="vertical-timeline-item vertical-timeline-element marginBottom-unset">
                                    <span class="vertical-timeline-element-icon bounce-in icon-timeline timeline-active">1</span>
                                    <div class="vertical-timeline-element-content bounce-in" id="formShowEditSuccess">
                                        @include('Sellings.ClaimIndemnify.ClaimHdi.component._detailFormItem')
                                    </div>
                                </div>

                                {{---Block 2---}}
                                <div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">2</span>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <div class="card-header card-header-tab-animation">
                                                <ul class="nav nav-justified">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link active" data-toggle="tab" href="#{{$tabOtherItem1}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem1}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetDataOfTab" data-input="{{json_encode(['type'=>$tabOtherItem1,'dataItem'=>[0=>$data]])}}" data-object-id="1">
                                                            <span><b>{{viewLanguage('Thông tin chi tiết bồi thường')}}</b></span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link" data-toggle="tab" href="#{{$tabOtherItem2}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem2}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetDataOfTab" data-input="{{json_encode(['type'=>$tabOtherItem2,'dataItem'=>[0=>$data]])}}" data-object-id="1">
                                                            <span><b>{{viewLanguage('Tài liệu đính kèm')}}</b></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content marginT20" >
                                                {{--Các giao dịch chưa map với đơn---}}
                                                <div class="tab-pane tabs-animation fade show active" id="{{$tabOtherItem1}}" role="tabpanel">
                                                    {{--@include('Sellings.ClaimIndemnify.ClaimHdi.component._detailFormItem2')--}}
                                                    @include('Sellings.ClaimIndemnify.ClaimHdi.component.editClaim._formInforClaim')
                                                </div>
                                                {{--Các giao dịch với khách hàng---}}
                                                <div class="tab-pane tabs-animation fade" id="{{$tabOtherItem2}}" role="tabpanel">
                                                    @include('Sellings.ClaimIndemnify.ClaimHdi.component._tableFilesAttack')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-eg11-1" role="tabpanel">
                            @include('Sellings.ClaimIndemnify.ClaimHdi.component._contentClaimApporve')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Nội dung form Edit Other by ajax--}}
    <div id="content-other-right"></div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        //chi tiết banks
        $('.detailOtherCommon').dblclick(function () {
            jqueryCommon.ajaxGetData(this);
        });
    });
    function showHistoryClaim(){
        $('#popupHistoryTimeLine').modal('show');
        $( ".modal-dialog" ).removeClass( "modal-lg" );
    }
</script>