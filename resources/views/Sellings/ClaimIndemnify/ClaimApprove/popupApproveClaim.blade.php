<div class="modal-content" id="{{$formName}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formName}}">
        <input type="hidden" id="objectId" name="objectId" value="0">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="actionApprove" name="actionApprove" value="{{$actionApprove}}">
        <input type="hidden" id="actionUpdate" name="actionUpdate" value="updateApproveStatus">
        <input type="hidden" id="dataClaim" name="dataClaim" value="{{json_encode($detailClaim)}}">
        <input type="hidden" id="dataItem" name="dataItem" value="{{json_encode($dataItem)}}">
        <input type="hidden" id="paramSearch" name="paramSearch" value="{{json_encode($paramSearch)}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body paddingBottom-unset">

            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('Nội dung trình')}}</label> <span class="red">(*)</span>
                        <textarea type="text" class="form-control input-sm" required name="REASON_APPROVE" placeholder="Nội dung trình" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            @if($permission_approve)
            <button type="button" class="btn btn-primary"
                onclick="jqueryCommon.doActionPopup('{{$formName}}','{{$urlUpdateData}}');">
                <i class="pe-7s-diskette"></i> {{viewLanguage('Cập nhật')}}
            </button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i>
                {{viewLanguage('Bỏ qua')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy'});
});
</script>