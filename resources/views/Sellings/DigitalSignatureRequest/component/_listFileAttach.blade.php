<div>
    <form id="form_file_acttach">
        <div class="row marginT10">
            <div class="table-responsive col-lg-12">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="5%" class="text-center middle">STT</th>
                        <th width="75%" class="text-left middle">Tên file</th>
                        <th width="25%" class="text-left middle">Xem file</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($arrFileAttach))
                        @foreach ($arrFileAttach as $key => $inforFile)
                            @if(isset($inforFile->file_name) && !empty($inforFile->file_name) && isset($inforFile->file_base64) && !empty($inforFile->file_base64))
                            <tr>
                                <td class="text-center middle">{{$key+1}}</td>
                                <td class="text-left middle">{{$inforFile->file_name}}</td>
                                <td class="text-left middle">
                                    <a href="javascript:void(0);" onclick="jqueryCommon.getDataByAjax(this);" data-url="{{$urlAjaxGetAction}}" data-function-action="_funcViewImage" data-input="{{json_encode(['fileExtension'=>$inforFile->file_extension, 'nameImage'=>$inforFile->file_name,'urlImage'=>$urlServiceFile.$inforFile->file_base64])}}" data-loading="2" data-form-name="addFormFilesAttack" data-show="1" data-div-show="" title="{{viewLanguage('Chi tiết ảnh: ')}}{{$inforFile->file_name}}" data-method="post" data-objectId="">
                                       <img style='display:block; width:50px;height:50px;' id='base64image{{$key}}' src='{{$urlServiceFile.$inforFile->file_base64}}' title="Image to upload"/>
                                    </a>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
</script>
