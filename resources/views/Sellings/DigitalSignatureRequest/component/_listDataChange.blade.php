<div>
    <form id="form_{{$formName}}">
    <div class="">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_request" name="data_request" value="{{json_encode($dataItem)}}">
        <input type="hidden" id="data_infor_change" name="data_infor_change" value="{{json_encode($arrInforChange)}}">
        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-lg-12">
                Người yêu cầu: <b class="showInforItem">@if(isset($dataItem->CONTACT_NAME)){{$dataItem->CONTACT_NAME}}@endif</b>
            </div>
            <div class="col-lg-12">
                Email người yêu cầu: <b class="showInforItem">@if(isset($dataItem->CONTACT_EMAIL)){{$dataItem->CONTACT_EMAIL}}@endif</b>
            </div>
            <div class="col-lg-12">
                Điện thoại người yêu cầu: <b class="showInforItem">@if(isset($dataItem->CONTACT_PHONE)){{$dataItem->CONTACT_PHONE}}@endif</b>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <label for="user_email">Lý do yêu cầu cập nhật: </label>
            </div>
            <div class="col-lg-10">
                <span class="font-bold">@if(isset($dataItem->NOTE_REQUEST)){!! $dataItem->NOTE_REQUEST !!}@endif</span>
            </div>
        </div>
        <div class="row marginT10">
            <div class="col-lg-2">
                <label for="user_email">Duyệt yêu cầu: </label>
            </div>
            <div class="col-lg-5">
                <select  class="form-control input-sm" name="STATUS" id="P_STATUS">
                    {!! $optionStatusEdit !!}
                </select>
            </div>
        </div>
        <div class="row marginT10">
            <div class="col-lg-12">
                <span class="font-bold"> Chọn các thông tin cho phép cập nhật</span>
            </div>
        </div>
        <div class="row marginT10">
            <div class="table-responsive col-lg-12">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="3%" class="text-center middle">STT</th>
                        <th width="2%" class="text-center middle"><input type="checkbox" class="check" id="checkAllOrder"></th>
                        <th width="25%" class="text-left middle">Loại thông tin</th>
                        <th width="35%" class="text-left middle">Thông tin trên giấy chứng nhận</th>
                        <th width="35%" class="text-left middle">Thông tin yêu cầu thay đổi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($arrInforChange))
                        <?php $stt = 1;?>
                        @foreach ($arrInforChange as $key_change => $arrInfor)
                            @if(!empty($arrInfor['VALUE_NEW']))
                                <tr>
                                    <td class="text-center middle">{{$stt}}</td>
                                    <td class="text-center middle">
                                        <input class="check" type="checkbox" name="checkItems[]" value="{{$key_change}}" @if(in_array($key_change,$arrChangeKeys)) checked @endif onchange="changeColorButton();">
                                    </td>
                                    <td class="text-left middle">{{$arrInfor['NAME_ALIAS']}}</td>
                                    <td class="text-left middle">{{$arrInfor['VALUE_OLD']}}</td>
                                    <td class="text-left middle">{{$arrInfor['VALUE_NEW']}}</td>
                                </tr>
                                <?php $stt ++;?>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row marginT10">
            <div class="col-lg-2">
                <label for="user_email">Phản hồi khách hàng: </label>
            </div>
            @if(isset($dataItem->NOTE_REPLY))
                <div class="col-lg-10">
                    <span class="font-italic">{!! $dataItem->NOTE_REPLY !!}</span>
                </div>
                @endif
        </div>
        <div class="row marginT10">
            <div class="col-lg-2">
                @if(isset($dataItem->STATUS) && in_array($dataItem->STATUS,[STATUS_INT_KHONG,STATUS_INT_HAI]))
                    <button class="btn btn-success w_100" type="button" name="approval_order" id="show_button_approval_order" value="0" onclick="clickApprovalOrderList('{{$urlAjaxGetAction}}')"><i class="fa fa-check"></i> {{viewLanguage('Cập nhật')}}</button>
                @endif
            </div>
            <div class="col-lg-10">
                <textarea rows="2" id="NOTE_REPLY" name="NOTE_REPLY" class="form-control input-sm"></textarea>
            </div>
        </div>
    </div>

    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#checkAllOrder").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
            changeColorButton();
        });
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    function changeColorButton(){
        return;
        var changeColor = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                changeColor = 1;
            }
        });
        /*if(changeColor == 1){
            $('#show_button_approval_order').prop("disabled", false);
            $("#show_button_approval_order").addClass("btn-success");
            $("#show_button_approval_order").removeClass("btn-light");
        }else {
            $('#show_button_approval_order').prop("disabled", true);
            $("#show_button_approval_order").removeClass("btn-success");
            $("#show_button_approval_order").addClass("btn-light");
        }*/
    }
    function clickApprovalOrderList(url_ajax){
        var dataKeyChange = [];
        var i = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                dataKeyChange[i] = $(this).val();
                i++;
            }
        });
        var status = $('#P_STATUS').val();
        var note_reply = $('#NOTE_REPLY').val();
        if (status == {{STATUS_INT_KHONG}}) {
            jqueryCommon.showMsgError(0,'Bạn chưa chọn trạng thái xử lý để cập nhật');
            return false;
        }
        if (dataKeyChange.length == 0 && status == {{STATUS_INT_MOT}}) {
            jqueryCommon.showMsgError(0,'Bạn chưa chọn data để cập nhật');
            return false;
        }
        if (note_reply.length == 0) {
            jqueryCommon.showMsgError(0,'Bạn chưa nhập nội dung phê duyệt để cập nhật');
            return false;
        }
        var _token = $('input[name="_token"]').val();
        var dataInforChange = $('#data_infor_change').val();
        var dataRequest = $('#data_request').val();

        var msg = 'Bạn có muốn đồng bộ các đơn này?';
        jqueryCommon.isConfirm(msg).then((confirmed) => {
            $('#loaderRight').show();
            $('#show_button_approval_order').prop("disabled", true);
            $.ajax({
                type: "post",
                url: url_ajax,
                data: {dataKeyChange: dataKeyChange,
                    dataInforChange: dataInforChange,
                    dataRequest: dataRequest,
                    status: status,
                    note_reply: note_reply,
                    _token: _token,
                    functionAction: '_funcUpdateRequestChange'},
                dataType: 'json',
                success: function (res) {
                    $('#loaderRight').hide();
                    $('#show_button_approval_order').prop("disabled", false);
                    if (res.success == 1) {
                        jqueryCommon.showMsg('success',res.message);
                        window.location.reload();
                    } else {
                        jqueryCommon.showMsgError(res.success,res.message);
                    }
                }
            });
        });
    }
</script>
