{{---ID > 0 và có thông tin data---}}
<div>
    <div class="card-header paddingLeft-unset">
        <div class="col-lg-10 text-left card-title-2 paddingLeft-unset">
             Thông tin đơn bảo hiểm
        </div>
        @if($objectId > 0)
            {{--<div class="col-lg-2 text-right card-title-2 display-none-block">
                <input type="hidden" id="show_block_detail_1" name="show_block_detail_1" value="1">
                <a href="javascript:;" data-block="detail_1" data-infor="formInforItem" data-edit="formEditItem" class="a_edit_block color_hdi"> Sửa</a>
            </div>--}}
        @endif
    </div>
    {{----Block thông tin----}}
    <form id="form_{{$formName}}">
    <div class="formInforItem " >
        <div class="marginT15">
            <div class="form-group form-infor-detail">
                <div class="row form-group">
                    <div class="col-lg-4">
                        Sản phẩm: <b class="showInforItem" data-field="PRODUCT_CODE"></b>
                    </div>
                    <div class="col-lg-4">
                        Số giấy chứng nhận: <b class="showInforItem" data-field="CERTIFICATE_NO"></b>
                    </div>
                    <div class="col-lg-4">
                        Tên NĐBH: <b class="showInforItem" data-field="NAME"></b>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-4">
                        Ngày sinh: <b class="showInforItem" data-field="DOB"></b>
                    </div>
                    <div class="col-lg-4">
                        Giới tính: <b class="showInforItem" data-field="GENDER"></b>
                    </div>
                    <div class="col-lg-4">
                        Số điện thoại: <b class="showInforItem" data-field="PHONE"></b>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-4">
                        Email: <b class="showInforItem" data-field="EMAIL"></b>
                    </div>
                    <div class="col-lg-8">
                        Địa chỉ: <b class="showInforItem" data-field="ADDRESS"></b>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-4">
                        Ngày cấp: <b class="showInforItem" data-field="">@if(isset($dataItem->DATE_SIGN) && trim($dataItem->DATE_SIGN) != ''){{date('d/m/Y',strtotime($dataItem->DATE_SIGN))}}@endif</b>
                    </div>
                    <div class="col-lg-4">
                        Hiệu lực: <b class="showInforItem" data-field="">
                            @if(isset($dataItem->EFFECTIVE_DATE) && trim($dataItem->EFFECTIVE_DATE) != ''){{date('d/m/Y',strtotime($dataItem->EFFECTIVE_DATE))}}@endif -
                            @if(isset($dataItem->EXPIRATION_DATE) && trim($dataItem->EXPIRATION_DATE) != ''){{date('d/m/Y',strtotime($dataItem->EXPIRATION_DATE))}}@endif
                        </b>
                    </div>
                    <div class="col-lg-4">
                        Phí: <b class="showInforItem" data-field="">
                            @if(isset($dataItem->TOTAL_AMOUNT) && (int)($dataItem->TOTAL_AMOUNT) > 0 ){{numberFormat($dataItem->TOTAL_AMOUNT)}}đ@endif
                        </b>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{----Form Edit----}}
    <div class="formEditItem" >
        <div class="">
            <input type="hidden" id="objectId" name="objectId" value="">
            <input type="hidden" id="url_action" name="url_action" value="{{$urlAjaxGetAction}}">
            <input type="hidden" id="formName" name="formName" value="{{$formName}}">
            <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataItem)}}">
            <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">
            {{ csrf_field() }}
        </div>
    </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".a_edit_block").on('click', function () {
            jqueryCommon.clickEditBlock(this);
        });

        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    function compareDate(){
        var startDate = $('#EFFECTIVE_DATE').val();
        alert(startDate);
        var job_start_date = "10-1-2014"; // Oct 1, 2014
        var job_end_date = "11-1-2014"; // Nov 1, 2014
        job_start_date = job_start_date.split('-');
        job_end_date = job_end_date.split('-');

        var new_start_date = new Date(job_start_date[2],job_start_date[0],job_start_date[1]);
        var new_end_date = new Date(job_end_date[2],job_end_date[0],job_end_date[1]);

        if(new_end_date <= new_start_date) {
            // your code
        }
    }
</script>
