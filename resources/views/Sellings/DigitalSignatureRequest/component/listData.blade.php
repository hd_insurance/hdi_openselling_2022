<div class="card">
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tabListData" role="tabpanel">
                {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
                    <div class="ibox">
                        <div class="row">
                            <div class=" col-lg-6">
                                <label for="user_email">Tìm kiếm</label>
                                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" placeholder="Số giấy chứng nhận, Mã ticket.." @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
                            </div>
                            <div class=" col-lg-4">
                                <label for="user_group">Đối tác</label>
                                <select  class="form-control input-sm chosen-select w-100" name="p_org_code" id="p_org_code">
                                    {!! $optionOrg !!}
                                </select>
                            </div>
                            <div class=" col-lg-2">
                                <label for="user_group">Tình trạng</label>
                                <select  class="form-control input-sm chosen-select w-100" name="p_status" id="p_status">
                                    {!! $optionStatus !!}
                                </select>
                            </div>
                        </div>

                        <div class="row marginT10">
                            <div class=" col-lg-6">
                                <label for="user_email">Sản phẩm</label>
                                <select  class="form-control input-sm chosen-select w-100" name="p_product_code" id="p_product_code">
                                    {!! $optionProduct !!}
                                </select>
                            </div>
                            <div class="col-lg-1_5">
                                <label for="user_email">Thời gian yêu cầu từ </label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_from_date" id="p_from_date" @if(isset($search['p_from_date']))value="{{$search['p_from_date']}}"@endif>
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>
                            <div class="col-lg-1_5">
                                <label for="user_email">Đến ngày</label>
                                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_to_date" id="p_to_date" @if(isset($search['p_to_date']))value="{{$search['p_to_date']}}"@endif >
                                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
                            </div>

                            <div class="col-lg-1_5 marginT30 text-right">
                                <button class="w_100 btn btn-primary" title="Tìm kiếm" type="submit" name="submit" value="1"><i class="fa fa-search fa-2x"></i> Tìm kiếm</button>
                                {{--<a href="javascript:void(0);" class="btn btn-success" onclick="jqueryCommon.doCallFunctionAction(this);" data-function-action="_funcPushDataTest" data-show-id="content-page-right" data-loading="2" data-input="" data-form-name="addFormContact" title="{{viewLanguage('Thông tin yêu cầu')}}" data-method="post" data-url="{{$urlAjaxGetAction}}">
                                    <i class="pe-7s-look fa-2x"></i>
                                </a>--}}
                            </div>
                        </div>
                        <hr class="marginT15">
                    </div>

                    @if($data && sizeof($data) > 0)
                        <div class="row marginB10">
                            <div class="col-lg-4 text-left paddingRight-unset">
                                <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif</h5>
                            </div>
                            <div class="col-lg-8 text-right">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead class="thin-border-bottom">
                                <tr class="table-background-header">
                                    <th width="3%" class="text-center middle">STT
                                        {{--<input type="checkbox" class="check" id="checkAllOrder">--}}
                                    </th>
                                    <th width="5%" class="text-center middle">TT</th>
                                    <th width="8%" class="text-center middle">{{viewLanguage('Mã ticket')}}</th>
                                    <th width="12%" class="text-center middle">{{viewLanguage('Số hợp đồng')}}</th>

                                    <th width="18%" class="text-center middle">{{viewLanguage('Khách hàng')}}</th>
                                    <th width="15%" class="text-center middle">{{viewLanguage('Đối tác')}}</th>

                                    <th width="12%" class="text-center middle">{{viewLanguage('Ngày yêu cầu')}}</th>
                                    <th width="10%" class="text-center middle">{{viewLanguage('Người xử lý')}}</th>
                                    <th width="10%" class="text-center middle">{{viewLanguage('Tình trạng')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <td class="text-center middle">
                                            {{$stt+$key+1}}<br/>
                                            {{--<input class="check" type="checkbox" name="checkItems[]" value="@if(isset($item->REQUEST_CODE)){{$item->REQUEST_CODE}}@endif" onchange="changeColorButton();" data-product-code="@if(isset($item->REQUEST_CODE)){{$item->REQUEST_CODE}}@endif">--}}
                                        </td>
                                        <td class="text-center middle">
                                            @if($is_root || $permission_view || $permission_add)
                                                <a href="javascript:void(0);" class="color_hdi" onclick="jqueryCommon.doCallFunctionAction(this);" data-function-action="_funcGetDetailItem" data-show-id="content-page-right" data-loading="2" data-input="{{json_encode(['dataItem'=>$item])}}" data-form-name="addFormContact" title="{{viewLanguage('Thông tin yêu cầu')}}" data-method="post" data-url="{{$urlAjaxGetAction}}">
                                                    <i class="pe-7s-look fa-2x"></i>
                                                </a>
                                                @if(isset($item->FILE_CODE) && trim($item->FILE_CODE) != '')
                                                    <a href="{{$urlServiceFile.$item->FILE_CODE}}" class="color_warning" target="_blank" title="Giấy chứng nhận">
                                                        <i class="pe-7s-note2 fa-2x"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                        <td class="text-center middle">{{$item->REQUEST_CODE}}</td>
                                        <td class="text-left middle">
                                            @if(isset($item->CERTIFICATE_NO)){{$item->CERTIFICATE_NO}}<br/>@endif
                                            @if(isset($item->PRODUCT_CODE)){{$item->PRODUCT_CODE}}<br/>@endif
                                            @if(isset($item->TOTAL_AMOUNT) && $item->TOTAL_AMOUNT > 0)<b class="red">{{numberFormat($item->TOTAL_AMOUNT)}}đ</b>@endif
                                        </td>

                                        <td class="text-left middle">
                                            @if(isset($item->NAME)){{$item->NAME}}<br/>@endif
                                            @if(isset($item->EMAIL)){{$item->EMAIL}}<br/>@endif
                                            @if(isset($item->PHONE)){{$item->PHONE}}<br/>@endif
                                            @if(isset($item->CONTACT_NAME)){{$item->CONTACT_NAME}}@endif
                                        </td>

                                        <td class="text-left middle">
                                            {{$item->ORG_CODE}}<br/>
                                            @if(isset($item->CONTACT_EMAIL)){{$item->CONTACT_EMAIL}}<br/>@endif
                                            @if(isset($item->CONTACT_PHONE)){{$item->CONTACT_PHONE}}@endif
                                        </td>

                                        <td class="text-center middle">{{date('d/m/Y h:i',strtotime($item->CREATE_DATE))}}</td>
                                        <td class="text-center middle">{{$item->MODIFIED_BY}}</td>
                                        <td class="text-center middle">
                                            @if(isset($arrStatus[$item->STATUS])){{$arrStatus[$item->STATUS]}}@endif
                                            <br>
                                            @if(isset($item->IS_RUN) && trim($item->IS_RUN) == STATUS_INT_HAI)
                                                <a href="#" class="color_hdi" target="_blank" title="Tự động ký số">
                                                    <i class="color_hdi fa fa-recycle fa-2x"></i>
                                                </a>
                                            @endif
                                            @if(isset($item->IS_RUN) && trim($item->IS_RUN) == STATUS_INT_MOT)
                                                <a href="#" class="color_hdi" target="_blank" title="Thao tác thủ công">
                                                    <i class="color_hdi fa fa-thumbs-up fa-2x"></i>
                                                </a>
                                            @endif
                                            @if(isset($item->IS_RUN) && trim($item->IS_RUN) == STATUS_INT_KHONG)
                                                <a href="#" class="color_hdi" target="_blank" title="Mới nhận yêu cầu">
                                                    <i class="color_hdi fa fa-share fa-2x"></i>
                                                </a>
                                            @endif

                                            @if(isset($item->IS_SEND_EMAIL) && trim($item->IS_SEND_EMAIL) == STATUS_INT_MOT)
                                               <a href="#" class="color_hdi" target="_blank" title="Đã gửi email">
                                                    <i class=" fa fa-envelope fa-2x"></i>
                                               </a>
                                            @else
                                                <a href="#" class="color_red" target="_blank" title="Chưa gửi email">
                                                    <i class="fa fa-envelope fa-2x"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="paging_simple_numbers">
                            {!! $paging !!}
                        </div>
                    @else
                        <div class="alert">
                            Không có dữ liệu
                        </div>
                    @endif
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        $("#checkAllOrder").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
            changeColorButton();
        });
        var config = {
            '.chosen-select'           : {width: "100%"},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

    });
    function changeColorButton(){
        var changeColor = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                changeColor = 1;
            }
        });
        if(changeColor == 1){
            $('#show_button_approval_order').removeClass("display-none-block");
            $("#approval_order").addClass("btn-success");
            $("#approval_order").removeClass("btn-light");
        }else {
            $('#show_button_approval_order').addClass("display-none-block");
            $("#approval_order").removeClass("btn-success");
            $("#approval_order").addClass("btn-light");
        }
    }
    function clickApprovalOrderList(url_ajax){
        var dataContractCode = [];
        var dataProductCode = [];
        var i = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                dataContractCode[i] = $(this).val();
                dataProductCode[i] = $(this).attr('data-product-code');
                i++;
            }
        });
        if (dataContractCode.length == 0 || dataProductCode.length == 0) {
            alert('Bạn chưa chọn đơn để thao tác.');
            return false;
        }
        var _token = $('input[name="_token"]').val();
        var msg = 'Bạn có muốn đồng bộ các đơn này?';
        jqueryCommon.isConfirm(msg).then((confirmed) => {
            $('#loader').show();
            $.ajax({
                type: "post",
                url: url_ajax,
                data: {dataContractCode: dataContractCode,_token: _token, dataProductCode: dataProductCode,actionUpdate: 'syncDataCore'},
                dataType: 'json',
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        jqueryCommon.showMsg('success',res.message);
                        window.location.reload();
                    } else {
                        jqueryCommon.showMsgError(res.success,res.message);
                    }
                }
            });
        });
    }
    function ajaxRemoveOrder(url){
        var contract_no = $('#p_search_contract_no').val();
        var programme_id = $('#p_search_programme_id').val();
        var product_id = $('#p_search_product_id').val();
        if(contract_no.trim() != '' && programme_id.trim() != '' && product_id.trim() != ''){
            var _token = $('input[name="_token"]').val();
            $('#loader').show();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url,
                data: {
                    '_token': _token,
                    'programme_id': programme_id,
                    'product_id': product_id,
                    'contract_no': contract_no,
                    'functionAction': 'ajaxRemoveOrder'
                },
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        window.location.load();
                    } else {
                        jqueryCommon.showMsgError(res.success, res.message);
                    }
                }
            });
        }else {
            jqueryCommon.showMsgError(0, 'Bạn chưa tìm số PLHĐ để xóa');
        }
    }
</script>





