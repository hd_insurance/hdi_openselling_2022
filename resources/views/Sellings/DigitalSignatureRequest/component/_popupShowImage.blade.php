<div class="modal-content" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_view_image">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$nameImage}}</h4>
        </div>
        <div class="modal-body paddingBottom-unset">
            <div class="form-group">
                @if($fileExtension == 'pdf')
                    <div class="row" style="height: 500px">
                        <object data="{{$urlImage}}" type="application/pdf" width="100%" height="100%">
                            <embed src="{{$urlImage}}" type="application/pdf">
                            <p>This browser does not support PDFs. Please download the PDF to view it: <a href="{{$urlImage}}">Download PDF</a>.</p>
                            </embed>
                        </object>
                    </div>
                @else
                    <div class="row">
                        <img id='base64imageView' src='{{$urlImage}}' title="{{$nameImage}}" width="100%" height="100%"/>
                    </div>
                @endif
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i>
                {{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
