@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Sellings.DigitalSignatureRequest.component.formSearch')

    {{--list data---}}
    @include('Sellings.DigitalSignatureRequest.component.listData')

@stop
