{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="row">
        <div class=" col-lg-3 paddingRight-unset">
            <label for="user_group">Đối tác</label>
            <select  class="form-control input-sm chosen-select w-100" name="p_org_code" id="p_org_code">
                {!! $optionOrg !!}
            </select>
        </div>
        <div class=" col-lg-3 paddingRight-unset">
            <label for="user_email">Sản phẩm</label>
            <select  class="form-control input-sm chosen-select w-100" name="p_product_code" id="p_product_code">
                {!! $optionProduct !!}
            </select>
        </div>
        <div class="col-lg-1_5 paddingRight-unset">
            <label for="user_email">Số hợp đồng</label>
            <input type="text" class="form-control input-sm" id="p_contract_no" name="p_contract_no" placeholder="Số HĐ" @if(isset($search['p_contract_no']))value="{{$search['p_contract_no']}}"@endif>
        </div>

        <div class="col-lg-1_5">
            <label for="user_email">Từ ngày tạo</label>
            <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_from_date" id="p_from_date" @if(isset($search['p_from_date']))value="{{$search['p_from_date']}}"@endif>
            <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
        </div>
        <div class="col-lg-1_5">
            <label for="user_email">Đến ngày tạo</label>
            <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_to_date" id="p_to_date" @if(isset($search['p_to_date']))value="{{$search['p_to_date']}}"@endif >
            <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
        </div>

        <div class="col-lg-1 marginT30 text-right">
            <button class="w_100 btn btn-primary" title="Tìm kiếm" type="submit" name="submit" value="1"><i class="fa fa-search fa-2x"></i></button>
        </div>
    </div>
    <hr class="marginT15">
</div>

@if($data && sizeof($data) > 0)
    <div class="row marginB10">
        <div class="col-lg-4 text-left paddingRight-unset">
            <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif</h5>
        </div>
        <div class="col-lg-8 text-right">
            @if($total >0)
                <button class="btn btn-success display-none-block" type="button" name="approval_order" id="show_button_approval_order" value="0" onclick="clickApprovalOrderList('{{$urlAjaxPostAction}}')"><i class="fa fa-check"></i> {{viewLanguage('Đồng bộ')}}</button>
            @endif
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead class="thin-border-bottom">
            <tr class="table-background-header">
                <th width="3%" class="text-center middle">
                    <input type="checkbox" class="check" id="checkAllOrder">
                </th>
                <th width="3%" class="text-center middle"></th>
                <th width="18%" class="text-center middle">{{viewLanguage('Tên khách hàng')}}</th>
                <th width="15%" class="text-center middle">{{viewLanguage('Thông tin thêm')}}</th>
                <th width="15%" class="text-center middle">{{viewLanguage('Sản phẩm')}}</th>

                <th width="6%" class="text-right middle">{{viewLanguage('Tổng GCN')}}</th>
                <th width="8%" class="text-right middle">{{viewLanguage('Tiền giảm')}}</th>
                <th width="8%" class="text-right middle">{{viewLanguage('Phí bổ sung')}}</th>

                <th width="8%" class="text-right middle">{{viewLanguage('Tiền')}}</th>
                <th width="8%" class="text-right middle">{{viewLanguage('Tổng tiền (VAT)')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $item)
                <tr>
                    <td class="text-center middle">
                        {{$stt+$key+1}}<br/>
                        <input class="check" type="checkbox" name="checkItems[]" value="@if(isset($item->CONTRACT_CODE)){{$item->CONTRACT_CODE}}@endif" onchange="changeColorButton();" data-product-code="@if(isset($item->PRODUCT_CODE)){{$item->PRODUCT_CODE}}@endif">
                    </td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);"  class="color_hdi" onclick="jqueryCommon.getDataByAjax(this);" data-loading="1" data-form-name="addFormContact" data-url="{{$urlAjaxGetAction}}" data-function-action="_ajaxFunctionAction" data-input="{{json_encode(['type'=>'','objectId'=>1,'funcAction'=>'getEditContract','dataItem'=>$item])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thông tin liên hệ')}}" data-method="post" data-objectId="">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>
                    <td class="text-left middle">
                        @if(isset($item->NAME)){{$item->NAME}}<br/>@endif
                        @if(isset($item->ADDRESS))<span class="font_10">{{$item->ADDRESS}}</span>@endif
                    </td>
                    <td class="text-left middle">
                         @if(isset($item->CONTRACT_NO))<b class="font_10">HĐ</b>: {{$item->CONTRACT_NO}}<br/>@endif
                         @if(isset($item->EFFECTIVE_DATE))<b class="font_10">Hiệu lực</b>: {{$item->EFFECTIVE_DATE}}<br/>@endif
                         @if(isset($item->EXPIRATION_DATE) && $item->EXPIRATION_DATE != '')<b class="font_10">Hết hạn</b>: {{$item->EXPIRATION_DATE}}@endif
                    </td>

                    <td class="text-left middle">
                        @if(isset($item->PRODUCT_CODE)){{$item->PRODUCT_CODE}}@endif
                        @if(isset($item->PRODUCT_NAME))<br/>{{$item->PRODUCT_NAME}}@endif
                    </td>

                    <td class="text-center middle">@if(isset($item->NUM_OF_CER)){{$item->NUM_OF_CER}}@endif</td>
                    <td class="text-right middle"><span class="red">@if(isset($item->TOTAL_DISCOUNT)){{numberFormat($item->TOTAL_DISCOUNT)}}@endif</span></td>
                    <td class="text-right middle"><span class="red">@if(isset($item->ADDITIONAL_FEES)){{numberFormat($item->ADDITIONAL_FEES)}}@endif</span></td>
                    <td class="text-right middle"><span class="red">@if(isset($item->AMOUNT)){{numberFormat($item->AMOUNT)}}@endif</span></td>
                    <td class="text-right middle"><span class="red">@if(isset($item->TOTAL_AMOUNT)){{numberFormat($item->TOTAL_AMOUNT)}}@endif</span></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="paging_simple_numbers">
        {!! $paging !!}
    </div>
@else
    <div class="alert">
        Không có dữ liệu
    </div>
@endif
{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        $("#checkAllOrder").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
            changeColorButton();
        });
        var config = {
            '.chosen-select'           : {width: "100%"},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

    });
    function changeColorButton(){
        var changeColor = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                changeColor = 1;
            }
        });
        if(changeColor == 1){
            $('#show_button_approval_order').removeClass("display-none-block");
            $("#approval_order").addClass("btn-success");
            $("#approval_order").removeClass("btn-light");
        }else {
            $('#show_button_approval_order').addClass("display-none-block");
            $("#approval_order").removeClass("btn-success");
            $("#approval_order").addClass("btn-light");
        }
    }
    function clickApprovalOrderList(url_ajax){
        var dataContractCode = [];
        var dataProductCode = [];
        var i = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                dataContractCode[i] = $(this).val();
                dataProductCode[i] = $(this).attr('data-product-code');
                i++;
            }
        });
        if (dataContractCode.length == 0 || dataProductCode.length == 0) {
            alert('Bạn chưa chọn đơn để thao tác.');
            return false;
        }
        var _token = $('input[name="_token"]').val();
        var msg = 'Bạn có muốn đồng bộ các đơn này?';
        jqueryCommon.isConfirm(msg).then((confirmed) => {
            $('#loader').show();
            $.ajax({
                type: "post",
                url: url_ajax,
                data: {dataContractCode: dataContractCode,_token: _token, dataProductCode: dataProductCode,actionUpdate: 'syncDataCore'},
                dataType: 'json',
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        jqueryCommon.showMsg('success',res.message);
                        window.location.reload();
                    } else {
                        jqueryCommon.showMsgError(res.success,res.message);
                    }
                }
            });
        });
    }
    function ajaxRemoveOrder(url){
        var contract_no = $('#p_search_contract_no').val();
        var programme_id = $('#p_search_programme_id').val();
        var product_id = $('#p_search_product_id').val();
        if(contract_no.trim() != '' && programme_id.trim() != '' && product_id.trim() != ''){
            var _token = $('input[name="_token"]').val();
            $('#loader').show();
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url,
                data: {
                    '_token': _token,
                    'programme_id': programme_id,
                    'product_id': product_id,
                    'contract_no': contract_no,
                    'functionAction': 'ajaxRemoveOrder'
                },
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        window.location.load();
                    } else {
                        jqueryCommon.showMsgError(res.success, res.message);
                    }
                }
            });
        }else {
            jqueryCommon.showMsgError(0, 'Bạn chưa tìm số PLHĐ để xóa');
        }
    }
</script>




