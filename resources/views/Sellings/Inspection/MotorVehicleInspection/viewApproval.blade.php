@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{ Form::open(array('method' => 'GET', 'role'=>'form','id'=>'formSeachIndex')) }}
    {{--Search---}}
    @include('Sellings.Inspection.MotorVehicleInspection.component.formSearch')

    {{--list data---}}
    @include('Sellings.Inspection.MotorVehicleInspection.component.listDataApproval')
    {{ Form::close() }}
@stop
