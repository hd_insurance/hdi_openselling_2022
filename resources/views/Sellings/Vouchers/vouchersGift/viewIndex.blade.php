@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Sellings.Vouchers.vouchersGift.component.formSearch')

    {{--list data---}}
    @include('Sellings.Vouchers.vouchersGift.component.listData')
@stop
