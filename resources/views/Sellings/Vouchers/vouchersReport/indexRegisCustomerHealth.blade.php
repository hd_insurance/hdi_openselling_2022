@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--list data---}}
    @include('Sellings.Vouchers.vouchersReport.component.listDataRegisCustomerHealth')
@stop
