<div class="modal-content" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form>
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body paddingBottom-unset">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="3%" class="text-center middle">{{viewLanguage('STT')}}</th>
                        <th width="67%" class="text-center middle">{{viewLanguage('Người được bảo hiểm')}}</th>
                        <th width="30%" class="text-center middle">{{viewLanguage('Thao tác file')}}</th>
                    </tr>
                    </thead>
                    @if(!empty($arrFileCer))
                    <tbody>
                    @foreach ($arrFileCer as $key => $item)
                        @if(isset($item['NAME']) && isset($item['ID_FILE']))
                            <?php
                                $linkView = $urlEcertificateFile.$item['ID_FILE'];
                                $linkDownload = $urlServiceFile.$item['ID_FILE'].'?download=true';
                            ?>
                            <tr>
                                <td class="text-center middle">{{$key+1}}</td>
                                <td class="text-left middle">
                                    {{$item['NAME']}}
                                </td>
                                <td class="text-center middle">
                                    <a href="{{$linkView}}" class="color_warning" target="_blank" title="Xem giấy chứng nhận">
                                        <i class="pe-7s-note2 fa-2x"></i>
                                    </a>&nbsp;&nbsp;&nbsp;
                                    <a href="{{$linkDownload}}" class="color_warning" title="Download giấy chứng nhận">
                                        <i class="pe-7s-cloud-download fa-2x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back">
                </i>{{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function() {

});
</script>