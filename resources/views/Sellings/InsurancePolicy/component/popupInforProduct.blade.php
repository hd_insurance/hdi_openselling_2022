<div style="position: relative">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>

    <div id="divDetailItem2">
        <div class="card-header">
            Thông tin chi tiết của sản phẩm
            <div class="btn-actions-pane-right">
                @include('Layouts.BaseAdmin.listButtonActionFormEdit')
            </div>
        </div>

        <div class="div-infor-right">
            <div class="card-body paddingTop-unset">

                {{---Thông tin khác---}}
                <div class="card-header card-header-tab-animation">
                    <ul class="nav nav-justified">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" data-toggle="tab" href="#{{$tabOtherItem1}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem1}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetDataOfTab" data-input="{{json_encode(['type'=>$tabOtherItem1,'item_id'=>0])}}" data-object-id="1">
                                <span>{{viewLanguage('Thông tin sản phẩm')}}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" data-toggle="tab" href="#{{$tabOtherItem2}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem1}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetDataOfTab" data-input="{{json_encode(['type'=>$tabOtherItem2,'item_id'=>0])}}" data-object-id="1">
                                <span>{{viewLanguage('Thông tin Q&A')}}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" data-toggle="tab" href="#{{$tabOtherItem3}}" @if($is_root || $permission_view)onclick="jqueryCommon.ajaxGetData(this);" @endif data-show-id="{{$tabOtherItem2}}" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetDataOfTab" data-input="{{json_encode(['type'=>$tabOtherItem3,'item_id'=>0])}}" data-object-id="1">
                                <span>{{viewLanguage('Thông tin bồi thường')}}</span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content marginT20" >
                    {{--Chi tiết hợp đồng---}}
                    <div class="tab-pane tabs-animation fade show active" id="{{$tabOtherItem1}}" role="tabpanel">
                        @if(!empty($inforBlock1))
                            <div id="accordion_{{$inforBlock1->KEY}}" class="accordion-wrapper mb-3">
                                <div class="card">
                                    @if(isset($inforBlock1->VALUE))
                                        @foreach($inforBlock1->VALUE as $kkk=>$arrInfor1)
                                            <div id="headingThree_{{$arrInfor1->KEY}}" class="card-header">
                                                <button type="button" data-toggle="collapse" data-target="#collapseOne_{{$arrInfor1->KEY}}" aria-expanded="false" aria-controls="collapseThree" class="text-left m-0 p-0 btn btn-link btn-block">
                                                    <h5 class="m-0 p-0 color_hdi">{{$arrInfor1->DESC}}</h5>
                                                </button>
                                            </div>
                                            <div data-parent="#accordion_{{$inforBlock1->KEY}}" id="collapseOne_{{$arrInfor1->KEY}}" class="collapse @if($arrInfor1->KEY == 1) show @endif">
                                                @foreach($arrInfor1->VALUE as $kkk1 => $infor11)
                                                    @if(in_array($infor11->TYPE,['HTML','PDF']))
                                                        @if($infor11->TYPE == 'HTML')
                                                            <div class="card-body">
                                                                {!! $infor11->VALUE !!}
                                                            </div>
                                                        @endif
                                                        @if($infor11->TYPE == 'PDF')
                                                            <div class="card-body">
                                                                <iframe src="{{$infor11->VALUE}}" style="width:100%; height:680px;" frameborder="0"></iframe>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="card-body">
                                                            {!! $infor11->VALUE !!}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>

                    {{--Thông tin hợp đồng vay---}}
                    <div class="tab-pane tabs-animation fade" id="{{$tabOtherItem2}}" role="tabpanel">
                        @if(!empty($inforBlock2))
                            <div id="accordion2_{{$inforBlock2->KEY}}" class="accordion-wrapper mb-3">
                                <div class="card">
                                    @if(isset($inforBlock2->VALUE))
                                        @foreach($inforBlock2->VALUE as $kk=>$arrInfor2)
                                            <div id="headingThree2_{{$arrInfor2->KEY}}" class="card-header">
                                                <button type="button" data-toggle="collapse" data-target="#collapseOne2_{{$arrInfor2->KEY}}" aria-expanded="false" aria-controls="collapseThree" class="text-left m-0 p-0 btn btn-link btn-block">
                                                    <h5 class="m-0 p-0 color_hdi">{{$arrInfor2->DESC}}</h5>
                                                </button>
                                            </div>
                                            <div data-parent="#accordion2_{{$inforBlock2->KEY}}" id="collapseOne2_{{$arrInfor2->KEY}}" class="collapse @if($arrInfor2->KEY == 1) show @endif">
                                                @foreach($arrInfor2->VALUE as $kk2 => $infor22)
                                                    @if(in_array($infor22->TYPE,['HTML','PDF']))
                                                        @if($infor22->TYPE == 'HTML')
                                                            <div class="card-body">
                                                                {!! $infor22->VALUE !!}
                                                            </div>
                                                        @endif
                                                        @if($infor22->TYPE == 'PDF')
                                                            <div class="card-body">
                                                                <object data="{{$infor22->VALUE}}" type="application/pdf" width="100%" height="680px">
                                                                    <embed src="{{$infor22->VALUE}}" type="application/pdf">
                                                                    <p>This browser does not support PDFs. Please download the PDF to view it: <a href="{{$infor22->VALUE}}">Download PDF</a>.</p>
                                                                    </embed>
                                                                </object>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="card-body">
                                                            {!! $infor22->VALUE !!}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>

                    {{--Thông tin hợp đồng vay---}}
                    <div class="tab-pane tabs-animation fade" id="{{$tabOtherItem3}}" role="tabpanel">
                        @if(!empty($inforBlock3))
                            <div id="accordion_{{$inforBlock3->KEY}}" class="accordion-wrapper mb-3">
                                <div class="card">
                                    @if(isset($inforBlock3->VALUE))
                                        @foreach($inforBlock3->VALUE as $kkk=>$arrInfor3)
                                            <div id="headingThree_{{$arrInfor3->KEY}}" class="card-header">
                                                <button type="button" data-toggle="collapse" data-target="#collapseOne_{{$arrInfor3->KEY}}" aria-expanded="false" aria-controls="collapseThree" class="text-left m-0 p-0 btn btn-link btn-block">
                                                    <h5 class="m-0 p-0 color_hdi">{{$arrInfor3->DESC}}</h5>
                                                </button>
                                            </div>
                                            <div data-parent="#accordion_{{$inforBlock3->KEY}}" id="collapseOne_{{$arrInfor3->KEY}}" class="collapse @if($arrInfor3->KEY == 1) show @endif">
                                                @foreach($arrInfor3->VALUE as $kkk3 => $infor33)
                                                    @if(in_array($infor22->TYPE,['HTML','PDF']))
                                                        @if($infor33->TYPE == 'HTML')
                                                            <div class="card-body">
                                                                {!! $infor33->VALUE !!}
                                                            </div>
                                                        @endif
                                                        @if($infor33->TYPE == 'PDF')
                                                            <div class="card-body">
                                                                <object data="{{$infor33->VALUE}}" type="application/pdf" width="100%" height="680px">
                                                                    <embed src="{{$infor33->VALUE}}" type="application/pdf">
                                                                    <p>This browser does not support PDFs. Please download the PDF to view it: <a href="{{$infor33->VALUE}}">Download PDF</a>.</p>
                                                                    </embed>
                                                                </object>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="card-body">
                                                            {!! $infor33->VALUE !!}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        //chi tiết banks
        $('.detailOtherCommon').dblclick(function () {
            jqueryCommon.ajaxGetData(this);
        });
    });
</script>