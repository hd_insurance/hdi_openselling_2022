@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{ Form::open(array('method' => 'GET', 'role'=>'form','id'=>'formSeachIndex')) }}
        {{--Search---}}
        @include('Sellings.InsurancePolicy.component.formSearch')

        {{--list data---}}
        @include('Sellings.InsurancePolicy.component.listDataSearchOrder')
    {{ Form::close() }}
@stop
