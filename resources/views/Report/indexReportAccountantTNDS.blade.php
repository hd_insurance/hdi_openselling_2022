@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--list data---}}
    @include('Report.accountant.listDataProductTNDS')
@stop
