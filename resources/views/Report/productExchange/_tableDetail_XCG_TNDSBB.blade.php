<div class="card-body">
    @if($data && sizeof($data) > 0)

        <div class="row">
            <div class="col-lg-5 text-left">
                <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif @if($totalMoney >0), tổng doanh thu: <b class="red">{{numberFormat($totalMoney)}}</b>  @endif </h5>
            </div>
            <div class="col-lg-7 text-right">
                @if($total >0)
                    <button class="border-0 btn-transition btn btn-outline-success marginDownT15" type="submit" name="submit" value="2" title="Xuất excel"><i class="fa fa-file-excel fa-2x"></i></button>
                @endif
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="3%" class="text-center middle">{{viewLanguage('STT')}}</th>
                    <th width="22%" class="text-center middle">{{viewLanguage('Đối tác')}}</th>
                    <th width="30%" class="text-left middle">{{viewLanguage('Thông tin đơn hàng')}}</th>

                    <th width="23%" class="text-left middle">{{viewLanguage('Thông tin khách hàng')}}</th>
                    <th width="10%" class="text-center middle">{{viewLanguage('Ngày mua')}}</th>
                    <th width="12%" class="text-center middle">{{viewLanguage('Trạng thái')}}</th>
                </tr>
                </thead>
                <tbody>
                {{--@if(isset($inforTotal->TOTAL_AMOUNT))
                    <tr>
                        <td class="text-right middle" colspan="5">Tổng tiền phí BH</td>
                        <td class="text-right middle" ><b class="red">{{numberFormat($inforTotal->TOTAL_AMOUNT)}}</b></td>
                        <td class="text-left middle" ></td>
                    </tr>
                @endif--}}
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-center middle">@if(isset($item->ORG_SELLER_NAME)){{$item->ORG_SELLER_NAME}}@endif</td>

                        <td class="text-left middle">
                            @if(isset($item->ORDER_CODE))<b>Mã đơn: </b>{{$item->ORDER_CODE}}<br>@endif
                            @if(isset($item->CERTIFICATE_NO))<b>GCN: </b>{{$item->CERTIFICATE_NO}}<br>@endif
                            @if(isset($item->SKU_NAME))<b>SKU: </b>{{$item->SKU_NAME}}<br>@endif
                        </td>
                        <td class="text-left middle">
                            @if(isset($item->NAME))<b>Tên: </b>{{$item->NAME}}<br>@endif
                            @if(isset($item->PHONE))<b>Phone: </b>{{$item->PHONE}}<br>@endif
                            @if(isset($item->EMAIL))<b>Email: </b>{{$item->EMAIL}}<br>@endif
                        </td>
                        <td class="text-center middle">@if(isset($item->DATE_BUY)){{$item->DATE_BUY}}@endif</td>
                        <td class="text-center middle">@if(isset($item->SKU_DETAIL_STATUS)){{$item->SKU_DETAIL_STATUS}}@endif</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
