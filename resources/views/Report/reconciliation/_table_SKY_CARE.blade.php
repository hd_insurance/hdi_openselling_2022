<div class="card-body">
    @if($data && sizeof($data) > 0)

        <div class="row">
            <div class="col-lg-5 text-left">
                <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif @if($totalMoney >0), tổng doanh thu: <b class="red">{{numberFormat($totalMoney)}}</b>  @endif </h5>
            </div>
            <div class="col-lg-7 text-right">
                @if($total >0)
                    <button class="border-0 btn-transition btn btn-outline-success marginDownT15" type="submit" name="submit" value="2" title="Xuất excel" onclick="changeStatus();"><i class="fa fa-file-excel fa-2x"></i></button>
                @endif
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="3%" class="text-center middle" >{{viewLanguage('STT')}}</th>
                    <th width="7%" class="text-center middle" >{{viewLanguage('Khởi hành')}}</th>

                    <th width="9%" class="text-center middle">{{viewLanguage('Hành trình')}}</th>
                    <th width="8%" class="text-center middle">{{viewLanguage('Quốc gia đi')}}</th>
                    <th width="8%" class="text-center middle">{{viewLanguage('Quốc gia đến')}}</th>

                    <th width="8%" class="text-center middle" >{{viewLanguage('SH chuyến bay')}}</th>
                    <th width="8%" class="text-center middle" >{{viewLanguage('SH chuyến bay nối chuyến')}}</th>
                    <th width="10%" class="text-center middle" >{{viewLanguage('Gói bảo hiểm')}}</th>
                    <th width="5%" class="text-center middle" >{{viewLanguage('Hành khách')}}</th>
                    <th width="5%" class="text-center middle" >{{viewLanguage('Em bé đi kèm')}}</th>

                    <th width="8%" class="text-right middle" >{{viewLanguage('Phí BH/ HK')}}</th>
                    <th width="8%" class="text-right middle" >{{viewLanguage('Tổng doanh thu')}}</th>
                    <th width="7%" class="text-center middle" >{{viewLanguage('Ngày cấp đơn')}}</th>
                </tr>
                </thead>
                <tbody>
               @if(isset($dataTotal) && !empty($dataTotal))
                    <tr class="font-bold red">
                        <td class="text-right middle" colspan="8">Tổng</td>
                        <td class="text-center middle">@if(isset($dataTotal->TOTAL_CUSTOMER))<b>{{numberFormat($dataTotal->TOTAL_CUSTOMER)}}@endif</b></td>

                        <td class="text-center middle">@if(isset($dataTotal->TOTAL_INFANT))<b>{{numberFormat($dataTotal->TOTAL_INFANT)}}@endif</b></td>
                        <td class="text-right middle"></td>

                        <td class="text-right middle">@if(isset($dataTotal->TOTAL_AMOUNT))<b>{{numberFormat($dataTotal->TOTAL_AMOUNT)}}@endif</b></td>
                        <td class="text-right middle"></td>
                    </tr>
                @endif
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-center middle">
                            @if(isset($item->DEPATURE_DATE)){{$item->DEPATURE_DATE}}@endif
                        </td>

                        <td class="text-center middle">@if(isset($item->ROUTE)){{$item->ROUTE}}@endif</td>
                        <td class="text-center middle">@if(isset($item->DEPARTURE_COUNTRY)){{$item->DEPARTURE_COUNTRY}}@endif</td>
                        <td class="text-center middle">@if(isset($item->ARRIVE_COUNTRY)){{$item->ARRIVE_COUNTRY}}@endif</td>

                        <td class="text-center middle">@if(isset($item->FLI_NO)){{$item->FLI_NO}}@endif</td>
                        <td class="text-center middle">@if(isset($item->CONNECT_FLI_NO)){{$item->CONNECT_FLI_NO}}@endif</td>
                        <td class="text-center middle">@if(isset($arrPackCode[$item->PACK_CODE])){{$arrPackCode[$item->PACK_CODE]}}@endif</td>
                        <td class="text-center middle">@if(isset($item->CUSTOMER)){{numberFormat($item->CUSTOMER)}}@endif</td>
                        <td class="text-center middle">@if(isset($item->INFANT)){{numberFormat($item->INFANT)}}@endif</td>

                        <td class="text-right middle">@if(isset($item->FEE_INSUR))<span class="red">{{numberFormat($item->FEE_INSUR)}}</span>@endif</td>
                        <td class="text-right middle">@if(isset($item->TOTAL_AMOUNT))<span class="red">{{numberFormat($item->TOTAL_AMOUNT)}}</span>@endif</td>
                        <td class="text-center middle">@if(isset($item->MAX_DATE_SIGN)){{convertDateDMY_HI($item->MAX_DATE_SIGN)}}@endif</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
