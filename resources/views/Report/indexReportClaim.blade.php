@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'GET', 'role'=>'form','id'=>'formSeachIndex')) }}
    {{--list data---}}
    @include('Report.claim.listDataClaim')
    {{ Form::close() }}
@stop
