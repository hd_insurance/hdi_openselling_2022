<div class="card-body">
    @if($data && sizeof($data) > 0)

        <div class="row">
            <div class="col-lg-5 text-left">
                <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif @if($totalMoney >0), tổng doanh thu: <b class="red">{{numberFormat($totalMoney)}}</b>  @endif </h5>
            </div>
            <div class="col-lg-7 text-right">
                @if($total >0)
                    <button class="border-0 btn-transition btn btn-outline-success marginDownT15" type="submit" name="submit" value="2" title="Xuất excel"><i class="fa fa-file-excel fa-2x"></i></button>
                @endif
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="3%" class="text-center middle" >{{viewLanguage('STT')}}</th>
                    <th width="10%" class="text-center middle" >{{viewLanguage('Gói bảo hiểm')}}</th>

                    <th width="10%" class="text-center middle">{{viewLanguage('Mốc thời gian')}}</th>
                    <th width="10%" class="text-right middle">{{viewLanguage('Số tiền bảo hiểm')}}</th>

                    <th width="10%" class="text-right middle" >{{viewLanguage('Số lượng khách hàng')}}</th>
                    <th width="10%" class="text-right middle" >{{viewLanguage('Tổng số tiền bồi thường')}}</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($dataTotal) && !empty($dataTotal))
                    <tr class="font-bold red">
                        <td class="text-right middle" colspan="4">Tổng</td>
                        <td class="text-right middle">@if(isset($dataTotal->TOTAL_CUSTOMER))<b class="price_sale">{{numberFormat($dataTotal->TOTAL_CUSTOMER)}}</b>@endif</td>
                        <td class="text-right middle">@if(isset($dataTotal->TOTAL_FEE_CLAIM))<b class="price_sale">{{numberFormat($dataTotal->TOTAL_FEE_CLAIM)}}</b>@endif</td>
                    </tr>
                @endif
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-center middle">
                            @if($item->PRODUCT_CODE == PRODUCT_CODE_SKY_CARE)
                                @if(isset($arrPackCode[$item->PACK_CODE])){{$arrPackCode[$item->PACK_CODE]}}@endif
                            @else
                                @if(isset($arrPackCode2[$item->PACK_CODE])){{$arrPackCode2[$item->PACK_CODE]}}@endif
                            @endif
                        </td>

                        <td class="text-center middle">@if(isset($item->GROUP_NAME)){{$item->GROUP_NAME}}@endif</td>
                        <td class="text-right middle">@if(isset($item->FEE_CLAIM)){{numberFormat($item->FEE_CLAIM)}}@endif</td>

                        <td class="text-right middle"> @if(isset($item->CUSTOMER)){{numberFormat($item->CUSTOMER)}}@endif</td>
                        <td class="text-right middle">@if(isset($item->TOTAL_FEE_CLAIM))<b class="price_sale">{{numberFormat($item->TOTAL_FEE_CLAIM)}}</b>@endif</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
