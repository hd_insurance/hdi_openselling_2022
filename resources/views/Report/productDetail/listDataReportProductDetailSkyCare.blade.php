{{ Form::open(array('method' => 'Post', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Search')}}</h5>
        <div class="ibox-tools marginDownT6">
            <button class="btn btn-primary" type="submit" name="submit" value="1" onclick="changeStatus();"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-3">
                <label for="user_group">Sản phẩm bảo hiểm</label>
                <select class="form-control input-sm" name="p_product_code" id="p_product_code" onchange="chosseProductCode();">
                    {!! $optionProduct !!}
                </select>
            </div>
            <div class="col-lg-5">
                <div id="list_pack_code_SKY_CARE">
                    <label for="user_group">Gói bảo hiểm</label><br>
                    <select  class="form-control input-sm" name="p_pack_code_SKY_CARE" id="p_pack_code_SKY_CARE" multiple="">
                        {!! $optionPackCode !!}
                    </select>
                    <input type="hidden" id="p_str_pack_code_SKY_CARE" name="p_str_pack_code_SKY_CARE" @if(isset($search['p_str_pack_code_SKY_CARE']))value="{{$search['p_str_pack_code_SKY_CARE']}}"@endif>
                </div>

                <div id="list_pack_code_SKY_CARE_DOMESTIC">
                    <label for="user_group">Gói bảo hiểm</label><br>
                    <select  class="form-control input-sm" name="p_pack_code_SKY_CARE_DOMESTIC" id="p_pack_code_SKY_CARE_DOMESTIC" multiple="">
                        {!! $optionPackCode2 !!}
                    </select>
                    <input type="hidden" id="p_str_pack_code_SKY_CARE_DOMESTIC" name="p_str_pack_code_SKY_CARE_DOMESTIC" @if(isset($search['p_pack_code_SKY_CARE_DOMESTIC']))value="{{$search['p_pack_code_SKY_CARE_DOMESTIC']}}"@endif>
                </div>
            </div>
            <div class=" col-lg-4">
                <label for="user_group">Loại khách hàng</label>
                <select  class="form-control input-sm" name="p_type_customer" id="p_type_customer" multiple="">
                    {!! $optionTypeCutomer !!}
                </select>
                <input type="hidden" id="p_str_type_customer" name="p_str_type_customer" @if(isset($search['p_str_type_customer']))value="{{$search['p_str_type_customer']}}"@endif>
            </div>
        </div>
        <div class="row marginT10">
            <div class=" col-lg-5">
                <label for="user_group">Số hiệu chuyến bay</label>
                <input class="form-control input-sm" name="p_fight_number" id="p_fight_number" @if(isset($search['p_fight_number']))value="{{$search['p_fight_number']}}"@endif>
            </div>
            <div class=" col-lg-2">
                <label for="user_group">Mã đặt chỗ</label>
                <input class="form-control input-sm" name="p_pnr_no" id="p_pnr_no" @if(isset($search['p_pnr_no']))value="{{$search['p_pnr_no']}}"@endif>
            </div>
            <div class=" col-lg-3">
                <label for="user_group">Theo ngày</label><span class="red"> (*)</span>
                <select  class="form-control input-sm" name="p_type_date_search" id="p_type_date_search">
                    {!! $optionTypeDateOrder !!}
                </select>
            </div>
            <div class="col-lg-2">
                <label for="user_group">Ngày</label>
                <input class="form-control input-sm input-date" name="p_date_search" id="p_date_search" @if(isset($search['p_date_search']))value="{{$search['p_date_search']}}"@else value="{{getDateNow()}}" @endif>
                <div class="icon_calendar" ><i class="fa fa-calendar-alt"></i></div>
            </div>
        </div>
    </div>
</div>
<div class="main-card mb-3 card">
    @if(trim($table_view) != '')
        @include('Report.productDetail.'.$table_view)
    @else
        <div class="alert">
            chưa có view
        </div>
    @endif
</div>
{{ Form::close() }}
<link media="all" type="text/css" rel="stylesheet" href="{{URL::asset('assets/backend/admin/lib/selectmulti/multi-styles.css')}}"/>
<script src="{{URL::asset('assets/backend/admin/lib/selectmulti/jquery.multi-select.js')}}"></script>
<script type="text/javascript">

    $(document).ready(function(){
        var product_code = $("#p_product_code").val();
        if(product_code == 'SKY_CARE'){
            $('#list_pack_code_SKY_CARE').show();
            $('#list_pack_code_SKY_CARE_DOMESTIC').hide();
        }
        else if(product_code == 'SKY_CARE_DOMESTIC'){
            $('#list_pack_code_SKY_CARE_DOMESTIC').show();
            $('#list_pack_code_SKY_CARE').hide();
        }

        $('#p_pack_code_SKY_CARE_DOMESTIC').multiSelect();
        $('#p_pack_code_SKY_CARE').multiSelect();

        $('#p_type_customer').multiSelect();
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });

    function chosseProductCode(){
        var product_code = $("#p_product_code").val();
        if(product_code == 'SKY_CARE'){
            $('#list_pack_code_SKY_CARE').show();
            $('#list_pack_code_SKY_CARE_DOMESTIC').hide();
        }
        else if(product_code == 'SKY_CARE_DOMESTIC'){
            $('#list_pack_code_SKY_CARE_DOMESTIC').show();
            $('#list_pack_code_SKY_CARE').hide();
        }

        //$('#p_pack_code_'+product_code).val('');
        //$('#p_str_pack_code_'+product_code).val('');
    }
    function changeStatus(){
        var product_code = $("#p_product_code").val();
        var strStatus = $('#p_pack_code_'+product_code+' option:selected').toArray().map(item => item.value).join();
        if(strStatus.trim() != ''){
            $('#p_str_pack_code_'+product_code).val(strStatus.trim());
        }else {
            $('#p_str_pack_code_'+product_code).val('');
        }

        var strCutomer = $('#p_type_customer option:selected').toArray().map(item => item.value).join();
        if(strCutomer.trim() != ''){
            $('#p_str_type_customer').val(strCutomer.trim());
        }else {
            $('#p_str_type_customer').val('');
        }
    }
</script>
