<div class="card-body">
    @if($data && sizeof($data) > 0)
        <div class="row">
            <div class="col-lg-4 text-left">
                <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif @if($totalMoney >0), tổng doanh thu: <b class="red">{{numberFormat($totalMoney)}}</b>  @endif </h5>
            </div>
            <div class="col-lg-8 text-right">
                @if($total >0)
                    <button class="border-0 btn-transition btn btn-outline-success marginDownT15" type="submit" name="submit" value="2" title="Xuất excel"><i class="fa fa-file-excel fa-2x"></i></button>
                @endif
            </div>
        </div>
        <div class="table-responsive"{{-- style="width:1350px; overflow: hidden; overflow-x: scroll"--}}>
            <table class="table table-bordered table-hover ">
                <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="3%" class="text-center middle">{{viewLanguage('STT')}}</th>
                        <th width="6%" class="text-center middle">{{viewLanguage('PNR_NO')}}</th>
                        <th width="9%" class="text-center middle">{{viewLanguage('POLICY_NUM')}}</th>
                        <th width="8%" class="text-center middle">{{viewLanguage('FULL_NAME')}}</th>

                        <th width="5%" class="text-center middle">{{viewLanguage('FLIGHT_ID')}}</th>
                        <th width="5%" class="text-center middle">{{viewLanguage('FLI_NO')}}</th>

                        <th width="7%" class="text-center middle">{{viewLanguage('DEPARTURE')}}</th>
                        <th width="7%" class="text-center middle">{{viewLanguage('ARRIVAL')}}</th>
                        <th width="7%" class="text-center middle">{{viewLanguage('DELAY_TIME')}}</th>

                        <th width="12%" class="text-center middle">{{viewLanguage('PACK NAME')}}</th>
                        <th width="7%" class="text-right middle">{{viewLanguage('PREMIUM AMOUNT')}}</th>

                        <th width="7%" class="text-center middle">{{viewLanguage('URL')}}</th>
                        {{--<th width="7%" class="text-center middle">{{viewLanguage('Phân loại nhóm NĐBH')}}</th>--}}
                    </tr>
                </thead>
                <tbody>
                @if(isset($dataTotal) && !empty($dataTotal))
                    <tr class="font-bold red">
                        <td class="text-right middle" colspan="9">Tổng</td>
                        <td class="text-right middle">@if(isset($dataTotal->TOTAL_PREMIUM_AMOUNT))<b>{{numberFormat($dataTotal->TOTAL_PREMIUM_AMOUNT)}}@endif</b></td>
                        <td class="text-right middle"></td>
                    </tr>
                @endif
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-left middle">@if(isset($item->BOOKING_ID)){{$item->BOOKING_ID}}@endif</td>
                        <td class="text-left middle">
                            @if(isset($item->POLICY_NUMBER)){{$item->POLICY_NUMBER}}@endif
                            {{--@if(isset($item->CREATE_ORDER))<br>{{convertDateDMY_HI($item->CREATE_ORDER)}}@endif--}}
                        </td>
                        <td class="text-left middle">@if(isset($item->INSURED_FULL_NAME)){{$item->INSURED_FULL_NAME}}@endif</td>

                        <td class="text-center middle">@if(isset($item->FLIGHT_ID)){{$item->FLIGHT_ID}}@endif</td>
                        <td class="text-center middle">@if(isset($item->FLIGHT_NO)){{$item->FLIGHT_NO}}@endif</td>

                        <td class="text-center middle">@if(isset($item->DEPARTURE_STATION_CODE)){{$item->DEPARTURE_STATION_CODE}}@endif</td>
                        <td class="text-center middle">@if(isset($item->ARRIVE_STATION_CODE)){{$item->ARRIVE_STATION_CODE}}@endif</td>
                        <td class="text-center middle">@if(isset($item->DELAY_TIME )){{$item->DELAY_TIME}}@endif</td>

                        <td class="text-left middle">
                            @if(isset($item->PACK_CODE))
                                [{{$item->PACK_CODE}}]
                                @if($product_code == PRODUCT_CODE_SKY_CARE)
                                    @if(isset($arrPackCode[$item->PACK_CODE]))
                                        {{$arrPackCode[$item->PACK_CODE]}}
                                    @endif
                                @endif
                                @if($product_code == PRODUCT_CODE_SKY_CARE_DOMESTIC)
                                    @if(isset($arrPackCode2[$item->PACK_CODE]))
                                        {{$arrPackCode2[$item->PACK_CODE]}}
                                    @endif
                                @endif
                            @endif
                        </td>
                        <td class="text-right middle">@if(isset($item->PREMIUM_AMOUNT))<b class="red">{{numberFormat($item->PREMIUM_AMOUNT)}}</b>@endif</td>

                        <td class="text-center middle"> <a href="@if(isset($item->LINK_CERT)){{$item->LINK_CERT}}@endif" target="_blank">Xem GCN</a></td>
                        {{--<td class="text-left middle">Chưa có</td>--}}

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
