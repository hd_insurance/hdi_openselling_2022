@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.userSystem.component.user.formSearch')

    {{--list data---}}
    @include('Systems.userSystem.component.user.listData')
@stop
