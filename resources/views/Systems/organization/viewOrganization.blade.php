@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.organization.component.organization.formSearch')

    {{--list data---}}
    @include('Systems.organization.component.organization.listData')
@stop
