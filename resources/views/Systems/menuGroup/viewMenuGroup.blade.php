@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {!!Form::open(array('method' => 'POST', 'role'=>'form')) !!}
    {{--Search---}}
    @include('Systems.menuGroup.component.formSearch')

    {{--list data---}}
    @include('Systems.menuGroup.component.listData')
    {!! Form::close() !!}
@stop
