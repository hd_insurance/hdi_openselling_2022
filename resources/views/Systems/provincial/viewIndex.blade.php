@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.provincial.component.formSearch')

    {{--list data---}}
    @include('Systems.provincial.component.listData')
@stop
