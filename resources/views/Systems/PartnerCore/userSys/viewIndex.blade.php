@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.PartnerCore.userSys.component.formSearch')
    {{--list data---}}
    @include('Systems.PartnerCore.userSys.component.listData')
@stop
