@include('Systems.PartnerCore.partnerSys.component.groupTabPage')
{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-3">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class=" col-lg-2">
                <label for="user_group">Trạng thái</label>
                <select class="form-control input-sm chosen-select w-100" name="ISACTIVE" id="ISACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
            <div class="col-lg-3 marginT20">
                <button class="btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                <a href="javascript:void(0);" class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                    Thêm mới
                </a>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                    <th width="30%" class="text-left">{{viewLanguage('USER_CODE')}}</th>
                    <th width="30%" class="text-left">{{viewLanguage('PARTNER_CODE')}}</th>

                    <th width="15%" class="text-center">{{viewLanguage('ENVIROMENT_CODE')}}</th>
                    <th width="15%" class="text-center">{{viewLanguage('STATUS')}}</th>
                    <th width="18%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-left middle">
                            @if(trim($item->USER_CODE) != ''){{$item->USER_CODE}}@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->PARTNER_CODE) != ''){{$item->PARTNER_CODE}}@endif
                        </td>
                        <td class="text-center middle">
                            @if(trim($item->ENVIROMENT_CODE) != ''){{$item->ENVIROMENT_CODE}}@endif
                        </td>

                        <td class="text-center middle">
                            @if($item->ISACTIVE == STATUS_INT_MOT)
                                <a href="javascript:void(0);" style="color: green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                            @else
                                <a href="javascript:void(0);" style="color: red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                            @endif
                        </td>
                        <td class="text-center middle">
                            @if($is_root || $permission_edit || $permission_add)
                                <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailItem" data-input="{{json_encode(['item'=>$item])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->USER_CODE}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="1">
                                    <i class="fa fa-pencil-square-o fa-2x"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
