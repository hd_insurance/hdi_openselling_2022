{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin User Partner: &nbsp;<span class="showInforItem" data-field="PACK_NAME"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-4">
                    USER_CODE: <b class="showInforItem" data-field="USER_CODE"></b>
                </div>
                <div class="col-lg-4">
                    PARTNER_CODE: <b class="showInforItem" data-field="PARTNER_CODE"></b>
                </div>
                <div class="col-lg-4">
                    ENVIROMENT_CODE: <b class="showInforItem" data-field="ENVIROMENT_CODE"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-4">
                    ISACTIVE: <b class="showInforItem">@if(isset($dataPrimary->ISACTIVE) && isset($arrStatus[$dataPrimary->ISACTIVE])) {{$arrStatus[$dataPrimary->ISACTIVE]}}@endif</b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        @if($objectId > 0)
            Thông tin User Partner - &nbsp;<span class="showInforItem" data-field="USER_CODE"></span>
        @else
            Thông tin User Partner
        @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" @if(isset($dataPrimary->GID))value="1" @else value="0" @endif>
        <input type="hidden" name="GID" id="form_{{$formName}}_GID" value="@if(isset($dataPrimary->GID)){{$dataPrimary->GID}}@endif">
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-lg-6">
                <label for="NAME" class="text-right control-label">{{viewLanguage('USER_CODE')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="USER_CODE" id="form_{{$formName}}_USER_CODE" @if($objectId > 0) readonly @endif>
            </div>
            <div class="col-lg-6">
                <label for="NAME" class="text-right control-label">{{viewLanguage('ENVIROMENT_CODE')}} </label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="ENVIROMENT_CODE" id="form_{{$formName}}_ENVIROMENT_CODE">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-6">
                <label for="NAME" class="text-right">{{viewLanguage('PARTNER_CODE')}}</label><span class="red">(*)</span>
                <select class="form-control input-sm" required name="PARTNER_CODE" id="form_{{$formName}}_PARTNER_CODE" >
                    {!! $optionPartnerConfig !!}
                </select>
            </div>
            <div class="col-lg-6">
                <label for="NAME" class="text-right">{{viewLanguage('ISACTIVE')}}</label><span class="red">(*)</span>
                <select class="form-control input-sm" required name="ISACTIVE" id="form_{{$formName}}_ISACTIVE" >
                    {!! $optionStatus !!}
                </select>
            </div>
        </div>
   </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
