<div style="position: relative">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>

    <div id="divDetailItem">
        <div class="card-header">
            @if($objectId > 0)Cập nhật thông tin User Partner @else Thêm thông tin User Partner @endif
            <div class="btn-actions-pane-right">
                @include('Layouts.BaseAdmin.listButtonActionFormEdit')
            </div>
        </div>

        <div class="div-infor-right">
            <div class="main-card mb-3">
                <div class="card-body paddingTop-unset">
                    <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column" style="padding-top: 0px!important;">

                        {{---Block 1---}}
                        <form id="form_{{$formName}}">
                            <div class="vertical-timeline-item vertical-timeline-element marginBottom-unset">
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline timeline-active">1</span>
                                <div class="vertical-timeline-element-content bounce-in" id="formShowEditSuccess">
                                    @include('Systems.PartnerCore.userSys.component._detailFormItem')
                                </div>
                            </div>
                        </form>

                        {{---Block 2---}}
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div>
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">2</span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <div class="card-header">
                                        <?php $totalPartnerUser = count($listPartnerUser);?>
                                        Gán User cho đối tác @if($totalPartnerUser > 0)({{$totalPartnerUser}})@endif
                                        @if(isset($dataPrimary->PARTNER_CODE))
                                            @if($is_root || $permission_edit || $permission_add)
                                                <div class="marginL50">
                                                    <a href="javascript:void(0);" class="red detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>0,'type'=>$tabOtherItem1,'itemInfor'=>[],'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Thêm đối tác: ').$dataPrimary->PARTNER_CODE}}" data-method="post" data-objectId="{{$dataPrimary->PARTNER_CODE}}">
                                                        <i class="pe-7s-plus"></i> {{viewLanguage('Add')}}
                                                    </a>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                    @if($objectId <= 0 || $totalPartnerUser == 0)
                                        <div class="marginT15">
                                            @if($objectId <= 0)
                                                Bạn cần thêm thông tin gói trước khi nhập thông tin liên quan
                                            @else
                                                Chưa có dữ liệu.
                                            @endif
                                        </div>
                                    @else
                                        <div class="listTabWithAjax">
                                            <div class="tab-content marginT10" id="{{$tabOtherItem1}}">
                                                @include('Systems.PartnerCore.userSys.component._listPartnerAssign')
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        //chi tiết banks
        $('.detailOtherCommon').dblclick(function () {
            jqueryCommon.ajaxGetData(this);
        });
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
