@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.PartnerCore.partnerSys.component.formSearch')

    {{--list data---}}
    @include('Systems.PartnerCore.partnerSys.component.listData')
@stop
