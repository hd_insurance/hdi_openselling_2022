<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('PARTNER_CODE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('APIGROUP_CODE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('ISDELETE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('ISUNDODELETE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('STATUS')}}</th>
            <th width="15%" class="text-center"></th>
        </tr>
        </thead>
        <tbody>
        @if(isset($listPartnerGroupApi) && $listPartnerGroupApi)
            @foreach ($listPartnerGroupApi as $kb => $itemPack)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>1,'type'=>$tabOtherItem3,'itemInfor'=>$itemPack,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem3}}" title="{{viewLanguage('Sửa group api: ').$itemPack->PARTNER_CODE}}" data-method="post" data-objectId="{{$dataPrimary->PARTNER_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>

                    <td class="text-center middle">{{$itemPack->PARTNER_CODE}}</td>
                    <td class="text-center middle">{{$itemPack->APIGROUP_CODE}}</td>
                    <td class="text-center middle">
                        @if($itemPack->ISDELETE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-center middle">
                        @if($itemPack->ISUNDODELETE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-center middle">
                        @if($itemPack->ISACTIVE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-left middle">
                        <span class="font_10">
                            @if(trim($itemPack->CREATEBY) != ''){{$itemPack->CREATEBY}}@endif
                            @if(trim($itemPack->CREATEDATE) != '') - {{convertDateDMY($itemPack->CREATEDATE)}} <br/>@endif
                        </span>
                        <span class="font_10 red">
                            @if(trim($itemPack->MODIFIEDBY) != ''){{$itemPack->MODIFIEDBY}}@endif
                            @if(trim($itemPack->MODIFIEDDATE) != '') - {{convertDateDMY($itemPack->MODIFIEDDATE)}} <br/>@endif
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
