<div class="tabs-lg-alternate card-header">
    <ul class="nav nav-justified">
        <li class="nav-item">
            <a href="{{URL::route('partnerSys.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'partnerSys.index')active @endif">
                <div class="widget-number1">Khai báo Partner Config</div>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{URL::route('userPartnerSys.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'userPartnerSys.index')active @endif">
                <div class="widget-number1">Khai báo Use partner</div>
            </a>
        </li>
    </ul>
</div>
