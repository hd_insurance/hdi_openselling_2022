<div class="modal-content" id="{{$formNameOther}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="dataPrimary" name="dataPrimary" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="PARTNER_CODE" name="PARTNER_CODE" @if(isset($dataPrimary->PARTNER_CODE))value="{{$dataPrimary->PARTNER_CODE}}"@endif>

        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('USER_CODE')}}</label> <span class="red"> (*)</span>
                    <select  class="form-control input-sm chosen-select w-100" required name="USER_CODE" id="form_{{$formNameOther}}_USER_CODE">
                        {!! $optionUserPartner !!}}
                    </select>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('ENVIROMENT_CODE')}}</label> <span class="red"> (*)</span>
                    <select  class="form-control input-sm" required name="ENVIROMENT_CODE" id="form_{{$formNameOther}}_ENVIROMENT_CODE">
                        {!! $optionEnvCode !!}}
                    </select>

                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('ISACTIVE')}}</label><span class="red"> (*)</span>
                    <select  class="form-control input-sm" required name="ISACTIVE" id="form_{{$formNameOther}}_ISACTIVE">
                        {!! $optionStatus !!}}
                    </select>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
