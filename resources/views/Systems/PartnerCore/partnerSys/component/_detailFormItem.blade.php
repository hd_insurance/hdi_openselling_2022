{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin Partner Config: &nbsp;<span class="showInforItem" data-field="PARTNER_NAME"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-3">
                    PARTNER_CODE: <b class="showInforItem" data-field="PARTNER_CODE"></b>
                </div>
                <div class="col-lg-3">
                    PARTNER_NAME: <b class="showInforItem" data-field="PARTNER_NAME"></b>
                </div>
                <div class="col-lg-4">
                    CLIENT_ID: <b class="showInforItem" data-field="CLIENT_ID"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    EFFECTIVEDATE: <b class="showInforItem" data-field="EFFECTIVEDATE"></b>
                </div>
                <div class="col-lg-3">
                    EFFECTIVENUM: <b class="showInforItem" data-field="EFFECTIVENUM"></b>
                </div>
                <div class="col-lg-3">
                    EXPIRATIONDATE: <b class="showInforItem" data-field="EXPIRATIONDATE"></b>
                </div>
                <div class="col-lg-3">
                    EXPIRATIONNUM: <b class="showInforItem" data-field="EXPIRATIONNUM"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    ISTOKEN: <b class="showInforItem">@if(isset($dataPrimary->ISTOKEN) && isset($arrTrueFalse[$dataPrimary->ISTOKEN]) ) {{$arrTrueFalse[$dataPrimary->ISTOKEN]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    ISWHITELIST: <b class="showInforItem" >@if(isset($dataPrimary->ISWHITELIST) && isset($arrTrueFalse[$dataPrimary->ISWHITELIST])) {{$arrTrueFalse[$dataPrimary->ISWHITELIST]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    ISSIGNATURE_REQUEST: <b class="showInforItem">@if(isset($dataPrimary->ISSIGNATURE_REQUEST) && isset($arrTrueFalse[$dataPrimary->ISSIGNATURE_REQUEST])) {{$arrTrueFalse[$dataPrimary->ISSIGNATURE_REQUEST]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    ISSIGNATURE_RESPONSE: <b class="showInforItem">@if(isset($dataPrimary->ISSIGNATURE_RESPONSE) && isset($arrTrueFalse[$dataPrimary->ISSIGNATURE_RESPONSE]) ) {{$arrTrueFalse[$dataPrimary->ISSIGNATURE_RESPONSE]}}@endif</b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    ISLIMIT: <b class="showInforItem">@if(isset($dataPrimary->ISLIMIT) && isset($arrTrueFalse[$dataPrimary->ISLIMIT])) {{$arrTrueFalse[$dataPrimary->ISLIMIT]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    COUNTLIMIT: <b class="showInforItem">@if(isset($dataPrimary->COUNTLIMIT) && isset($arrTrueFalse[$dataPrimary->COUNTLIMIT]) ) {{$arrTrueFalse[$dataPrimary->COUNTLIMIT]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    ISALL_ACTION: <b class="showInforItem" >@if(isset($dataPrimary->ISALL_ACTION) && isset($arrTrueFalse[$dataPrimary->ISALL_ACTION])) {{$arrTrueFalse[$dataPrimary->ISALL_ACTION]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    ISACTIVE: <b class="showInforItem" >@if(isset($dataPrimary->ISACTIVE) && isset($arrStatus[$dataPrimary->ISACTIVE])) {{$arrStatus[$dataPrimary->ISACTIVE]}}@endif</b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        @if($objectId > 0)
            Thông tin Partner Config - &nbsp;<span class="showInforItem" data-field="PARTNER_NAME"></span>
        @else
            Thông tin Partner Config
        @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" @if(isset($dataPrimary->PARTNER_CODE))value="1" @else value="0" @endif>
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PARTNER_CODE')}}</label><span class="red"> (*)</span>
                <input type="text" class="form-control input-sm" maxlength="100" required name="PARTNER_CODE" id="form_{{$formName}}_PARTNER_CODE" @if($objectId > 0) readonly @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PARTNER_NAME')}} </label><span class="red"> (*)</span>
                <input type="text" class="form-control input-sm" maxlength="100" required name="PARTNER_NAME" id="form_{{$formName}}_PARTNER_NAME">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('COUNTLIMIT')}} </label>
                <input type="text" class="form-control input-sm" maxlength="100" name="COUNTLIMIT" id="form_{{$formName}}_COUNTLIMIT">
            </div>
        </div>

        <div class="row form-group">
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISTOKEN')}}</label>
                <select  class="form-control input-sm" required name="ISTOKEN" id="form_{{$formName}}_ISTOKEN">
                    {!! $optionIsToken !!}}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('IS_REFESH_TOKEN')}}</label>
                <select  class="form-control input-sm" required name="IS_REFESH_TOKEN" id="form_{{$formName}}_IS_REFESH_TOKEN">
                    {!! $optionRefeshToken !!}}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISALL_ACTION')}}</label>
                <select  class="form-control input-sm" required name="ISALL_ACTION" id="form_{{$formName}}_ISALL_ACTION">
                    {!! $optionAllAction !!}}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISACTIVE')}}</label>
                <select  class="form-control input-sm" required name="ISACTIVE" id="form_{{$formName}}_ISACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISDELETE')}}</label>
                <select  class="form-control input-sm" required name="ISDELETE" id="form_{{$formName}}_ISDELETE">
                    {!! $optionIsDelete !!}}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISLIMIT')}}</label>
                <select  class="form-control input-sm" required name="ISLIMIT" id="form_{{$formName}}_ISLIMIT">
                    {!! $optionIsLimit !!}}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISSIGNATURE_REQUEST')}}</label>
                <select  class="form-control input-sm" required name="ISSIGNATURE_REQUEST" id="form_{{$formName}}_ISSIGNATURE_REQUEST">
                    {!! $optionIsRequest !!}}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('ISSIGNATURE_RESPONSE')}}</label>
                <select  class="form-control input-sm" required name="ISSIGNATURE_RESPONSE" id="form_{{$formName}}_ISSIGNATURE_RESPONSE">
                    {!! $optionIsResponse !!}}
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('EFFECTIVEDATE')}} </label>
                    <input type="text" class="form-control input-sm" name="EFFECTIVEDATE" id="form_{{$formName}}_EFFECTIVEDATE">
                </div>
                @if(isset($dataPrimary->PARTNER_CODE))
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('EFFECTIVENUM')}} </label>
                    <input type="text" class="form-control input-sm" name="EFFECTIVENUM" id="form_{{$formName}}_EFFECTIVENUM" readonly>
                </div>
                @endif
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('EXPIRATIONDATE')}} </label>
                    <input type="text" class="form-control input-sm" name="EXPIRATIONDATE" id="form_{{$formName}}_EXPIRATIONDATE">
                </div>
                @if(isset($dataPrimary->PARTNER_CODE))
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('EXPIRATIONNUM')}} </label>
                    <input type="text" class="form-control input-sm" maxlength="100" name="EXPIRATIONNUM" id="form_{{$formName}}_EXPIRATIONNUM" readonly>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
