<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="3%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('PARTNER_CODE')}}</th>
            <th width="20%" class="text-center">{{viewLanguage('SECRET')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('ENVIROMENT')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('APPROVED')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('ACTIVE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('DELETE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('UNDO DELETE')}}</th>
            <th width="15%" class="text-center"></th>
        </tr>

        </thead>
        <tbody>
        @if(isset($listPartnerSecret) && !empty($listPartnerSecret))
            @foreach ($listPartnerSecret as $kb => $itemPartnerSecret)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>1,'type'=>$tabOtherItem1,'itemInfor'=>$itemPartnerSecret,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Chi tiết: ').$itemPartnerSecret->SECRET}}" data-method="post" data-objectId="{{$dataPrimary->PARTNER_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>
                    <td class="text-center middle">{{$itemPartnerSecret->PARTNER_CODE}}</td>
                    <td class="text-left middle">{{$itemPartnerSecret->SECRET}}</td>
                    <td class="text-center middle">{{$itemPartnerSecret->ENVIROMENT_CODE}}</td>
                    <td class="text-center middle">
                        @if($itemPartnerSecret->ISAPPROVED == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-center middle">
                        @if($itemPartnerSecret->ISACTIVE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-center middle">
                        @if($itemPartnerSecret->ISDELETE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-center middle">
                        @if($itemPartnerSecret->ISUNDODELETE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-left middle">
                        <span class="font_10">
                            @if(trim($itemPartnerSecret->CREATEBY) != ''){{$itemPartnerSecret->CREATEBY}}@endif
                            @if(trim($itemPartnerSecret->CREATEDATE) != '') - {{convertDateDMY($itemPartnerSecret->CREATEDATE)}} <br/>@endif
                        </span>
                        <span class="font_10 red">
                            @if(trim($itemPartnerSecret->MODIFIEDBY) != ''){{$itemPartnerSecret->MODIFIEDBY}}@endif
                            @if(trim($itemPartnerSecret->MODIFIEDDATE) != '') - {{convertDateDMY($itemPartnerSecret->MODIFIEDDATE)}} <br/>@endif
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
