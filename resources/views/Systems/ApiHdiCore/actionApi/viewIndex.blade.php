@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.ApiHdiCore.actionApi.component.formSearch')

    {{--list data---}}
    @include('Systems.ApiHdiCore.actionApi.component.listData')
@stop
