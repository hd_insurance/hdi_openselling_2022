<div class="modal-content" id="popupCache" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_popupCache">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">Chi tiết Cache</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                            myDebug($dataCache,false);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    });
</script>
