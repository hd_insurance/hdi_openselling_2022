<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="8%" class="text-center">{{viewLanguage('TT')}}</th>
            <th width="20%" class="text-center">{{viewLanguage('Action code')}}</th>
            <th width="30%" class="text-center">{{viewLanguage('Partner code')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('Trạng thái')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('Ngày')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('User')}}</th>
        </tr>

        </thead>
        <tbody>
        @if(isset($listActionPartner) && !empty($listActionPartner))
            @foreach ($listActionPartner as $kb => $itemOther)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem3,'itemId'=>$itemOther->ID,'itemInfor'=>$itemOther,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem3}}" title="{{viewLanguage('View DB code: ').$itemOther->PARTNER_CODE}}" data-method="post" data-objectId="{{$dataPrimary->ACTION_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>
                    <td class="text-center middle">{{$itemOther->ACTION_CODE}}</td>
                    <td class="text-center middle">@if(isset($itemOther->PARTNER_CODE)){{$itemOther->PARTNER_CODE}}@endif</td>
                    <td class="text-center middle">
                        @if($itemOther->IS_ACTIVE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-minus fa-2x"></i></a>
                        @endif
                    </td>
                    <td class="text-center middle">
                        @if(trim($itemOther->CREATE_DATE) != ''){{convertDateDMY($itemOther->CREATE_DATE)}} <br/>@endif
                        @if(trim($itemOther->MODIFIED_DATE) != '')<span class="red">{{convertDateDMY($itemOther->MODIFIED_DATE)}}</span>@endif
                    </td>
                    <td class="text-center middle">
                        @if(trim($itemOther->CREATE_BY) != ''){{$itemOther->CREATE_BY}}<br/>@endif
                        @if(trim($itemOther->MODIFIED_BY) != '')<span class="red">{{$itemOther->MODIFIED_BY}}</span>@endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
