{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin Action code: &nbsp;<span class="showInforItem" data-field="ACTION_CODE"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-3">
                    ACTION_CODE: <b class="showInforItem" data-field="ACTION_CODE"></b>
                </div>
                <div class="col-lg-3">
                    OWNER: <b class="showInforItem" data-field="OWNER"></b>
                </div>
                <div class="col-lg-6">
                    PROC_PATH: <b class="showInforItem" data-field="PROC_PATH"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    PROC_IN: <b class="showInforItem" data-field="PROC_IN"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    PROC_OUT: <b class="showInforItem" data-field="">@if(isset($dataPrimary->PROC_OUT)){{str_replace('","',", ",$dataPrimary->PROC_OUT)}}@endif</b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    DESCRIPTION: <b class="showInforItem" data-field="DESCRIPTION"></b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        @if($objectId > 0)
            Thông tin&nbsp;<span class="showInforItem" data-field="ACTION_CODE"></span>
        @else
            Thông tin Action code
        @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" value="@if(isset($dataPrimary->ID))1 @else 0 @endif">
        <input type="hidden" name="ID" @if(isset($dataPrimary->ID))value="{{$dataPrimary->ID}}" @else value="0" @endif>
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="form-group">
            <div class="row">
                @if($objectId > 0)
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Action Code')}}</label>
                    <input type="text" class="form-control input-sm" maxlength="100" name="ACTION_CODE" id="form_{{$formName}}_ACTION_CODE" @if($objectId > 0) readonly @endif>
                </div>
                @else
                    <input type="hidden" class="form-control input-sm" maxlength="100" name="ACTION_CODE" id="form_{{$formName}}_ACTION_CODE">
                @endif
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('OWNER')}} </label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="100" required name="OWNER" id="form_{{$formName}}_OWNER">
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PROC_PATH')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required name="PROC_PATH" id="form_{{$formName}}_PROC_PATH">
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-10">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Description')}} </label><span class="red"> (*)</span>
                <input type="text" class="form-control input-sm" maxlength="350" required name="DESCRIPTION" @if($objectId > 0) id="form_{{$formName}}_DESCRIPTION" @else value="Mô tả Api" @endif>
            </div>
            <div class="col-lg-2">
                <label for="status" class="control-label">{{viewLanguage('Trạng thái')}}</label> <span class="red"> (*)</span>
                <select  class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formName}}_IS_ACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PROC_IN')}} </label><span class="red"> @if(count((array)$arrProcIn) > 0)(<b>{{count((array)$arrProcIn)}} param</b>) @endif</span>
                <div style="height: 200px; overflow: hidden; overflow-y: scroll">
                    <table class="table table-bordered table-hover">
                        <thead class="thin-border-bottom">
                        <tr class="table-background-header">
                            <th width="10%" class="text-center">{{viewLanguage('STT')}}</th>
                            <th width="50%" class="text-left">{{viewLanguage('Key input')}}</th>
                            <th width="40%" class="text-left">{{viewLanguage('Kiểu input')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $key = 1;?>
                            @if(!empty($arrProcIn))
                                @foreach ($arrProcIn as $keyIN => $valuIN)
                                <tr>
                                    <td class="text-center middle">{{$key}}</td>
                                    <td class="text-center middle">
                                        <input type="text" class="form-control input-sm" id="KEY_IN_{{$key}}" name="KEY_IN_{{$key}}" value="{{$keyIN}}">
                                    </td>
                                    <td class="text-center middle">
                                        <select  class="form-control input-sm" name="VALUE_IN_{{$key}}" id="VALUE_IN_{{$key}}">
                                            @foreach ($arrTypeData as $keydb => $valudb)
                                                <option value="{{$keydb}}" @if($valuIN === $keydb) selected @endif>{{$valuIN}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <?php $key++;?>
                                @endforeach
                            @endif
                            @foreach ($arrTypeData as $keydb => $valudb)
                                <tr id="TR_KEY_IN_1">
                                    <td class="text-center middle">{{$key}}</td>
                                    <td class="text-center middle">
                                        <input type="text" class="form-control input-sm" id="KEY_IN_{{$key}}" name="KEY_IN_{{$key}}" value="">
                                    </td>
                                    <td class="text-center middle">
                                        <select  class="form-control input-sm" name="VALUE_IN_{{$key}}" id="VALUE_IN_{{$key}}">
                                            @foreach ($arrTypeData as $keydb => $valudb)
                                                <option value="{{$keydb}}" >{{$valudb}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <?php $key++;?>
                            @endforeach
                        </tbody>
                        <input type="hidden" id="total_key_in" name="total_key_in" value="1">
                    </table>
                </div>
            </div>

            <div class="col-lg-6">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PROC_OUT')}} </label><span class="red"> @if(count((array)$arrProcOut) > 0)(<b>{{count((array)$arrProcOut)}} param</b>) @endif</span>
                <div style="height: 200px; overflow: hidden; overflow-y: scroll">
                    <table class="table table-bordered table-hover">
                        <thead class="thin-border-bottom">
                        <tr class="table-background-header">
                            <th width="10%" class="text-center">{{viewLanguage('STT')}}</th>
                            <th width="50%" class="text-left">{{viewLanguage('Key input')}}</th>
                            <th width="40%" class="text-left">{{viewLanguage('Kiểu input')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $key = 1;?>
                        @if(!empty($arrProcOut))
                            @foreach ($arrProcOut as $keyOUT => $valuOUT)
                                <tr>
                                    <td class="text-center middle">{{$key}}</td>
                                    <td class="text-center middle">
                                        <input type="text" class="form-control input-sm" id="KEY_OUT_{{$key}}" name="KEY_OUT_{{$key}}" value="{{$keyOUT}}">
                                    </td>
                                    <td class="text-center middle">
                                        <select  class="form-control input-sm" name="VALUE_OUT_{{$key}}" id="VALUE_OUT_{{$key}}">
                                            @foreach ($arrTypeData as $keydb => $valudb)
                                                <option value="{{$keydb}}" @if($keydb == $valuOUT) selected @endif>{{$valudb}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <?php $key++;?>
                            @endforeach
                        @endif
                        @foreach ($arrTypeData as $keydb => $valudb)
                            <tr id="TR_KEY_OUT_1">
                                <td class="text-center middle">{{$key}}</td>
                                <td class="text-center middle">
                                    <input type="text" class="form-control input-sm" id="KEY_OUT_{{$key}}" name="KEY_OUT_{{$key}}" value="">
                                </td>
                                <td class="text-center middle">
                                    <select  class="form-control input-sm" name="VALUE_OUT_{{$key}}" id="VALUE_OUT_{{$key}}">
                                        @foreach ($arrTypeData as $keydb => $valudb)
                                            <option value="{{$keydb}}" >{{$valudb}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <?php $key++;?>
                        @endforeach
                        </tbody>
                        <input type="hidden" id="total_key_in" name="total_key_in" value="1">
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    function ajaxAddParam(url,key,total_key,divShow){
        var p_key = $('#'+total_key).val();
        var _token = $('input[name="_token"]').val();
        $('#loaderRight').show();
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                '_token': _token,
                'key': p_key,
                'nameKey': key,
                'divShow': divShow,
                'functionAction': '_ajaxAddParam'
            },
            success: function (res) {
                $('#loaderRight').hide();
                if (res.success == 1) {
                    $('#'+divShow).append(res.html);
                } else {
                    jqueryCommon.showMsgError(res.success, res.message);
                }
            }
        });
    }
</script>
