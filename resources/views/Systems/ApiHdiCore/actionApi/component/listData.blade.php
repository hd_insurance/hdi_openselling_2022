
{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Search')}}</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="form-group col-lg-3">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Trạng thái')}}</label>
                <select  class="form-control input-sm" name="IS_ACTIVE" id="IS_ACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                @if($is_root || $permission_view)
                    <button class="btn-icon btn btn-primary marginT25" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                @endif
                @if($is_root || $permission_add)
                    <a href="javascript:void(0);" class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                        Thêm mới
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

<div class="main-card mb-3 card">
    <div class="card-body">
        @if($data && sizeof($data) > 0)
            <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                        <th width="6%" class="text-center">{{viewLanguage('TT')}}</th>
                        <th width="8%" class="text-center">{{viewLanguage('Action code')}}</th>
                        <th width="70%" class="text-center">{{viewLanguage('ProcIn/ProcOut')}}</th>
                        <th width="14%" class="text-center">{{viewLanguage('Description')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $item)
                        <tr>
                            <td class="text-center middle">{{$stt+$key+1}}</td>
                            <td class="text-center middle">
                                @if($is_root || $permission_view || $permission_add)
                                    <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailApi" data-input="{{json_encode(['item'=>$item])}}" data-show="2" data-loading="1" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->ACTION_CODE}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="{{$item->ACTION_CODE}}">
                                        <i class="pe-7s-look fa-2x"></i>
                                    </a> <br/>
                                @endif
                                @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                    <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                                @else
                                    <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-minus fa-2x"></i></a>
                                @endif <br/>
                                @if($is_root)
                                   <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);"data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$item, 'tableSyn'=>TABLE_OAPI_ACTION, 'keySyn'=>$item->ACTION_CODE])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                        <i class="fa fa-arrow-circle-up fa-2x"></i>
                                    </a>
                                @endif
                            </td>
                            <td class="text-center middle">
                                {{$item->ACTION_CODE}}
                                @if($is_root)
                                    <br/><br/>
                                    <a href="javascript:void(0);" style="color: #966c05" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxClearCacheActionCode" data-input="{{json_encode(['action_code'=>$item->ACTION_CODE,'env_run'=>'DEV'])}}" data-show="1" data-show-id="" title="{{viewLanguage('Xóa cache trên DEV: ').$item->ACTION_CODE}}" data-method="post" data-objectId="{{$item->ACTION_CODE}}">
                                        <i class="fa fa-refresh fa-2x"></i>
                                    </a>
                                    <a href="javascript:void(0);"  onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxClearCacheActionCode" data-input="{{json_encode(['action_code'=>$item->ACTION_CODE,'env_run'=>'UAT'])}}" data-show="1" data-show-id="" title="{{viewLanguage('Xóa cache trên UAT: ').$item->ACTION_CODE}}" data-method="post" data-objectId="{{$item->ACTION_CODE}}">
                                        <i class="fa fa-refresh fa-2x"></i>
                                    </a>
                                    <a href="javascript:void(0);" style="color: red" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxClearCacheActionCode" data-input="{{json_encode(['action_code'=>$item->ACTION_CODE,'env_run'=>'LIVE'])}}" data-show="1" data-show-id="" title="{{viewLanguage('Xóa cache trên LIVE: ').$item->ACTION_CODE}}" data-method="post" data-objectId="{{$item->ACTION_CODE}}">
                                        <i class="fa fa-refresh fa-2x"></i>
                                    </a>
                                @endif

                            </td>
                            <td class="text-left middle">
                                OWNER: <b>{{$item->OWNER}}</b><br/>
                                PROC_PATH: <b>{{$item->PROC_PATH}}</b><br/>
                                IN: {{$item->PROC_IN}}<br/>
                                OUT: @if(isset($item->PROC_OUT)){{str_replace('","',", ",$item->PROC_OUT)}}@endif
                            </td>

                            <td class="text-left middle">
                                {{$item->DESCRIPTION}}<br/>
                                <span class="font_10">
                                    @if(trim($item->CREATE_DATE) != ''){{convertDateDMY($item->CREATE_DATE)}}@endif @if(trim($item->CREATE_BY) != '')-{{$item->CREATE_BY}}@endif
                                    @if(trim($item->MODIFIED_DATE) != '')<br/><span class="red">{{convertDateDMY($item->MODIFIED_DATE)}}</span>@endif @if(trim($item->MODIFIED_BY) != '')<span class="red"> - {{$item->MODIFIED_BY}}</span>@endif
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="paging_simple_numbers">
                {!! $paging !!}
            </div>
        @else
            <div class="alert">
                Không có dữ liệu
            </div>
        @endif
    </div>
</div>
