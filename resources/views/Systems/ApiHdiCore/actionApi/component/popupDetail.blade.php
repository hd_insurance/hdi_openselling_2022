<div style="position: relative">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>

    <div id="divDetailItem">
        <div class="card-header">
            @if($objectId > 0)Cập nhật thông tin Api @else Thêm thông tin Api @endif
            <div class="btn-actions-pane-right">
                @include('Layouts.BaseAdmin.listButtonActionFormEdit')
            </div>
        </div>

        <div class="div-infor-right">
            <div class="main-card mb-3">
                <div class="card-body paddingTop-unset">
                    <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column" style="padding-top: 0px!important;">

                        {{---Block 1---}}
                        <form id="form_{{$formName}}">
                            <div class="vertical-timeline-item vertical-timeline-element marginBottom-unset">
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline timeline-active">1</span>
                                <div class="vertical-timeline-element-content bounce-in" id="formShowEditSuccess">
                                    @include('Systems.ApiHdiCore.actionApi.component._detailFormItem')
                                </div>
                            </div>
                        </form>

                        {{---$tabOtherItem1---}}
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div>
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">2</span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <div class="card-header">
                                        <div class="card-header">Thông tin ACTION ENV</div>
                                        @if(($is_root || $permission_edit || $permission_add) && ($objectId > 0))
                                            <div class="marginL50">
                                                <a href="javascript:void(0);" class="red detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem1,'itemId'=>'','isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Thêm Action Env: ').$dataPrimary->ACTION_CODE}}" data-method="post" data-objectId="{{$dataPrimary->ACTION_CODE}}">
                                                    <i class="pe-7s-plus"></i> {{viewLanguage('Add')}}
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    @if($objectId <= 0)
                                        <div class="marginT15">
                                            Bạn cần thêm thông tin Action code trước khi nhập thông tin liên quan
                                        </div>
                                    @else
                                        <div class="listTabWithAjax">
                                            <div class="tab-content marginT10" id="{{$tabOtherItem1}}">
                                                @include('Systems.ApiHdiCore.actionApi.component._listActionEnv')
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{---$tabOtherItem3---}}
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div>
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">3</span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <div class="card-header">
                                        <div class="card-header">Thông tin ACTION PARTNER</div>
                                        @if(($is_root || $permission_edit || $permission_add) && ($objectId > 0))
                                            <div class="marginL50">
                                                <a href="javascript:void(0);" class="red detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem3,'itemId'=>'','isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem3}}" title="{{viewLanguage('Thêm Action Partner: ').$dataPrimary->ACTION_CODE}}" data-method="post" data-objectId="{{$dataPrimary->ACTION_CODE}}">
                                                    <i class="pe-7s-plus"></i> {{viewLanguage('Add')}}
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    @if($objectId <= 0)
                                        <div class="marginT15">
                                            Bạn cần thêm thông tin Action code trước khi nhập thông tin liên quan
                                        </div>
                                    @else
                                        <div class="listTabWithAjax">
                                            <div class="tab-content marginT10" id="{{$tabOtherItem3}}">
                                                @include('Systems.ApiHdiCore.actionApi.component._listActionPartner')
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{---$tabOtherItem2---}}
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div>
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">4</span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <div class="card-header">
                                        <div class="card-header">Thông tin ACTION PUBLIC</div>
                                        @if(($is_root || $permission_edit || $permission_add) && ($objectId > 0))
                                            <div class="marginL50">
                                                <a href="javascript:void(0);" class="red detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem4,'itemId'=>'','isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem4}}" title="{{viewLanguage('Thêm Action System: ').$dataPrimary->ACTION_CODE}}" data-method="post" data-objectId="{{$dataPrimary->ACTION_CODE}}">
                                                    <i class="pe-7s-plus"></i> {{viewLanguage('Add')}}
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    @if($objectId <= 0)
                                        <div class="marginT15">
                                            Bạn cần thêm thông tin Action code trước khi nhập thông tin liên quan
                                        </div>
                                    @else
                                        <div class="listTabWithAjax">
                                            <div class="tab-content marginT10" id="{{$tabOtherItem2}}">
                                                {{--Api Databases---}}
                                                @include('Systems.ApiHdiCore.actionApi.component._listActionPublic')
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{---$tabOtherItem2---}}
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div>
                                <span class="vertical-timeline-element-icon bounce-in icon-timeline @if($objectId > 0) timeline-active @endif">4</span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <div class="card-header">
                                        <div class="card-header">Thông tin ACTION SYSTEM</div>
                                        @if(($is_root || $permission_edit || $permission_add) && ($objectId > 0))
                                            <div class="marginL50">
                                                <a href="javascript:void(0);" class="red detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem2,'itemId'=>'','isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem2}}" title="{{viewLanguage('Thêm Action System: ').$dataPrimary->ACTION_CODE}}" data-method="post" data-objectId="{{$dataPrimary->ACTION_CODE}}">
                                                    <i class="pe-7s-plus"></i> {{viewLanguage('Add')}}
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    @if($objectId <= 0)
                                        <div class="marginT15">
                                            Bạn cần thêm thông tin Action code trước khi nhập thông tin liên quan
                                        </div>
                                    @else
                                        <div class="listTabWithAjax">
                                            <div class="tab-content marginT10" id="{{$tabOtherItem2}}">
                                                {{--Api Databases---}}
                                                @include('Systems.ApiHdiCore.actionApi.component._listActionSystem')
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});

        //chi tiết banks
        $('.detailOtherCommon').dblclick(function () {
            jqueryCommon.ajaxGetData(this);
        });
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
