<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Tìm kiếm')}}</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-6">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_search" name="p_search" autocomplete="off" @if(isset($search['p_search']))value="{{$search['p_search']}}"@endif>
            </div>
            <div class=" col-lg-3">
                <label for="user_group">Status</label>
                <select class="form-control input-sm chosen-select w-100" name="IS_ACTIVE" id="IS_ACTIVE">
                    {!! $optionStatus !!}
                </select>
            </div>
            <div class="col-lg-3 marginT30">
                @if($is_root || $permission_view)
                    <button class="btn-icon btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                @endif
                @if($is_root || $permission_edit || $permission_add)
                    <a href="javascript:void(0);"class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                        Thêm mới
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>

                    <th width="15%" class="text-center">{{viewLanguage('DATABASE_CODE')}}</th>
                    <th width="10%" class="text-center">{{viewLanguage('ENV_CODE')}}</th>
                    <th width="15%" class="text-center">{{viewLanguage('DESCRIPTION')}}</th>
                    <th width="10%" class="text-center">{{viewLanguage('User TT')}}</th>

                    <th width="10%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>

                        <td class="text-center middle">
                            <a href="{{URL::route('databaseApi.index',array('p_search' => $item->DATABASE_CODE))}}" title="tìm nhanh theo {{$item->DATABASE_CODE}}">
                                {{$item->DATABASE_CODE}}
                            </a>&nbsp;
                        </td>
                        <td class="text-center middle">{{$item->ENV_CODE}}</td>
                        <td class="text-left middle">{{$item->DESCRIPTION}}</td>

                        <td class="text-center middle">
                            <span class="font_10">
                                @if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                            </span>
                            <span class="font_10 red">
                                @if(trim($item->MODIFIED_BY) != ''){{$item->MODIFIED_BY}}@endif
                                @if(trim($item->MODIFIED_DATE) != '') - {{convertDateDMY($item->MODIFIED_DATE)}} <br/>@endif
                            </span>
                        </td>

                        <td class="text-center middle">
                            @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                <a href="javascript:void(0);" style="color: green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                            @else
                                <a href="javascript:void(0);" style="color: red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                            @endif
                            @if($is_root || $permission_edit || $permission_add)
                            &nbsp;
                            <a href="javascript:void(0);"class="color_hdi sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Cập nhật Database: ').$item->DATABASE_CODE}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item])}}" data-objectId="{{$item->ID}}">
                                <i class="fa fa-pencil-square-o fa-2x"></i>
                            </a>
                            &nbsp;
                            <a href="javascript:void(0);"class="color_warning sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Thêm Database')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item,'is_copy'=>STATUS_INT_MOT])}}" data-objectId="{{$item->ID}}">
                                <i class="fa fa-copy fa-2x"></i>
                            </a>
                            @endif
                            @if($is_root)
                                &nbsp;&nbsp;<a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);" data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$item, 'tableSyn'=>TABLE_OAPI_DB_ENV, 'keySyn'=>$item->DATABASE_CODE])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                    <i class="fa fa-arrow-circle-up fa-2x"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
