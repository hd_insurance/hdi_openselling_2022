<div class="modal-content" id="{{$form_id}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$form_id}}">
        <input type="hidden" id="objectId" name="objectId" @if($is_copy == STATUS_INT_MOT) value="0" @else value="{{$oject_id}}"@endif>
        <input type="hidden" id="formName" name="formName" value="{{$form_id}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="form_group">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATABASE_CODE')}} <span class="red">(*)</span></label>
                            <input type="text" class="form-control input-sm" required @if($oject_id > STATUS_INT_KHONG)readonly @endif name="DATABASE_CODE" id="form_{{$form_id}}_DATABASE_CODE">
                        </div>
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right">{{viewLanguage('ENV_CODE')}} <span class="red">(*)</span></label>
                            <select class="form-control input-sm" required name="ENV_CODE" id="form_{{$form_id}}_ENV_CODE" >
                                {!! $optionEnvCode !!}
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right">{{viewLanguage('IS_ACTIVE')}} <span class="red">(*)</span></label>
                            <select class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$form_id}}_IS_ACTIVE" >
                                {!! $optionStatus !!}
                            </select>
                        </div>
                    </div>
                </div><div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('SYSTEM_CODE')}} <span class="red">(*)</span></label>
                            <textarea type="text" class="form-control input-sm" required name="SYSTEM_CODE" id="form_{{$form_id}}_SYSTEM_CODE" rows="2"></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('USER_NAME')}} <span class="red">(*)</span></label>
                            <input type="text" class="form-control input-sm" required name="USER_NAME" id="form_{{$form_id}}_USER_NAME">
                        </div>
                        <div class="col-lg-6">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('USER_PASS')}} <span class="red">(*)</span></label>
                            <input type="password" placeholder="xxxxxx" class="form-control input-sm" required name="USER_PASS" id="form_{{$form_id}}_USER_PASS">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DESCRIPTION')}}</label>
                            <input type="text" class="form-control input-sm" required  name="DESCRIPTION" id="form_{{$form_id}}_DESCRIPTION">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
            <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$form_id}}','{{$urlPostItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});
        showDataIntoForm('form_{{$form_id}}');
    });
</script>
