@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
    {{--Search---}}
    @include('Systems.ApiHdiCore.databaseApi.component.formSearch')

    {{--list data---}}
    @include('Systems.ApiHdiCore.databaseApi.component.listData')
    {{ Form::close() }}
@stop
