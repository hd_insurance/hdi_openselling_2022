@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.ApiHdiCore.partnerApi.component.formSearch')

    {{--list data---}}
    @include('Systems.ApiHdiCore.partnerApi.component.listData')
@stop
