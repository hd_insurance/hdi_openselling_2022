{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin Partner Api: &nbsp;<span class="showInforItem" data-field="PARTNER_CODE"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-3">
                    PARTNER_CODE: <b class="showInforItem" data-field="PARTNER_CODE"></b>
                </div>
                <div class="col-lg-3">
                    COMPANY_NAME: <b class="showInforItem" data-field="COMPANY_NAME"></b>
                </div>
                <div class="col-lg-3">
                    CEO_NAME: <b class="showInforItem" data-field="CEO_NAME"></b>
                </div>
                <div class="col-lg-3">
                    IS_ACTIVE: <b class="showInforItem" >@if(isset($dataPrimary->IS_ACTIVE) && isset($arrStatus[$dataPrimary->IS_ACTIVE])) {{$arrStatus[$dataPrimary->IS_ACTIVE]}}@endif</b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    EMAIL: <b class="showInforItem" data-field="EMAIL"></b>
                </div>
                <div class="col-lg-3">
                    PHONE: <b class="showInforItem" data-field="PHONE"></b>
                </div>
                <div class="col-lg-3">
                    WEBSITE: <b class="showInforItem" data-field="WEBSITE"></b>
                </div>
                <div class="col-lg-3">
                    TAX_CODE: <b class="showInforItem" data-field="TAX_CODE"></b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        Thông tin Partner Api @if($objectId > 0) - &nbsp;<span class="showInforItem" data-field="PARTNER_NAME"></span>@endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" @if(isset($dataPrimary->PARTNER_CODE))value="1" @else value="0" @endif>
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PARTNER_CODE')}}</label><span class="red"> (*)</span>
                <input type="text" class="form-control input-sm" maxlength="100" required name="PARTNER_CODE" id="form_{{$formName}}_PARTNER_CODE" @if($objectId > 0) readonly @endif>
            </div>
            <div class="col-lg-6">
                <label for="NAME" class="text-right control-label">{{viewLanguage('COMPANY_NAME')}} </label><span class="red"> (*)</span>
                <input type="text" class="form-control input-sm" maxlength="100" required name="COMPANY_NAME" id="form_{{$formName}}_COMPANY_NAME">
            </div>
            <div class="col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('IS_ACTIVE')}}</label>
                <select  class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formName}}_IS_ACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('CEO_NAME')}}</label>
                <input type="text" class="form-control input-sm" maxlength="100" name="CEO_NAME" id="form_{{$formName}}_CEO_NAME">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('EMAIL')}} </label>
                <input type="text" class="form-control input-sm" maxlength="100"  name="EMAIL" id="form_{{$formName}}_EMAIL">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PHONE')}} </label>
                <input type="text" class="form-control input-sm" maxlength="100"  name="PHONE" id="form_{{$formName}}_PHONE">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('TAX_CODE')}} </label>
                <input type="text" class="form-control input-sm" maxlength="100"  name="TAX_CODE" id="form_{{$formName}}_TAX_CODE">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('WEBSITE')}} </label>
                <input type="text" class="form-control input-sm" maxlength="100"  name="WEBSITE" id="form_{{$formName}}_WEBSITE">
            </div>
            <div class="col-lg-9">
                <label for="NAME" class="text-right control-label">{{viewLanguage('ADDRESS')}} </label>
                <input type="text" class="form-control input-sm" maxlength="100" name="ADDRESS" id="form_{{$formName}}_ADDRESS">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
