{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="form-group col-lg-3">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Status')}}</label>
                <select  class="form-control input-sm chosen-select w-100" name="ISACTIVE" id="ISACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                @if($is_root || $permission_view)
                    <button class="btn-icon btn btn-primary marginT25" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                @endif
                @if($is_root || $permission_add)
                    <a href="javascript:void(0);" class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                        Thêm mới
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

<div class="main-card mb-3 card">
    <div class="card-body">
        @if($data && sizeof($data) > 0)
            <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        {{--<th width="3%" class="text-center"><input type="checkbox" class="check" id="checkAll"></th>--}}
                        <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                        <th width="15%" class="text-left">PARTNER CODE</th>
                        <th width="20%" class="text-left">NAME</th>
                        <th width="15%" class="text-center">WEBSITE</th>
                        <th width="25%" class="text-center">ADDRESS</th>
                        <th width="10%" class="text-center">TT</th>
                        <th width="15%" class="text-center"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $item)
                        <tr>
                            <td class="text-center middle">{{$stt+$key+1}}</td>

                            <td class="text-left middle">
                                @if(trim($item->PARTNER_CODE) != ''){{$item->PARTNER_CODE}}@endif
                            </td>
                            <td class="text-left middle">
                                @if(trim($item->COMPANY_NAME) != ''){{$item->COMPANY_NAME}}@endif
                            </td>
                            <td class="text-left middle">
                                @if(trim($item->WEBSITE) != ''){{$item->WEBSITE}}@endif
                            </td>
                            <td class="text-left middle">
                                @if(trim($item->ADDRESS) != ''){{$item->ADDRESS}}@endif
                            </td>

                            <td class="text-center middle">
                                @if($is_root || $permission_view || $permission_add)
                                    <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailApi" data-input="{{json_encode(['item'=>$item])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->COMPANY_NAME}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="{{$item->ID}}">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>&nbsp;&nbsp;
                                @endif
                                @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                    <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                                @else
                                    <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                                @endif
                                @if($is_root)
                                    &nbsp;&nbsp;<a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);" data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$item, 'tableSyn'=>TABLE_OAPI_PARTNER, 'keySyn'=>$item->PARTNER_CODE])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                        <i class="fa fa-arrow-circle-up fa-2x"></i>
                                    </a>
                                @endif
                            </td>
                            <td class="text-left middle">
                                <span class="font_10">
                                    @if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                    @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                                </span>
                                <span class="font_10 red">
                                    @if(trim($item->MODIFIED_BY) != ''){{$item->MODIFIED_BY}}@endif
                                    @if(trim($item->MODIFIED_DATE) != '') - {{convertDateDMY($item->MODIFIED_DATE)}} <br/>@endif
                                </span>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="paging_simple_numbers">
                {!! $paging !!}
            </div>
        @else
            <div class="alert">
                Không có dữ liệu
            </div>
        @endif
    </div>
</div>
