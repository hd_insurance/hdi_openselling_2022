<div class="modal-content" id="{{$form_id}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$form_id}}">
        <input type="hidden" id="objectId" name="objectId" @if($is_copy == STATUS_INT_MOT) value="0" @else value="{{$objectId}}"@endif>
        <input type="hidden" id="formName" name="formName" value="{{$form_id}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TPA_CODE')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="TPA_CODE"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_TPA_CODE"@endif>
                </div>
                <div class="col-lg-8">
                    <label for="NAME" class="text-right">{{viewLanguage('PRODUCT_CODE')}}</label> <span class="red">(*)</span>
                    <select class="form-control input-sm" required name="PRODUCT_CODE" id="form_{{$form_id}}_PRODUCT_CODE" >
                        {!! $optionProductCore !!}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('Header')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="E_HEADER"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_E_HEADER"@endif>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('TO')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="E_TO"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_E_TO"@endif>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('CC')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="E_CC"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_E_CC"@endif>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('BCC')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="E_BCC"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_E_BCC"@endif>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('TEMPLATE')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="E_TEMPLATE"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_E_TEMPLATE"@endif>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
            <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$form_id}}','{{$url_action}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});
        showDataIntoForm('form_{{$form_id}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
