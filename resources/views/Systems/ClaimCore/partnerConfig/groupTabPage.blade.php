<div class="tabs-lg-alternate card-header">
    <ul class="nav nav-justified">
        <li class="nav-item">
            <a href="{{URL::route('partnerConfig.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'partnerConfig.index')active @endif">
                <div class="widget-number1">Confign bồi thường</div>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{URL::route('partnerConfigDetail.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'partnerConfigDetail.index')active @endif">
                <div class="widget-number1">Confign Detail bồi thường</div>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{URL::route('partnerEmail.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'partnerEmail.index')active @endif">
                <div class="widget-number1">Email bồi thường</div>
            </a>
        </li>
    </ul>
</div>
