@include('Systems.ClaimCore.partnerConfig.groupTabPage')
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-6">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class=" col-lg-3">
                <label for="user_group">Trạng thái</label>
                <select class="form-control input-sm chosen-select w-100" name="IS_ACTIVE" id="IS_ACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
            <div class="col-lg-3 marginT20">
                <button class="btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                <a href="javascript:void(0);" class="btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                    Thêm mới
                </a>
            </div>
        </div>
    </div>
</div>

<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                    <th width="10%" class="text-center">{{viewLanguage('TPA_CODE')}}</th>
                    <th width="25%" class="text-left">{{viewLanguage('TPA_NAME')}}</th>

                    <th width="15%" class="text-left">{{viewLanguage('TYPE_FILE')}}</th>
                    <th width="10%" class="text-left">{{viewLanguage('SEND_METHOD')}}</th>
                    <th width="8%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-center middle">
                            @if(trim($item->TPA_CODE) != '')<b>{{$item->TPA_CODE}}</b>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->TPA_NAME) != ''){{$item->TPA_NAME}}@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->TYPE_FILE) != ''){{$item->TYPE_FILE}}<br/>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->SEND_METHOD) != ''){{$item->SEND_METHOD}}<br/>@endif
                        </td>
                        <td class="text-center middle">
                            @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                <a href="javascript:void(0);" style="color: green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                            @else
                                <a href="javascript:void(0);" style="color: red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                            @endif
                            @if($is_root || $permission_edit || $permission_add)
                            &nbsp;
                            <a href="javascript:void(0);"class="color_warning sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Thêm confign')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item,'is_copy'=>STATUS_INT_MOT])}}" data-objectId="1">
                                <i class="fa fa-copy fa-2x"></i>
                            </a>
                            &nbsp;
                            <a href="javascript:void(0);"class="color_hdi sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Cập nhật confign')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item])}}" data-objectId="1">
                                <i class="fa fa-pencil-square-o fa-2x"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
