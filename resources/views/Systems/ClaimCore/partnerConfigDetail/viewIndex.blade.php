@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
        {{--list data---}}
        @include('Systems.ClaimCore.partnerConfigDetail.component.listData')
    {{ Form::close() }}
@stop
