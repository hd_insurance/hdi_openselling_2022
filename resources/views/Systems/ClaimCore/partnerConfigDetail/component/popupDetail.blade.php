<div class="modal-content" id="{{$form_id}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$form_id}}">
        <input type="hidden" id="objectId" name="objectId" @if($is_copy == STATUS_INT_MOT) value="0" @else value="{{$objectId}}"@endif>
        <input type="hidden" id="formName" name="formName" value="{{$form_id}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TPA_CODE')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="TPA_CODE"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_TPA_CODE"@endif>
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('SYN_TIME')}}</label>
                    <input type="text" class="form-control input-sm input-date" name="SYN_TIME"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_SYN_TIME"@endif value="{{date('d-m-Y',time())}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('PRODUCT_CODE')}}</label> <span class="red">(*)</span>
                    <select class="form-control input-sm chosen-select w-100" required name="PRODUCT_CODE" id="form_{{$form_id}}_PRODUCT_CODE" >
                        {!! $optionProductCore !!}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right">{{viewLanguage('PACK_CODE')}}</label> <span class="red">(*)</span>
                    <select class="form-control input-sm chosen-select w-100" required name="PACK_CODE" id="form_{{$form_id}}_PACK_CODE" >
                        {!! $optionPackageCore !!}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="FEES"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_FEES"@endif>
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('SYN_FEES')}}</label> <span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="SYN_FEES"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_SYN_FEES"@endif>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
            <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$form_id}}','{{$url_action}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy'});
        showDataIntoForm('form_{{$form_id}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
