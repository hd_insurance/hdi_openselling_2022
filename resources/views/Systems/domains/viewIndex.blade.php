@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.domains.component.formSearch')

    {{--list data---}}
    @include('Systems.domains.component.listData')
@stop
