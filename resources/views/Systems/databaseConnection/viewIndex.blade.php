@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.databaseConnection.component.formSearch')

    {{--list data---}}
    @include('Systems.databaseConnection.component.listData')
@stop
