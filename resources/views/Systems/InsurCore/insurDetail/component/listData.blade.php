{{--@include('Systems.InsurCore.insurDetail.component.groupTabPage')--}}
{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-2 paddingRight-unset">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class=" col-lg-3 paddingRight-unset">
                <label for="user_group">Sản phẩm</label>
                <select class="form-control input-sm chosen-select w-100" name="PRODUCT_CODE" id="PRODUCT_CODE">
                    {!! $optionProductCore !!}}
                </select>
            </div>
            <div class=" col-lg-2 paddingRight-unset">
                <label for="user_group">Trạng thái</label>
                <select class="form-control input-sm chosen-select w-100" name="STATUS" id="STATUS">
                    {!! $optionStatusContract !!}}
                </select>
            </div>
            <div class="col-lg-1_5 paddingRight-unset">
                <label for="user_email">Từ ngày tạo</label>
                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_from_date" id="p_from_date" @if(isset($search['p_from_date']))value="{{$search['p_from_date']}}"@endif>
                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
            </div>
            <div class="col-lg-1_5 paddingRight-unset">
                <label for="user_email">Đến ngày tạo</label>
                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_to_date" id="p_to_date" @if(isset($search['p_to_date']))value="{{$search['p_to_date']}}"@endif >
                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
            </div>
            <div class="col-lg-1_5 marginT30 paddingRight-unset">
                <button class="btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                {{--<a href="javascript:void(0);" class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                    Thêm mới
                </a>--}}
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                    <th width="20%" class="text-left">{{viewLanguage('Product_code/Certificate_no')}}</th>
                    <th width="20%" class="text-left">{{viewLanguage('Thông tin thêm')}}</th>
                    <th width="20%" class="text-left">{{viewLanguage('Thông tin cá nhân')}}</th>
                    <th width="15%" class="text-left">{{viewLanguage('Thông tin bảo hiểm')}}</th>
                    <th width="15%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-left middle">
                            @if(trim($item->PRODUCT_CODE) != '')<b class="font_10">Product_code: </b>
                                <a href="{{URL::route('insurDetail.index',array('PRODUCT_CODE' => $item->PRODUCT_CODE))}}" title="tìm nhanh theo {{$item->PRODUCT_CODE}}">
                                    {{$item->PRODUCT_CODE}}
                                </a><br/>
                            @endif
                            @if(trim($item->CERTIFICATE_NO) != '')<b class="font_10">Certificate_no: </b>
                                <a href="{{URL::route('insurDetail.index',array('p_keyword' => $item->CERTIFICATE_NO))}}" title="tìm nhanh theo {{$item->CERTIFICATE_NO}}">
                                    {{$item->CERTIFICATE_NO}}
                                </a><br/>
                            @endif
                            @if(trim($item->STATUS) != '' && isset($arrStatusContract[$item->STATUS]))<b class="font_10">Status: </b>
                                <a href="{{URL::route('insurDetail.index',array('STATUS' => $item->STATUS))}}" title="tìm nhanh theo {{$item->STATUS}}">
                                {{$arrStatusContract[$item->STATUS]}}
                                </a>
                                <br/>
                            @endif
                        </td>

                        <td class="text-left middle">
                            @if(trim($item->CATEGORY) != '')<b class="font_10">Category: </b>{{$item->CATEGORY}}<br/>@endif
                            @if(trim($item->ORG_CODE) != '')<b class="font_10">Org_code: </b>{{$item->ORG_CODE}}<br/>@endif
                            @if(trim($item->PACK_CODE) != '')<b class="font_10">Pack_code: </b>{{$item->PACK_CODE}}<br/>@endif
                            @if(trim($item->PACK_FEES) != '')<b class="font_10">Pack_fees: </b>{{numberFormat($item->PACK_FEES)}}<br/>@endif
                            @if(trim($item->DATE_SIGN) != '')<b class="font_10">Date_sign: </b>{{convertDateDMY($item->DATE_SIGN)}}<br/>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->NAME) != '')<b class="font_10">Name: </b>{{$item->NAME}}<br/>@endif
                            @if(trim($item->RELATIONSHIP) != '' && isset($arrRelationship[$item->RELATIONSHIP]))<b class="font_10">Relationship: </b>{{$arrRelationship[$item->RELATIONSHIP]}}<br/>@endif
                            @if(trim($item->EMAIL) != '')<b class="font_10">Email: </b>
                                <a href="{{URL::route('insurDetail.index',array('p_keyword' => $item->EMAIL))}}" title="tìm nhanh theo {{$item->EMAIL}}">
                                    {{$item->EMAIL}}
                                </a><br/>
                            @endif
                            @if(trim($item->PHONE) != '')<b class="font_10">Phone: </b>
                                <a href="{{URL::route('insurDetail.index',array('p_keyword' => $item->PHONE))}}" title="tìm nhanh theo {{$item->PHONE}}">
                                    {{$item->PHONE}}
                                </a><br/>
                            @endif
                            @if(trim($item->ADDRESS) != '')<b class="font_10">Address: </b>{{$item->ADDRESS}}<br/>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->AMOUNT) != '')<b class="font_10">Amount: </b> <span class="red">{{numberFormat($item->AMOUNT)}}</span><br/>@endif
                            @if(trim($item->TOTAL_AMOUNT) != '')<b class="font_10">Total_amount: </b> <span class="red">{{numberFormat($item->TOTAL_AMOUNT)}}</span><br/>@endif
                            @if(trim($item->EFFECTIVE_DATE) != '')<b class="font_10">Effective_date: </b>{{$item->EFFECTIVE_DATE}}<br/>@endif
                            @if(trim($item->EXPIRATION_DATE) != '')<b class="font_10">Expiration_date: </b>{{$item->EXPIRATION_DATE}}<br/>@endif
                        </td>

                        <td class="text-left middle">
                            @if($is_root || $permission_edit || $permission_add)
                                @if($is_root)
                                    <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailItem" data-input="{{json_encode(['item'=>$item])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->PRODUCT_CODE}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="13">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>
                                @endif
                                @if(isset($item->FILE_CODE) && trim($item->FILE_CODE) != '')
                                    &nbsp;&nbsp;&nbsp;<a href="{{$urlServiceFile.$item->FILE_CODE}}" class="color_hdi" target="_blank" title="Giấy chứng nhận">
                                        <i class="pe-7s-note2 fa-2x"></i>
                                    </a>
                                @endif

                                @if(isset($item->PRODUCT_CODE) && in_array($item->PRODUCT_CODE,$arrProductResign))
                                    &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="color_hdi" onclick="jqueryCommon.getAjaxWithConfirm(this);" data-loading="1" data-msg-confirm="Bạn có muốn ký số lại GCN: {{$item->CERTIFICATE_NO}}" data-input="{{json_encode(['item'=>$item])}}" title="{{viewLanguage('Ký số lại : ')}}{{$item->CERTIFICATE_NO}}" data-function-action="_resignCertificate" data-method="post" data-url="{{$urlAjaxGetData}}">
                                        <i class="fa fa-repeat fa-2x"></i>
                                    </a>
                                @endif
                            @endif
                            <span class="font_10">
                                <br/>@if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                            </span>
                            <span class="font_10 red">
                                @if(trim($item->MODIFIED_BY) != ''){{$item->MODIFIED_BY}}@endif
                                @if(trim($item->MODIFIED_DATE) != '') - {{convertDateDMY($item->MODIFIED_DATE)}} <br/>@endif
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
