<div class="tabs-lg-alternate card-header">
    <ul class="nav nav-justified">
        <li class="nav-item">
            <a href="{{URL::route('productDeclaration.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'productDeclaration.index')active @endif">
                <div class="widget-number1">Khai báo sản phẩm</div>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{URL::route('packageDeclaration.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'packageDeclaration.index')active @endif">
                <div class="widget-number1">Khai báo gói</div>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{URL::route('categoryDeclaration.index')}}" style="padding-top:1rem!important; padding-bottom:1rem!important;" class="nav-link @if($pageCurrent == 'categoryDeclaration.index')active @endif">
                <div class="widget-number1">Khai báo Category</div>
            </a>
        </li>
    </ul>
</div>
