{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin bảo hiểm: &nbsp;<span class="showInforItem" data-field="NAME"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-3">
                    PRODUCT_CODE: <b class="showInforItem" data-field="PRODUCT_CODE"></b>
                </div>
                <div class="col-lg-3">
                    CERTIFICATE_NO: <b class="showInforItem" data-field="CERTIFICATE_NO"></b>
                </div>
                <div class="col-lg-3">
                    CATEGORY: <b class="showInforItem" data-field="CATEGORY"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    NAME: <b class="showInforItem" data-field="NAME"></b>
                </div>
                <div class="col-lg-3">
                    RELATIONSHIP: <b class="showInforItem" data-field="">@if(isset($arrRelationship[$dataPrimary->RELATIONSHIP])) {{$arrRelationship[$dataPrimary->RELATIONSHIP]}}@endif</b>
                </div>
                <div class="col-lg-3">
                    DOB: <b class="showInforItem" data-field="">@if(isset($dataPrimary->DOB)){{$dataPrimary->DOB}}@endif</b>
                </div>
                <div class="col-lg-3">
                    GENDER: <b class="showInforItem" data-field="">@if(isset($arrGender[$dataPrimary->GENDER])) {{$arrGender[$dataPrimary->GENDER]}}@endif</b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    EMAIL: <b class="showInforItem" data-field="EMAIL"></b>
                </div>
                <div class="col-lg-3">
                    PHONE: <b class="showInforItem" data-field="PHONE"></b>
                </div>
                <div class="col-lg-3">
                    IDCARD: <b class="showInforItem" data-field="IDCARD"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    DATE_SIGN: <b class="showInforItem" data-field="">@if(trim($dataPrimary->DATE_SIGN) != ''){{$dataPrimary->DATE_SIGN}}@endif</b>
                </div>
                <div class="col-lg-3">
                    EFFECTIVE_DATE: <b class="showInforItem" data-field="">@if(trim($dataPrimary->EFFECTIVE_DATE) != ''){{$dataPrimary->EFFECTIVE_DATE}}@endif</b>
                </div>
                <div class="col-lg-3">
                    EXPIRATION_DATE: <b class="showInforItem" data-field="">@if(trim($dataPrimary->EXPIRATION_DATE) != ''){{$dataPrimary->EXPIRATION_DATE}}@endif</b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    ADDRESS: <b class="showInforItem" data-field="ADDRESS"></b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        @if($objectId > 0)
            Thông tin bảo hiểm: &nbsp;<span class="showInforItem" data-field="NAME"></span>
        @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" value="{{$objectId}}">
        <input type="hidden" name="DETAIL_CODE" id="form_{{$formName}}_DETAIL_CODE" >
        <input type="hidden" name="PRODUCT_CODE" id="form_{{$formName}}_PRODUCT_CODE" >
        <input type="hidden" name="CONTRACT_CODE" id="form_{{$formName}}_CONTRACT_CODE" >
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-lg-6">
                <label for="NAME" class="text-right control-label ">{{viewLanguage('Product_code')}}</label><span class="red">(*)</span>
                <select class="form-control input-sm chosen-select w-100" required name="PRODUCT_CODE" id="form_{{$formName}}_PRODUCT_CODE" disabled>
                    {!! $optionProductCore !!}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('CERTIFICATE_NO')}}</label>
                <input type="text" class="form-control input-sm" name="CERTIFICATE_NO" id="form_{{$formName}}_CERTIFICATE_NO" @if($objectId > 0) readonly @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('CATEGORY')}}</label>
                <input type="text" class="form-control input-sm" name="CATEGORY" id="form_{{$formName}}_CATEGORY" @if($objectId > 0) readonly @endif>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('NAME')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="NAME" id="form_{{$formName}}_NAME">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('EMAIL')}}</label>
                <input type="text" class="form-control input-sm" name="EMAIL" id="form_{{$formName}}_EMAIL">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('PHONE')}}</label>
                <input type="text" class="form-control input-sm" name="PHONE" id="form_{{$formName}}_PHONE">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('RELATIONSHIP')}}</label>
                <select class="form-control input-sm chosen-select w-100" name="RELATIONSHIP" id="form_{{$formName}}_RELATIONSHIP">
                    {!! $optionRelationship !!}
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('GENDER')}}</label>
                <select class="form-control input-sm" name="GENDER" id="form_{{$formName}}_GENDER">
                    {!! $optionGender !!}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('DOB')}}</label>
                <input type="text" class="form-control input-sm input-date" name="DOB" @if(isset($dataPrimary->DOB))value="{{$dataPrimary->DOB}}"@endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('IDCARD')}}</label>
                <input type="text" class="form-control input-sm" name="IDCARD" id="form_{{$formName}}_IDCARD">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('REGION')}}</label>
                <select class="form-control input-sm" name="REGION" id="form_{{$formName}}_REGION">
                    {!! $optionRegion !!}
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('DATE_SIGN')}}</label>
                <input type="text" class="form-control input-sm input-date" name="DATE_SIGN" @if(isset($dataPrimary->DATE_SIGN))value="{{$dataPrimary->DATE_SIGN}}" @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('EFFECTIVE_DATE')}}</label>
                <input type="text" class="form-control input-sm input-date" name="EFFECTIVE_DATE" @if(isset($dataPrimary->EFFECTIVE_DATE))value="{{$dataPrimary->EFFECTIVE_DATE}}"@endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('EXPIRATION_DATE')}}</label>
                <input type="text" class="form-control input-sm input-date" name="EXPIRATION_DATE" @if(isset($dataPrimary->EXPIRATION_DATE))value="{{$dataPrimary->EXPIRATION_DATE}}"@endif>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Tỉnh thành phố')}}</label>
                <select class="form-control input-sm chosen-select" name="PROVINCE" id="form_{{$formName}}_PROVINCE" onchange="jqueryCommon.buildOptionCommon('form_{{$formName}}_PROVINCE','OPTION_DISTRICT_CODE','form_{{$formName}}_DISTRICT')">
                    {!! $optionProvince !!}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Quận huyện')}}</label>
                <select class="form-control input-sm" name="DISTRICT" id="form_{{$formName}}_DISTRICT" onchange="jqueryCommon.buildOptionCommon('form_{{$formName}}_DISTRICT','OPTION_WARD_CODE','form_{{$formName}}_WARDS')">
                    {!! $optionDistrict !!}
                </select>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Phường xã')}}</label>
                <select class="form-control input-sm" name="WARDS" id="form_{{$formName}}_WARDS" >
                    {!! $optionWard !!}
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-12">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Địa chỉ')}} </label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" name="ADDRESS" id="form_{{$formName}}_ADDRESS">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
