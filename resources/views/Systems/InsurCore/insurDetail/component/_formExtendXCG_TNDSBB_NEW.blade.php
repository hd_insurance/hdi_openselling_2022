<form id="form_{{$formNameOther}}">
    <input type="hidden" id="objectId" name="objectId" value="{{$objectId}}">
    <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
    <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
    <input type="hidden" id="dataPrimary" name="dataPrimary" value="{{json_encode($dataPrimary)}}">
    <input type="hidden" name="PRODUCT_CODE" id="PRODUCT_CODE" @if(isset($dataPrimary->PRODUCT_CODE)) value="{{$dataPrimary->PRODUCT_CODE}}" @endif>
    <input type="hidden" name="CONTRACT_CODE" id="CONTRACT_CODE" @if(isset($dataPrimary->CONTRACT_CODE)) value="{{$dataPrimary->CONTRACT_CODE}}" @endif >
    <input type="hidden" name="CERTIFICATE_NO" id="CERTIFICATE_NO" @if(isset($dataPrimary->CERTIFICATE_NO)) value="{{$dataPrimary->CERTIFICATE_NO}}" @endif >

    <input type="hidden" id="data_item" name="data_item" value="{{json_encode($extendInsur)}}">
    <input type="hidden" id="form_{{$formNameOther}}_VEH_CODE" name="VEH_CODE">
    <input type="hidden" id="{{$formNameOther}}actionUpdate" name="actionUpdate" value="updateVehicleInfo">
    <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
    <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
    <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

    {{ csrf_field() }}
    <div class="modal-body">
        <div class="row form-group">
            {{--<div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('NONE_NUMBER_PLATE')}}</label>
            </div>--}}
            <input type="hidden" class="form-control input-sm" name="NONE_NUMBER_PLATE" id="form_{{$formNameOther}}_NONE_NUMBER_PLATE">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Biển số')}}</label>
                <input type="text" class="form-control input-sm" name="NUMBER_PLATE" id="form_{{$formNameOther}}_NUMBER_PLATE">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Số khung')}}</label>
                <input type="text" class="form-control input-sm" name="CHASSIS_NO" id="form_{{$formNameOther}}_CHASSIS_NO">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Số máy')}}</label>
                <input type="text" class="form-control input-sm" name="ENGINE_NO" id="form_{{$formNameOther}}_ENGINE_NO">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Tải trọng')}}</label>
                <input type="text" class="form-control input-sm" name="WEIGH" id="form_{{$formNameOther}}_WEIGH">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Chỗ ngồi')}}</label>
                <input type="text" class="form-control input-sm" name="SEAT_NO" id="form_{{$formNameOther}}_SEAT_NO">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Năm sản xuất')}}</label>
                <input type="text" class="form-control input-sm" name="MFG" id="form_{{$formNameOther}}_MFG">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Hãng')}}</label>
                <input type="text" class="form-control input-sm" name="BRAND" id="form_{{$formNameOther}}_BRAND">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Kiểu mẫu')}}</label>
                <input type="text" class="form-control input-sm" name="MODEL" id="form_{{$formNameOther}}_MODEL">
            </div>
        </div>

        <div class="row form-group">
            <div class="col-lg-3">
                @if($is_root || $permission_edit || $permission_add)
                    <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
                @endif
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        showDataIntoForm('form_{{$formNameOther}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
