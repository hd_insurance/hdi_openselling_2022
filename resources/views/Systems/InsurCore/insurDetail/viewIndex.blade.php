@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.InsurCore.insurDetail.component.formSearch')
    {{--list data---}}
    @include('Systems.InsurCore.insurDetail.component.listData')
@stop
