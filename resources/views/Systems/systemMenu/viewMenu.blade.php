@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.systemMenu.component.formSearch')

    {{--list data---}}
    @include('Systems.systemMenu.component.listData')
@stop
