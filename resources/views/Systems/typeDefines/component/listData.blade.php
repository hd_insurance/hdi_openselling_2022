<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Tìm kiếm')}}</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-4">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="s_search" name="s_search" autocomplete="off" @if(isset($search['s_search']))value="{{$search['s_search']}}"@endif>
            </div>
            <div class=" col-lg-4">
                <label for="user_group">Define code</label>
<!--                <select class="multiselect-dropdown form-control" name="s_define_code" id="s_define_code">-->
                <select class="form-control input-sm chosen-select w-100" name="s_define_code" id="s_define_code">
                    {!! $optionSearchDefineCode !!}}
                </select>
            </div>
            <div class="col-lg-3 marginT20">
                <button class="btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
            </div>
        </div>
    </div>
</div>

<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>

                    <th width="10%" class="text-left">{{viewLanguage('Define code')}}</th>
                    <th width="15%" class="text-left">{{viewLanguage('Define name')}}</th>

                    <th width="15%" class="text-left">{{viewLanguage('Type code')}}</th>
                    <th width="15%" class="text-left">{{viewLanguage('Type name')}}</th>
                    <th width="5%" class="text-center">{{viewLanguage('Order')}}</th>

                    <th width="5%" class="text-center">{{viewLanguage('Project')}}</th>
                    <th width="5%" class="text-center">{{viewLanguage('Lang')}}</th>
                    <th width="10%" class="text-center">{{viewLanguage('User TT')}}</th>

                    <th width="10%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>

                        <td class="text-center middle">
                            <a href="{{URL::route('typeDefines.index',array('s_search' => $item->DEFINE_CODE))}}" title="tìm nhanh theo {{$item->DEFINE_CODE}}">{{$item->DEFINE_CODE}}</a>&nbsp;
                        </td>
                        <td class="text-left middle">
                            <a href="{{URL::route('typeDefines.index',array('s_search' => $item->DEFINE_CODE))}}" title="tìm nhanh theo {{$item->DEFINE_CODE}}">
                            {{$item->DEFINE_NAME}}
                            </a>
                        </td>

                        <td class="text-left middle">{{$item->TYPE_CODE}}</td>
                        <td class="text-left middle">{{$item->TYPE_NAME}}</td>
                        <td class="text-center middle">{{$item->SORTORDER}}</td>
                        <td class="text-center middle">{{$item->PROJECT_CODE}}</td>
                        <td class="text-center middle">{{$item->LANGUAGE}}</td>
                        <td class="text-left middle">
                            <span class="font_10">
                                @if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                            </span>
                            <span class="font_10 red">
                                @if(trim($item->MODIFIED_BY) != ''){{$item->MODIFIED_BY}}@endif
                                @if(trim($item->MODIFIED_DATE) != '') - {{convertDateDMY($item->MODIFIED_DATE)}} <br/>@endif
                            </span>
                        </td>

                        <td class="text-center middle">
                            @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                <a href="javascript:void(0);" style="color: green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                            @else
                                <a href="javascript:void(0);" style="color: red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                            @endif
                            @if($is_root || $permission_edit || $permission_add)
                            &nbsp;
                            <a href="javascript:void(0);"class="color_warning sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Thêm định nghĩa')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item,'is_copy'=>STATUS_INT_MOT])}}" data-objectId="{{$item->ID}}">
                                <i class="fa fa-copy fa-2x"></i>
                            </a>
                            &nbsp;
                            <a href="javascript:void(0);"class="color_hdi sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Cập nhật định nghĩa')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item])}}" data-objectId="{{$item->ID}}">
                                <i class="fa fa-pencil-square-o fa-2x"></i>
                            </a>
                            @endif
                            @if($is_root)
                                &nbsp;&nbsp;<a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);"data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$item, 'tableSyn'=>TABLE_SYS_TYPE_DEFINES, 'keySyn'=>$item->ID])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                    <i class="fa fa-arrow-circle-up fa-2x"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
