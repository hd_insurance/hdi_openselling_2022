@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
    {{--Search---}}
    @include('Systems.typeDefines.component.formSearch')

    {{--list data---}}
    @include('Systems.typeDefines.component.listData')
    {{ Form::close() }}
@stop
