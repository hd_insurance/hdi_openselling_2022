@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.apiSystem.component.formSearch')

    {{--list data---}}
    @include('Systems.apiSystem.component.listData')
@stop
