<div class="modal-content" id="list_temp_detail" style="position: relative; width: 1000px!important;">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_list_temp_detail">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">Danh sách template Detail</h4>
        </div>
        <div class="modal-body">
            @if($is_root || $permission_edit || $permission_add)
                <a href="javascript:void(0);" class="btn btn-info detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemTempDetail" data-input="{{json_encode(['type'=>$tabOtherItem3,'itemId'=>'0','isDetail'=>STATUS_INT_MOT,'arrKey'=>['DataTemplateConfig'=>$data]])}}" data-show="1" data-show-id="{{$tabOtherItem3}}" title="{{viewLanguage('Thêm Template Detail: ')}}" data-method="post" data-objectId="{{$TEMP_ID}}">
                    <i class="pe-7s-plus"></i> {{viewLanguage('Add Template Detail')}}
                </a>
            @endif
            <div class="marginT5 table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                        <th width="3%" class="text-center">{{viewLanguage('TT')}}</th>

                        <th width="20%" class="text-center">{{viewLanguage('Thông tin')}}</th>
                        <th width="20%" class="text-center">{{viewLanguage('LIST_REPLACE')}}</th>
                        <th width="45%" class="text-center">{{viewLanguage('DATA')}}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(isset($data) && !empty($data))
                        @foreach ($data as $kb => $itemDetail)
                            <tr>
                                <td class="text-center middle">{{$kb+1}}</td>
                                <td class="text-center middle">
                                    @if($is_root || $permission_view || $permission_add)
                                        <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemTempDetail" data-input="{{json_encode(['type'=>$tabOtherItem3,'DataItemTempDetail'=>$itemDetail,'itemId'=>$itemDetail->DETAIL_ID,'isDetail'=>STATUS_INT_MOT,'arrKey'=>[]])}}" data-show="1" data-show-id="{{$tabOtherItem3}}" title="{{viewLanguage('Chi tiết Template Detail: ')}}" data-method="post" data-objectId="{{$itemDetail->TEMP_ID}}">
                                            <i class="pe-7s-look fa-2x"></i>
                                        </a>
                                    @endif
                                    @if($itemDetail->IS_ACTIVE == STATUS_INT_MOT)
                                        <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                                    @else
                                        <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-minus fa-2x"></i></a>
                                    @endif
                                </td>

                                <td class="text-left middle">
                                    @if(isset($itemDetail->TEMP_CODE))<span class="font_10">Temp_code</span>: {{$itemDetail->TEMP_CODE}}<br/>@endif
                                    @if(isset($itemDetail->DATA_TYPE))<span class="font_10">Data_type</span>: {{$itemDetail->DATA_TYPE}}<br/>@endif
                                    @if(isset($itemDetail->PARAM_NAME))<span class="font_10">Param_name</span>: {{$itemDetail->PARAM_NAME}}<br/>@endif
                                    @if(isset($itemDetail->TEMP_ID))<span class="font_10">Temp_id</span>: {{$itemDetail->TEMP_ID}}<br/>@endif
                                </td>
                                <td class="text-left middle">@if(isset($itemDetail->LIST_REPLACE)){{str_replace(';','; ',$itemDetail->LIST_REPLACE)}}@endif</td>
                                <td class="text-left middle">
                                    @if(isset($itemDetail->DATA))
                                        <textarea class="form-control input-sm" name="STR_DATA" rows="4">{{$itemDetail->DATA}}</textarea>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
