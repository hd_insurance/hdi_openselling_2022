
{{----Edit và thêm mới----}}
<div class="formEditItemInput">
    <div class="card-header">
            Thông tin Template Config @if($objectId > 0): <span class="showInforItem" data-field="TEMP_ID"></span>@endif
            @if($objectId > 0)
                <div style="position: absolute; right: 0px; z-index: 10">
                    <button type="button" class="btn btn-success" onclick="jqueryCommon.clickShowEditFormItem();"><i class="fa fa-edit"></i></button>
                </div>
            @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" value="@if(isset($data->TEMP_ID) && $data->TEMP_ID > 0) 1 @else 0 @endif">
        <input type="hidden" name="TEMP_ID" id="form_{{$formName}}_TEMP_ID" value="@if(isset($data->TEMP_ID)){{$data->TEMP_ID}}@else @endif">
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="form-group">
            <div class="row">
                <div class="col-lg-9">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Title mail')}} </label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required maxlength="150" name="DESCRIPTION" id="form_{{$formName}}_DESCRIPTION">
                </div>
                <div class="col-lg-3">
                    <label for="status" class="control-label">{{viewLanguage('Trạng thái')}}</label> <span class="red"> (*)</span>
                    <select  class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formName}}_IS_ACTIVE">
                        {!! $optionStatus !!}}
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Product_code')}} </label><span class="red"> (*)</span>
                    {{--<input type="text" class="form-control input-sm" required maxlength="100" name="PRODUCT_CODE" id="form_{{$formName}}_PRODUCT_CODE">--}}
                    <select  class="form-control input-sm chosen-select w-100" required name="PRODUCT_CODE" id="form_{{$formName}}_PRODUCT_CODE">
                        {!! $optionProduct !!}}
                    </select>
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Org_code')}} </label><span class="red"> (*)</span>
                    <select  class="form-control input-sm chosen-select w-100" required name="ORG_CODE" id="form_{{$formName}}_ORG_CODE">
                        {!! $optionOrg !!}}
                    </select>
                </div>

            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Channel')}} </label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="100" required name="CHANNEL"  @if(isset($data->CHANNEL)) value="{{$data->CHANNEL}}" @else value="WEB" @endif>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Temp_type')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required name="TEMP_TYPE" @if(isset($data->TEMP_TYPE)) value="{{$data->TEMP_TYPE}}" @else value="PDF" @endif>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Pack_code')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required maxlength="100" name="PACK_CODE" id="form_{{$formName}}_PACK_CODE">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Struct_code')}} </label>
                    <input type="text" class="form-control input-sm" maxlength="100" name="STRUCT_CODE" id="form_{{$formName}}_STRUCT_CODE">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày bắt đầu')}} </label>
                    <input type="text" class="form-control input-sm input-date" data-valid = "text" required name="EFFECTIVE_DATE" value="@if(isset($data->EFFECTIVE_DATE) && trim($data->EFFECTIVE_DATE) != ''){{date('d/m/Y',strtotime($data->EFFECTIVE_DATE))}}@else {{date('d/m/Y')}}@endif">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Ngày kết thúc')}} </label>
                    <input type="text" class="form-control input-sm input-date" data-valid = "text" name="EXPIRATION_DATE" value="@if(isset($data->EXPIRATION_DATE) && trim($data->EXPIRATION_DATE) != ''){{date('d/m/Y',strtotime($data->EXPIRATION_DATE))}}@endif">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Mail_bcc')}} </label>
                    <input type="text" class="form-control input-sm" name="MAIL_BCC" id="form_{{$formName}}_MAIL_BCC">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Isqueue')}} </label>
                    <input type="text" class="form-control input-sm" name="ISQUEUE"  @if(isset($data->ISQUEUE)) value="{{$data->ISQUEUE}}" @else value="0" @endif>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
