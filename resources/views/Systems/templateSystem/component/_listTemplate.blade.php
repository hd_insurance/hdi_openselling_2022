<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="6%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="17%" class="text-center">{{viewLanguage('Thông tin')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('Thông tin thêm')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('Thông tin thêm')}}</th>

            <th width="45%" class="text-center">{{viewLanguage('Param')}}</th>
        </tr>

        </thead>
        <tbody>
        @if(isset($dataOther) && !empty($dataOther))
            @foreach ($dataOther as $kb => $itemOther)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem1,'itemId'=>$itemOther->TEMP_CONFIG,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['DataTemplateConfig'=>$data,'DataTemplate'=>$itemOther]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Chi tiết Template: ').$itemOther->TITLE}}" data-method="post" data-objectId="{{$data->TEMP_ID}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                            <a href="javascript:void(0);" style="color: #f7b924" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetListTempDetail" data-input="{{json_encode(['type'=>$tabOtherItem1,'itemId'=>$itemOther->TEMP_CONFIG,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['DataTemplateConfig'=>$data,'DataTemplate'=>$itemOther]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="Template detail" data-method="post" data-objectId="{{$itemOther->TEMP_ID}}">
                                <i class="pe-7s-note2 fa-2x"></i>
                            </a><br/>
                        @endif

                        @if($itemOther->IS_ACTIVE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-minus fa-2x"></i></a>
                        @endif

                        @if($is_root)
                                <?php
                                $dataSyn = $itemOther;
                                $dataSyn->HEADER = '';
                                $dataSyn->FOOTER = '';
                                $dataSyn->DATA = '';
                                ?>
                            <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);"data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$dataSyn, 'tableSyn'=>TABLE_MD_TEMPLATES, 'keySyn'=>$itemOther->TEMP_ID])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                <i class="fa fa-arrow-circle-up fa-2x"></i>
                            </a>
                        @endif
                    </td>

                    <td class="text-left middle">
                        @if(isset($itemOther->ORG_CODE))<span class="font_10">Org_code</span>: {{$itemOther->ORG_CODE}}<br/>@endif
                        @if(isset($itemOther->PRODUCT_CODE))<span class="font_10">Pro_code</span>: {{$itemOther->PRODUCT_CODE}}<br/>@endif
                        @if(isset($itemOther->TEMP_CODE))<span class="font_10">Temp_code</span>: {{$itemOther->TEMP_CODE}}<br/>@endif
                        @if(isset($itemOther->TEMP_TYPE))<span class="font_10">Temp_type</span>: {{$itemOther->TEMP_TYPE}}<br/>@endif
                    </td>

                    <td class="text-left middle">
                        @if(isset($itemOther->TITLE))<span class="font_10">Title</span>: {{$itemOther->TITLE}}<br/>@endif
                        @if(isset($itemOther->PACK_CODE))<span class="font_10">Pack_code</span>: {{$itemOther->PACK_CODE}}<br/>@endif
                        @if(isset($itemOther->DATA_TYPE))<span class="font_10">Data_type</span>: {{$itemOther->DATA_TYPE}}<br/>@endif
                    </td>
                    <td class="text-left middle">
                        @if(isset($itemOther->TEXT_CA))<span class="font_10">Text_ca</span>: {{$itemOther->TEXT_CA}}<br/>@endif
                        @if(isset($itemOther->DATA_SRC))<span class="font_10">Data_src</span>: {{$itemOther->DATA_SRC}}<br/>@endif
                        @if(isset($itemOther->TYPE_SEND))<span class="font_10">Type_send</span>: {{$itemOther->TYPE_SEND}}<br/>@endif
                    </td>
                    <td class="text-left middle">
                        {{str_replace(';','; ',$itemOther->PARAM_REPLACE)}}
                        <br/>
                        <span class="font_10">
                            @if(trim($itemOther->CREATE_BY) != ''){{$itemOther->CREATE_BY}}@endif
                            @if(trim($itemOther->CREATE_DATE) != '') - {{convertDateDMY($itemOther->CREATE_DATE)}} <br/>@endif
                        </span>
                        <span class="font_10 red">
                            @if(trim($itemOther->MODIFIED_BY) != ''){{$itemOther->MODIFIED_BY}}@endif
                            @if(trim($itemOther->MODIFIED_DATE) != '') - {{convertDateDMY($itemOther->MODIFIED_DATE)}} <br/>@endif
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
