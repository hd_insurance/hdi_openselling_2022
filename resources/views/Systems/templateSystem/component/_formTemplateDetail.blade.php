<div class="modal-content" id="{{$formNameOther}}" style="position: relative; width: 1000px!important;">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">

        <input type="hidden" id="TEMP_ID" name="TEMP_ID" @if(isset($temp_id))value="{{$temp_id}}"@endif>
        <input type="hidden" id="form_{{$formNameOther}}_DETAIL_ID" name="DETAIL_ID">
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="form_group">
                <div class="form-group">
                    <div class="row form-group">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('TEMP_CODE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="TEMP_CODE" id="form_{{$formNameOther}}_TEMP_CODE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATA_TYPE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="DATA_TYPE" id="form_{{$formNameOther}}_DATA_TYPE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('PARAM_NAME')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="PARAM_NAME" id="form_{{$formNameOther}}_PARAM_NAME">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right">{{viewLanguage('Trạng thái')}}</label>
                            <select class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formNameOther}}_IS_ACTIVE" >
                                {!! $optionStatus !!}
                            </select>
                        </div>
                        </div>
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('LIST_REPLACE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="LIST_REPLACE" id="form_{{$formNameOther}}_LIST_REPLACE">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATA')}}</label>
                            <textarea class="form-control input-sm" name="DATA" id="form_{{$formNameOther}}_DATA" rows="8"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
