
{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Search')}}</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="form-group col-lg-6">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Sản phẩm')}}</label>
                <select  class="form-control input-sm chosen-select w-100" name="p_product_code" id="p_product_code">
                    {!! $optionProduct !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Đối tác')}}</label>
                <select  class="form-control input-sm chosen-select w-100" name="p_org_code" id="p_org_code">
                    {!! $optionOrg !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Gói')}}</label>
                <select  class="form-control input-sm chosen-select w-100" name="p_pack_code" id="p_pack_code">
                    {!! $optionPack !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Trạng thái')}}</label>
                <select  class="form-control input-sm" name="p_is_active" id="p_is_active">
                    {!! $optionStatus !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                @if($is_root || $permission_view)
                    <button class="mb-2 mr-2 btn-icon btn btn-primary marginT25" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                @endif
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

<div class="main-card mb-3 card">
    <div class="card-body">
        @if($data && sizeof($data) > 0)
            <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        {{--<th width="3%" class="text-center"><input type="checkbox" class="check" id="checkAll"></th>--}}
                        <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                        <th width="6%" class="text-center">{{viewLanguage('TT')}}</th>

                        <th width="22%" class="text-center">{{viewLanguage('Thông tin code')}}</th>
                        <th width="22%" class="text-center">{{viewLanguage('Thông tin thêm')}}</th>
                        <th width="30%" class="text-center">{{viewLanguage('Mô tả')}}</th>

                        <th width="15%" class="text-center">{{viewLanguage('Hiệu lực')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $item)
                        <tr>
                            <td class="text-center middle">{{$stt+$key+1}}</td>
                            <td class="text-center middle">
                                @if($is_root || $permission_view || $permission_add)
                                    <a href="javascript:void(0);" class="color_hdi" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailTemplateConfig" data-input="{{json_encode(['item'=>$item,'isCopy'=>STATUS_INT_KHONG])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->TEMP_ID}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="{{$item->TEMP_ID}}">
                                        <i class="fa fa-eye fa-2x"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="color_hdi" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailTemplateConfig" data-input="{{json_encode(['item'=>$item,'isCopy'=>STATUS_INT_MOT])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Copy template: ')}}{{$item->TEMP_ID}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="{{$item->TEMP_ID}}">
                                        <i class="fa fa-copy fa-2x"></i>
                                    </a>
                                    <br/>
                                @endif

                                @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                    <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                                @else
                                    <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-minus fa-2x"></i></a>
                                @endif
                                @if($is_root)
                                    <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);"data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$item, 'tableSyn'=>TABLE_MD_TEMP_CONFIG, 'keySyn'=>$item->TEMP_ID])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                        <i class="fa fa-arrow-circle-up fa-2x"></i>
                                    </a>
                                @endif
                            </td>

                            <td class="text-left middle">
                                @if(isset($item->ORG_CODE))<b>OrgCode:</b> {{$item->ORG_CODE}}<br/>@endif
                                @if(isset($item->PRODUCT_CODE))<b>ProCode:</b> {{$item->PRODUCT_CODE}}<br/>@endif
                                @if(isset($item->TEMP_CODE))<b>TempCode:</b> {{$item->TEMP_CODE}}<br/>@endif
                                @if(isset($item->PACK_CODE))<b>PackCode:</b> {{$item->PACK_CODE}}<br/>@endif

                            </td>
                            <td class="text-left middle">
                                @if(isset($item->TEMP_ID))<b>TempId:</b> {{$item->TEMP_ID}}<br/>@endif
                                @if(isset($item->CHANNEL))<b>Channel:</b> {{$item->CHANNEL}}<br/>@endif
                                @if(isset($item->TEMP_TYPE))<b>Type:</b> {{$item->TEMP_TYPE}}<br/>@endif
                                @if(isset($item->STRUCT_CODE))<b>StructCode:</b> {{$item->STRUCT_CODE}}<br/>@endif
                                @if(isset($item->MAIL_BCC))<b>MailBcc:</b> {{$item->MAIL_BCC}}<br/>@endif
                            </td>
                            <td class="text-left middle">@if(isset($item->DESCRIPTION)){{$item->DESCRIPTION}}@endif</td>
                            <td class="text-left middle">
                                @if(isset($item->EFFECTIVE_DATE) && trim($item->EFFECTIVE_DATE) != ''){{convertDateDMY($item->EFFECTIVE_DATE)}}<br/>@endif
                                @if(isset($item->EXPIRATION_DATE) && trim($item->EXPIRATION_DATE) != ''){{convertDateDMY($item->EXPIRATION_DATE)}}<br/>@endif
                                <span class="font_10">
                                    @if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                    @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                                </span>
                                <span class="font_10 red">
                                    @if(trim($item->MODIFIED_BY) != ''){{$item->MODIFIED_BY}}@endif
                                    @if(trim($item->MODIFIED_DATE) != '') - {{convertDateDMY($item->MODIFIED_DATE)}} <br/>@endif
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="paging_simple_numbers">
                {!! $paging !!}
            </div>
        @else
            <div class="alert">
                Không có dữ liệu
            </div>
        @endif
    </div>
</div>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

</script>
