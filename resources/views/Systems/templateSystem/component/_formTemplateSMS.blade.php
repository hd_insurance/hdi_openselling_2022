<div class="modal-content" id="{{$formNameOther}}" style="position: relative; width: 1000px!important;">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">
        <input type="hidden" id="dataTemplateConfig" name="dataTemplateConfig" value="{{json_encode($data)}}">

        <input type="hidden" id="TEMP_CONFIG" name="TEMP_CONFIG" @if(isset($data->TEMP_ID))value="{{$data->TEMP_ID}}"@endif>
        <input type="hidden" id="form_{{$formNameOther}}_TEMP_SMS_ID" name="TEMP_SMS_ID">
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="form_group">
                <div class="form-group">
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('TEMP_CODE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="TEMP_CODE" id="form_{{$formNameOther}}_TEMP_CODE">
                        </div>
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('TYPE_SEND')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="TYPE_SEND" id="form_{{$formNameOther}}_TYPE_SEND">
                        </div>
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right">{{viewLanguage('Trạng thái')}}</label>
                            <select class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formNameOther}}_IS_ACTIVE" >
                                {!! $optionStatus !!}
                            </select>
                        </div>
                        </div>
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('PARAM_REPLACE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="PARAM_REPLACE" id="form_{{$formNameOther}}_PARAM_REPLACE">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATA')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="DATA" id="form_{{$formNameOther}}_DATA">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATA_REG')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="DATA_REG" id="form_{{$formNameOther}}_DATA_REG">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DESCRIPTION')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="DESCRIPTION" id="form_{{$formNameOther}}_DESCRIPTION">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('SEND_NOW')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="SEND_NOW" id="form_{{$formNameOther}}_SEND_NOW">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('MAX_API')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="MAX_API" id="form_{{$formNameOther}}_MAX_API">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('ISCALLBACK')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="ISCALLBACK" id="form_{{$formNameOther}}_ISCALLBACK">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
