<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="3%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('Thông tin')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('Thông tin thêm')}}</th>

            <th width="25%" class="text-center">{{viewLanguage('DATA')}}</th>
            <th width="12%" class="text-center">{{viewLanguage('DATA_REG')}}</th>
            <th width="13%" class="text-center">{{viewLanguage('DESCRIPTION')}}</th>
            <th width="20%" class="text-center">{{viewLanguage('PARAM_REPLACE')}}</th>
        </tr>

        </thead>
        <tbody>
        @if(isset($dataOtherSMS) && !empty($dataOtherSMS))
            @foreach ($dataOtherSMS as $kb => $itemOtherSMS)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem2,'itemId'=>$itemOtherSMS->TEMP_CONFIG,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['DataTemplateConfig'=>$data,'DataTemplateSMS'=>$itemOtherSMS]])}}" data-show="1" data-show-id="{{$tabOtherItem2}}" title="{{viewLanguage('Chi tiết Template SMS: ')}}" data-method="post" data-objectId="{{$data->TEMP_ID}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                        @if($itemOtherSMS->IS_ACTIVE == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-minus fa-2x"></i></a>
                        @endif
                        @if($is_root)
                            <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);"data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['dataSyn'=>$itemOtherSMS, 'tableSyn'=>TABLE_MD_TEMPLATES_SMS, 'keySyn'=>$itemOtherSMS->TEMP_SMS_ID])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_pushAddDataSynch" data-method="post" data-url="{{$urlActionSynch}}">
                                <i class="fa fa-arrow-circle-up fa-2x"></i>
                            </a>
                        @endif
                    </td>

                    <td class="text-left middle">
                        @if(isset($itemOtherSMS->TEMP_CONFIG))<span class="font_10">Temp_config</span>: {{$itemOtherSMS->TEMP_CONFIG}}<br/>@endif
                        @if(isset($itemOtherSMS->TEMP_CODE))<span class="font_10">Temp_code</span>: {{$itemOtherSMS->TEMP_CODE}}<br/>@endif
                        @if(isset($itemOtherSMS->TYPE_SEND))<span class="font_10">Type_send</span>: {{$itemOtherSMS->TYPE_SEND}}<br/>@endif
                    </td>

                    <td class="text-left middle">
                        @if(isset($itemOtherSMS->MAX_API))<span class="font_10">Max_api</span>: {{$itemOtherSMS->MAX_API}}<br/>@endif
                        @if(isset($itemOtherSMS->ISCALLBACK))<span class="font_10">Iscallback</span>: {{$itemOtherSMS->ISCALLBACK}}<br/>@endif
                        @if(isset($itemOtherSMS->SEND_NOW))<span class="font_10">Send_now</span>: {{$itemOtherSMS->SEND_NOW}}<br/>@endif
                    </td>
                    <td class="text-left middle">{{$itemOtherSMS->DATA}}</td>
                    <td class="text-left middle">{{$itemOtherSMS->DATA_REG}}</td>
                    <td class="text-left middle">{{$itemOtherSMS->DESCRIPTION}}</td>
                    <td class="text-left middle">
                        {{str_replace(';','; ',$itemOtherSMS->PARAM_REPLACE)}}
                        <br/>
                        <span class="font_10">
                            @if(trim($itemOtherSMS->CREATE_BY) != ''){{$itemOtherSMS->CREATE_BY}}@endif
                            @if(trim($itemOtherSMS->CREATE_DATE) != '') - {{convertDateDMY($itemOtherSMS->CREATE_DATE)}} <br/>@endif
                        </span>
                        <span class="font_10 red">
                            @if(trim($itemOtherSMS->MODIFIED_BY) != ''){{$itemOtherSMS->MODIFIED_BY}}@endif
                            @if(trim($itemOtherSMS->MODIFIED_DATE) != '') - {{convertDateDMY($itemOtherSMS->MODIFIED_DATE)}} <br/>@endif
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
