<div class="modal-content" id="{{$formNameOther}}" style="position: relative; width: 1000px!important;">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">
        <input type="hidden" id="dataTemplateConfig" name="dataTemplateConfig" value="{{json_encode($data)}}">

        <input type="hidden" id="TEMP_CONFIG" name="TEMP_CONFIG" @if(isset($data->TEMP_ID))value="{{$data->TEMP_ID}}"@endif>
        <input type="hidden" id="form_{{$formNameOther}}_TEMP_ID" name="TEMP_ID">
        {{--<input type="hidden" id="HEADER" name="HEADER" value="1">
        <input type="hidden" id="FOOTER" name="FOOTER" value="1">
        <input type="hidden" id="DATA" name="DATA" value="1">--}}
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        <input type="hidden" class="form-control input-sm" maxlength="100" name="TEMP_CODE" @if(isset($dataOther->TEMP_CODE)) value="{{$dataOther->TEMP_CODE}}" @else value="" @endif>
        <input type="hidden" class="form-control input-sm" maxlength="100" name="PRODUCT_CODE" @if(isset($dataOther->PRODUCT_CODE)) value="{{$dataOther->PRODUCT_CODE}}" @else value="" @endif>
        <input type="hidden" class="form-control input-sm" maxlength="100" name="ORG_CODE" @if(isset($dataOther->ORG_CODE)) value="{{$dataOther->ORG_CODE}}" @else value="" @endif>
        <input type="hidden" class="form-control input-sm" maxlength="100" name="TITLE" @if(isset($dataOther->TITLE)) value="{{$dataOther->TITLE}}" @else value="TAO MAU" @endif>
        <input type="hidden" class="form-control input-sm" maxlength="100" name="TEXT_CA" @if(isset($dataOther->TEXT_CA)) value="{{$dataOther->TEXT_CA}}" @else value="BO TRUONG" @endif>

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="form_group">
                <div class="form-group">
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('LIST_REPLACE')}}</label>
                            <textarea class="form-control input-sm" name="LIST_REPLACE" id="form_{{$formNameOther}}_LIST_REPLACE" rows="3"></textarea>
                        </div>
                        <div class="col-lg-8">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('PARAM_REPLACE')}}</label>
                            <textarea class="form-control input-sm" placeholder="AAA;BBB;CCC" name="PARAM_REPLACE" id="form_{{$formNameOther}}_PARAM_REPLACE" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('PACK_CODE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="PACK_CODE" id="form_{{$formNameOther}}_PACK_CODE">
                        </div>
                        <div class="col-lg-9">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('URL_PAGE_ADD')}}</label>
                            <input type="text" class="form-control input-sm" placeholder="ID quyền lợi sau khi upload" maxlength="100" name="URL_PAGE_ADD" id="form_{{$formNameOther}}_URL_PAGE_ADD">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATA_SRC')}}</label>
                            <input type="text" class="form-control input-sm" placeholder="" maxlength="100" name="DATA_SRC"  @if(isset($dataOther->DATA_SRC)) id="form_{{$formNameOther}}_DATA_SRC" @else value="JSON" @endif>
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('TEMP_TYPE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="TEMP_TYPE" @if(isset($dataOther->TEMP_TYPE)) id="form_{{$formNameOther}}_TEMP_TYPE" @else value="PDF" @endif>
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('DATA_TYPE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="DATA_TYPE"  @if(isset($dataOther->DATA_TYPE)) id="form_{{$formNameOther}}_DATA_TYPE" @else value="HTML" @endif >
                        </div>

                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('TYPE_SEND')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="TYPE_SEND"  @if(isset($dataOther->TYPE_SEND)) id="form_{{$formNameOther}}_TYPE_SEND" @else value="ONE" @endif >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-2">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('IS_ATTACH')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="IS_ATTACH" id="form_{{$formNameOther}}_IS_ATTACH">
                        </div>
                        <div class="col-lg-2">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('IS_BACK_DATE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="IS_BACK_DATE" id="form_{{$formNameOther}}_IS_BACK_DATE">
                        </div>
                        <div class="col-lg-2">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('ORIEN')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="ORIEN" id="form_{{$formNameOther}}_ORIEN">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('PAGE_SIZE')}}</label>
                            <input type="text" class="form-control input-sm" maxlength="100" name="PAGE_SIZE" id="form_{{$formNameOther}}_PAGE_SIZE">
                        </div>
                        <div class="col-lg-3">
                            <label for="NAME" class="text-right">{{viewLanguage('Trạng thái')}}</label>
                            <select class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formNameOther}}_IS_ACTIVE" >
                                {!! $optionStatus !!}
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('CONTENT DATA TEMPLATE')}}</label>
                            <textarea class="form-control input-sm" rows="10" name="DATA">{{$contentDataTemplate}}</textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-6">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('CONTENT HEADER TEMPLATE')}}</label>
                            <textarea class="form-control input-sm" rows="10" name="HEADER">{{$contentHeaderTemplate}}</textarea>
                        </div>
                        <div class="col-lg-6">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('CONTENT FOOTER TEMPLATE')}}</label>
                            <textarea class="form-control input-sm" rows="10" name="FOOTER">{{$contentFooterTemplate}}</textarea>
                        </div>
                    </div>

                    {{--<div class="row form-group">
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right">{{viewLanguage('Heater')}}</label>
                            <label title="{{viewLanguage('Upload Header')}}" for="inputFileHeader" class="w-100 btn-transition btn btn-outline-success">
                                <input type="file" name="inputFileHeader" id="inputFileHeader" style="display:none">
                                <i class="fa fa-share-square"></i> {{viewLanguage('Upload Header(html)')}}
                            </label>
                        </div>
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right">{{viewLanguage('Footer')}}</label>
                            <label title="{{viewLanguage('Upload Footer')}}" for="inputFileFooter" class="w-100 btn-transition btn btn-outline-success">
                                <input type="file" name="inputFileFooter" id="inputFileFooter" style="display:none">
                                <i class="fa fa-share-square"></i> {{viewLanguage('Upload Footer(html)')}}
                            </label>
                        </div>
                        <div class="col-lg-4">
                            <label for="NAME" class="text-right">{{viewLanguage('Data')}}</label>
                            <label title="{{viewLanguage('Upload Data')}}" for="inputFileData" class="w-100 btn-transition btn btn-outline-success">
                                <input type="file" name="inputFileData" id="inputFileData" style="display:none">
                                <i class="fa fa-share-square"></i> {{viewLanguage('Upload Data(html)')}}
                            </label>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" id="submitEditTemplate" ><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
        $("#submitEditTemplate").click(function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            // Get form
            submitAjaxFormMultipart('form_{{$formNameOther}}','submitEditTemplate','{{$urlActionOtherItem}}')
        });
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    function submitAjaxFormMultipart(form_id,btnSubmit,urlAjax){
        var form = $('#'+form_id)[0];
        // Create an FormData object
        var data = new FormData(form);

        // disabled the submit button
        $("#"+btnSubmit).prop("disabled", true);
        $('#loaderPopup').show();
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: urlAjax,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (res) {
                $('#loaderPopup').hide();
                $("#"+btnSubmit).prop("disabled", false);
                if (res.success == 1) {
                    $('#sys_showPopupCommon').modal('hide');
                    jqueryCommon.showMsg('success', res.message);
                    if (res.loadPage == 1) {
                        location.reload();
                    } else {
                        $('#' + res.divShowInfor).html(res.html);
                    }
                } else {
                    jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.message);
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        });
    }
</script>
