@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    {{--@include('Systems.templateSystem.component.formSearch')--}}

    {{--list data---}}
    @include('Systems.templateSystem.component.listDataTemplates')
@stop
