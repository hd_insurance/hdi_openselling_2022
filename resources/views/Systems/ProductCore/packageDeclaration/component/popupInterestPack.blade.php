<div class="modal-content" id="{{$formNameOther}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="actionUpdate" name="actionUpdate" value="EditBenefitPack">
        <input type="hidden" id="listBenefits" name="listBenefits" value="{{json_encode($listBenefits)}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body paddingBottom-unset">
            <div class="form-group" @if(count($listBenefits) > 7)style="height: 500px; overflow: scroll"@endif>
                <table class="table table-bordered table-hover marginBottom-unset">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        <th width="5%" class="text-center middle"><input type="checkbox" class="check" id="checkAllOrder"></th>
                        <th width="45%" class="text-left middle">Quyền lợi</th>
                        <th width="20%" class="text-center middle">Mức quyền lợi</th>
                        <th width="18%" class="text-center middle">Phí bảo hiểm</th>
                        <th width="12%" class="text-center middle">Đơn vị tính</th>
                    </tr>
                    </thead>

                    <tbody id="table_claim">
                    @foreach ($listBenefits as $key => $benefits)
                        <tr>
                            <td class="text-center middle">
                                <input class="check" type="checkbox" id="be_code_{{$benefits->BE_CODE}}" name="be_code_{{$benefits->BE_CODE}}" @if($benefits->IS_CHECK == STATUS_INT_MOT) checked @endif>
                                <br/>{{$key+1}}
                            </td>
                            <td class="text-left middle">{{$benefits->BENEFIT_NAME}}</td>
                            <td class="text-center middle">
                                <input class="form-control input-sm" type="text" id="benefit_amount_{{$benefits->BE_CODE}}" name="benefit_amount_{{$benefits->BE_CODE}}" value="{{$benefits->BENEFIT_AMOUNT}}">
                            </td>
                            <td class="text-center middle">
                                <input class="form-control input-sm" type="text" id="fees_{{$benefits->BE_CODE}}" name="fees_{{$benefits->BE_CODE}}" value="{{$benefits->FEES}}">
                            </td>
                            <td class="text-center middle">
                                <select class="form-control input-sm" id="unit_{{$benefits->BE_CODE}}" name="unit_{{$benefits->BE_CODE}}">
                                    @foreach($arrDiscountUnit as $keyDis => $nameDis)
                                        <option value="{{$keyDis}}">{{$nameDis}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        <div class="modal-footer">
            @if($is_root || $permission_edit || $permission_add)
            <button type="button" class="btn btn-primary"
                onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i>{{viewLanguage('Cancel')}}</button>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy'});
    jQuery('.formatMoney').autoNumeric('init');
    $("#checkAllOrder").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));
    });

});

</script>
