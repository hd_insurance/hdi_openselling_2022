@if($is_root || $permission_edit || $permission_add)
    {{--<div class="">
         <a href="javascript:void(0);" class="btn btn-info detailOtherCommon" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['type'=>$tabOtherItem3,'itemId'=>0,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['PKGA_ID'=>0]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Thêm gói theo sản phẩm: ').$dataPrimary->PRODUCT_CODE}}" data-method="post" data-objectId="{{$dataPrimary->PRODUCT_CODE}}">
             <i class="pe-7s-plus"></i> {{viewLanguage('Add')}}
         </a>
    </div>--}}
@endif
<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="3%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="11%" class="text-center">{{viewLanguage('ORG CODE')}}</th>
            <th width="25%" class="text-center">{{viewLanguage('PACK')}}</th>

            <th width="15%" class="text-center">{{viewLanguage('Thông tin phí')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('Thông tin giảm giá')}}</th>
            <th width="18%" class="text-center">{{viewLanguage('Thông tin khác')}}</th>
            <th width="18%" class="text-center">{{viewLanguage('Thông tin chung')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('Status')}}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($listPackagesAssign) && $listPackagesAssign)
            @foreach ($listPackagesAssign as $kb => $itemPack)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>1,'type'=>$tabOtherItem1,'itemInfor'=>$itemPack,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Sửa gói theo sản phẩm: ').$itemPack->PRODUCT_CODE}}" data-method="post" data-objectId="{{$dataPrimary->PRODUCT_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>

                    <td class="text-left middle">{{$itemPack->ORG_CODE}}</td>
                    <td class="text-left middle">
                        <b class="font_10">Code: </b>{{$itemPack->PACK_CODE}}<br/>
                        {{$itemPack->PACK_NAME}}
                    </td>

                    <td class="text-left middle">
                        <b class="font_10">Fees: </b>{{numberFormat($itemPack->FEES)}}<br/>
                        <b class="font_10">Fees_type: </b>{{$itemPack->FEES_TYPE}}<br/>
                        <b class="font_10">Is_pay: </b>{{$itemPack->IS_PAY}}<br/>
                        <b class="font_10">Is_confirm: </b>{{$itemPack->IS_CONFIRM}}<br/>
                    </td>
                    <td class="text-left middle">
                        <b class="font_10">Discount_max: </b>{{numberFormat($itemPack->DISCOUNT_MAX)}}<br/>
                        <b class="font_10">Discount_unit: </b>{{numberFormat($itemPack->DISCOUNT_UNIT)}}<br/>
                        <b class="font_10">Min_discount_val: </b>{{numberFormat($itemPack->MIN_DISCOUNT_VAL)}}<br/>
                        <b class="font_10">Max_discount_val: </b>{{numberFormat($itemPack->MAX_DISCOUNT_VAL)}}<br/>
                    </td>
                    <td class="text-left middle">
                        {{--<b class="font_10">Block: </b>{{$itemPack->BLOCK}}<br/>
                        <b class="font_10">Block_unit: </b>{{$itemPack->BLOCK_UNIT}}<br/>--}}
                        <b class="font_10">User_code: </b>{{$itemPack->USER_CODE}}<br/>
                    </td>
                    <td class="text-left middle">
                        <b class="font_10">Currency: </b>{{$itemPack->CURRENCY}}<br/>
                        {{--<b class="font_10">Tip: </b>{{$itemPack->TIP}}<br/>
                        <b class="font_10">Tip_unit: </b>{{$itemPack->TIP_UNIT}}<br/>--}}
                    </td>
                    <td class="text-center middle">
                        @if($itemPack->IS_APPROVED == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
