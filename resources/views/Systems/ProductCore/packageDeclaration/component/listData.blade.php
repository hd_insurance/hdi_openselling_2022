@include('Systems.ProductCore.productDeclaration.component.groupTabPage')
{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-3">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class=" col-lg-4">
                <label for="user_group">Sản phẩm</label>
                <select class="form-control input-sm chosen-select w-100" name="PRODUCT_CODE" id="PRODUCT_CODE">
                    {!! $optionProductCore !!}}
                </select>
            </div>
            <div class=" col-lg-2">
                <label for="user_group">Trạng thái</label>
                <select class="form-control input-sm chosen-select w-100" name="IS_ACTIVE" id="IS_ACTIVE">
                    {!! $optionStatus !!}}
                </select>
            </div>
            <div class="col-lg-3 marginT20">
                <button class="btn btn-primary" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                <a href="javascript:void(0);" class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                    Thêm mới
                </a>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                    <th width="15%" class="text-left">{{viewLanguage('Pack')}}</th>
                    <th width="15%" class="text-left">{{viewLanguage('Thông tin sản phẩm')}}</th>

                    <th width="15%" class="text-left">{{viewLanguage('Thông tin độ tuổi')}}</th>
                    <th width="15%" class="text-left">{{viewLanguage('Thông tin phí')}}</th>
                    {{--<th width="15%" class="text-center">{{viewLanguage('Thông tin khác')}}</th>--}}

                    <th width="15%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt+$key+1}}</td>
                        <td class="text-left middle">
                            @if(trim($item->PACK_CODE) != '')<b>{{$item->PACK_CODE}}</b><br/>@endif
                            @if(trim($item->PACK_NAME) != ''){{$item->PACK_NAME}}<br/>@endif
                            @if(trim($item->PACK_NAME_EN) != ''){{$item->PACK_NAME_EN}}<br/>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->PRODUCT_CODE) != '')<b class="font_10">PRODUCT_CODE: </b>{{$item->PRODUCT_CODE}}<br/>@endif
                            @if(trim($item->CATEGORY) != '')<b class="font_10">CATEGORY: </b>{{$item->CATEGORY}}<br/>@endif
                            @if(trim($item->CURRENCY) != '')<b class="font_10">CURRENCY: </b>{{$item->CURRENCY}}<br/>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->AGE_MIN) != '')<b class="font_10">AGE_MIN: </b>{{$item->AGE_MIN}}<br/>@endif
                            @if(trim($item->AGE_MIN_UNIT) != '')<b class="font_10">AGE_MIN_UNIT: </b>{{$item->AGE_MIN_UNIT}}<br/>@endif
                            @if(trim($item->AGE_MAX) != '')<b class="font_10">AGE_MAX: </b>{{$item->AGE_MAX}}<br/>@endif
                            @if(trim($item->AGE_MAX_UNIT) != '')<b class="font_10">AGE_MAX_UNIT: </b>{{$item->AGE_MAX_UNIT}}<br/>@endif
                        </td>
                        <td class="text-left middle">
                            @if(trim($item->FEES_MIN) != '')<b class="font_10">FEES_MIN: </b>{{$item->FEES_MIN}}<br/>@endif
                            @if(trim($item->FEES_DEFAULT) != '')<b class="font_10">FEES_DEFAULT: </b>{{$item->FEES_DEFAULT}}<br/>@endif
                            @if(trim($item->FEES_MAX) != '')<b class="font_10">AGE_MAX: </b>{{$item->FEES_MAX}}<br/>@endif
                            @if(trim($item->FEES_TYPE) != '')<b class="font_10">FEES_TYPE: </b>{{$item->FEES_TYPE}}<br/>@endif
                        </td>
                        {{--<td class="text-left middle">
                            @if(trim($item->GRACE_MIN) != '')<b class="font_10">GRACE_MIN: </b>{{$item->GRACE_MIN}}<br/>@endif
                            @if(trim($item->GRACE_MAX) != '')<b class="font_10">GRACE_MAX: </b>{{$item->GRACE_MAX}}<br/>@endif
                            @if(trim($item->GRACE_DEFAULT) != '')<b class="font_10">GRACE_DEFAULT: </b>{{$item->GRACE_DEFAULT}}<br/>@endif
                            @if(trim($item->IS_CUMULATIVE) != '')<b class="font_10">FEES_TYPE: </b>{{$item->IS_CUMULATIVE}}<br/>@endif
                        </td>--}}

                        <td class="text-center middle">
                            @if($is_root || $permission_edit || $permission_add)
                            {{--<a href="javascript:void(0);"class="color_warning sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Thêm gói')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item,'is_copy'=>STATUS_INT_MOT])}}" data-objectId="{{$item->PKG_ID}}">
                                <i class="fa fa-copy fa-2x"></i>
                            </a>
                            &nbsp;
                            <a href="javascript:void(0);"class="color_warning sys_show_popup_common" data-size="1" data-form-name="detailItem" title="{{viewLanguage('Cập nhật thông tin gói')}}" data-method="get" data-url="{{$urlGetItem}}" data-input="{{json_encode(['item'=>$item])}}" data-objectId="{{$item->PKG_ID}}">
                                <i class="fa fa-pencil-square-o fa-2x"></i>
                            </a>--}}
                            &nbsp;
                            <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailItem" data-input="{{json_encode(['item'=>$item])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->PRODUCT_CODE}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="{{$item->PKG_ID}}">
                                <i class="fa fa-pencil-square-o fa-2x"></i>
                            </a>&nbsp;&nbsp;
                            <a href="javascript:void(0);"  class="color_hdi" onclick="jqueryCommon.getDataByAjax(this);" data-loading="1" data-show="1" data-div-show="content-page-right" data-form-name="listInterestPack" data-url="{{$urlAjaxGetData}}" data-function-action="_functionGetData" data-method="post" data-input="{{json_encode(['funcAction'=>'getListInterestPack','dataPack'=>$item])}}" data-objectId="222" title="{{viewLanguage('Danh sách quyền lợi của gói')}}">
                                <i class="fa fa-list fa-2x"></i>
                            </a>
                            @endif
                            @if($item->IS_ACTIVE == STATUS_INT_MOT)
                                <a href="javascript:void(0);" style="color: green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                            @else
                                <a href="javascript:void(0);" style="color: red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>
