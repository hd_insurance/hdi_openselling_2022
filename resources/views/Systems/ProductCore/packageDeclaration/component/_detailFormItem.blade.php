{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin gói: &nbsp;<span class="showInforItem" data-field="PACK_NAME"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-3">
                    PACK_CODE: <b class="showInforItem" data-field="PACK_CODE"></b>
                </div>
                <div class="col-lg-9">
                    PACK_NAME: <b class="showInforItem" data-field="PACK_NAME"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    FEES_DEFAULT: <b class="showInforItem" data-field="FEES_DEFAULT"></b>
                </div>
                <div class="col-lg-3">
                    FEES_TYPE: <b class="showInforItem" data-field="FEES_TYPE"></b>
                </div>
                <div class="col-lg-3">
                    CURRENCY: <b class="showInforItem" data-field="CURRENCY"></b>
                </div>
                <div class="col-lg-3">
                    IS_ACTIVE: <b class="showInforItem">@if(isset($dataPrimary->IS_ACTIVE) && isset($arrStatus[$dataPrimary->IS_ACTIVE])) {{$arrStatus[$dataPrimary->IS_ACTIVE]}}@endif</b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        @if($objectId > 0)
            Thông tin gói - &nbsp;<span class="showInforItem" data-field="PACK_NAME"></span>
        @else
            Thông tin gói
        @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" @if(isset($dataPrimary->PKG_ID))value="1" @else value="0" @endif>
        <input type="hidden" name="PKG_ID" id="form_{{$formName}}_PKG_ID" value="@if(isset($dataPrimary->PKG_ID)){{$dataPrimary->PKG_ID}}@endif">
        <input type="hidden" name="GROUP_INSUR" id="GROUP_INSUR" value="CON_NGUOI">
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-lg-12">
                <label for="NAME" class="text-right control-label ">{{viewLanguage('Product_code')}}</label><span class="red">(*)</span>
                @if($objectId == 0 || $is_copy == STATUS_INT_MOT)
                    <select class="form-control input-sm chosen-select w-100" required name="PRODUCT_CODE" id="form_{{$formName}}_PRODUCT_CODE">
                        {!! $optionProductCore !!}
                    </select>
                @else
                    <select class="form-control input-sm chosen-select w-100" required name="PRODUCT_CODE_2" id="form_{{$formName}}_PRODUCT_CODE" disabled>
                        {!! $optionProductCore !!}
                    </select>
                    <input type="hidden" name="PRODUCT_CODE" value="{{$dataPrimary->PRODUCT_CODE}}">
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Pack_code')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="PACK_CODE"  @if($is_copy == STATUS_INT_KHONG )id="form_{{$formName}}_PACK_CODE"@endif @if($objectId > 0) readonly @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Pack_name')}} </label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="PACK_NAME" @if($is_copy == STATUS_INT_KHONG)id="form_{{$formName}}_PACK_NAME"@endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Pack_name English')}} </label>
                <input type="text" class="form-control input-sm" name="PACK_NAME_EN" @if($is_copy == STATUS_INT_KHONG)id="form_{{$formName}}_PACK_NAME_EN"@endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right">{{viewLanguage('Status')}}></label><span class="red">(*)</span>
                <select class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$formName}}_IS_ACTIVE" >
                    {!! $optionStatus !!}
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Age_min')}} </label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="AGE_MIN" @if($objectId > 0 || $is_copy == STATUS_INT_MOT)id="form_{{$formName}}_AGE_MIN" @else value="0" @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Age_min_unit')}} </label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="AGE_MIN_UNIT" @if($objectId > 0 || $is_copy == STATUS_INT_MOT)id="form_{{$formName}}_AGE_MIN_UNIT" @else value="D" @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Age_max')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="AGE_MAX"  @if($objectId > 0 || $is_copy == STATUS_INT_MOT)id="form_{{$formName}}_AGE_MAX" @else value="0" @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Age_max_unit')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="AGE_MAX_UNIT" @if($objectId > 0 || $is_copy == STATUS_INT_MOT)id="form_{{$formName}}_AGE_MAX_UNIT" @else value="Y" @endif>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Fees_default')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="FEES_DEFAULT" id="form_{{$formName}}_FEES_DEFAULT">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Fees_type')}}</label><span class="red">(*)</span>
                <input type="text" class="form-control input-sm" required name="FEES_TYPE" @if($objectId > 0 || $is_copy == STATUS_INT_MOT)id="form_{{$formName}}_FEES_TYPE" @else value="PHI" @endif>
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Fees_min')}}</label>
                <input type="text" class="form-control input-sm" name="FEES_MIN" id="form_{{$formName}}_FEES_MIN">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Fees_max')}}</label>
                <input type="text" class="form-control input-sm" name="FEES_MAX" id="form_{{$formName}}_FEES_MAX">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-9">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Description')}}</label>
                <input type="text" class="form-control input-sm" name="DESCRIPTION" id="form_{{$formName}}_DESCRIPTION">
            </div>
            <div class="col-lg-3">
                <label for="NAME" class="text-right control-label">{{viewLanguage('Currency')}}</label>
                <input type="text" class="form-control input-sm" name="CURRENCY" @if($objectId > 0 || $is_copy == STATUS_INT_MOT)id="form_{{$formName}}_CURRENCY" @else value="VND" @endif>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
