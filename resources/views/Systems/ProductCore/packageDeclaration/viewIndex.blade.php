@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.ProductCore.packageDeclaration.component.formSearch')
    {{--list data---}}
    @include('Systems.ProductCore.packageDeclaration.component.listData')
@stop
