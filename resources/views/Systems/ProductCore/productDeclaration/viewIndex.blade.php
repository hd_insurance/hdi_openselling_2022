@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.ProductCore.productDeclaration.component.formSearch')

    {{--list data---}}
    @include('Systems.ProductCore.productDeclaration.component.listData')
@stop
