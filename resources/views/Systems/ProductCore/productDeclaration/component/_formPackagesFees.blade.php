<div class="modal-content" id="{{$formNameOther}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="dataPrimary" name="dataPrimary" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="PRODUCT_CODE" name="PRODUCT_CODE" @if(isset($dataPrimary->PRODUCT_CODE))value="{{$dataPrimary->PRODUCT_CODE}}"@endif>
        <input type="hidden" id="CATEGORY" name="CATEGORY" @if(isset($dataPrimary->CATEGORY))value="{{$dataPrimary->CATEGORY}}"@endif>

        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">
        <input type="hidden" id="form_{{$formNameOther}}_PKGF_ID" name="PKGF_ID">
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PACK_CODE')}}</label> <span class="red"> (*) - <a class="red font_12" href="#" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="addForm" data-input="{{json_encode([])}}" title="{{viewLanguage('Thêm mới Gói')}}" data-method="get" data-url="{{URL::route('packageDeclaration.ajaxGetItem')}}" data-objectId="0">Thêm mới</a></span>
                    @if($actionEdit == 0)
                        <select  class="form-control input-sm chosen-select w-100" required name="PACK_CODE" id="form_{{$formNameOther}}_PACK_CODE">
                            {!! $optionPackage !!}}
                        </select>
                    @else
                        <select  class="form-control input-sm chosen-select w-100" required name="PACK_CODE" id="form_{{$formNameOther}}_PACK_CODE" disabled>
                            {!! $optionPackage !!}}
                        </select>
                        <input type="hidden" name="PACK_CODE" value="{{$dataOther->PACK_CODE}}">
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TYPE_CODE')}}</label> <span class="red"> (*)</span>
                    @if($actionEdit == 0)
                        <select  class="form-control input-sm chosen-select w-100" required name="TYPE_CODE" id="form_{{$formNameOther}}_TYPE_CODE">
                            {!! $optionTypeCode !!}}
                        </select>
                    @else
                        <select  class="form-control input-sm chosen-select w-100" required name="TYPE_CODE" id="form_{{$formNameOther}}_TYPE_CODE" disabled>
                            {!! $optionTypeCode !!}}
                        </select>
                        <input type="hidden" name="TYPE_CODE" value="{{$dataOther->TYPE_CODE}}">
                    @endif

                </div>
                <div class="col-lg-4" >
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FROM_VALUE')}}</label>
                    <input type="text" class="form-control input-sm" name="FROM_VALUE" id="form_{{$formNameOther}}_FROM_VALUE">
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FROM_UNIT')}}</label>
                    <input type="text" class="form-control input-sm" name="FROM_UNIT" id="form_{{$formNameOther}}_FROM_UNIT">
                </div>
                {{--<div class="row" id="block_select_from" @if(isset($dataOther->TYPE_CODE) && $dataOther->TYPE_CODE == 'AGE') style="display: none" @endif>
                    <div class="col-lg-4" >
                        <label for="NAME" class="text-right control-label">{{viewLanguage('FROM_VALUE')}}</label>
                        <select class="form-control input-sm chosen-select w-100" required name="FROM_VALUE" id="form_{{$formNameOther}}_FROM_VALUE">
                            {!! $optionFromValue !!}}
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label for="NAME" class="text-right control-label">{{viewLanguage('FROM_UNIT')}}</label>
                        <select class="form-control input-sm chosen-select w-100" required name="FROM_UNIT" id="form_{{$formNameOther}}_FROM_UNIT">
                            {!! $optionFromUnit !!}}
                        </select>
                    </div>
                </div>--}}
            </div>
            <div class="row form-group">
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('OPERATORS')}}</label><span class="red"> (*)</span>
                    <select  class="form-control input-sm chosen-select w-100" required name="OPERATORS" id="form_{{$formNameOther}}_OPERATORS">
                        {!! $optionOperators !!}}
                    </select>
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('VALUE')}}</label>
                    <input type="text" class="form-control input-sm" name="VALUE" id="form_{{$formNameOther}}_VALUE">
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('VALUE_UNIT')}}</label>
                    <select  class="form-control input-sm chosen-select w-100" name="VALUE_UNIT" id="form_{{$formNameOther}}_VALUE_UNIT">
                        {!! $optionTypePrice !!}}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TO_VALUE')}}</label>
                    <input type="text" class="form-control input-sm" name="TO_VALUE" id="form_{{$formNameOther}}_TO_VALUE">
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TO_UNIT')}}</label>
                    <input type="text" class="form-control input-sm"  name="TO_UNIT" id="form_{{$formNameOther}}_TO_UNIT">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
    });
    function changeFromInput(form){
        var type_code = $('#form_'+form+'_TYPE_CODE').val();
        if(type_code == 'REGION'){

        }
    }
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
