<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="3%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="20%" class="text-center">{{viewLanguage('ORG_CODE')}}</th>
            <th width="20%" class="text-center">{{viewLanguage('STRUCT_CODE')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('IS_CONFIRM')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('IS_PAY')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('USER_REF')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('STATUS')}}</th>
        </tr>

        </thead>
        <tbody>
        @if(isset($listProductAssign) && !empty($listProductAssign))
            @foreach ($listProductAssign as $kb => $itemProAssign)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>1,'type'=>$tabOtherItem1,'itemInfor'=>$itemProAssign,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem1}}" title="{{viewLanguage('Chi tiết đối tác: ').$itemProAssign->ORG_CODE}}" data-method="post" data-objectId="{{$dataPrimary->PRODUCT_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>
                    <td class="text-left middle">{{$itemProAssign->ORG_CODE}}</td>
                    <td class="text-left middle">{{$itemProAssign->STRUCT_CODE}}</td>

                    <td class="text-center middle">{{$itemProAssign->IS_CONFIRM}}</td>
                    <td class="text-center middle">{{$itemProAssign->IS_PAY}}</td>
                    <td class="text-left middle">{{$itemProAssign->USER_REF}}</td>
                    <td class="text-center middle">
                        @if($itemProAssign->STATUS == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>

                    {{--<td class="text-left middle">
                        @if(trim($itemProAssign->CREATEDATE) != ''){{convertDateDMY($itemProAssign->CREATEDATE)}} <br/>@endif
                        @if(trim($itemProAssign->MODIFIEDDATE) != '')<span class="red">{{convertDateDMY($itemProAssign->MODIFIEDDATE)}}</span>@endif
                    </td>
                    <td class="text-left middle">
                        @if(trim($itemProAssign->CREATEBY) != ''){{$itemProAssign->CREATEBY}}<br/>@endif
                        @if(trim($itemProAssign->MODIFIEDBY) != '')<span class="red">{{$itemProAssign->MODIFIEDBY}}</span>@endif
                    </td>--}}
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
