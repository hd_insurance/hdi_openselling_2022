{{---ID > 0 và có thông tin data---}}
<div class="formInforItem @if($objectId <= 0)display-none-block @endif">

    <div class="card-header">
        @if($objectId > 0)
            Thông tin sản phẩm: &nbsp;<span class="showInforItem" data-field="PRODUCT_NAME"></span>
        @endif
    </div>
    <div class="marginT15">
        <div class="form-group" style="position: relative">
            @include('Layouts.BaseAdmin.buttonShowFormEdit')
            <div class="row form-group">
                <div class="col-lg-3">
                    PRODUCT_CODE: <b class="showInforItem" data-field="PRODUCT_CODE"></b>
                </div>
                <div class="col-lg-9">
                    PRODUCT_NAME: <b class="showInforItem" data-field="PRODUCT_NAME"></b>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    CATEGORY: <b class="showInforItem" data-field="CATEGORY"></b>
                </div>
                <div class="col-lg-3">
                    GROUP_INSUR: <b class="showInforItem" data-field="GROUP_INSUR"></b>
                </div>
                <div class="col-lg-3">
                    CURRENCY: <b class="showInforItem" data-field="CURRENCY"></b>
                </div>
                <div class="col-lg-3">
                    STATUS: <b class="showInforItem" data-field="STATUS"></b>
                </div>
            </div>
        </div>
    </div>
</div>

{{----Edit và thêm mới----}}
<div class="formEditItem @if($objectId > 0)display-none-block @endif" >
    <div class="card-header">
        @if($objectId > 0)
            Thông tin sản phẩm - &nbsp;<span class="showInforItem" data-field="PRODUCT_NAME"></span>
        @else
            Thông tin sản phẩm
        @endif
    </div>
    <div class="marginT15">
        <input type="hidden" id="objectId" name="objectId" @if(isset($dataPrimary->BP_ID))value="1" @else value="0" @endif>
        <input type="hidden" name="BP_ID" id="form_{{$formName}}_BP_ID" value="@if(isset($dataPrimary->BP_ID)){{$dataPrimary->BP_ID}}@endif">
        <input type="hidden" id="url_action" name="url_action" value="{{$urlPostItem}}">
        <input type="hidden" id="formName" name="formName" value="{{$formName}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="load_page" name="load_page" value="{{STATUS_INT_KHONG}}">
        <input type="hidden" id="div_show_edit_success" name="div_show_edit_success" value="formShowEditSuccess">

        {{ csrf_field() }}
        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PRODUCT_CODE')}}</label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="100" required name="PRODUCT_CODE" id="form_{{$formName}}_PRODUCT_CODE" @if($objectId > 0) readonly @endif>
                </div>
                <div class="col-lg-9">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PRODUCT_NAME')}} </label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="100" required name="PRODUCT_NAME" id="form_{{$formName}}_PRODUCT_NAME">
                </div>
            </div>
        </div>
        <div class="form-group">
            @if($objectId > 0)
                @if(isset($dataPrimary->PRODUCT_NAME_EN) && trim($dataPrimary->PRODUCT_NAME_EN) != '')
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="NAME" class="text-right control-label">{{viewLanguage('PRODUCT_NAME_EN 2')}} </label>
                            <input type="text" class="form-control input-sm" name="PRODUCT_NAME_EN" id="form_{{$formName}}_PRODUCT_NAME_EN">
                        </div>
                    </div>
                @endif
            @else
            <div class="row">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PRODUCT_NAME_EN')}} </label>
                    <input type="text" class="form-control input-sm" name="PRODUCT_NAME_EN" id="form_{{$formName}}_PRODUCT_NAME_EN">
                </div>
            </div>
            @endif
        </div>
        <div class="form-group">
            <div class="row">
                {{--<div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('GROUP_INSUR')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="150" name="GROUP_INSUR" id="form_{{$formName}}_GROUP_INSUR">
                </div>--}}
                <div class="col-lg-6">
                    <label for="status" class="control-label">{{viewLanguage('CATEGORY')}}</label> <span class="red"> (*)</span>
                    <select  class="form-control input-sm chosen-select w-100" required name="CATEGORY" id="form_{{$formName}}_CATEGORY">
                        {!! $optionCategory !!}}
                    </select>
                </div>
                <div class="col-lg-3">
                    <label for="status" class="control-label">{{viewLanguage('GROUP_INSUR')}}</label> <span class="red"> (*)</span>
                    <select  class="form-control input-sm chosen-select w-100" required name="GROUP_INSUR" id="form_{{$formName}}_GROUP_INSUR">
                        {!! $optionGroupInsur !!}}
                    </select>
                </div>
                <div class="col-lg-3">
                    <label for="status" class="control-label">{{viewLanguage('STATUS')}}</label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="150" name="STATUS"  @if($objectId == 0) value="APPROVED" @else id="form_{{$formName}}_STATUS" @endif>
                    {{--<select  class="form-control input-sm" required name="STATUS" id="form_{{$formName}}_STATUS">
                        {!! $optionStatus !!}}
                    </select>--}}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-lg-2">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('AGE_MIN')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="10" required name="AGE_MIN" @if($objectId > 0)id="form_{{$formName}}_AGE_MIN" @else value="0" @endif>
                </div>
                <div class="col-lg-2">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('AGE_MIN_UNIT')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="10" required name="AGE_MIN_UNIT"  @if($objectId > 0)id="form_{{$formName}}_AGE_MIN_UNIT" @else value="D" @endif>
                </div>
                <div class="col-lg-2">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('AGE_MAX')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="10" required name="AGE_MAX"  @if($objectId > 0)id="form_{{$formName}}_AGE_MAX" @else value="0" @endif>
                </div>
                <div class="col-lg-2">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('AGE_MAX_UNIT')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="10" required name="AGE_MAX_UNIT"  @if($objectId > 0)id="form_{{$formName}}_AGE_MAX_UNIT" @else value="Y" @endif>
                </div>
                <div class="col-lg-2">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('CURRENCY')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="10" required name="CURRENCY" @if($objectId == 0) value="VND" @else id="form_{{$formName}}_CURRENCY" @endif>
                </div>
                <div class="col-lg-2">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('VAT_PERCENT')}} </label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" maxlength="100" required name="VAT_PERCENT"  @if($objectId > 0)id="form_{{$formName}}_VAT_PERCENT" @else value="0" @endif>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES_MIN')}} </label>
                    <input type="number" class="form-control input-sm" name="FEES_MIN" id="form_{{$formName}}_FEES_MIN">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES_DEFAULT')}} </label>
                    <input type="number" class="form-control input-sm" name="FEES_DEFAULT" id="form_{{$formName}}_FEES_DEFAULT">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES_MAX')}} </label>
                    <input type="number" class="form-control input-sm" name="FEES_MAX" id="form_{{$formName}}_FEES_MAX">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES_TYPE')}} </label>
                    <input type="text" class="form-control input-sm" maxlength="100" name="FEES_TYPE" @if($objectId > 0)id="form_{{$formName}}_FEES_TYPE" @else value="PHI" @endif>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        showDataIntoForm('form_{{$formName}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
