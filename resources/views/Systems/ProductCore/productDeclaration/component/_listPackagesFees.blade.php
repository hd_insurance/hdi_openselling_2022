<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('PRODUCT_CODE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('PACK_CODE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('TYPE_CODE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('FROM_VALUE')}}</th>

            <th width="10%" class="text-center">{{viewLanguage('FROM')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('TO')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('VALUE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('OPERATORS')}}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($listPackagesFees) && $listPackagesFees)
            @foreach ($listPackagesFees as $kb => $itemPack)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-loading="1" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>1,'type'=>$tabOtherItem4,'itemInfor'=>$itemPack,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem4}}" title="{{viewLanguage('Sửa phí cho gói theo sản phẩm: ').$itemPack->PRODUCT_CODE}}" data-method="post" data-objectId="{{$dataPrimary->PRODUCT_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                        @if($is_root || $permission_full)
                            &nbsp;<a href="javascript:void(0);" class="red" title="Xóa" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-loading="1" data-show="2" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxActionOther" data-input="{{json_encode(['itemId'=>1,'actionCode'=>'removePackagesFees','type'=>$tabOtherItem4,'itemInfor'=>$itemPack,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}"  data-show-id="{{$tabOtherItem4}}" data-method="post" data-objectId="{{$dataPrimary->PRODUCT_CODE}}">
                                 <i class="pe-7s-trash fa-2x"></i>
                             </a>
                        @endif
                    </td>

                    <td class="text-left middle">{{$itemPack->PRODUCT_CODE}}</td>
                    <td class="text-left middle">{{$itemPack->PACK_CODE}}</td>
                    <td class="text-left middle">{{$itemPack->TYPE_CODE}}</td>
                    <td class="text-left middle">{{$itemPack->FROM_VALUE}}</td>

                    <td class="text-left middle">
                        <b class="font_10">Category: </b>{{$itemPack->CATEGORY}}<br/>
                        <b class="font_10">From_unit: </b>{{$itemPack->FROM_UNIT}}<br/>
                    </td>
                    <td class="text-left middle">
                        <b class="font_10">To_value: </b>{{$itemPack->TO_VALUE}}<br/>
                        <b class="font_10">To_unit: </b>{{$itemPack->TO_UNIT}}<br/>
                    </td>
                    <td class="text-left middle">
                        <b class="font_10">Value: </b>{{$itemPack->VALUE}}<br/>
                        <b class="font_10">Value_unit: </b>{{$itemPack->VALUE_UNIT}}<br/>
                    </td>
                    <td class="text-left middle">{{$itemPack->OPERATORS}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
