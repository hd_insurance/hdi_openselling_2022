<div class="modal-content" id="{{$formNameOther}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="dataPrimary" name="dataPrimary" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="PRODUCT_CODE" name="PRODUCT_CODE" @if(isset($dataPrimary->PRODUCT_CODE))value="{{$dataPrimary->PRODUCT_CODE}}"@endif>

        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">
        <input type="hidden" id="form_{{$formNameOther}}_PKGA_ID" name="PKGA_ID">
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-7">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PACK_CODE')}}</label> <span class="red"> (*)</span>
                    @if($actionEdit == 0)
                        <select  class="form-control input-sm chosen-select w-100" required name="PACK_CODE" id="form_{{$formNameOther}}_PACK_CODE">
                            {!! $optionPackage !!}}
                        </select>
                    @else
                        <select  class="form-control input-sm chosen-select w-100" required name="PACK_CODE" id="form_{{$formNameOther}}_PACK_CODE" disabled>
                            {!! $optionPackage !!}}
                        </select>
                        <input type="hidden" name="PACK_CODE" value="{{$dataOther->PACK_CODE}}">
                    @endif
                </div>
                <div class="col-lg-5">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('PACK_NAME')}}</label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required name="PACK_NAME" id="form_{{$formNameOther}}_PACK_NAME">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('ORG_CODE')}}</label> <span class="red"> (*)</span>
                    @if($actionEdit == 0)
                        <select  class="form-control input-sm chosen-select w-100" required name="ORG_CODE" id="form_{{$formNameOther}}_ORG_CODE">
                            {!! $optionOrgCode !!}}
                        </select>
                    @else
                        <select  class="form-control input-sm chosen-select w-100" required name="ORG_CODE" id="form_{{$formNameOther}}_ORG_CODE" disabled>
                            {!! $optionOrgCode !!}}
                        </select>
                        <input type="hidden" name="ORG_CODE" value="{{$dataOther->ORG_CODE}}">
                    @endif
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('CATEGORY')}}</label><span class="red"> (*)<a class="red font_12" href="#" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="addForm" data-input="{{json_encode([])}}" title="{{viewLanguage('Thêm mới Category')}}" data-method="get" data-url="{{URL::route('categoryDeclaration.ajaxGetItem')}}" data-objectId="0">Thêm mới</a></span>
                    <select  class="form-control input-sm chosen-select w-100" required name="CATEGORY" id="form_{{$formNameOther}}_CATEGORY">
                        {!! $optionCategory !!}}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES')}}</label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required name="FEES" id="form_{{$formNameOther}}_FEES">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('FEES_TYPE')}}</label> <span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required name="FEES_TYPE" @if($obj_id > 0)id="form_{{$formNameOther}}_FEES_TYPE" @else value="PHI" @endif>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('CURRENCY')}}</label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" required name="CURRENCY" @if($obj_id > 0)id="form_{{$formNameOther}}_CURRENCY" @else value="VND" @endif>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('IS_APPROVED')}}</label><span class="red"> (*)</span>
                    <select  class="form-control input-sm" required name="IS_APPROVED" id="form_{{$formNameOther}}_IS_APPROVED">
                        {!! $optionStatus !!}}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('DISCOUNT_MAX')}}</label><span class="red"> (*)</span>
                    <input type="text" class="form-control input-sm" name="DISCOUNT_MAX" required @if($obj_id > 0)id="form_{{$formNameOther}}_DISCOUNT_MAX" @else value="0" @endif>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('DISCOUNT_UNIT')}}</label>
                    <input type="text" class="form-control input-sm" name="DISCOUNT_UNIT" id="form_{{$formNameOther}}_DISCOUNT_UNIT">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('MIN_DISCOUNT_VAL')}}</label>
                    <input type="text" class="form-control input-sm" name="MIN_DISCOUNT_VAL" id="form_{{$formNameOther}}_MIN_DISCOUNT_VAL">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('MAX_DISCOUNT_VAL')}}</label>
                    <input type="text" class="form-control input-sm" name="MAX_DISCOUNT_VAL" id="form_{{$formNameOther}}_MAX_DISCOUNT_VAL">
                </div>
            </div>
            {{--<div class="row form-group">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TIP')}}</label>
                    <input type="text" class="form-control input-sm"  name="TIP" id="form_{{$formNameOther}}_TIP">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('TIP_UNIT')}}</label>
                    <input type="text" class="form-control input-sm"  name="TIP_UNIT" id="form_{{$formNameOther}}_TIP_UNIT">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('BLOCK')}}</label>
                    <input type="text" class="form-control input-sm"  name="BLOCK" id="form_{{$formNameOther}}_BLOCK">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('BLOCK_UNIT')}}</label>
                    <input type="text" class="form-control input-sm"  name="BLOCK_UNIT" id="form_{{$formNameOther}}_BLOCK_UNIT">
                </div>
            </div>--}}
            <div class="row form-group">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('IS_CONFIRM')}}</label>
                    <input type="text" class="form-control input-sm"  name="IS_CONFIRM" id="form_{{$formNameOther}}_IS_CONFIRM">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('IS_PAY')}}</label>
                    <input type="text" class="form-control input-sm"  name="IS_PAY" id="form_{{$formNameOther}}_IS_PAY">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('USER_CODE')}}</label>
                    <input type="text" class="form-control input-sm"  name="USER_CODE" id="form_{{$formNameOther}}_USER_CODE">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('STRUCT_CODE')}}</label>
                    <input type="text" class="form-control input-sm" name="STRUCT_CODE" id="form_{{$formNameOther}}_STRUCT_CODE">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
