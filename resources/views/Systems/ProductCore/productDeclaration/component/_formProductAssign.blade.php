<div class="modal-content" id="{{$formNameOther}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$formNameOther}}">
        <input type="hidden" id="objectId" name="objectId" value="{{$obj_id}}">
        <input type="hidden" id="formName" name="formName" value="{{$formNameOther}}">
        <input type="hidden" id="typeTab" name="typeTab" value="{{$typeTab}}">
        <input type="hidden" id="dataPrimary" name="dataPrimary" value="{{json_encode($dataPrimary)}}">
        <input type="hidden" id="PRODUCT_CODE" name="PRODUCT_CODE" @if(isset($dataPrimary->PRODUCT_CODE))value="{{$dataPrimary->PRODUCT_CODE}}"@endif>
        <input type="hidden" id="PRODUCT_NAME" name="PRODUCT_NAME" @if(isset($dataPrimary->PRODUCT_NAME))value="{{$dataPrimary->PRODUCT_NAME}}"@endif>
        <input type="hidden" id="PRODUCT_NAME_EN" name="PRODUCT_NAME_EN" @if(isset($dataPrimary->PRODUCT_NAME_EN))value="{{$dataPrimary->PRODUCT_NAME_EN}}"@endif>

        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($dataOther)}}">
        <input type="hidden" id="form_{{$formNameOther}}_PROA_ID" name="PROA_ID">
        <input type="hidden" id="{{$formNameOther}}ACTION_FORM" name="ACTION_FORM" value="{{$actionEdit}}">
        <input type="hidden" id="{{$formNameOther}}typeTabAction" name="typeTabAction" value="{{$typeTab}}">
        <input type="hidden" id="{{$formNameOther}}divShowIdAction" name="divShowIdAction" value="{{$divShowId}}">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('ORG_CODE')}}</label> <span class="red"> (*)</span>
                    @if($actionEdit == 0)
                    <select  class="form-control input-sm chosen-select w-100" required name="ORG_CODE" id="form_{{$formNameOther}}_ORG_CODE" >
                        {!! $optionOrgCode !!}}
                    </select>
                    @else
                        <select  class="form-control input-sm chosen-select w-100" required name="ORG_CODE" id="form_{{$formNameOther}}_ORG_CODE" disabled>
                            {!! $optionOrgCode !!}}
                        </select>
                        <input type="hidden" name="ORG_CODE" value="{{$dataOther->ORG_CODE}}">
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-9">
                    <label for="status" class="control-label">{{viewLanguage('CATEGORY')}}</label> <span class="red"> (*)   - <a class="red font_12" href="#" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="addForm" data-input="{{json_encode([])}}" title="{{viewLanguage('Thêm mới Category')}}" data-method="get" data-url="{{URL::route('categoryDeclaration.ajaxGetItem')}}" data-objectId="0">Thêm mới</a></span>
                    <select  class="form-control input-sm chosen-select w-100" required name="CATEGORY" id="form_{{$formNameOther}}_CATEGORY">
                        {!! $optionCategory !!}}
                    </select>
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('STATUS')}}</label> <span class="red"> (*)</span>
                    <select  class="form-control input-sm" required name="STATUS" id="form_{{$formNameOther}}_STATUS">
                        {!! $optionStatus !!}}
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('IS_CONFIRM')}}</label>
                    <input type="text" class="form-control input-sm" name="IS_CONFIRM" id="form_{{$formNameOther}}_IS_CONFIRM">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('IS_PAY')}}</label>
                    <input type="text" class="form-control input-sm" name="IS_PAY" id="form_{{$formNameOther}}_IS_PAY">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('USER_REF')}}</label>
                    <input type="text" class="form-control input-sm" name="USER_REF" id="form_{{$formNameOther}}_USER_REF">
                </div>
                <div class="col-lg-3">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('STRUCT_CODE')}}</label>
                    <input type="text" class="form-control input-sm" name="STRUCT_CODE" id="form_{{$formNameOther}}_STRUCT_CODE">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
                <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$formNameOther}}','{{$urlActionOtherItem}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});

        showDataIntoForm('form_{{$formNameOther}}');
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
