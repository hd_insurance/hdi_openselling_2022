@include('Systems.ProductCore.productDeclaration.component.groupTabPage')
{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="form-group col-lg-3">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="p_keyword" name="p_keyword" autocomplete="off" @if(isset($search['p_keyword']))value="{{$search['p_keyword']}}"@endif>
            </div>
            <div class="form-group col-lg-3">
                <label for="status" class="control-label">{{viewLanguage('Category')}}</label>
                <select  class="form-control input-sm chosen-select w-100" name="CATEGORY" id="CATEGORY">
                    {!! $optionCategory !!}}
                </select>
            </div>
            <div class="form-group col-lg-3">
                @if($is_root || $permission_view)
                    <button class="btn-icon btn btn-primary marginT25" type="submit" name="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                @endif
                @if($is_root || $permission_add)
                    <a href="javascript:void(0);" class="area-btn-right btn-edit-right btn btn-success sys_show_popup_common" data-form-name="addForm" data-input="{{json_encode([])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Thêm ')}}{{$pageTitle}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="0">
                        Thêm mới
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

<div class="main-card mb-3 card">
    <div class="card-body">
        @if($data && sizeof($data) > 0)
            <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="thin-border-bottom">
                    <tr class="table-background-header">
                        {{--<th width="3%" class="text-center"><input type="checkbox" class="check" id="checkAll"></th>--}}
                        <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
                        <th width="35%" class="text-left">{{viewLanguage('Sản phẩm')}}</th>
                        <th width="15%" class="text-left">{{viewLanguage('Category')}}</th>

                        <th width="15%" class="text-left">{{viewLanguage('Phí')}}</th>
                        <th width="10%" class="text-left">{{viewLanguage('Thông tin khác')}}</th>
                        <th width="15%" class="text-center">TT</th>
                        <th width="6%" class="text-center"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $item)
                        <tr>
                            <td class="text-center middle">{{$stt+$key+1}}</td>

                            <td class="text-left middle">
                                @if(trim($item->PRODUCT_CODE) != '')<b>{{$item->PRODUCT_CODE}}</b><br/>@endif
                                @if(trim($item->PRODUCT_NAME) != ''){{$item->PRODUCT_NAME}}<br/>@endif
                            </td>
                            <td class="text-left middle">
                                @if(trim($item->CATEGORY) != '')<b class="font_10">Cate: </b>{{$item->CATEGORY}}<br/>@endif
                                @if(trim($item->GROUP_INSUR) != '')<b class="font_10">Group: </b>{{$item->GROUP_INSUR}}<br/>@endif
                                @if(trim($item->CURRENCY) != '')<b class="font_10">Currency: </b>{{$item->CURRENCY}}<br/>@endif
                                @if(trim($item->VAT_PERCENT) > 0)<b class="font_10">Vat_percent: </b>{{numberFormat($item->VAT_PERCENT)}}<br/>@endif
                            </td>
                            <td class="text-left middle">
                                @if(trim($item->FEES_MIN) != '')<b class="font_10">Fees_min: </b>{{numberFormat($item->FEES_MIN)}}<br/>@endif
                                @if(trim($item->FEES_DEFAULT) != '')<b class="font_10">Fees_default: </b>{{numberFormat($item->FEES_DEFAULT)}}<br/>@endif
                                @if(trim($item->FEES_MAX) != '')<b class="font_10">Fees_max: </b>{{numberFormat($item->FEES_MAX)}}<br/>@endif
                                @if(trim($item->FEES_TYPE) != '')<b class="font_10">Fees_type: </b>{{numberFormat($item->FEES_TYPE)}}<br/>@endif
                            </td>

                            <td class="text-left middle">
                                @if(trim($item->AGE_MIN) != '')<b class="font_10">Age_min: </b>{{$item->AGE_MIN}}<br/>@endif
                                @if(trim($item->AGE_MIN_UNIT) != '')<b class="font_10">Age_min_unit: </b>{{$item->AGE_MIN_UNIT}}<br/>@endif
                                @if(trim($item->AGE_MAX) != '')<b class="font_10">Age_max: </b>{{$item->AGE_MAX}}<br/>@endif
                                @if(trim($item->AGE_MAX_UNIT) != '')<b class="font_10">Age_max_unit: </b>{{$item->AGE_MAX_UNIT}}<br/>@endif
                            </td>

                            <td class="text-left middle">
                                @if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                                @if(trim($item->MODIFIED_BY) != '')<span class="red">{{$item->MODIFIED_BY}}</span>@endif
                                @if(trim($item->MODIFIED_DATE) != '')- <span class="red">{{convertDateDMY($item->MODIFIED_DATE)}}</span>@endif
                            </td>
                            <td class="text-center middle">
                                @if($is_root || $permission_view || $permission_add)
                                    <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getDetailCommonByAjax(this);" data-form-name="detailApi" data-input="{{json_encode(['item'=>$item])}}" data-show="2" data-div-show="content-page-right" title="{{viewLanguage('Cập nhật: ')}}{{$item->PRODUCT_CODE}}" data-method="get" data-url="{{$urlGetItem}}" data-objectId="{{$item->BP_ID}}">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>&nbsp;&nbsp;
                                @endif
                                @if($item->STATUS == 'APPROVED')
                                    <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                                @else
                                    <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                                @endif
                                <br/>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="paging_simple_numbers">
                {!! $paging !!}
            </div>
        @else
            <div class="alert">
                Không có dữ liệu
            </div>
        @endif
    </div>
</div>
