<div class="marginT5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr class="table-background-header">
            <th width="2%" class="text-center">{{viewLanguage('STT')}}</th>
            <th width="3%" class="text-center">{{viewLanguage('TT')}}</th>

            <th width="20%" class="text-center">{{viewLanguage('CHANNEL')}}</th>
            <th width="20%" class="text-center">{{viewLanguage('ORG_CODE')}}</th>
            <th width="10%" class="text-center">{{viewLanguage('CATEGORY')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('EFFECTIVE_DATE')}}</th>
            <th width="15%" class="text-center">{{viewLanguage('STATUS')}}</th>
        </tr>

        </thead>
        <tbody>
        @if(isset($listChannelAssign) && !empty($listChannelAssign))
            @foreach ($listChannelAssign as $kb => $itemChannelAssign)
                <tr>
                    <td class="text-center middle">{{$kb+1}}</td>
                    <td class="text-center middle">
                        @if($is_root || $permission_view || $permission_add)
                            <a href="javascript:void(0);" style="color: green" onclick="jqueryCommon.getDataByAjax(this);" data-form-name="addFormOther" data-url="{{$urlAjaxGetData}}" data-function-action="_ajaxGetItemOther" data-input="{{json_encode(['itemId'=>1,'type'=>$tabOtherItem3,'itemInfor'=>$itemChannelAssign,'isDetail'=>STATUS_INT_MOT,'arrKey'=>['dataPrimary'=>$dataPrimary]])}}" data-show="1" data-show-id="{{$tabOtherItem3}}" title="{{viewLanguage('Chi tiết kênh: ').$itemChannelAssign->CHANNEL}}" data-method="post" data-objectId="{{$dataPrimary->PRODUCT_CODE}}">
                                <i class="pe-7s-look fa-2x"></i>
                            </a>
                        @endif
                    </td>
                    <td class="text-left middle">{{$itemChannelAssign->CHANNEL}}</td>
                    <td class="text-left middle">{{$itemChannelAssign->ORG_CODE}}</td>

                    <td class="text-center middle">{{$itemChannelAssign->CATEGORY}}</td>
                    <td class="text-left middle">{{$itemChannelAssign->EFFECTIVE_DATE}}</td>
                    <td class="text-center middle">
                        @if($itemChannelAssign->STATUS == STATUS_INT_MOT)
                            <a href="javascript:void(0);" class="green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                        @else
                            <a href="javascript:void(0);" class="red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="paging_simple_numbers">

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
    });
    //tim kiem
    var config = {
        '.chosen-select'           : {width: "58%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
