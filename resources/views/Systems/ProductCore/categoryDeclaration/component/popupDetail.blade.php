<div class="modal-content" id="{{$form_id}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
    <form id="form_{{$form_id}}">
        <input type="hidden" id="objectId" name="objectId" @if($is_copy == STATUS_INT_MOT) value="0" @else value="{{$objectId}}"@endif>
        <input type="hidden" id="formName" name="formName" value="{{$form_id}}">
        <input type="hidden" id="data_item" name="data_item" value="{{json_encode($data)}}">
        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Category')}} </label><span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="CATEGORY"  @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_CATEGORY"@endif @if($objectId > 0) readonly @endif>
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Category_name')}}</label><span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="CATEGORY_NAME" @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_CATEGORY_NAME"@endif>
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right">{{viewLanguage('Status')}}</label><span class="red">(*)</span>
                    <select class="form-control input-sm" required name="IS_ACTIVE" id="form_{{$form_id}}_IS_ACTIVE" >
                        {!! $optionStatus !!}
                    </select>
                </div>
            </div>
            {{--<div class="row form-group">
                <div class="col-lg-12">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Product_code')}} <span class="red">(*)</span></label>
                    <select class="form-control input-sm chosen-select w-100" name="PRODUCT_CODE" @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_PRODUCT_CODE"@endif>
                        {!! $optionProductCore !!}
                    </select>
                </div>
            </div>--}}
            <div class="row form-group">
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Description')}}</label><span class="red">(*)</span>
                    <input type="text" class="form-control input-sm" required name="DESCRIPTION" @if($is_copy == STATUS_INT_KHONG)id="form_{{$form_id}}_DESCRIPTION"@endif>
                </div>
                <div class="col-lg-6">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Decision_no')}}</label>
                    <input type="text" class="form-control input-sm"  name="DECISION_NO" id="form_{{$form_id}}_DECISION_NO">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-4">
                    <label for="NAME" class="text-right">{{viewLanguage('Is_aprove')}}</label>
                    <select class="form-control input-sm" name="IS_APROVE" id="form_{{$form_id}}_IS_APROVE" >
                        {!! $optionAprove !!}
                    </select>
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Aprove_by')}}</label>
                    <input type="text" class="form-control input-sm" name="APROVE_BY" id="form_{{$form_id}}_APROVE_BY">
                </div>
                <div class="col-lg-4">
                    <label for="NAME" class="text-right control-label">{{viewLanguage('Core_code')}}</label>
                    <input type="text" class="form-control input-sm" name="CORE_CODE" id="form_{{$form_id}}_CORE_CODE">
                </div>
            </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="pe-7s-back"></i> {{viewLanguage('Cancel')}}</button>
            @if($is_root || $permission_edit || $permission_add)
            <button type="button" class="btn btn-primary" onclick="jqueryCommon.doActionPopup('{{$form_id}}','{{$url_action}}');"><i class="pe-7s-diskette"></i> {{viewLanguage('Save')}}</button>
            @endif
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});
        showDataIntoForm('form_{{$form_id}}');
    });
    var config = {
        '.chosen-select'           : {width: "100%"},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không có kết quả'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
