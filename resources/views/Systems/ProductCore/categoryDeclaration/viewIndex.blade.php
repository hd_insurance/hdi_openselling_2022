@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
        {{--list data---}}
        @include('Systems.ProductCore.categoryDeclaration.component.listData')
    {{ Form::close() }}
@stop
