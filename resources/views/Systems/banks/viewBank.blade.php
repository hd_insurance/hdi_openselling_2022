@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('Systems.banks.component.formSearch')

    {{--list data---}}
    @include('Systems.banks.component.listData')
@stop
