@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {!!Form::open(array('method' => 'POST', 'role'=>'form')) !!}
    {{--Search---}}
    @include('Systems.depart.component.formSearch')

    {{--list data---}}
    @include('Systems.depart.component.listData')
    {!! Form::close() !!}
@stop

