<div class="modal-content" id="{{$form_id}}" style="position: relative">
    <div id="loaderPopup"><span class="loadingAjaxPopup"></span></div>
     <form id="import_excel_golive" enctype="multipart/form-data">
        <input name="_token" type="hidden" value="{{ csrf_token() }}">
        <input type="hidden" id="isLoading" name="isLoading" value="1">
        <input type="hidden" id="formName" name="formName" value="{{$form_id}}">
        <input type="hidden" id="functionAction" name="functionAction" value="_actionImportExcel">

        {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="sysTitleModalCommon">{{$title_popup}}</h4>
        </div>
        <div class="modal-body">
            <div class="form_group">

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="user_group">Upload file excel</label>
                            <label title="{{viewLanguage('Upload file excel')}}" for="inputFileExcel" class="w-100 btn-transition btn btn-outline-success">
                                <input type="file" name="inputFileExcel" id="inputFileExcel" style="display:none">
                                <i class="fa fa-share-square"></i> {{viewLanguage('Upload (xlsx.xls)')}}
                            </label>
                        </div>
                        <div class="col-lg-6">
                            <button class="btn btn-success marginT30" type="button" name="submit_import" id="submit_import" value="1" onclick="jqueryCommon.submitAjaxFormMultipart('import_excel_golive','submit_import','{{$urlActionFunction}}')"><i class="fa fa-search"></i> {{viewLanguage('Import data')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy h:i'});
    });
    function submitAjaxFormMultipart(form_id,btnSubmit,urlAjax){
        var form = $('#'+form_id)[0];
        var data = new FormData(form);

        // disabled the submit button
        //$("#"+btnSubmit).prop("disabled", true);
        $('#loader').show();
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: urlAjax,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (res) {
                $('#loader').hide();
                if(res.success == 1){
                    jqueryCommon.showMsg('success', 'Đã cấp đơn thành công');
                    location.reload();
                }else {
                    jqueryCommon.showMsg('error', '', 'Thông báo lỗi', res.msg);
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        });
    }
</script>
