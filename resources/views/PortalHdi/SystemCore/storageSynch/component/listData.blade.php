<div class="ibox">
    <div class="ibox-title">
        <h5>{{viewLanguage('Tìm kiếm')}}</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class=" col-lg-4">
                <label for="depart_name">{{viewLanguage('Tìm kiếm')}}</label>
                <input type="text" class="form-control input-sm" id="s_search" name="s_search" autocomplete="off" @if(isset($search['s_search']))value="{{$search['s_search']}}"@endif>
            </div>
            <div class="col-lg-8 marginT30">
                <button class="btn btn-primary" type="submit" name="submit" id="submit" value="1"><i class="fa fa-search"></i> {{viewLanguage('Search')}}</button>
                <button class="btn btn-danger" type="button" name="delete_list" id="delete_list" onclick="clickDeleteItem('{{$urlActionFunction}}')"><i class="pe-7s-trash"></i> {{viewLanguage('Xóa dữ liệu')}}</button>

                <button class="btn btn-light display-none-block" type="submit" name="approval_order" id="show_button_approval_order" value="0" onclick="clickExportExcel()"><i class="fa fa-check"></i> {{viewLanguage('Xuất excel')}}</button>
                <input type="hidden" value="" name="export_excel" id="export_excel">
                <input type="hidden" value="" name="str_id_search" id="str_id_search">

                <button type="button" class="btn btn-warning" onclick="jqueryCommon.getDataByAjax(this);" data-show="0" data-form-name="detailItem" title="{{viewLanguage('Import Data')}}" data-function-action="_openPopupImportExcel" data-method="post" data-url="{{$urlActionFunction}}" data-input="" data-objectId="1">
                    <i class="fa fa-check"></i> {{viewLanguage('Import excel')}}
                </button>
            </div>
        </div>
    </div>
</div>

<div class="ibox-content">
    @if($data && sizeof($data) > 0)
        <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{$total}}</b> item @endif </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <th width="6%" class="text-center">{{viewLanguage('STT')}}
                        <input type="checkbox" class="check" id="checkAllOrder">
                    </th>
                    <th width="10%" class="text-left">{{viewLanguage('REF_TABLE')}}</th>
                    <th width="8%" class="text-left">{{viewLanguage('REF_ID')}}</th>

                    <th width="70%" class="text-left">{{viewLanguage('INFO_SYNCH')}}</th>
                    <th width="5%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td class="text-center middle">
                            {{$stt+$key+1}}
                            <input class="check" type="checkbox" name="checkItems[]" value="@if(isset($item->RID)){{$item->RID}}@endif" onchange="changeColorButton();" >
                        </td>
                        <td class="text-left middle">
                            {{$item->REF_TABLE}}
                            <br/>
                            <span class="font_10">
                                @if(trim($item->CREATE_BY) != ''){{$item->CREATE_BY}}@endif
                                @if(trim($item->CREATE_DATE) != '') - {{convertDateDMY($item->CREATE_DATE)}} <br/>@endif
                            </span>
                            <span class="font_10 red">
                                @if(trim($item->MODIFIED_BY) != ''){{$item->MODIFIED_BY}}@endif
                                @if(trim($item->MODIFIED_DATE) != '') - {{convertDateDMY($item->MODIFIED_DATE)}} <br/>@endif
                            </span>
                        </td>
                        <td class="text-left middle">
                            {{$item->REF_ID}}
                        </td>
                        <td class="text-left middle">{{str_replace('","', '" , "',$item->INFO_SYNCH)}}</td>
                        <td class="text-left middle">
                            {{--@if($item->STATUS == STATUS_INT_MOT)
                                <a href="javascript:void(0);" style="color: green" title="Hiện"><i class="fa fa-check fa-2x"></i></a>
                            @else
                                <a href="javascript:void(0);" style="color: red" title="Ẩn"><i class="fa fa-times fa-2x"></i></a>
                            @endif--}}

                            @if($is_root || $permission_edit || $permission_add)
                                &nbsp;
                                <a href="javascript:void(0);" class="color_warning" onclick="jqueryCommon.getAjaxWithConfirm(this);" data-loading="1" data-msg-confirm="Bạn có muốn đồng bộ dữ liệu golive" data-input="{{json_encode(['keySyn'=>$item->RID])}}" title="{{viewLanguage('Đồng bộ dữ liệu golive')}}" data-function-action="_synchDataGolive" data-method="post" data-url="{{$urlActionFunction}}">
                                    <i class="fa fa-copy fa-2x"></i>
                                </a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="paging_simple_numbers">
            {!! $paging !!}
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif
</div>

<script type="text/javascript">
    var hdisdk=null;
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd/mm/yy'});
        $("#checkAllOrder").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
            changeColorButton();
        });

    });
    function changeColorButton(){
        var changeColor = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                changeColor = 1;
            }
        });
        if(changeColor == 1){
            $('#show_button_approval_order').removeClass("display-none-block");
            $("#show_button_approval_order").addClass("btn-success");
            $("#show_button_approval_order").removeClass("btn-light");
        }else {
            $('#show_button_approval_order').addClass("display-none-block");
            $("#show_button_approval_order").removeClass("btn-success");
            $("#show_button_approval_order").addClass("btn-light");
        }
    }

    function clickExportExcel(){
        var dataId = [];
        var i = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                dataId[i] = $(this).val();
                i++;
            }
        });
        if (dataId.length == 0) {
            alert('Bạn chưa chọn đơn để thao tác.');
            return false;
        }
        $('#str_id_search').val(dataId.toString());
        $('#export_excel').val(1);
    }
    function clickDeleteItem(urlActionFunction){
        var dataId = [];
        var i = 0;
        $("input[name*='checkItems']").each(function () {
            if ($(this).is(":checked")) {
                dataId[i] = $(this).val();
                i++;
            }
        });
        if (dataId.length == 0) {
            alert('Bạn chưa chọn đơn để thao tác.');
            return false;
        }
        if (confirm('Bạn có muốn xóa dữ liệu này')) {
            var _token = $('input[name="_token"]').val();
            $('#loader').show();
            $.ajax({
                dataType: 'json',
                type: 'Post',
                url: urlActionFunction,
                data: {
                    '_token': _token,
                    'strId': dataId.toString(),
                    'functionAction': '_deleteListData'
                },
                success: function (res) {
                    $('#loader').hide();
                    if (res.success == 1) {
                        jqueryCommon.showMsg('success', res.message);
                        location.reload();
                    } else {
                        jqueryCommon.showMsgError(res.success, res.message);
                    }
                }
            });
        }
    }
</script>
