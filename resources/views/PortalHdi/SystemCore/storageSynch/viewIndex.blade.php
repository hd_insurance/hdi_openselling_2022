@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')
    {{ Form::open(array('method' => 'POST', 'role'=>'form', 'id'=>'form_synch')) }}
    {{--Search---}}
    @include('Systems.SystemCore.storageSynch.component.formSearch')

    {{--list data---}}
    @include('Systems.SystemCore.storageSynch.component.listData')
    {{ Form::close() }}
@stop
