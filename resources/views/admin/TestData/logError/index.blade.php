@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('admin.TestData.logError.component.formSearch')

    {{--list data---}}
    @include('admin.TestData.logError.component.listData')

@stop
