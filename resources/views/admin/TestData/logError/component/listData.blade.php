{{ Form::open(array('method' => 'GET', 'role'=>'form')) }}
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-2">
                <label for="user_email">Ngày log</label>
                <input type="text" class="form-control input-sm input-date" data-valid = "text" name="p_from_date" id="p_from_date" @if(isset($search['p_from_date']))value="{{$search['p_from_date']}}"@endif>
                <div class="icon_calendar"><i class="fa fa-calendar-alt"></i></div>
            </div>
            <div class="col-lg-2 marginT30 text-left">
                <button class="btn btn-primary" title="Tìm kiếm" type="submit" name="submit" value="1"><i class="fa fa-search fa-2x"></i> Tìm kiếm</button>
            </div>
        </div>
        <hr class="marginT15">
    </div>
</div>
{{ Form::close() }}
<div class="ibox-content">
    <div class="row marginB10">
        <div class="col-lg-2 text-left paddingRight-unset">
            <h5 class="clearfix"> @if($total >0) Có tổng số <b>{{numberFormat($total)}}</b> bản ghi @endif</h5>
        </div>
    </div>
    @if($arrayContent && sizeof($arrayContent) > 0)
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thin-border-bottom">
                <tr class="table-background-header">
                    <!--<th width="3%" class="text-center middle"><input type="checkbox" class="check" id="checkAllOrder"></th>-->
                    <th width="5%" class="text-center middle">{{viewLanguage('STT')}}</th>
                    <th width="95%" class="text-left middle">{{viewLanguage('Content log')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($arrayContent as $key => $item)
                    <tr>
                        <td class="text-center middle">{{$stt}}</td>
                        <td class="text-left middle">{!! str_replace(',', ' , ',$item) !!}</td>
                    </tr>
                    <?php $stt++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif

</div>

<script type="text/javascript">
    var hdisdk=null;
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy'});
    });
</script>





