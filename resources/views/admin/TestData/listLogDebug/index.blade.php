@extends('Layouts.BaseAdmin.indexHDI')
@section('content')
    {{---breadcrumbs---}}
    @include('Layouts.BaseAdmin.breadcrumbs')

    {{--Search---}}
    @include('admin.TestData.listLogDebug.component.formSearch')

    {{--list data---}}
    @include('admin.TestData.listLogDebug.component.listData')

@stop
