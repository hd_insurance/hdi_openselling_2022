<div class="ibox-content">
    <div class="row marginB10">
        <div class="col-lg-2 text-left paddingRight-unset">
            <h5 class="clearfix"> Danh sách log Debug system</h5>
        </div>
    </div>
    @if($arrFiles && sizeof($arrFiles) > 0)
        <ul id="ft-id-1" class="ui-fancytree fancytree-container fancytree-plain" tabindex="0" role="tree" aria-multiselectable="true" aria-activedescendant="ui-id-2">
            <div id="blockListProvice" data-children=".item">
                <div class="item row">
                    @foreach ($arrFiles as $nameDir => $arrItem)
                        <li role="treeitem" aria-expanded="true" aria-selected="false" id="ui-id-1" class="col-lg-12">
                            <button type="button" aria-expanded="true" aria-controls="" data-toggle="collapse" href="#childListProvice{{$nameDir}}" class="m-0 p-0 btn btn-link" >
                                <b class="green"> {{$nameDir}}</b>
                            </button>
                            <div data-parent="#blockListProvice" id="childListProvice{{$nameDir}}" class="collapse" >
                                <ul role="group" style="" id="groupListDistrict{{$nameDir}}">
                                    <div id="blockListDistrict{{$nameDir}}" data-children=".itemDistrict">
                                        <div class="itemDistrict row">
                                            @foreach ($arrItem as $key2 => $item2)
                                                @if(isset($item2['name']))
                                                <li role="treeitem" aria-selected="false" class="fancytree-lastsib col-lg-3">
                                                    <a href="{{URL::route('debugData.detaiDebug',['path'=>$item2['path']])}}" target="_blank" class="m-0 p-0 btn btn-link" title="{{viewLanguage('Thông tin chi tiết')}}">
                                                        <b>{{$item2['name']}}</b>
                                                    </a>
                                                </li>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </li>
                    @endforeach
                </div>
            </div>
        </ul>
    @else
        <div class="alert">
            Không có dữ liệu
        </div>
    @endif

</div>

<script type="text/javascript">
    var hdisdk=null;
    $(document).ready(function(){
        var date_time = $('.input-date').datepicker({dateFormat: 'dd-mm-yy'});
    });
</script>





