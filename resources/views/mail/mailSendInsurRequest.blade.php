@if(isset($data['CONTENT_MSG']) && trim($data['CONTENT_MSG']) != '')
    {!! $data['CONTENT_MSG'] !!}
@endif
<br/>
@if(isset($data['DATA_CONTENT']) && !empty($data['DATA_CONTENT']))
    @foreach($data['DATA_CONTENT'] as $k => $contentEmail)
        <br/> Mã yêu cầu : <b>{!! $contentEmail['REQUEST_CODE'] !!}</b>
        <br/> Certificate no : <b>{!! $contentEmail['CERTIFICATE_NO'] !!}</b>
        <br/> Product code : <b>{!! $contentEmail['PRODUCT_CODE'] !!}</b>
            @if(!empty($contentEmail['DATA_CHANGE']))
                <br/> Data yêu cầu thay đổi :
                <table class="table table-bordered" style="border: 1px solid #CCCCCC">
                    <tr style="border: 1px solid #CCCCCC">
                        <th width="3%" style="border: 1px solid #CCCCCC">STT</th>
                        <th width="25%" style="border: 1px solid #CCCCCC">Loại thông tin</th>
                        <th width="35%" style="border: 1px solid #CCCCCC">Thông tin cũ trên giấy chứng nhận</th>
                        <th width="35%" style="border: 1px solid #CCCCCC">Thông tin yêu cầu thay đổi</th>
                    </tr>
                    <tbody>
                        <?php $stt = 1;?>
                        @foreach ($contentEmail['DATA_CHANGE'] as $key_change => $arrInfor)
                            @if(!empty($arrInfor['VALUE_NEW']) && isset($contentEmail['ARR_KEY_CHANGE'][$key_change]))
                                <tr style="border: 1px solid #CCCCCC">
                                    <td style="border: 1px solid #CCCCCC">{{$stt}}</td>
                                    <td style="border: 1px solid #CCCCCC">{{$contentEmail['ARR_KEY_CHANGE'][$key_change]}}</td>
                                    <td style="border: 1px solid #CCCCCC">{{$arrInfor['VALUE_OLD']}}</td>
                                    <td style="border: 1px solid #CCCCCC">{{$arrInfor['VALUE_NEW']}}</td>
                                </tr>
                                <?php $stt ++;?>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @endif
    @endforeach
@endif
<br/>
<br/>Trân trọng ./.
<br/>Không trả lời mail tự động này. Xin cảm ơn.
