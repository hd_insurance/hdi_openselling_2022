HDI gửi thông báo ký số theo yêu cầu:
<br/>
@if(isset($data['FILE_SIGNED']) && trim($data['FILE_SIGNED']) != '')
    <br/><b>Ký số thành công</b>
    <br/> Ghi chú : <b>{!! $data['NOTE_REPLY'] !!}</b>
    <br/> Giấy chứng nhận mới : <a href="{{ $data['URL_SERVICE_FILE'].trim($data['FILE_SIGNED']) }}" target="_blank">{!! $data['NOTE_REPLY'] !!}</a>
@else
    <br/><b>Ký số không thành công! Hãy liên hệ với HDI để được hỗ trợ.</b>
@endif
<br/>
<br/> Mã yêu cầu : <b>{!! $data['REQUEST_CODE'] !!}</b>
<br/> Certificate no : <b>{!! $data['CERTIFICATE_NO'] !!}</b>
<br/> Product code : <b>{!! $data['PRODUCT_CODE'] !!}</b>

<br/><br/>
<br/>Trân trọng ./.
<br/>Không trả lời mail tự động này. Xin cảm ơn.
