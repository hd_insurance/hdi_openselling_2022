@extends('Layouts.BasePartner.index')
@section('content')
    <div class="main-card mb-3 card">
        <div  style="width: 100%; text-align: center">
            @if(isset($msg))
                <h3 class="marginT40">{{$msg}}</h3>
            @endif
            <img src="{{\Illuminate\Support\Facades\Config::get('config.WEB_ROOT')}}assets/backend/theme/assets/images/page_error.gif">
        </div>
    </div>
@stop
