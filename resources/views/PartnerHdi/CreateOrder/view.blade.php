<!doctype html>
<html lang="en">
<?php
use App\Library\AdminFunction\CGlobal;
$imageDefault = Config::get('config.WEB_ROOT').'assets/backend/img/HDInsurance.png';
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{!! CGlobal::$pageAdminTitle !!}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="{!! CGlobal::$pageAdminTitle !!}">
    <meta name="msapplication-tap-highlight" content="no">
    @include('Layouts.BaseAdmin.component._files_header')
</head>

<body>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div id="loader"><span class="loadingAjax"></span></div>
            @include('PartnerHdi.CreateOrder.component.listData')
        </div>
    </div>
</div>

<script>
    var WEB_ROOT = "{{URL::to('/')}}";
    $(document).ready(function() {
        $('.phpdebugbar-minimized').css('display','none');
    });

</script>
<input type="hidden" id="ipaddress" name="ipaddress">
</body>
<foot></foot>
</html>
