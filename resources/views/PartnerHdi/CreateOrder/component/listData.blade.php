<div class="main-card mb-3 card">
    <div id="loaderRight"><span class="loadingAjaxRight"></span></div>
    <div id="content-page-right-layout" style="display: none">
        <div id="divDetailItem">
            <div class="card-header">
                <span id="title_cap_don"></span>
            </div>
            <div class="card-body" id="body-insurance-policy">
                <!-- Nội dung form cấp đơn hiển thị ở đây-->
            </div>
        </div>
    </div>
</div>
<script id="script_layout" src="{{\Illuminate\Support\Facades\Config::get('config.URL_SDK_' . \Illuminate\Support\Facades\Config::get('config.ENVIRONMENT'))}}source/4874b3fae14c29fd43d9d7533651cb60/integrate.js"></script>
<script type="text/javascript">
    var hdisdk=null;

    $(document).ready(function(){
        $('#loader').show();
        const config = {
            classId: "body-insurance-policy",
            partnerId: "",
            publickey: ""
        }
        hdisdk = new HDISdk(config)
        hdisdk.init((initresult)=>{
            if(initresult.sucess){
                //do something
            }
        })
        //khi submit xong
        hdisdk.onFormsubmit((data, result)=>{
            // when form is submit
        })

        var category = '{{$category}}';
        var tilte = '{{$tilte}}';
        var action = '{{$action}}';
        var contract_code = '{{$contract_code}}';
        var detail_code = '{{$detail_code}}';
        var product_code = '{{$product_code}}';
        var channel = '{{$channel}}';
        var org_code = '{{$org_code}}';
        var user_name = '{{$user_name}}';
        var partner = '{{$partner}}';
        var user_permission = '{{$user_permission}}';

        setTimeout(function () {
            openApplicationInsurance(category,tilte,action,contract_code,detail_code,product_code,channel,org_code,user_name,partner,user_permission);
        }, 4000);
    });

    function openApplicationInsurance(category,tilte,action,contract_code,detail_code,product_code,channel,org_code,user_name,partner,user_permission){
        $('#content-page-right-layout').show();
        $('#title_cap_don').html(tilte)
        const sdk_params = {
            partner: partner, //Đối tác sử dụng
            action: action, //  Với trường hợp cấp đơn mới là: 'ADD', còn sửa là 'EDIT'
            category: category, // loại sản phẩm
            contract_code: contract_code, //Mã hợp đồng truyền lên
            detail_code: detail_code, //Mã detail_code truyền lên
            org_code: org_code, //mã đơn vị người sử dụng
            user_name: user_name, //user name
            user_permission: user_permission, //nhóm quyền giám định
            product_code: product_code, //product_code
            channel: channel, //channel
            onClose: function(){}
        }
        console.log(sdk_params);
        hdisdk.open(sdk_params);
        $('#loader').hide();
    }

</script>
