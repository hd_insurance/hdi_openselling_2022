<?php

namespace App\Exceptions;

use App\Library\AdminFunction\CGlobal;
use App\Services\ServiceCommon;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    /*public function register()
    {
        $this->renderable(function (NotFoundHttpException $exception, $request) {
            $is_dev = Config::get('config.IS_DEV');
            $is_live = Config::get('config.IS_LIVE');

            if(!$is_dev && $is_live){//live
                $is_send = 0;
                $stringException = $exception->__toString();
                if (strpos($stringException,'ErrorException') == 0 && strpos($stringException,'Undefined index') == 16){
                    $is_send = 1;
                }elseif(strpos($stringException,'ErrorException') == 0 && strpos($stringException,'Undefined variable') == 16){
                    $is_send = 1;
                }
                if (strpos($stringException,'PDOException') !== false) {
                    $is_send = 1;
                }
                if($is_send == 1){
                    $dataSenmail['CONTENT'] = $stringException;
                    $dataSenmail['TO'] = CGlobal::mail_test;
                    $dataSenmail['TYPE'] = 'THONG_BAO';
                    $dataSenmail['SUBJECT'] = getCurrentDateTime().' có lỗi trên '.Config::get('config.WEB_ROOT');
                    app(ServiceCommon::class)->sendMailWithContent($dataSenmail);
                }
                return Redirect::route('admin.login');
            }else{//dev
                return Redirect::route('admin.login');
            }
        });
    }*/

    public function register()
    {
        $this->reportable(function (Throwable $e) {

        });
    }
}
