<?php
/**
 * QuynhTM add
 */
namespace App\Library\AdminFunction;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;


class Memcache
{
    const CACHE_ON = STATUS_INT_MOT;// 0: khong dung qua cache, 1: dung qua cache
    /************************************************************************************************
     * Cache Admin
     ************************************************************************************************/
    const CACHE_OPTION_USER = 'cache_option_user';

    //DMS
    const CACHE_CALENDAR_WORKING_ID = 'cache_calendar_working_id_';
    const CACHE_ROLE_ID = 'cache_role_id_';
    const CACHE_OPTION_ROLE = 'cache_option_role';
    const CACHE_ROLE_ALL = 'cache_option_all';

    const CACHE_PROVINCE_ID = 'cache_province_id_';
    const CACHE_OPTION_PROVINCE = 'cache_option_province';

    /************************************************************************************************
     * Cache HD Insurance
     ************************************************************************************************/
    const CACHE_API_TOKEN_HD = 'cache_api_token_hd';

    const CACHE_LIST_FIELD_TABLE_NAME = 'cache_list_field_table_';

    const CACHE_TYPE_DEFINE_ALL = 'cache_type_define_all';
    const CACHE_DEFINE_CODE = 'cache_define_code_';

    const CACHE_PROVINCE_DISTRICT_WARD_ALL = 'cache_province_district_ward_all';

    const CACHE_BANK_ALL = 'cache_bank_all';
    const CACHE_BANK_BY_KEY = 'cache_bank_by_key_';

    const CACHE_MENU_SYSTEM_BY_ID = 'cache_menu_system_by_id_';
    const CACHE_MENU_SYSTEM_BY_PROJECT_CODE = 'cache_menu_system_by_project_code_';
    const CACHE_TREE_MENU_SYSTEM_BY_PROJECT_CODE = 'cache_tree_menu_system_by_project_code_';

    const CACHE_USERS_BY_KEY = 'cache_users_by_key_';
    const CACHE_INFOR_USERS_BY_KEY = 'cache_infor_users_by_key_';
    const CACHE_USERS_ABOUT_BY_KEY = 'cache_users_about_by_key_';
    const CACHE_USER_GROUP_MENU_BY_KEY = 'user_group_menu_by_key_';
    const CACHE_USER_MENU_BY_KEY = 'cache_user_menu_by_key_';

    const CACHE_ORGANIZATION_BY_KEY = 'cache_organization_by_key_';
    const CACHE_ORGANIZATION_ALL = 'cache_organization_all';
    const CACHE_ORGANIZATION_DATA_RELATION_BY_ORG_CODE = 'cache_organization_data_relation_by_org_code_';

    const CACHE_ORG_BANK_BY_KEY = 'cache_org_bank_by_key_';
    const CACHE_ORG_CONTRACT_BY_KEY = 'cache_org_contract_by_key_';
    const CACHE_ORG_RELATIONSHIP_BY_KEY = 'cache_org_relationship_by_key_';
    const CACHE_ORG_STRUCT_BY_KEY = 'cache_org_struct_by_key_';

    const CACHE_ALL_DEPARTMENT_BY_KEY_CODE = 'cache_all_department_by_key_code_';
    const CACHE_DEPARTMENT_BY_KEY = 'cache_department_by_key_';

    const CACHE_GROUP_MENU_BY_ID = 'cache_group_menu_by_id_';
    const CACHE_DETAIL_GROUP_MENU_BY_KEY = 'cache_detail_group_menu_by_key_';
    const CACHE_DETAIL_GROUP_MENU_BY_ORG_CODE = 'cache_detail_group_menu_by_org_code_';

    const CACHE_TYPE_DEFINE_BY_ID = 'cache_type_define_by_id_';

    const CACHE_VERSIONS_BY_ID = 'cache_versions_by_id_';
    const CACHE_VERSIONS_ALL = 'cache_versions_all';
    const CACHE_LIST_DETAIL_BY_VERSIONS_CODE = 'cache_list_detail_by_versions_code_';
    const CACHE_DETAIL_VERSIONS_BY_ID = 'cache_detail_versions_by_id_';

    const CACHE_DATABASES_BY_KEY = 'cache_databases_by_key_';
    const CACHE_DATABASES_ALL = 'cache_databases_all';

    const CACHE_DOMAINS_BY_KEY = 'cache_domains_by_key_';
    const CACHE_DOMAINS_ALL = 'cache_domains_all';

    //API
    const CACHE_APIS_ALL = 'cache_apis_all';
    const CACHE_APIS_BY_KEY = 'cache_apis_by_key_';
    const CACHE_DATABASES_APIS_BY_ID = 'cache_databases_apis_by_id_';
    const CACHE_DATABASES_APIS_BY_KEY = 'cache_databases_apis_by_key_';

    const CACHE_ALL_PARTNER_CORE = 'cache_all_partner_core';
    const CACHE_ALL_DATABASE_CORE = 'cache_all_database_core';
    const CACHE_ALL_USER_PARTNER = 'cache_all_user_partner';

    //B CONTRACT
    const CACHE_ALL_PRODUCT_CORE = 'cache_all_product_core';
    const CACHE_ALL_CATEGORY_CORE = 'cache_all_category_core';
    const CACHE_ALL_PACKAGE_CORE = 'cache_all_package_core';

    //MEDIA
    const CACHE_DATA_CONFIG_CODE_BY_KEY = 'cache_data_config_code_by_key_';
    const CACHE_ALL_VOUCHER_VALUE_BY_KEY = 'cache_all_voucher_value_by_key_';
    const CACHE_VOUCHER_VALUE_BY_KEY = 'cache_voucher_value_by_key_';
    const CACHE_CAMPAIGN_INFO_BY_CAMPAIGN_CODE = 'cache_campaign_info_by_campaign_code_';

    const CACHE_CAMPAIGNS_ALL = 'cache_campaigns_all';
    const CACHE_ALL_DEFINE_POLICY = 'cache_all_define_policy';
    /**
     * @param string $key_cache
     * @return bool
     */
    public static function getCache($key_cache = ''){
        $useCache = Config::get('config.IS_CACHE');
        return (trim($key_cache) != '' && $useCache) ? Cache::get($key_cache) : false;
    }

    /**
     * @param string $key_cache
     * @param array $data
     * @param int $time
     * @return bool
     */
    public static function putCache($key_cache = '', $data = [] , $time = CACHE_ONE_YEAR){
        $useCache = Config::get('config.IS_CACHE');
        return (trim($key_cache) != ''  && !empty($data) && $useCache) ? Cache::put($key_cache, $data, $time) : false;
    }

    /**
     * @param string $key_cache
     * @return bool
     */
    public static function forgetCache($key_cache = '',$arrDomain = []){
        if(!empty($arrDomain)){
            if(!empty($arrDomain)){
                $curl = Curl::getInstance();
                foreach ($arrDomain as $domain){
                    $curl->execUrlOnsite($domain.'/manager/clear');
                }
            }
        }
        $useCache = Config::get('config.IS_CACHE');
        return (trim($key_cache) != '' && $useCache) ? Cache::forget($key_cache) : false;
    }
}
