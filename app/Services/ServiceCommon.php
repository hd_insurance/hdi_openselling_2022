<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 13/03/2022
* @Version   : 1.0
*/

namespace App\Services;

use App\Models\System\InsurCore;
use App\Models\System\UserSystem;
use App\Library\AdminFunction\CGlobal;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class ServiceCommon
{
    public function debugLive($data, $is_die = true)
    {
        $user = app(UserSystem::class)->userLogin();
        $debug = Request::get('debug', 0);
        if (isset($user['user_type']) && $user['user_type'] == 'ROOT' && $debug == 1) {
            myDebug($data, $is_die);
        }
    }

    function moveFileToServerStore($linkFile, $is_dev = true)
    {
        $curl = curl_init();
        $is_dev = (Config::get('config.ENVIRONMENT') == 'DEV') ? true : false;
        $urlServer = ($is_dev ? Config::get('config.URL_HYPERSERVICES_DEV') : Config::get('config.URL_HYPERSERVICES_LIVE')) . "upload";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlServer,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('files' => new \CURLFile($linkFile)),
            CURLOPT_HTTPHEADER => array(
                //"Content-Type:multipart/form-data",
                "ParentCode: HDI_UPLOAD",
                "Secret: HDI_UPLOAD_198282911FASE1239212",
                "UserName: HDI_UPLOAD",
                "environment: LIVE",
                "DeviceEnvironment: WEB",
                "ActionCode: UPLOAD_SIGN"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $dataApi = json_decode($response);
        if (isset($dataApi->success) && $dataApi->success == 1) {
            return $dataApi->data[0]->file_key;
        }
        return '';
    }

    function moveFileCreateOrder($dataInput = [], $is_dev = true)
    {
        $curl = curl_init();
        $linkFile = isset($dataInput['urlFile']) ? $dataInput['urlFile'] : '';
        $is_dev = (Config::get('config.ENVIRONMENT') == 'DEV') ? true : false;
        $urlCreateOrder = ($is_dev ? Config::get('config.URL_API_DEV') : Config::get('config.URL_API_LIVE')) . "OpenApi/v1/import/care/add";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlCreateOrder,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('files' => new \CURLFile($linkFile)),
            CURLOPT_HTTPHEADER => array(
                "ACTION: IMP_ADD",
                "ID:" . (isset($dataInput['programme_id']) ? $dataInput['programme_id'] : ''),//id chương trình
                "IS_SMS:" . (isset($dataInput['check_send_sms']) ? $dataInput['check_send_sms'] : '0'),
                "IS_EMAIL:" . (isset($dataInput['check_send_email']) ? $dataInput['check_send_email'] : '0'),
                "ENV:" . (isset($dataInput['check_create_test']) ? $dataInput['check_create_test'] : '0'),
                "IS_SGN:" . (isset($dataInput['check_creat_certification']) ? $dataInput['check_creat_certification'] : '0'),//dùng số SGN sinh trước
                "ParentCode:" . Config::get('config.API_PARENT_CODE'),
                "UserName:" . Config::get('config.API_USER_NAME'),
                "Secret: " . Config::get('config.API_SECRET'),
            )
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $dataApi = json_decode($response);
        return $dataApi;
    }

    /**
     * Send email
     * @param array $dataInput
     * @return false
     * $dataSendMail = ["DATE_REQUIRED" => ''];
     * $dataSendMail['TEMP'][] = [
        "TEMP_CODE" => 'TCYC_SDBS',
        "PRODUCT_CODE" => 'TCYC_SDBS',
        "ORG_CODE" => ORG_HDI,
        "TYPE" => "EMAIL",
        "TO" => $email_to,//$emailSend,
        "CC" => '',
        "BCC" => $email_cc];
     */
    public function sendMailWithTemplate($dataInput = array())
    {
        $dataRequest['Data'] = $dataInput;
        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            "ActionCode" => "HDI_EMAIL_JSON"];
        $param_url = Config::get('config.MAIL_SERVICE');
        $serviceUrl = new ServiceCurl();
        $resultApi = $serviceUrl->postApiUrl($dataRequest, $param_url);
        return $resultApi;
    }

    public function sendMailCommon($dataInput = array())
    {
        $content = isset($dataInput['CONTENT']) ? $dataInput['CONTENT'] : '';
        $emailTo = isset($dataInput['TO']) ? $dataInput['TO'] : '';
        $emailCC = isset($dataInput['CC']) ? $dataInput['CC'] : '';
        if (trim($content) == '' || trim($emailTo) == '') {
            return false;
        }

        $arrFile = [];
        //chưa dùng vội
        if (1 != 1 && isset($dataInput['FILE_NAME']) && !empty($dataInput['FILE_NAME']) && isset($dataInput['FILE_PATH']) && !empty($dataInput['FILE_PATH'])) {
            $arrFile['name'] = $dataInput['FILE_NAME'];
            $arrFile['path'] = $dataInput['FILE_PATH'];
        }

        $dataSendMail = [
            "REFF_ID" => 'PORTAL',
            //"CONTENT" => base64_encode($content),
            "CONTENT" => $content,
        ];
        $dataSendMail['TEMP'][] = [
            "TEMP_CODE" => 'PORTAL_EMAIL',
            "PRODUCT_CODE" => 'PORTAL_EMAIL',
            "ORG_CODE" => "HDI_PORTAL",
            "ACCOUNT" => "ADMIN",
            "TYPE" => "EMAIL",
            "TO" => $emailTo,
            "CC" => $emailCC,
            "BCC" => CGlobal::mail_test,
            "file" => json_encode($arrFile)];

        $dataRequest['Data'] = $dataSendMail;
        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            "ActionCode" => "HDI_EMAIL_JSON"];

        $param_url = Config::get('config.MAIL_SERVICE');
        $serviceUrl = new ServiceCurl();
        $resultApi = $serviceUrl->postApiUrl($dataRequest, $param_url);
        return $resultApi;
    }

    public function sendMailWithContent($dataInput = array())
    {
        $content = isset($dataInput['CONTENT']) ? $dataInput['CONTENT'] : '';
        $emailTo = isset($dataInput['TO']) ? $dataInput['TO'] : '';
        $emailCC = isset($dataInput['CC']) ? $dataInput['CC'] : '';
        $type = isset($dataInput['TYPE']) ? $dataInput['TYPE'] : 'THONG_BAO';
        $subject = isset($dataInput['SUBJECT']) ? $dataInput['SUBJECT'] : 'PORTAL_EMAIL';
        if (trim($content) == '' || trim($emailTo) == '') {
            return false;
        }

        $arrFile['name'] = '';
        $arrFile['path'] = '';

        $dataSendMail[] = [
            "CONTENT" => base64_encode($content),
            "REFF_ID" => 'TCBT',
            "SUBJECT" => $subject,
            "ACCOUNT" => "ADMIN",
            "TYPE" => $type,
            "TO" => $emailTo,
            "CC" => $emailCC,
            "BCC" => CGlobal::mail_test,
            "file" => ''];
        //"file" => [json_encode($arrFile)]];

        $dataRequest['Data'] = $dataSendMail;
        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            "ActionCode" => "HDI_EMAIL_JSON"];

        $param_url = Config::get('config.SEND_MAIL_ADMIN');
        $serviceUrl = new ServiceCurl();
        $resultApi = $serviceUrl->postApiUrl($dataRequest, $param_url);
        return $resultApi;
    }

    /**
     * Lấy quyền theo page
     * @param string $nameController
     * @return string[]
     */
    public function getGroupPermissonWithController($nameController = '')
    {
        $inforUserLogin = app(UserSystem::class)->userLogin();
        $arrPermission = ['PER_VIEW' => '0','APPROVE' => '0', 'CREATE_ORDER' => '0', 'INSPECTION' => '0'];
        if (!empty($inforUserLogin)) {
            if ($inforUserLogin['user_type'] != USER_ROOT) {
                if (isset($inforUserLogin['user_permission'][$nameController])) {
                    if (isset($inforUserLogin['user_permission'][$nameController][PERMISS_APPROVE])) {
                        $arrPermission['APPROVE'] = '1';
                    }
                    if (isset($inforUserLogin['user_permission'][$nameController][PERMISS_CREATE_ORDER])) {
                        $arrPermission['CREATE_ORDER'] = '1';
                    }
                    if (isset($inforUserLogin['user_permission'][$nameController][PERMISS_INSPECTION])) {
                        $arrPermission['INSPECTION'] = '1';
                    }
                    if (isset($inforUserLogin['user_permission'][$nameController][PERMISS_VIEW])) {
                        $arrPermission['PER_VIEW'] = '1';
                    }
                }
            } else {
                $arrPermission['PER_VIEW'] = '1';
                $arrPermission['APPROVE'] = '1';
                $arrPermission['CREATE_ORDER'] = '1';
                $arrPermission['INSPECTION'] = '1';
            }
        }
        return $arrPermission;
    }

    /**
     * @param string $fileId
     * @return mixed|string
     * Content-Type: image/jpeg
     * Content-Type: application/pdf
     */
    public function getTypeFileId($fileId = '')
    {
        $url = Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/' . $fileId;
        $content_type = '';
        $type_file = 'image';
        $headers = get_headers($url);
        foreach ($headers as $k => $val) {
            if ($k == 2) {
                $content_type = $val;
                break;
            }
        }
        switch ($content_type) {
            case 'Content-Type: application/pdf':
                $type_file = 'pdf';
                break;
            case 'Content-Type: image/jpeg':
            case 'Content-Type: image/jpg':
            case 'Content-Type: image/png':
                $type_file = 'image';
                break;
        }
        return $type_file;
    }

    /**
     *Ký số lại cho đơn hàng
     */
    public function resignCertificate($dataInput)
    {
        $folderLog = FOLDER_LOG_RESIGN.getIntDateM_Y();
        $fileName = 'resignCertificate'.getIntDateD_M_Y().'.log';

        $certificate_no = $dataInput['CERTIFICATE_NO'];
        $product_code = $dataInput['PRODUCT_CODE'];
        $arrAjax = ['success' => STATUS_INT_KHONG,'file_signed' => '', 'message' => 'Không thành công! Ký số lỗi.'];
        if (trim($certificate_no) != '' && trim($product_code) != '') {
            $dataRequest['CERTIFICATE_NO'] = $certificate_no;
            $dataRequest['PRODUCT_CODE'] = $product_code;

            debugLog('KY SO CERTIFICATE_NO='.$certificate_no. ' VA PRODUCT_CODE='.$product_code,$fileName,$folderLog);

            $modelInsurCore = new InsurCore();
            $result = $modelInsurCore->getDataResignCertificate($dataRequest);
            debugLog('Data getDataResignCertificate',$fileName,$folderLog);
            debugLog($result,$fileName,$folderLog);

            if (isset($result['Success']) && $result['Success'] == STATUS_INT_MOT && isset($result['Data'][0]->UPDATE_TRUE)) {
                $updateTrue = isset($result['Data'][0]->UPDATE_TRUE) ? $result['Data'][0]->UPDATE_TRUE : '0';

                if (strlen($updateTrue) > 1) {
                    $arrInput = json_decode($updateTrue, true);

                    debugLog('Data arrInput',$fileName,$folderLog);
                    debugLog($arrInput,$fileName,$folderLog);
                    if (isset($arrInput['data']['openapi']) && trim($arrInput['data']['openapi']) != '' && isset($arrInput['data']['DATA']) && !empty($arrInput['data']['DATA'])) {
                        $urlResign = $arrInput['data']['openapi'];
                        $dataResign = $arrInput['data']['DATA'][0];
                        $resultResign = $modelInsurCore->resignCertificate($dataResign, $urlResign);

                        debugLog('Data resultResign',$fileName,$folderLog);
                        debugLog($resultResign,$fileName,$folderLog);

                        //ký số thành công
                        if (isset($resultResign['Success']) && $resultResign['Success'] == STATUS_INT_MOT && isset($resultResign['Data'][0]) && !empty($resultResign['Data'][0])) {
                            $file_sign = isset($resultResign['Data'][0]->FILE_ID) ? $resultResign['Data'][0]->FILE_ID : '';
                            if (trim($file_sign) != '') {
                                //update lại file id
                                $updateFileSign = $dataRequest;
                                $updateFileSign['FILE_SIGNED'] = $file_sign;
                                $resultFile = $modelInsurCore->updateFileSigned($updateFileSign);

                                debugLog('Data $resultFile',$fileName,$folderLog);
                                debugLog($resultResign,$fileName,$folderLog);

                                if (isset($resultFile['Success']) && $resultFile['Success'] == STATUS_INT_MOT) {
                                    $arrAjax['success'] = STATUS_INT_MOT;
                                    $arrAjax['file_signed'] = $file_sign;
                                    $arrAjax['message'] = 'Ký lại số thành công!';
                                }
                            }else{
                                $arrAjax['message'] = 'Không sinh ra file ký số mới!';
                            }
                        }else{
                            $arrAjax['message'] = 'Ký số không thành công!';
                        }
                    }else{
                        $arrAjax['message'] = 'Data không đủ để ký số lại!';
                    }
                } else {
                    $arrAjax['message'] = 'Đơn này không có thay đổi để ký số lại!';
                }
            }else{
                $arrAjax['message'] = 'Không có dữ liệu ký số UPDATE_TRUE';
            }
            endLog($fileName,$folderLog);
        }
        return $arrAjax;
    }
}

