<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 13/03/2022
* @Version   : 1.0
*/

namespace App\Services;

use App\Models\System\UserSystem;
use Illuminate\Support\Facades\Config;

//use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use Excel;


/**
 * Class ActionExcel
 * @package App\Services
 * Các function liên quan đến ActionExcel
 */
class ActionExcel
{
    protected $objUser = [];

    //voucher
    const EXPORT_EXCEL_VOUCHER_DETAIL = 'VOUCHER_DETAIL';
    const EXPORT_EXCEL_CUSTOMER_VOUCHER = 'CUSTOMER_VOUCHER';
    const EXPORT_EXCEL_CUSTOMER_HEALTH = 'CUSTOMER_HEALTH';
    const EXPORT_EXCEL_VOUCHER_COMMON = 'VOUCHER_COMMON';

    //bồi thường
    const EXPORT_EXCEL_CLAIM_HDI = 'CLAIM_HDI';
    const EXPORT_EXCEL_CLAIM_VIETJET = 'CLAIM_VIETJET';

    //insmart
    const EXPORT_EXCEL_INSMART = 'EXPORT_EXCEL_INSMART';

    //Excel product
    const EXPORT_PRODUCT_REPORT = 'EXPORT_PRODUCT_REPORT';

    //Cấp đơn theo lô
    const EXPORT_ORDERS_IN_BATCHES = 'EXPORT_ORDERS_IN_BATCHES';//rút gọn
    const EXPORT_ORDERS_IN_BATCHES_DETAIL = 'EXPORT_ORDERS_IN_BATCHES_DETAIL';//chi tiết

    //Excel detail product
    const EXPORT_PRODUCT_DETAIL_XCG_TNDSBB = 'EXPORT_PRODUCT_DETAIL_XCG_TNDSBB';
    const EXPORT_PRODUCT_DETAIL_XCG_VCX_NEW = 'EXPORT_PRODUCT_DETAIL_XCG_VCX_NEW';
    const EXPORT_PRODUCT_DETAIL_BAY_AT = 'EXPORT_PRODUCT_DETAIL_BAY_AT';
    const EXPORT_PRODUCT_DETAIL_SKY_CARE = 'EXPORT_PRODUCT_DETAIL_SKY_CARE';
    const EXPORT_PRODUCT_DETAIL_SKY_CARE_DOMESTIC = 'EXPORT_PRODUCT_DETAIL_SKY_CARE_DOMESTIC';
    const EXPORT_PRODUCT_DETAIL_LOST_BAGGAGE = 'EXPORT_PRODUCT_DETAIL_LOST_BAGGAGE';
    const EXPORT_PRODUCT_DETAIL_ATTD_NEW = 'EXPORT_PRODUCT_DETAIL_ATTD_NEW';
    const EXPORT_PRODUCT_DETAIL_COMMON = 'EXPORT_PRODUCT_DETAIL_COMMON';

    //Excel Exchange - export theo sàn
    const EXPORT_PRODUCT_EXCHANGE_XCG_TNDSBB = 'EXPORT_PRODUCT_EXCHANGE_XCG_TNDSBB';

    //Account: đối soát bảo hiểm hành lý
    const EXPORT_ACCOUNTANT_LOST_BAGGAGE = 'EXPORT_ACCOUNTANT_LOST_BAGGAGE';
    const EXPORT_ACCOUNTANT_COMMON = 'EXPORT_ACCOUNTANT_COMMON';

    //Excel Đối soát Bay AT
    const EXPORT_RECONCILIATION_BAY_AT = 'EXPORT_RECONCILIATION_BAY_AT';

    //Excel Đối soát Bay AT
    const EXPORT_SALES_FLIGHT_DELAY = 'EXPORT_SALES_FLIGHT_DELAY';
    const EXPORT_DETAIL_FLIGHT_DELAY = 'EXPORT_DETAIL_FLIGHT_DELAY';

    //Excel Đối soát Bay AT
    const EXPORT_RECONCILIATION_SKY_CARE = 'EXPORT_RECONCILIATION_SKY_CARE';
    const EXPORT_RECONCILIATION_SKY_CARE_DOMESTIC = 'EXPORT_RECONCILIATION_SKY_CARE_DOMESTIC';

    const EXPORT_DATA_GOLIVE = 'EXPORT_DATA_GOLIVE';

    public function __construct()
    {
        $this->objUser = new UserSystem();
    }

    private $link_style = array(
        'font' => array(
            'color' => array('rgb' => '0000FF'),
            'underline' => 'single'
        ),
        'alignment' => array(
            'horizontal' => 'center',
            'vertical' => 'center'
        )
    );

    public function exportExcel($dataView, $type = '', $dataOther = [])
    {
        switch (strtoupper($type)) {
            //voucher
            case self::EXPORT_EXCEL_VOUCHER_DETAIL:
                $this->exportVoucherDetail($dataView);
                break;
            case self::EXPORT_EXCEL_CUSTOMER_VOUCHER:
                $this->exportCustomerVoucher($dataView, $dataOther);
                break;
            case self::EXPORT_PRODUCT_DETAIL_ATTD_NEW://Bình an cá nhân
                $this->exportAttdNew($dataView, $dataOther);
                break;
            case self::EXPORT_EXCEL_CUSTOMER_HEALTH:
                $this->exportCustomerHealth($dataView, $dataOther);
                break;
            case self::EXPORT_EXCEL_VOUCHER_COMMON:
                $this->exportVoucherCommon($dataView, $dataOther);
                break;
            case self::EXPORT_EXCEL_INSMART:
                $this->exportInsmart($dataView, $dataOther);
                break;
            //bồi thường
            case self::EXPORT_EXCEL_CLAIM_HDI:
                $this->exportClaimHdi($dataView, $dataOther);
                break;
            case self::EXPORT_EXCEL_CLAIM_VIETJET:
                $this->exportClaimVietJet($dataView, $dataOther);
                break;
            //sản phẩm
            case self::EXPORT_PRODUCT_REPORT:
                $this->exportProductVietjet($dataView, $dataOther);
                break;
            //export theo sàn
            case self::EXPORT_PRODUCT_EXCHANGE_XCG_TNDSBB:
                $this->exportProductExchangeXCG_TNDSBB($dataView, $dataOther);
                break;
            //chi tiết sản phẩm
            case self::EXPORT_PRODUCT_DETAIL_COMMON:
                $this->exportProductDetail($dataView, $dataOther);
                break;
            case self::EXPORT_PRODUCT_DETAIL_XCG_TNDSBB:
                $this->exportProductDetailXCG_TNDSBB($dataView, $dataOther);
                break;
            case self::EXPORT_PRODUCT_DETAIL_XCG_VCX_NEW:
                $this->exportProductDetailVCX_NEW($dataView, $dataOther);
                break;
            case self::EXPORT_PRODUCT_DETAIL_BAY_AT:
                $this->exportProductDetailBayAT($dataView, $dataOther);
                break;
            case self::EXPORT_PRODUCT_DETAIL_SKY_CARE:
            case self::EXPORT_PRODUCT_DETAIL_SKY_CARE_DOMESTIC:
                $this->exportProductDetailSkyCare($dataView, $dataOther);

            //báo cáo trễ chuyến bay
            case self::EXPORT_SALES_FLIGHT_DELAY:
                $this->exportSalesFlightDelay($dataView, $dataOther);
                break;
            case self::EXPORT_DETAIL_FLIGHT_DELAY:
                $this->exportDetailFlightDelay($dataView, $dataOther);
                break;

            case self::EXPORT_PRODUCT_DETAIL_LOST_BAGGAGE:
                $this->exportProductDetailLOST_BAGGAGE($dataView, $dataOther);
                break;
            //đối soát kế toán: bảo hiểm hành lý
            case self::EXPORT_ACCOUNTANT_LOST_BAGGAGE:
                $this->exportAccountantLOST_BAGGAGE($dataView, $dataOther);
                break;
            //đối soát kế toán common
            case self::EXPORT_ACCOUNTANT_COMMON:
                $this->exportAccountantCOMMON($dataView, $dataOther);
                break;

            //Cấp đơn theo lô
            case self::EXPORT_ORDERS_IN_BATCHES:
                $this->exportOrdersInBatches($dataView, $dataOther);
                break;
            case self::EXPORT_ORDERS_IN_BATCHES_DETAIL:
                $this->exportOrdersInBatchesDetail($dataView, $dataOther);
                break;
            //đối soát bay AT
            case self::EXPORT_RECONCILIATION_BAY_AT:
                $this->exportReconciliationBayAT($dataView, $dataOther);
                break;
            //đối soát SKY_CARE
            case self::EXPORT_RECONCILIATION_SKY_CARE:
            case self::EXPORT_RECONCILIATION_SKY_CARE_DOMESTIC:
                $this->exportReconciliationSkyCare($dataView, $dataOther);
                break;
            //data golive
            case self::EXPORT_DATA_GOLIVE:
                $this->exportDataGolive($dataView, $dataOther);
                break;
            default:
                $this->exportDefault($dataView);
                break;
        }
    }

    //voucher
    public function exportVoucherDetail($data)
    {
        if (empty($data))
            return;
        $user = $this->objUser->userLogin();

        $spreadsheet = new Spreadsheet();
        $sheetExcel = $spreadsheet->getActiveSheet();
        $spreadsheet->getProperties()->setCreator('HD Insurance')
            ->setLastModifiedBy('HD Insurance')
            ->setTitle('Office 2007 XLSX Document')
            ->setSubject('Office 2007 XLSX Document')
            ->setDescription('Export HD Insurance')
            ->setKeywords('office 2007 openxml php');
        $dataOne = $data[0];
        $fileName = 'Danh Sách Mã Voucher';
        $sheetExcel->mergeCells('A1:B1');
        $sheetExcel->setCellValue('A1', $fileName)->getStyle('A1:B1')->applyFromArray(array(
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        $sheetExcel->mergeCells('A3:B3');
        $sheetExcel->setCellValue('A3', 'Người xuất: ' . $user['user_full_name']);

        $sheetExcel->mergeCells('A4:B4');
        $sheetExcel->setCellValue('A4', 'Thời gian xuất file: ' . date('d-m-Y'));

        $sheetExcel->mergeCells('A5:B5');
        $sheetExcel->setCellValue('A5', 'Đối tác: ' . $dataOne->ORG_NAME);

        $sheetExcel->mergeCells('A6:B6');
        $sheetExcel->setCellValue('A6', 'Chiến dịch: ' . $dataOne->CAMPAIGN_NAME);

        $sheetExcel->mergeCells('A7:B7');
        $sheetExcel->setCellValue('A7', 'Sản phẩm: ' . $dataOne->PRODUCT_NAME);

        $sheetExcel->mergeCells('A8:B8');
        $sheetExcel->setCellValue('A8', 'Gói: ' . $dataOne->PACK_NAME);

        $sheetExcel->mergeCells('A9:B9');
        $sheetExcel->setCellValue('A9', 'Mã block: ' . $dataOne->BLOCK_CODE);

        $sheetExcel->mergeCells('A10:B10');
        $sheetExcel->setCellValue('A10', 'Số lượng cấp phát: ' . $dataOne->AMOUNT_ALLOCATE);

        //title
        $sheetExcel->setCellValue('A12', "STT");
        $sheetExcel->getStyle('A12')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheetExcel->setCellValue('B12', "Mã Voucher");
        $sheetExcel->getStyle('A12:B12')->applyFromArray(array(
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        //fill data
        $i = 13;
        if (!empty($data)) {
            foreach ($data as $k => $item) {
                $sheetExcel->setCellValue('A' . $i, $k + 1)->getStyle('A' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheetExcel->setCellValue('B' . $i, $item->ACTIVATION_CODE)->getStyle('B' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $i++;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="danh_sach_voucher_.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function exportCustomerVoucher($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = isset($dataInput['total']) ? $dataInput['total'] : 0;
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:M2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:M2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Chương trình");

        $sheetExcel->setCellValue('D' . $i, "Tên khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Email");
        $sheetExcel->setCellValue('F' . $i, "Phone");
        $sheetExcel->setCellValue('G' . $i, "CMT");
        $sheetExcel->setCellValue('H' . $i, "Ngày sinh");
        $sheetExcel->setCellValue('I' . $i, "Địa chỉ");
        $sheetExcel->setCellValue('J' . $i, "Tài khoản ngân hàng");

        $sheetExcel->setCellValue('K' . $i, "Gói");
        $sheetExcel->setCellValue('L' . $i, "Mã");
        $sheetExcel->setCellValue('M' . $i, "Số hiệu");
        $sheetExcel->setCellValue('N' . $i, "Ngày đăng ký");
        $sheetExcel->getStyle('A' . $i . ':N' . $i)->getFill()->setFillType('solid')->getStartColor()->setARGB('47aa5e');

        // style header cell of table
        $sheetExcel->getStyle('A' . $i . ':N' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':N' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // style content
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':N' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':L' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_NAME) ? $item->ORG_NAME : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CAMPAIGN_NAME) ? $item->CAMPAIGN_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->PHONE) ? $item->PHONE : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->IDCARD) ? $item->IDCARD : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->DOB) ? convertDateDMY($item->DOB) : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');
            $sheetExcel->setCellValue('J' . $i, isset($item->BANK_ACCOUNT_NUM) ? $item->BANK_ACCOUNT_NUM : '');

            $sheetExcel->setCellValue('K' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->GIFT_CODE) ? $item->GIFT_CODE : '');
            $sheetExcel->setCellValue('M' . $i, isset($item->SERY_NO) ? $item->SERY_NO : '');
            $sheetExcel->setCellValue('N' . $i, isset($item->CREATE_DATE) ? date('d/m/Y H:i', strtotime($item->CREATE_DATE)) : '');
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    public function exportProductDetail($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = isset($dataInput['total']) ? $dataInput['total'] : 0;
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();

        //myDebug($dataInput);
        if (empty($data))
            return;
        set_time_limit(500000);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();

        $sheetExcel->mergeCells('A1:Z2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:Z2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;

        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Chương trình");

        $sheetExcel->setCellValue('D' . $i, "Khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Người được bảo hiểm");
        $sheetExcel->setCellValue('F' . $i, "Email NĐBH");
        $sheetExcel->setCellValue('G' . $i, "Số điện thoại NĐBH");
        $sheetExcel->setCellValue('H' . $i, "CMT/CCCD/ ID card NĐBH");
        $sheetExcel->setCellValue('I' . $i, "Ngày sinh NĐBH");
        $sheetExcel->setCellValue('J' . $i, "Địa chỉ NĐBH");

        $sheetExcel->setCellValue('K' . $i, "Gói");
        $sheetExcel->setCellValue('L' . $i, "Số GCN");
        $sheetExcel->setCellValue('M' . $i, "Số serial");
        $sheetExcel->setCellValue('N' . $i, "Ngày cấp GCN");
        $sheetExcel->setCellValue('O' . $i, "Url GCN");

        $sheetExcel->setCellValue('P' . $i, "Phí bảo hiểm (chưa VAT)");
        $sheetExcel->setCellValue('Q' . $i, "VAT của phí bảo hiểm");
        $sheetExcel->setCellValue('R' . $i, "Phí dịch vụ bổ trợ (chưa VAT)");
        $sheetExcel->setCellValue('S' . $i, "VAT của phí dịch vụ bổ trợ");
        $sheetExcel->setCellValue('T' . $i, "Tổng tiền (đã gồm VAT)");
        $sheetExcel->setCellValue('U' . $i, "Số tiền bảo hiểm");
        $sheetExcel->setCellValue('V' . $i, "Thời gian bắt đầu bảo hiểm");
        $sheetExcel->setCellValue('W' . $i, "Thời gian kết thúc bảo hiểm");

        $sheetExcel->setCellValue('X' . $i, "Tên NV cấp");
        $sheetExcel->setCellValue('Y' . $i, "Mã NV cấp");
        $sheetExcel->setCellValue('Z' . $i, "Email NV cấp");

        $sheetExcel->getStyle('A' . $i . ':Z' . $i)->getFill()->setFillType('solid')->getStartColor()->setARGB('47aa5e');

        // style header cell of table
        $sheetExcel->getStyle('A' . $i . ':Z' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':Z' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // style content
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':Z' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':L' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_NAME) ? $item->ORG_NAME : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->PROG_NAME) ? $item->PROG_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');//tên khách hàng
            $sheetExcel->setCellValue('E' . $i, isset($item->INSURED_NAME) ? $item->INSURED_NAME : '');//tên người được bảo hiểm
            $sheetExcel->setCellValue('F' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->PHONE) ? $item->PHONE : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->IDCARD) ? $item->IDCARD : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->DOB) ? convertDateDMY($item->DOB) : '');
            $sheetExcel->setCellValue('J' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');

            $sheetExcel->setCellValue('K' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');//số GCN
            $sheetExcel->setCellValue('M' . $i, isset($item->SERIAL) ? $item->SERIAL : '');//số seri
            $sheetExcel->setCellValue('N' . $i, isset($item->CREATE_DATE) ? date('d/m/Y H:i', strtotime($item->CREATE_DATE)) : '');

            $sheetExcel->setCellValue('O' . $i, isset($item->URL_CER) ? $item->URL_CER : '');
            //$sheetExcel->getCell('O' . $i)->getHyperlink()->setUrl(isset($item->URL_CER) ? $item->URL_CER : '#');
            //$sheetExcel->getStyle('O' . $i)->applyFromArray($this->link_style);

            $sheetExcel->setCellValue('P' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');//Phí bảo hiểm (chưa VAT): $item->FEES: cũ
            $sheetExcel->setCellValue('Q' . $i, isset($item->VAT) ? $item->VAT : '');//VAT của phí bảo hiểm
            $sheetExcel->setCellValue('R' . $i, isset($item->ADDITIONAL_FEES) ? $item->ADDITIONAL_FEES : '');//Phí dịch vụ bổ trợ (chưa VAT)
            $sheetExcel->setCellValue('S' . $i, isset($item->VAT_ADDITIONAL_FEES) ? $item->VAT_ADDITIONAL_FEES : '');//VAT của phí dịch vụ bổ trợ
            $sheetExcel->setCellValue('T' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');//Tổng tiền (đã gồm VAT)
            $sheetExcel->setCellValue('U' . $i, isset($item->INSUR_AMOUNT) ? $item->INSUR_AMOUNT : '');//Số tiền bảo hiểm

            $sheetExcel->setCellValue('V' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');//Ngày hiệu bắt đầu lực bảo hiểm
            $sheetExcel->setCellValue('W' . $i, isset($item->EXPIRATION_DATE) ? $item->EXPIRATION_DATE : '');//Ngày hiệu kết thúc hiệu lực bảo hiểm

            $sheetExcel->setCellValue('X' . $i, isset($item->SELLER_NAME) ? $item->SELLER_NAME : '');//Tên nhân viên cấp đơn
            $sheetExcel->setCellValue('Y' . $i, isset($item->SELLER_CODE) ? $item->SELLER_CODE : '');//Mã NV
            $sheetExcel->setCellValue('Z' . $i, isset($item->SELLER_EMAIL) ? $item->SELLER_EMAIL : '');//Mail NV
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //bình an cá nhân
    public function exportAttdNew($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = isset($dataInput['total']) ? $dataInput['total'] : 0;
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:W2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:W2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Chương trình");

        $sheetExcel->setCellValue('D' . $i, "Tên khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Email");
        $sheetExcel->setCellValue('F' . $i, "Phone");
        $sheetExcel->setCellValue('G' . $i, "CMT");
        $sheetExcel->setCellValue('H' . $i, "Ngày sinh");
        $sheetExcel->setCellValue('I' . $i, "Địa chỉ");
        $sheetExcel->setCellValue('J' . $i, "Tài khoản ngân hàng");

        $sheetExcel->setCellValue('K' . $i, "Gói");
        $sheetExcel->setCellValue('L' . $i, "Mã");
        $sheetExcel->setCellValue('M' . $i, "Số hiệu");
        $sheetExcel->setCellValue('N' . $i, "Ngày đăng ký");

        $sheetExcel->setCellValue('O' . $i, "Số GCN");
        $sheetExcel->setCellValue('P' . $i, "Số serial");
        $sheetExcel->setCellValue('Q' . $i, "Phí bảo hiểm (chưa VAT)");
        $sheetExcel->setCellValue('R' . $i, "VAT của phí bảo hiểm");
        $sheetExcel->setCellValue('S' . $i, "Phí dịch vụ bổ trợ (chưa VAT)");
        $sheetExcel->setCellValue('T' . $i, "VAT của phí dịch vụ bổ trợ");
        $sheetExcel->setCellValue('U' . $i, "Tổng tiền (đã gồm VAT)");
        $sheetExcel->setCellValue('V' . $i, "Số tiền bảo hiểm");
        $sheetExcel->setCellValue('W' . $i, "Thời hạn bảo hiểm");

        $sheetExcel->getStyle('A' . $i . ':W' . $i)->getFill()->setFillType('solid')->getStartColor()->setARGB('47aa5e');

        // style header cell of table
        $sheetExcel->getStyle('A' . $i . ':W' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':W' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // style content
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':W' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':L' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_NAME) ? $item->ORG_NAME : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CAMPAIGN_NAME) ? $item->CAMPAIGN_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->PHONE) ? $item->PHONE : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->IDCARD) ? $item->IDCARD : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->DOB) ? convertDateDMY($item->DOB) : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');
            $sheetExcel->setCellValue('J' . $i, isset($item->BANK_ACCOUNT_NUM) ? $item->BANK_ACCOUNT_NUM : '');

            $sheetExcel->setCellValue('K' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->GIFT_CODE) ? $item->GIFT_CODE : '');
            $sheetExcel->setCellValue('M' . $i, isset($item->SERY_NO) ? $item->SERY_NO : '');
            $sheetExcel->setCellValue('N' . $i, isset($item->CREATE_DATE) ? date('d/m/Y H:i', strtotime($item->CREATE_DATE)) : '');

            $sheetExcel->setCellValue('O' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');//số GCN
            $sheetExcel->setCellValue('P' . $i, isset($item->SERIAL) ? $item->SERIAL : '');//serial
            $sheetExcel->setCellValue('Q' . $i, isset($item->FEES) ? $item->FEES : '');//Phí bảo hiểm (chưa VAT)
            $sheetExcel->setCellValue('R' . $i, isset($item->VAT) ? $item->VAT : '');//VAT của phí bảo hiểm
            $sheetExcel->setCellValue('S' . $i, '');//Phí dịch vụ bổ trợ (chưa VAT)
            $sheetExcel->setCellValue('T' . $i, '');//VAT của phí dịch vụ bổ trợ
            $sheetExcel->setCellValue('U' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');//Tổng tiền (đã gồm VAT)
            $sheetExcel->setCellValue('V' . $i, isset($item->INSUR_AMOUNT) ? $item->INSUR_AMOUNT : '');//Số tiền bảo hiểm
            $sheetExcel->setCellValue('W' . $i, isset($item->INSUR_TIME) ? $item->INSUR_TIME : '');//Ngày hiệu lực bảo hiểm
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    public function exportCustomerHealth($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = isset($dataInput['total']) ? $dataInput['total'] : 0;
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:O2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:O2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Chương trình");

        $sheetExcel->setCellValue('D' . $i, "Tên khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Người mua bảo hiểm");
        $sheetExcel->setCellValue('F' . $i, "Đối tượng bảo hiểm");
        $sheetExcel->setCellValue('G' . $i, "Email");
        $sheetExcel->setCellValue('H' . $i, "Phone");
        $sheetExcel->setCellValue('I' . $i, "Mã NV");

        $sheetExcel->setCellValue('J' . $i, "CMND");
        $sheetExcel->setCellValue('K' . $i, "Ngày sinh");
        $sheetExcel->setCellValue('L' . $i, "Địa chỉ");
        $sheetExcel->setCellValue('M' . $i, "Tài khoản ngân hàng");
        $sheetExcel->setCellValue('N' . $i, "Gói");
        $sheetExcel->setCellValue('O' . $i, "Mã");
        $sheetExcel->setCellValue('P' . $i, "Số tiền");
        $sheetExcel->setCellValue('Q' . $i, "Số hiệu");
        $sheetExcel->setCellValue('R' . $i, "Ngày đăng ký");

        $sheetExcel->getStyle('A' . $i . ':R' . $i)->getFill()->setFillType('solid')->getStartColor()->setARGB('47aa5e');
        // style header cell of table
        $sheetExcel->getStyle('A' . $i . ':R' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':R' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // style content
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':R' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':M' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_CODE) ? $item->ORG_CODE : '');//đối tác
            $sheetExcel->setCellValue('C' . $i, isset($item->CAMPAIGN_NAME) ? $item->CAMPAIGN_NAME : '');//chương trình

            $sheetExcel->setCellValue('D' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');//tên khách hàng
            $sheetExcel->setCellValue('E' . $i, isset($item->STAFF_NAME) ? $item->STAFF_NAME : '');//người mua bảo hiểm
            $sheetExcel->setCellValue('F' . $i, isset($item->RELATIONSHIP) ? $item->RELATIONSHIP : '');//đối tượng bảo hiểm
            $sheetExcel->setCellValue('G' . $i, isset($item->EMAIL) ? $item->EMAIL : '');//email
            $sheetExcel->setCellValue('H' . $i, isset($item->PHONE) ? $item->PHONE : '');//phone
            $sheetExcel->setCellValue('I' . $i, isset($item->STAFF_CODE) ? $item->STAFF_CODE : '');//mã nhân viên

            $sheetExcel->setCellValue('J' . $i, isset($item->IDCARD) ? $item->IDCARD : '');//CMND
            $sheetExcel->setCellValue('K' . $i, isset($item->DOB) ? convertDateDMY($item->DOB) : '');//ngày sinh
            $sheetExcel->setCellValue('L' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');//địa chỉ
            $sheetExcel->setCellValue('M' . $i, isset($item->BANK_ACCOUNT_NUM) ? $item->BANK_ACCOUNT_NUM : '');//tài khoản ngân hàng
            $sheetExcel->setCellValue('N' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');//gói
            $sheetExcel->setCellValue('O' . $i, isset($item->GIFT_CODE) ? $item->GIFT_CODE : '');//mã
            $sheetExcel->setCellValue('P' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');//số tiền
            $sheetExcel->setCellValue('Q' . $i, isset($item->SERY_NO) ? $item->SERY_NO : '');//số hiệu
            $sheetExcel->setCellValue('R' . $i, isset($item->CREATE_DATE) ? date('d/m/Y H:i', strtotime($item->CREATE_DATE)) : '');//ngày đăng ký
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    public function exportVoucherCommon($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:I2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:I2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Chương trình");

        $sheetExcel->setCellValue('D' . $i, "Sản phẩm");
        $sheetExcel->setCellValue('E' . $i, "Gói");
        $sheetExcel->setCellValue('F' . $i, "Mã");

        $sheetExcel->setCellValue('G' . $i, "Tổng kích hoạt");
        $sheetExcel->setCellValue('H' . $i, "Tổng cấp phát");
        $sheetExcel->setCellValue('I' . $i, "Doanh thu");

        $sheetExcel->getStyle('A' . $i . ':I' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':I' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // style content
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':I' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':I' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_NAME) ? $item->ORG_NAME : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CAMPAIGN_NAME) ? $item->CAMPAIGN_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->REF_CODE) ? $item->REF_CODE : '');

            $sheetExcel->setCellValue('G' . $i, isset($item->NUM_OF_ACTIVE) ? $item->NUM_OF_ACTIVE : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->AMOUNT_ALLOCATE) ? $item->AMOUNT_ALLOCATE : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->REVENUE) ? $item->REVENUE : '');

            $i++;
        }
        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //save file and move file excel
    public function exportInsmart($data = [], $dataOther = [])
    {
        if (empty($data))
            return;
        $total_staff = array_key_last($data);
        $spreadsheet = new Spreadsheet();
        $fileName = $dataOther['fileName'];
        $spreadsheet->getProperties()->setCreator('HDI')
            ->setLastModifiedBy('HDI')
            ->setTitle('Office 2007 XLSX Document')
            ->setSubject('Office 2007 XLSX Document')
            ->setDescription('Export ' . $fileName)
            ->setKeywords('office 2007 openxml php');
        $sheetExcel = $spreadsheet->getActiveSheet();
        //tên file excel
        $sheetExcel->mergeCells('A1:P2');
        $sheetExcel->setCellValue('A1', $fileName . ' - Ngày ' . date('d-m-Y H:i'));
        $sheetExcel->getStyle('A1:P2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $total_staff = $total_staff + 1;
        $i = 3;
        //header 1
        $sheetExcel->getStyle('A' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->mergeCells('B' . $i . ':G' . $i);
        $sheetExcel->setCellValue('B' . $i, 'Thông tin Hợp đồng bảo hiểm');
        $sheetExcel->getStyle('B' . $i . ':G' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('A7C538');

        $sheetExcel->mergeCells('H' . $i . ':O' . $i);
        $sheetExcel->setCellValue('H' . $i, 'Thông tin Người được bảo hiểm');
        $sheetExcel->getStyle('H' . $i . ':O' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4E943C');

        $sheetExcel->getStyle('P' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':P' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        //header 2
        //header title table excel
        $sheetExcel->setCellValue('A' . $i, "STT");
        $sheetExcel->setCellValue('B' . $i, "Ngày hiệu lực");
        $sheetExcel->setCellValue('C' . $i, "Ngày kết thúc");
        $sheetExcel->setCellValue('D' . $i, "Số hợp đồng");
        $sheetExcel->setCellValue('E' . $i, "Gói bảo hiểm");
        $sheetExcel->setCellValue('F' . $i, "Phí bảo hiểm");
        $sheetExcel->setCellValue('G' . $i, "Ngày đóng phí");

        $sheetExcel->setCellValue('H' . $i, "Tên người bảo hiểm");
        $sheetExcel->setCellValue('I' . $i, "DOB");
        $sheetExcel->setCellValue('J' . $i, "Số CMND/ Căn cước");
        $sheetExcel->setCellValue('K' . $i, "Giới tính");
        $sheetExcel->setCellValue('L' . $i, "Địa chỉ");
        $sheetExcel->setCellValue('M' . $i, "Email");
        $sheetExcel->setCellValue('N' . $i, "Số điện thoại");
        $sheetExcel->setCellValue('O' . $i, "Ghi chú đặc biệt nếu có");

        $sheetExcel->setCellValue('P' . $i, "Link GCN");
        // style header cell of table
        $sheetExcel->getStyle('A' . $i . ':P' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':P' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // style content
        $col_next = $total_staff + $i;
        $i = $i + 1;
        $sheetExcel->getStyle('A' . $i . ':P' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':I' . $col_next);
        if (!empty($data)) {
            foreach ($data as $k => $staff) {
                $sheetExcel->setCellValue('A' . $i, $k + 1);// index column
                $sheetExcel->setCellValue('B' . $i, isset($staff->EFFECTIVE_DATE) ? convertDateDMY($staff->EFFECTIVE_DATE) : '');//ngày hiệu lực
                $sheetExcel->setCellValue('C' . $i, isset($staff->EXPIRATION_DATE) ? convertDateDMY($staff->EXPIRATION_DATE) : '');//ngày kết thúc
                $sheetExcel->setCellValue('D' . $i, isset($staff->CONTRACT_NO) ? $staff->CONTRACT_NO : '');//số hợp đồng
                $sheetExcel->setCellValue('E' . $i, isset($staff->PACK_NAME) ? $staff->PACK_NAME : '');//gói BH
                $sheetExcel->setCellValue('F' . $i, isset($staff->FEES) ? $staff->FEES : '');//phí
                $sheetExcel->setCellValue('G' . $i, isset($staff->PAID) ? (($staff->PAID == STATUS_INT_MOT) ? 'Đã đóng phí' : 'Chưa đóng phí') : '');//ngày đóng phí

                $sheetExcel->setCellValue('H' . $i, isset($staff->CUS_NAME) ? $staff->CUS_NAME : '');//tên KH
                $sheetExcel->setCellValue('I' . $i, isset($staff->DOB) ? convertDateDMY($staff->DOB) : '');//ngày sinh
                $sheetExcel->setCellValue('J' . $i, isset($staff->IDCARD) ? $staff->IDCARD : '');//số CMND
                $sheetExcel->setCellValue('K' . $i, isset($staff->GENDER) ? (($staff->GENDER == GIOI_TINH_NAM) ? 'Nam' : 'Nữ') : '');//giới tính
                $sheetExcel->setCellValue('L' . $i, isset($staff->ADDRESS) ? $staff->ADDRESS : '');//địa chỉ
                $sheetExcel->setCellValue('M' . $i, isset($staff->EMAIL) ? $staff->EMAIL : '');//email
                $sheetExcel->setCellValue('N' . $i, isset($staff->PHONE) ? $staff->PHONE : '');//sdt
                $sheetExcel->setCellValue('O' . $i, '');//ghi chú
                $sheetExcel->setCellValue('P' . $i, isset($staff->LINK_INSMART) ? $staff->LINK_INSMART : '');//link

                $i++;
            }
        }

        //xóa file tồn tại
        $path_folder_upload = Config::get('config.DIR_ROOT');
        $url_file = $path_folder_upload . '/' . $fileName . '.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($url_file);
        return $url_file;
    }

    //excel bồi thường
    public function exportClaimHdi($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:I2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:I2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Sản phẩm");
        $sheetExcel->setCellValue('C' . $i, "Hợp đồng bảo hiểm");

        $sheetExcel->setCellValue('D' . $i, "Khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Số tiền bồi thường");
        $sheetExcel->setCellValue('F' . $i, "Kênh tiếp nhận");

        $sheetExcel->setCellValue('G' . $i, "Ngày yêu cầu");
        $sheetExcel->setCellValue('H' . $i, "Người xử lý");
        $sheetExcel->setCellValue('I' . $i, "Trạng thái");

        //style content
        $sheetExcel->getStyle('A' . $i . ':I' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':I' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':I' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':I' . $col_next);
        foreach ($data as $k => $staff) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($staff->PRODUCT_NAME) ? $staff->PRODUCT_NAME : '');
            $sheetExcel->setCellValue('C' . $i, isset($staff->CONTRACT_NO) ? $staff->CONTRACT_NO : '');

            $sheetExcel->setCellValue('D' . $i, isset($staff->NAME) ? $staff->NAME : '');
            $sheetExcel->setCellValue('E' . $i, isset($staff->REQUIRED_AMOUNT) ? $staff->REQUIRED_AMOUNT : '');
            $sheetExcel->setCellValue('F' . $i, isset($staff->CHANNEL) ? $staff->CHANNEL : '');

            $sheetExcel->setCellValue('G' . $i, isset($staff->CLAIM_DATE) ? $staff->CLAIM_DATE : '');
            $sheetExcel->setCellValue('H' . $i, isset($staff->STAFF_NAME) ? $staff->STAFF_NAME : '');
            $sheetExcel->setCellValue('I' . $i, isset($staff->STATUS_NAME) ? $staff->STATUS_NAME : '');

            $i++;
        }
        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    public function exportClaimVietJet($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:R2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:R2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Số hiệu chuyến bay");
        $sheetExcel->setCellValue('C' . $i, "Mã đặt chỗ");

        $sheetExcel->setCellValue('D' . $i, "Số ghế");
        $sheetExcel->setCellValue('E' . $i, "Tên khách hàng");
        $sheetExcel->setCellValue('F' . $i, "CMND/CCCD/Hộ chiếu");

        $sheetExcel->setCellValue('G' . $i, "Số điện thoại");
        $sheetExcel->setCellValue('H' . $i, "Email");
        $sheetExcel->setCellValue('I' . $i, "Ngày mua bảo hiểm");

        $sheetExcel->setCellValue('J' . $i, "Sản phẩm bảo hiểm");
        $sheetExcel->setCellValue('K' . $i, "Số giấy chứng nhận");
        $sheetExcel->setCellValue('L' . $i, "Ngày yêu cầu bồi thường");

        $sheetExcel->setCellValue('M' . $i, "Ngày HDI nhận hồ sơ");
        $sheetExcel->setCellValue('N' . $i, "Ngày HDI bồi thường");
        $sheetExcel->setCellValue('O' . $i, "Số tiền yêu cầu bồi thường");

        $sheetExcel->setCellValue('P' . $i, "Số tiền HDI bồi thường");
        $sheetExcel->setCellValue('Q' . $i, "Trạng thái bồi thường");
        $sheetExcel->setCellValue('R' . $i, "Ghi chú");

        //style content
        $sheetExcel->getStyle('A' . $i . ':R' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':R' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':R' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':I' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->FLIGHT_NO) ? $item->FLIGHT_NO : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->BOOKING_ID) ? $item->BOOKING_ID : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->SEAT) ? $item->SEAT : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->NAME) ? $item->NAME : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->IDCARD) ? $item->IDCARD : '');

            $sheetExcel->setCellValue('G' . $i, isset($item->PHONE) ? $item->PHONE : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');//NGÀY MUA

            $sheetExcel->setCellValue('J' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->CLAIM_DATE) ? $item->CLAIM_DATE : '');
            $sheetExcel->setCellValue('M' . $i, isset($item->PROCESS_DATE) ? $item->PROCESS_DATE : '');//ngày HDI nhận

            $sheetExcel->setCellValue('N' . $i, isset($item->PAY_DATE) ? $item->PAY_DATE : '');
            $sheetExcel->setCellValue('O' . $i, isset($item->REQUIRED_AMOUNT) ? $item->REQUIRED_AMOUNT : '');//số tiền yêu cầu
            $sheetExcel->setCellValue('P' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');//số tiền bồi thường

            $sheetExcel->setCellValue('Q' . $i, isset($item->TYPE_NAME) ? $item->TYPE_NAME : '');
            $sheetExcel->setCellValue('R' . $i, isset($item->CONTENT) ? $item->CONTENT : '');

            $i++;
        }
        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product Vietjet
    public function exportProductVietjet($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:G2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:G2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đơn vị");
        $sheetExcel->setCellValue('C' . $i, "Thông tin sản phẩm");

        $sheetExcel->setCellValue('D' . $i, "Ngày ký");
        $sheetExcel->setCellValue('E' . $i, "Ngày hiệu lực");
        $sheetExcel->setCellValue('F' . $i, "Số hợp đồng");
        $sheetExcel->setCellValue('G' . $i, "Doanh thu");

        //style content
        $sheetExcel->getStyle('A' . $i . ':G' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':G' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':G' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':I' . $col_next);
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_NAME) ? $item->ORG_NAME : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->DATE_SIGN) ? $item->DATE_SIGN : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->NUM_OF_CONTRACT) ? $item->NUM_OF_CONTRACT : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');
            $i++;
        }
        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product Exchange - theo sàn TNDSBB
    public function exportProductExchangeXCG_TNDSBB($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:AC2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:AC2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Sản phẩm");
        $sheetExcel->setCellValue('D' . $i, "Mã đơn hàng");

        $sheetExcel->setCellValue('E' . $i, "Số giấy chứng nhận");
        $sheetExcel->setCellValue('F' . $i, "Mã Gói/SKU");
        $sheetExcel->setCellValue('G' . $i, "Tên SKU");
        $sheetExcel->setCellValue('H' . $i, "Phí của 1 SKU");
        $sheetExcel->setCellValue('I' . $i, "Số lượng");

        $sheetExcel->setCellValue('J' . $i, "Tổng tiền");
        $sheetExcel->setCellValue('K' . $i, "Tên người mua");
        $sheetExcel->setCellValue('L' . $i, "Email");
        $sheetExcel->setCellValue('M' . $i, "Điện thoại");

        $sheetExcel->setCellValue('N' . $i, "Ngày mua");
        $sheetExcel->setCellValue('O' . $i, "Ngày cấp GCN");
        $sheetExcel->setCellValue('P' . $i, "Ngày hết hiệu lực voucher");
        $sheetExcel->setCellValue('Q' . $i, "Trạng thái");

        $sheetExcel->setCellValue('R' . $i, "Mã người cấp");
        $sheetExcel->setCellValue('S' . $i, "Người cấp đơn");
        $sheetExcel->setCellValue('T' . $i, "Email người cấp đơn");

        //style content
        $sheetExcel->getStyle('A' . $i . ':T' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':T' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':T' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        $tongS = $tongT = $tongU = $tongV = $tongV2 = $tongW = $tongX = $tongY = $tongZ = $tongY2 = $tongAA = 0;
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_SELLER_NAME) ? $item->ORG_SELLER_NAME : '');//đối tác
            $sheetExcel->setCellValue('C' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');//sản phẩm
            $sheetExcel->setCellValue('D' . $i, isset($item->ORDER_CODE) ? $item->ORDER_CODE : '');//mã đơn hàng

            $sheetExcel->setCellValue('E' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');//gcn
            $sheetExcel->setCellValue('F' . $i, isset($item->PACK_SKU) ? $item->PACK_SKU : '');//sku
            $sheetExcel->setCellValue('G' . $i, isset($item->SKU_NAME) ? $item->SKU_NAME : '');//sku name
            $sheetExcel->setCellValue('H' . $i, isset($item->SKU_FEES) ? $item->SKU_FEES : '');//
            $sheetExcel->setCellValue('I' . $i, isset($item->QUANTITY) ? $item->QUANTITY : '');//

            $sheetExcel->setCellValue('J' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');// tong tien
            $sheetExcel->setCellValue('K' . $i, isset($item->NAME) ? $item->NAME : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('M' . $i, isset($item->PHONE) ? $item->PHONE : '');

            $sheetExcel->setCellValue('N' . $i, isset($item->DATE_BUY) ? $item->DATE_BUY : '');//ngày mua
            $sheetExcel->setCellValue('O' . $i, isset($item->DATE_SIGN) ? $item->DATE_SIGN : '');//ngày cấp
            $sheetExcel->setCellValue('P' . $i, isset($item->EV_EXPIRATION_DATE) ? $item->EV_EXPIRATION_DATE : '');//Ngày hết hiệu lực voucher
            $sheetExcel->setCellValue('Q' . $i, isset($item->SKU_DETAIL_STATUS) ? $item->SKU_DETAIL_STATUS : '');//trạng thái

            $sheetExcel->setCellValue('R' . $i, isset($item->SELLER_CODE) ? $item->SELLER_CODE : '');//Mã NV
            $sheetExcel->setCellValue('S' . $i, isset($item->SELLER_NAME) ? $item->SELLER_NAME : '');//Tên nhân viên cấp đơn
            $sheetExcel->setCellValue('T' . $i, isset($item->SELLER_EMAIL) ? $item->SELLER_EMAIL : '');//Mail NV
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product detail xe cơ giới - TNDSBB
    public function exportProductDetailXCG_TNDSBB($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:AC2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:AC2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Mã tham chiếu");
        $sheetExcel->setCellValue('D' . $i, "Số giấy chứng nhận");

        $sheetExcel->setCellValue('E' . $i, "Gói/SKU");
        $sheetExcel->setCellValue('F' . $i, "Tên người được bảo hiểm");
        $sheetExcel->setCellValue('G' . $i, "Địa chỉ người được bảo hiểm");
        $sheetExcel->setCellValue('H' . $i, "CMND/CCCD/Hộ chiếu");
        $sheetExcel->setCellValue('I' . $i, "Điện thoại");

        $sheetExcel->setCellValue('J' . $i, "Email");
        $sheetExcel->setCellValue('K' . $i, "Ngày hiệu lực");
        $sheetExcel->setCellValue('L' . $i, "Ngày hết hiệu lực");
        $sheetExcel->setCellValue('M' . $i, "Ngày cấp GCN");

        $sheetExcel->setCellValue('N' . $i, "Nhóm xe");
        $sheetExcel->setCellValue('O' . $i, "Loại xe");
        $sheetExcel->setCellValue('P' . $i, "Mục đích sử dụng xe");
        $sheetExcel->setCellValue('Q' . $i, "Biển số xe");

        $sheetExcel->setCellValue('R' . $i, "Số khung");
        $sheetExcel->setCellValue('S' . $i, "Số máy");
        $sheetExcel->setCellValue('T' . $i, "Phí BH TNDS BB");
        $sheetExcel->setCellValue('U' . $i, "VAT TNDS BB");

        $sheetExcel->setCellValue('V' . $i, "STBH TNDS Tự nguyện");
        $sheetExcel->setCellValue('W' . $i, "Phí BH TNDS tự nguyện");
        $sheetExcel->setCellValue('X' . $i, "VAT TNDS tự nguyện");
        $sheetExcel->setCellValue('Y' . $i, "STBH người ngồi trên xe");
        $sheetExcel->setCellValue('Z' . $i, "Phí BH tai nạn người ngồi trên xe");
        $sheetExcel->setCellValue('AA' . $i, "STBH TNDS chủ xe với hàng hóa");

        $sheetExcel->setCellValue('AB' . $i, "Phí BH TNDS chủ xe với hàng hóa");
        $sheetExcel->setCellValue('AC' . $i, "VAT TNDS chủ xe với hàng hóa");
        $sheetExcel->setCellValue('AD' . $i, "Tổng phí BH");
        $sheetExcel->setCellValue('AE' . $i, "Url GCN");

        $sheetExcel->setCellValue('AF' . $i, "Mã người cấp");
        $sheetExcel->setCellValue('AG' . $i, "Tên người cấp");
        $sheetExcel->setCellValue('AH' . $i, "Email người cấp");

        //style content
        $sheetExcel->getStyle('A' . $i . ':AH' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':AH' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':AH' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        $tongS = $tongT = $tongU = $tongV = $tongV2 = $tongW = $tongX = $tongY = $tongZ = $tongY2 = $tongAA = 0;
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_SELLER_NAME) ? $item->ORG_SELLER_NAME : '');//đối tác
            $sheetExcel->setCellValue('C' . $i, isset($item->PARTNER_ORDER) ? $item->PARTNER_ORDER : '');//mã từ đối tác
            $sheetExcel->setCellValue('D' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');//giấy chứng nhận

            $sheetExcel->setCellValue('E' . $i, isset($item->PACK_SKU) ? $item->PACK_SKU : '');//SKU
            $sheetExcel->setCellValue('F' . $i, isset($item->NAME) ? $item->NAME : '');//tên người bảo hiểm
            $sheetExcel->setCellValue('G' . $i, isset($item->FULL_ADDRESS) ? $item->FULL_ADDRESS : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->IDCARD) ? $item->IDCARD : '');//CMT
            $sheetExcel->setCellValue('I' . $i, isset($item->PHONE) ? $item->PHONE : '');//phone

            $sheetExcel->setCellValue('J' . $i, isset($item->EMAIL) ? $item->EMAIL : '');//email
            $sheetExcel->setCellValue('K' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->EXPIRATION_DATE) ? $item->EXPIRATION_DATE : '');
            $sheetExcel->setCellValue('M' . $i, isset($item->DATE_SIGN) ? $item->DATE_SIGN : '');//ngày cấp GCN

            $sheetExcel->setCellValue('N' . $i, isset($item->GROUP_VEH) ? $item->GROUP_VEH : '');//nhóm xe
            $sheetExcel->setCellValue('O' . $i, isset($item->VEHICLE_TYPE) ? $item->VEHICLE_TYPE : '');//loại xe
            $sheetExcel->setCellValue('P' . $i, isset($item->VEHICLE_USE) ? $item->VEHICLE_USE : '');//mục dich sử dụng
            $sheetExcel->setCellValue('Q' . $i, isset($item->NUMBER_PLATE) ? $item->NUMBER_PLATE : '');//biển số xe

            $sheetExcel->setCellValue('R' . $i, isset($item->CHASSIS_NO) ? $item->CHASSIS_NO : '');//số khung
            $sheetExcel->setCellValue('S' . $i, isset($item->ENGINE_NO) ? $item->ENGINE_NO : '');//số máy

            $phiS = isset($item->FEES_TNDSBB) ? $item->FEES_TNDSBB : 0;
            $tongS = $tongS + $phiS;
            $sheetExcel->setCellValue('T' . $i, $phiS);//Phí BH

            $phiT = isset($item->VAT_TNDSNBB) ? $item->VAT_TNDSNBB : 0;
            $tongT = $tongT + $phiT;
            $sheetExcel->setCellValue('U' . $i, $phiT);//Vat BH

            $phiV2 = isset($item->INSUR_VOLUNTARY_AMOUNT) ? $item->INSUR_VOLUNTARY_AMOUNT : 0;
            $tongV2 = $tongV2 + $phiV2;
            $sheetExcel->setCellValue('V' . $i, $phiV2);//Vat BH

            $phiU = isset($item->FEES_TNDSTN) ? $item->FEES_TNDSTN : 0;
            $tongU = $tongU + $phiU;
            $sheetExcel->setCellValue('W' . $i, $phiU);//bh tự nguyện


            $phiV = isset($item->VAT_TNDSTN) ? $item->VAT_TNDSTN : 0;
            $tongV = $tongV + $phiV;
            $sheetExcel->setCellValue('X' . $i, $phiV);//vat tự nguyên

            $phiY2 = isset($item->INSUR_DRIVER_AMOUNT) ? $item->INSUR_DRIVER_AMOUNT : 0;
            $tongY2 = $tongY2 + $phiY2;
            $sheetExcel->setCellValue('Y' . $i, $phiY2);//

            $phiW = isset($item->FEES_NTX) ? $item->FEES_NTX : 0;
            $tongW = $tongW + $phiW;
            $sheetExcel->setCellValue('Z' . $i, $phiW);//BH tai nạn

            $phiAA = isset($item->INSUR_CARGO_AMOUNT) ? $item->INSUR_CARGO_AMOUNT : 0;
            $tongAA = $tongAA + $phiAA;
            $sheetExcel->setCellValue('AA' . $i, $phiAA);

            $phiX = isset($item->FEES_BHHH) ? $item->FEES_BHHH : 0;
            $tongX = $tongX + $phiX;
            $sheetExcel->setCellValue('AB' . $i, $phiX);//phí hàng hóa

            $phiY = isset($item->VAT_BHHH) ? $item->VAT_BHHH : 0;
            $tongY = $tongY + $phiY;
            $sheetExcel->setCellValue('AC' . $i, $phiY);//VAT với hàng hóa

            $phiZ = isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : 0;
            $tongZ = $tongZ + $phiZ;
            $sheetExcel->setCellValue('AD' . $i, $phiZ);//tổng phí

            $sheetExcel->setCellValue('AE' . $i, isset($item->LINK_CER) ? $item->LINK_CER : '');//link giấy chứng nhận
            $sheetExcel->setCellValue('AF' . $i, isset($item->SELLER_CODE) ? $item->SELLER_CODE : '');//Mã NV
            $sheetExcel->setCellValue('AG' . $i, isset($item->SELLER_NAME) ? $item->SELLER_NAME : '');//Tên nhân viên cấp đơn
            $sheetExcel->setCellValue('AH' . $i, isset($item->SELLER_EMAIL) ? $item->SELLER_EMAIL : '');//Mail NV
            $i++;
        }
        //tổng tiền danh sách
        $sheetExcel->getStyle('A' . $i . ':AH' . ($col_next + 1))->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FE2E2E'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        $sheetExcel->mergeCells('A' . $i . ':R' . ($col_next + 1));
        $sheetExcel->setCellValue('A' . $i, 'Tổng tiền');
        $sheetExcel->setCellValue('T' . $i, $tongS);

        $sheetExcel->setCellValue('U' . $i, $tongT);
        $sheetExcel->setCellValue('V' . $i, $tongV2);
        $sheetExcel->setCellValue('W' . $i, $tongU);

        $sheetExcel->setCellValue('X' . $i, $tongV);
        $sheetExcel->setCellValue('Y' . $i, $tongY2);
        $sheetExcel->setCellValue('Z' . $i, $tongW);

        $sheetExcel->setCellValue('AA' . $i, $tongAA);
        $sheetExcel->setCellValue('AB' . $i, $tongX);

        $sheetExcel->setCellValue('AC' . $i, $tongY);
        $sheetExcel->setCellValue('AD' . $i, $tongZ);

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    public function exportProductDetailXCG_Cu($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:H2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:H2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Loại xe");

        $sheetExcel->setCellValue('C' . $i, "SL đầu kỳ");
        $sheetExcel->setCellValue('D' . $i, "SL phát sinh");
        $sheetExcel->setCellValue('E' . $i, "SL cuối kỳ");

        $sheetExcel->setCellValue('F' . $i, "Phí đầu kỳ");
        $sheetExcel->setCellValue('G' . $i, "Phí phát sinh");
        $sheetExcel->setCellValue('H' . $i, "Phí cuối kỳ");

        //style content
        $sheetExcel->getStyle('A' . $i . ':H' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':H' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':B' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        $sheetExcel->getStyle('C' . $i . ':H' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));
        // auto filter
        //$sheetExcel->setAutoFilter('B3' . ':I' . $col_next);
        $total_C = $total_D = $total_E = $total_F = $total_G = $total_H = 0;
        foreach ($data as $k => $item) {
            if ($item->GROUPING == 1) {
                if (isset($item->DK_NUM_OF_VEH)) {
                    $total_C = (int)($total_C + $item->DK_NUM_OF_VEH);
                }
                if (isset($item->PS_NUM_OF_VEH)) {
                    $total_D = (int)($total_D + $item->PS_NUM_OF_VEH);
                }
                if (isset($item->EOT_NUM_OF_VEH)) {
                    $total_E = (int)($total_E + $item->EOT_NUM_OF_VEH);
                }

                if (isset($item->DK_TOTAL_AMOUNT)) {
                    $total_F = (int)($total_F + $item->DK_TOTAL_AMOUNT);
                }
                if (isset($item->PS_TOTAL_AMOUNT)) {
                    $total_G = (int)($total_G + $item->PS_TOTAL_AMOUNT);
                }
                if (isset($item->EOT_TOTAL_AMOUNT)) {
                    $total_H = (int)($total_H + $item->EOT_TOTAL_AMOUNT);
                }
            }

            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->VH_TYPE) ? $item->VH_TYPE : '');

            $sheetExcel->setCellValue('C' . $i, isset($item->DK_NUM_OF_VEH) ? $item->DK_NUM_OF_VEH : '');
            $sheetExcel->setCellValue('D' . $i, isset($item->PS_NUM_OF_VEH) ? $item->PS_NUM_OF_VEH : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->EOT_NUM_OF_VEH) ? $item->EOT_NUM_OF_VEH : '');

            $sheetExcel->setCellValue('F' . $i, isset($item->DK_TOTAL_AMOUNT) ? $item->DK_TOTAL_AMOUNT : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->PS_TOTAL_AMOUNT) ? $item->PS_TOTAL_AMOUNT : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->EOT_TOTAL_AMOUNT) ? $item->EOT_TOTAL_AMOUNT : '');
            $i++;
        }
        $sheetExcel->getStyle('A' . $i . ':H' . ($col_next + 1))->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FE2E2E'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));
        //tổng
        $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
        $sheetExcel->setCellValue('B' . $i, 'Tổng');

        $sheetExcel->setCellValue('C' . $i, $total_C);
        $sheetExcel->setCellValue('D' . $i, $total_D);
        $sheetExcel->setCellValue('E' . $i, $total_E);

        $sheetExcel->setCellValue('F' . $i, $total_F);
        $sheetExcel->setCellValue('G' . $i, $total_G);
        $sheetExcel->setCellValue('H' . $i, $total_H);

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product detail vật chất xe new
    public function exportProductDetailVCX_NEW($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:AP2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:AP2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Đối tác");
        $sheetExcel->setCellValue('C' . $i, "Mã tham chiếu");

        $sheetExcel->setCellValue('D' . $i, "Số giấy chứng nhận");
        $sheetExcel->setCellValue('E' . $i, "Sản phẩm");
        $sheetExcel->setCellValue('F' . $i, "Người được bảo hiểm");

        $sheetExcel->setCellValue('G' . $i, "CMND/CCCD/Hộ chiếu");
        $sheetExcel->setCellValue('H' . $i, "Số điện thoại");
        $sheetExcel->setCellValue('I' . $i, "Mail");

        $sheetExcel->setCellValue('J' . $i, "Địa chỉ");
        $sheetExcel->setCellValue('K' . $i, "Biển số");
        $sheetExcel->setCellValue('L' . $i, "Số khung");

        $sheetExcel->setCellValue('M' . $i, "Số máy");
        $sheetExcel->setCellValue('N' . $i, "Nhóm xe");
        $sheetExcel->setCellValue('O' . $i, "Loại xe");

        $sheetExcel->setCellValue('P' . $i, "Hãng xe");
        $sheetExcel->setCellValue('Q' . $i, "Hiệu xe");
        $sheetExcel->setCellValue('R' . $i, "Số ghế");

        $sheetExcel->setCellValue('S' . $i, "Trọng tải");
        $sheetExcel->setCellValue('T' . $i, "Ngày hiệu lực");
        $sheetExcel->setCellValue('U' . $i, "Ngày hết hiệu lực");

        $sheetExcel->setCellValue('V' . $i, "Ngày cấp GCN");
        $sheetExcel->setCellValue('W' . $i, "Mức miễn thường");
        $sheetExcel->setCellValue('X' . $i, "STBH VCX");

        $sheetExcel->setCellValue('Y' . $i, "Phí VCX");
        $sheetExcel->setCellValue('Z' . $i, "VAT phí VCX");
        $sheetExcel->setCellValue('AA' . $i, "Điều khoản bổ sung");

        $sheetExcel->setCellValue('AB' . $i, "Phí ĐKBS");
        $sheetExcel->setCellValue('AC' . $i, "VAT phí ĐKBS");
        $sheetExcel->setCellValue('AD' . $i, "STBH TNDS tự nguyện");

        $sheetExcel->setCellValue('AE' . $i, "Phí BH TNDS tự nguyện");
        $sheetExcel->setCellValue('AF' . $i, "VAT TNDS tự nguyện");
        $sheetExcel->setCellValue('AG' . $i, "STBH người ngồi trên xe");

        $sheetExcel->setCellValue('AH' . $i, "Phí BH tai nạn người ngồi trên xe");
        $sheetExcel->setCellValue('AI' . $i, "STBH TNDS chủ xe với hàng");
        $sheetExcel->setCellValue('AJ' . $i, "Phí BH TNDS chủ xe với hàng hóa");

        $sheetExcel->setCellValue('AK' . $i, "VAT phí TNDS chủ với hàng hóa");
        $sheetExcel->setCellValue('AL' . $i, "Tổng phí BH");
        $sheetExcel->setCellValue('AM' . $i, "URL GCN");

        $sheetExcel->setCellValue('AN' . $i, "Tên NV cấp");
        $sheetExcel->setCellValue('AO' . $i, "Mã NV cấp");
        $sheetExcel->setCellValue('AP' . $i, "Mail NV cấp");

        //style content
        $sheetExcel->getStyle('A' . $i . ':AP' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':AP' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':AP' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->ORG_SELLER_NAME) ? $item->ORG_SELLER_NAME : '');//đối tác
            $sheetExcel->setCellValue('C' . $i, isset($item->PARTNER_ORDER) ? $item->PARTNER_ORDER : '');//mã tham chiếu

            $sheetExcel->setCellValue('D' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');//Số giấy chứng nhận
            $sheetExcel->setCellValue('E' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');//sản phẩm
            $sheetExcel->setCellValue('F' . $i, isset($item->NAME) ? $item->NAME : '');//người đc bảo hiểm

            $sheetExcel->setCellValue('G' . $i, isset($item->IDCARD) ? $item->IDCARD : '');//CMND
            $sheetExcel->setCellValue('H' . $i, isset($item->PHONE) ? $item->PHONE : '');//PHONE
            $sheetExcel->setCellValue('I' . $i, isset($item->EMAIL) ? $item->EMAIL : '');//EMAIL

            $sheetExcel->setCellValue('J' . $i, isset($item->FULL_ADDRESS) ? $item->FULL_ADDRESS : '');//địa chỉ
            $sheetExcel->setCellValue('K' . $i, isset($item->NUMBER_PLATE) ? $item->NUMBER_PLATE : '');//biển số
            $sheetExcel->setCellValue('L' . $i, isset($item->CHASSIS_NO) ? $item->CHASSIS_NO : '');//số khung

            $sheetExcel->setCellValue('M' . $i, isset($item->ENGINE_NO) ? $item->ENGINE_NO : '');//số máy
            $sheetExcel->setCellValue('N' . $i, isset($item->GROUP_VEH) ? $item->GROUP_VEH : '');//nhóm xe
            $sheetExcel->setCellValue('O' . $i, isset($item->VEHICLE_TYPE) ? $item->VEHICLE_TYPE : '');//loại xe

            $sheetExcel->setCellValue('P' . $i, isset($item->BRAND) ? $item->BRAND : '');//hãng xe
            $sheetExcel->setCellValue('Q' . $i, isset($item->MODEL_NAME) ? $item->MODEL_NAME : '');//hiệu xe
            $sheetExcel->setCellValue('R' . $i, isset($item->SEAT_CODE) ? $item->SEAT_CODE : '');//số ghê

            $sheetExcel->setCellValue('S' . $i, isset($item->CAR_WEIGHT) ? $item->CAR_WEIGHT : '');//trọng tải
            $sheetExcel->setCellValue('T' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');//ngày hiệu lực
            $sheetExcel->setCellValue('U' . $i, isset($item->EXPIRATION_DATE) ? $item->EXPIRATION_DATE : '');//ngày hết

            $sheetExcel->setCellValue('V' . $i, isset($item->DATE_SIGN) ? $item->DATE_SIGN : '');//ngày cấp
            $sheetExcel->setCellValue('W' . $i, isset($item->INSUR_DEDUCTIBLE) ? $item->INSUR_DEDUCTIBLE : '');//mức miễn thường
            $sheetExcel->setCellValue('X' . $i, isset($item->VCX_INSUR_AMOUNT) ? $item->VCX_INSUR_AMOUNT : ''); //STBH VCX

            $sheetExcel->setCellValue('Y' . $i, isset($item->VCX_FEES) ? $item->VCX_FEES : '');//phí VCX
            $sheetExcel->setCellValue('Z' . $i, isset($item->VCX_VAT) ? $item->VCX_VAT : '');//vat phí vcx
            $sheetExcel->setCellValue('AA' . $i, isset($item->ADDITIONAL_TERMS) ? $item->ADDITIONAL_TERMS : '');//điều khoảng bổ sung

            $sheetExcel->setCellValue('AB' . $i, isset($item->VCX_ADD_FEES) ? $item->VCX_ADD_FEES : '');//phí DKBS
            $sheetExcel->setCellValue('AC' . $i, isset($item->VCX_ADD_VAT) ? $item->VCX_ADD_VAT : '');//vat phí DKBS
            $sheetExcel->setCellValue('AD' . $i, isset($item->INSUR_VOLUNTARY_AMOUNT) ? $item->INSUR_VOLUNTARY_AMOUNT : '');//STBH TNDS tự nguyện

            $sheetExcel->setCellValue('AE' . $i, isset($item->FEES_TNDSTN) ? $item->FEES_TNDSTN : '');//phí BH TNDS tự nguyện
            $sheetExcel->setCellValue('AF' . $i, isset($item->VAT_TNDSTN) ? $item->VAT_TNDSTN : '');//VAT TNDS tự nguyện
            $sheetExcel->setCellValue('AG' . $i, isset($item->INSUR_DRIVER_AMOUNT) ? $item->INSUR_DRIVER_AMOUNT : '');//STBH người ngồi trên xe

            $sheetExcel->setCellValue('AH' . $i, isset($item->FEES_NTX) ? $item->FEES_NTX : '');//phí bảo hiểm tại nạn người ngồi trên xe
            $sheetExcel->setCellValue('AI' . $i, isset($item->INSUR_CARGO_AMOUNT) ? $item->INSUR_CARGO_AMOUNT : '');//STBH  TNDS chủ xe với hàng
            $sheetExcel->setCellValue('AJ' . $i, isset($item->FEES_BHHH) ? $item->FEES_BHHH : '');// phí BH TNDS chủ xe với HH

            $sheetExcel->setCellValue('AK' . $i, isset($item->VAT_BHHH) ? $item->VAT_BHHH : '');//VAT phí TNDS chủ với HH
            $sheetExcel->setCellValue('AL' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');//Tổng phí
            $sheetExcel->setCellValue('AM' . $i, isset($item->LINK_CER) ? $item->LINK_CER : '');//url GCN

            $sheetExcel->setCellValue('AN' . $i, isset($item->SELLER_NAME) ? $item->SELLER_NAME : '');//Tên nhân viên cấp đơn
            $sheetExcel->setCellValue('AO' . $i, isset($item->SELLER_CODE) ? $item->SELLER_CODE : '');//Mã NV
            $sheetExcel->setCellValue('AP' . $i, isset($item->SELLER_EMAIL) ? $item->SELLER_EMAIL : '');//Mail NV
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product detail bay antoan
    public function exportProductDetailBayAT($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $total_staff = count($data);
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:J2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:J2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Tên khách hàng");

        $sheetExcel->setCellValue('C' . $i, "Mã đặt chỗ");
        $sheetExcel->setCellValue('D' . $i, "Số ghế");
        $sheetExcel->setCellValue('E' . $i, "Loại khách hàng");

        $sheetExcel->setCellValue('F' . $i, "Loại vé");
        $sheetExcel->setCellValue('G' . $i, "Số hiệu chuyến bay");
        $sheetExcel->setCellValue('H' . $i, "Nơi đi");
        $sheetExcel->setCellValue('I' . $i, "Nơi đến");
        $sheetExcel->setCellValue('J' . $i, "Ngày khởi hành");

        //style content
        $sheetExcel->getStyle('A' . $i . ':J' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':J' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':J' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->NAME) ? $item->NAME : '');

            $sheetExcel->setCellValue('C' . $i, isset($item->BOOKING_ID) ? $item->BOOKING_ID : '');
            $sheetExcel->setCellValue('D' . $i, isset($item->SEAT) ? $item->SEAT : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->CUS_TYPE) ? $item->CUS_TYPE : '');

            $sheetExcel->setCellValue('F' . $i, isset($item->FARE_CLASS) ? $item->FARE_CLASS : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->FLI_NO) ? $item->FLI_NO : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->DEP) ? $item->DEP : '');

            $sheetExcel->setCellValue('I' . $i, isset($item->ARR) ? $item->ARR : '');
            $sheetExcel->setCellValue('J' . $i, isset($item->FLI_R_DATE) ? $item->FLI_R_DATE : '');
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product detail Sky care
    public function exportProductDetailSkyCare($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        $dataSearch = isset($dataInput['dataSearch']) ? $dataInput['dataSearch'] : [];
        if (empty($data))
            return;

        set_time_limit(500000);
        $total_staff = isset($dataInput['total']) ? $dataInput['total']: (array_key_last($data) + 1);

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();

        $sheetExcel->mergeCells('A1:Z2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:Z2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        //tiêu đề excel =
        $arr_p_pack_code = isset($dataSearch['p_pack_code']) ? explode(';', $dataSearch['p_pack_code']) : [];
        $arrPackCode = $dataSearch['arrPackCode'];
        if (!empty($arrPackCode)) {
            foreach ($arrPackCode as $key1 => $va1) {
                if (!in_array($key1, $arr_p_pack_code)) {
                    unset($arrPackCode[$key1]);
                }
            }
        }

        $arr_p_type_customer = isset($dataSearch['p_type_customer']) ? explode(';', $dataSearch['p_type_customer']) : [];
        $arrTypeCutomer = $dataSearch['arrTypeCutomer'];
        if (!empty($arrTypeCutomer)) {
            foreach ($arrTypeCutomer as $key2 => $va2) {
                if (!in_array($key2, $arr_p_type_customer)) {
                    unset($arrTypeCutomer[$key2]);
                }
            }
        }

        $sheetExcel->mergeCells('A3:G3');
        $sheetExcel->setCellValue('A3', 'Xuất theo ngày: ' . (isset($dataSearch['arrTypeDateOrder'][$dataSearch['p_type_date_search']]) ? $dataSearch['arrTypeDateOrder'][$dataSearch['p_type_date_search']] : ''));
        $sheetExcel->mergeCells('A4:G4');
        $sheetExcel->setCellValue('A4', 'Ngày: ' . (isset($dataSearch['p_date_search']) ? $dataSearch['p_date_search'] : ''));
        $sheetExcel->mergeCells('A5:G5');
        $sheetExcel->setCellValue('A5', 'Số hiệu chuyến bay: ' . (isset($dataSearch['p_fight_number']) ? $dataSearch['p_fight_number'] : 'Tất cả'));
        $sheetExcel->mergeCells('A6:G6');
        $sheetExcel->setCellValue('A6', 'Gói bảo hiểm: ' . implode(',', $arrPackCode));
        $sheetExcel->mergeCells('A7:G7');
        $sheetExcel->setCellValue('A7', 'Loại hành khách: ' . implode(',', $arrTypeCutomer));
        $sheetExcel->getStyle('A3:G7')->applyFromArray(array(
            'font' => array(
                'size' => 12,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        $j = $i = 9;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");

        $sheetExcel->setCellValue('B' . $i, "PNR_NO");
        $sheetExcel->setCellValue('C' . $i, "POLICY_NUM");
        $sheetExcel->setCellValue('D' . $i, "FULL_NAME");
        $sheetExcel->setCellValue('E' . $i, "CURRENCY_CODE");

        $sheetExcel->setCellValue('F' . $i, "PREMIUM_AMOUNT");
        $sheetExcel->setCellValue('G' . $i, "FLIGHT_ID");
        $sheetExcel->setCellValue('H' . $i, "FLI_NO");
        $sheetExcel->setCellValue('I' . $i, "DEPARTURE_COUNTRY");

        $sheetExcel->setCellValue('J' . $i, "DEPARTURE_STATION_CODE");
        $sheetExcel->setCellValue('K' . $i, "ARRIVE_COUNTRY");
        $sheetExcel->setCellValue('L' . $i, "ARRIVAL_STATION_CODE");

        $sheetExcel->setCellValue('M' . $i, "LOCAL_ETD1");
        $sheetExcel->setCellValue('N' . $i, "LOCAL_ETD2");
        $sheetExcel->setCellValue('O' . $i, "LOCAL_ATD");
        $sheetExcel->setCellValue('P' . $i, "DELAY_TIME");

        $sheetExcel->setCellValue('Q' . $i, "FARE_CLASS");
        $sheetExcel->setCellValue('R' . $i, "CUS_TYPE");
        $sheetExcel->setCellValue('S' . $i, "CONNECTING_FLIGHTID");
        $sheetExcel->setCellValue('T' . $i, "CONNECTING_FLIGHTNMBR");

        $sheetExcel->setCellValue('U' . $i, "DEPARTURE2");
        $sheetExcel->setCellValue('V' . $i, "ARRIVAL2");
        $sheetExcel->setCellValue('W' . $i, "CLOSE_DATE");

        $sheetExcel->setCellValue('X' . $i, "DTM_CREATION_DATE");
        $sheetExcel->setCellValue('Y' . $i, "URL");
        $sheetExcel->setCellValue('Z' . $i, "Phân loại nhóm NĐBH");

        //style content
        $sheetExcel->getStyle('A' . $i . ':Z' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':Z' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + $j;
        $sheetExcel->getStyle('A' . $i . ':Z' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $k + 1);// index column

            $sheetExcel->setCellValueExplicit('B' . $i, $item->BOOKING_ID, 's');//hiển thị text
            $sheetExcel->setCellValue('C' . $i, $item->POLICY_NUMBER);
            $sheetExcel->setCellValue('D' . $i, $item->INSURED_FULL_NAME);
            $sheetExcel->setCellValue('E' . $i, $item->CURRENCY_CODE);

            $sheetExcel->setCellValue('F' . $i, $item->PREMIUM_AMOUNT);
            $sheetExcel->setCellValue('G' . $i, $item->FLIGHT_ID);
            $sheetExcel->setCellValue('H' . $i, $item->FLIGHT_NO);
            $sheetExcel->setCellValue('I' . $i, $item->DEPARTURE_COUNTRY);

            $sheetExcel->setCellValue('J' . $i, $item->DEPARTURE_STATION_CODE);
            $sheetExcel->setCellValue('K' . $i, $item->ARRIVE_COUNTRY);
            $sheetExcel->setCellValue('L' . $i, $item->ARRIVE_STATION_CODE);

            $sheetExcel->setCellValue('M' . $i, $item->LOCAL_ETD1);
            $sheetExcel->setCellValue('N' . $i, $item->LOCAL_ETD);
            $sheetExcel->setCellValue('O' . $i, $item->LOCAL_ATD);
            $sheetExcel->setCellValue('P' . $i, $item->DELAY_TIME);
            $sheetExcel->setCellValue('Q' . $i, $item->FARE_CLASS);

            $sheetExcel->setCellValue('R' . $i, $item->CUSTOMER_TYPE);
            $sheetExcel->setCellValue('S' . $i, $item->CONNECT_FLI_ID);
            $sheetExcel->setCellValue('T' . $i, $item->CONNECT_FLI_NO);

            $sheetExcel->setCellValue('U' . $i, $item->DEPARTURE2);
            $sheetExcel->setCellValue('V' . $i, $item->ARRIVAL2);
            $sheetExcel->setCellValue('W' . $i, $item->FLIGHT_CLOSE_DATE);
            $sheetExcel->setCellValue('X' . $i, $item->DTM_CREATION_DATE);

            $sheetExcel->setCellValue('Y' . $i, 'Xem GCN');
            $sheetExcel->getCell('Y' . $i)->getHyperlink()->setUrl($item->LINK_CERT);
            $sheetExcel->getStyle('Y' . $i)->applyFromArray($this->link_style);
            $sheetExcel->setCellValue('Z' . $i, isset($arrPackCode[$item->PACK_CODE]) ? $arrPackCode[$item->PACK_CODE] : '');
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //báo cáo chi tiết trễ chuyến bay
    public function exportDetailFlightDelay($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        $dataSearch = isset($dataInput['dataSearch']) ? $dataInput['dataSearch'] : [];
        if (empty($data))
            return;

        set_time_limit(500000);
        $total_staff = isset($dataInput['total']) ? $dataInput['total']: (array_key_last($data) + 1);

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();

        $sheetExcel->mergeCells('A1:H2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:H2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        //tiêu đề excel =
        $arr_p_pack_code = isset($dataSearch['p_pack_code']) ? explode(';', $dataSearch['p_pack_code']) : [];
        $arrPackCode = $dataSearch['arrPackCode'];
        if (!empty($arrPackCode)) {
            foreach ($arrPackCode as $key1 => $va1) {
                if (!in_array($key1, $arr_p_pack_code)) {
                    unset($arrPackCode[$key1]);
                }
            }
        }

        $sheetExcel->mergeCells('A3:G3');
        $sheetExcel->setCellValue('A3', 'Loại báo cáo: Theo ' . (isset($dataSearch['arrTypeDateOrder'][$dataSearch['p_type_date_sky_care']]) ? $dataSearch['arrTypeDateOrder'][$dataSearch['p_type_date_sky_care']] : ''));
        $sheetExcel->mergeCells('A4:G4');
        $sheetExcel->setCellValue('A4', 'Thời gian lọc: ' . $dataSearch['p_from_date'].' - '.$dataSearch['p_to_date']);
        $sheetExcel->mergeCells('A5:G5');
        $sheetExcel->setCellValue('A5', 'Sản phẩm bảo hiểm: ' . (isset($dataSearch['arrProductOrder'][$dataSearch['p_product_code']]) ? $dataSearch['arrProductOrder'][$dataSearch['p_product_code']] : ''));
        $sheetExcel->mergeCells('A6:G6');
        $sheetExcel->setCellValue('A6', 'Gói bảo hiểm: ' . implode(',', $arrPackCode));
        $sheetExcel->mergeCells('A7:G7');
        $sheetExcel->setCellValue('A7', 'Số hiệu chuyến bay: ' . $dataSearch['p_fight_number']);
        $sheetExcel->mergeCells('A8:G8');
        $sheetExcel->setCellValue('A8', 'Mốc thời gian trễ: ' . (isset($dataSearch['arrGroupDelay'][$dataSearch['p_group_time_delay']]) ? $dataSearch['arrGroupDelay'][$dataSearch['p_group_time_delay']] : 'Tất cả'));
          $sheetExcel->getStyle('A3:G8')->applyFromArray(array(
            'font' => array(
                'size' => 12,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        $j = $i = 10;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "STT");

        $sheetExcel->setCellValue('B' . $i, "Gói bảo biểm");
        $sheetExcel->setCellValue('C' . $i, "Ngày bay");
        $sheetExcel->setCellValue('D' . $i, "Mốc thời gian");
        $sheetExcel->setCellValue('E' . $i, "Số hiệu chuyến bay");

        $sheetExcel->setCellValue('F' . $i, "Số tiền bảo hiểm");
        $sheetExcel->setCellValue('G' . $i, "Số lượng khách hàng");
        $sheetExcel->setCellValue('H' . $i, "Tổng số tiền bồi thường");
        //style content
        $sheetExcel->getStyle('A' . $i . ':H' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':H' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + $j;
        $sheetExcel->getStyle('A' . $i . ':H' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        $totalCust = 0;
        $totalFeeClaim = 0;
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $k + 1);// index column
            $sheetExcel->setCellValue('B' . $i, isset($dataSearch['arrPackCode'][$item->PACK_CODE]) ? $dataSearch['arrPackCode'][$item->PACK_CODE] : $item->PACK_CODE);
            $sheetExcel->setCellValue('C' . $i, convertDateDMY($item->FLI_DATE));
            $sheetExcel->setCellValue('D' . $i, $item->GROUP_NAME);
            $sheetExcel->setCellValue('E' . $i, $item->FLI_NO);

            $sheetExcel->setCellValue('F' . $i, numberFormat($item->FEE_CLAIM,',',','))->getStyle('F' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $sheetExcel->setCellValue('G' . $i, numberFormat($item->CUSTOMER,',',','))->getStyle('G' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $sheetExcel->setCellValue('H' . $i, numberFormat($item->TOTAL_FEE_CLAIM,',',','))->getStyle('H' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $totalCust = $totalCust + $item->CUSTOMER;
            $totalFeeClaim = $totalFeeClaim + $item->TOTAL_FEE_CLAIM;
            $i++;
        }

        //tổng tiền
        $sheetExcel->getStyle('A' . $i . ':H' . ($col_next+1))->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FE2E2E'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));
        $sheetExcel->mergeCells('A' . $i . ':F' . ($col_next+1));
        $sheetExcel->setCellValue('A'.$i, 'Tổng tiền');
        $sheetExcel->setCellValue('G' . $i, numberFormat($totalCust,',',','));
        $sheetExcel->setCellValue('H' . $i, numberFormat($totalFeeClaim,',',','));

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }
    //báo cáo tổng hợp trễ chuyến bay
    public function exportSalesFlightDelay($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        $dataSearch = isset($dataInput['dataSearch']) ? $dataInput['dataSearch'] : [];
        if (empty($data))
            return;

        set_time_limit(500000);
        $total_staff = isset($dataInput['total']) ? $dataInput['total']: (array_key_last($data) + 1);

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();

        $sheetExcel->mergeCells('A1:F2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:F2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));

        //tiêu đề excel =
        $arr_p_pack_code = isset($dataSearch['p_pack_code']) ? explode(';', $dataSearch['p_pack_code']) : [];
        $arrPackCode = $dataSearch['arrPackCode'];
        if (!empty($arrPackCode)) {
            foreach ($arrPackCode as $key1 => $va1) {
                if (!in_array($key1, $arr_p_pack_code)) {
                    unset($arrPackCode[$key1]);
                }
            }
        }

        $sheetExcel->mergeCells('A3:G3');
        $sheetExcel->setCellValue('A3', 'Loại báo cáo: Theo ' . (isset($dataSearch['arrTypeDateOrder'][$dataSearch['p_type_date_sky_care']]) ? $dataSearch['arrTypeDateOrder'][$dataSearch['p_type_date_sky_care']] : ''));
        $sheetExcel->mergeCells('A4:G4');
        $sheetExcel->setCellValue('A4', 'Thời gian lọc: ' . $dataSearch['p_from_date'].' - '.$dataSearch['p_to_date']);
        $sheetExcel->mergeCells('A5:G5');
        $sheetExcel->setCellValue('A5', 'Sản phẩm bảo hiểm: ' . (isset($dataSearch['arrProductOrder'][$dataSearch['p_product_code']]) ? $dataSearch['arrProductOrder'][$dataSearch['p_product_code']] : ''));
        $sheetExcel->mergeCells('A6:G6');
        $sheetExcel->setCellValue('A6', 'Gói bảo hiểm: ' . implode(',', $arrPackCode));
          $sheetExcel->getStyle('A3:G6')->applyFromArray(array(
            'font' => array(
                'size' => 12,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        $j = $i = 8;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "STT");

        $sheetExcel->setCellValue('B' . $i, "Gói bảo biểm");
        $sheetExcel->setCellValue('C' . $i, "Mốc thời gian");
        $sheetExcel->setCellValue('D' . $i, "Số tiền bảo hiểm");
        $sheetExcel->setCellValue('E' . $i, "Số lượng khách hàng");
        $sheetExcel->setCellValue('F' . $i, "Tổng số tiền bồi thường");
        //style content
        $sheetExcel->getStyle('A' . $i . ':F' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':F' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + $j;
        $sheetExcel->getStyle('A' . $i . ':F' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));

        $totalCust = 0;
        $totalFeeClaim = 0;
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $k + 1);// index column
            //$sheetExcel->setCellValue('B' . $i, $item->PACK_CODE);
            $sheetExcel->setCellValue('B' . $i, isset($dataSearch['arrPackCode'][$item->PACK_CODE]) ? $dataSearch['arrPackCode'][$item->PACK_CODE] : $item->PACK_CODE);
            $sheetExcel->setCellValue('C' . $i, $item->GROUP_NAME);

            $sheetExcel->setCellValue('D' . $i, numberFormat($item->FEE_CLAIM,',',','))->getStyle('D' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $sheetExcel->setCellValue('E' . $i, numberFormat($item->CUSTOMER,',',','))->getStyle('E' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $sheetExcel->setCellValue('F' . $i, numberFormat($item->TOTAL_FEE_CLAIM,',',','))->getStyle('F' . $i)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $totalCust = $totalCust + $item->CUSTOMER;
            $totalFeeClaim = $totalFeeClaim + $item->TOTAL_FEE_CLAIM;
            $i++;
        }

        //tổng tiền
        $sheetExcel->getStyle('A' . $i . ':F' . ($col_next+1))->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FE2E2E'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));
        $sheetExcel->mergeCells('A' . $i . ':D' . ($col_next+1));
        $sheetExcel->setCellValue('A'.$i, 'Tổng tiền');
        $sheetExcel->setCellValue('E' . $i, numberFormat($totalCust,',',','));
        $sheetExcel->setCellValue('F' . $i, numberFormat($totalFeeClaim,',',','));

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //product detail bảo hiểm hành lý
    public function exportProductDetailLOST_BAGGAGE($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $dataExten = isset($dataInput['dataExten']) ? $dataInput['dataExten'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();

        $sheetExcel->mergeCells('A1:U2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:U2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Số giấy chứng nhận");
        $sheetExcel->setCellValue('C' . $i, "Tình trạng cấp GCN");

        $sheetExcel->setCellValue('D' . $i, "Tên khách hàng");
        $sheetExcel->setCellValue('E' . $i, "CMND/CCCD/Hộ chiếu");
        $sheetExcel->setCellValue('F' . $i, "Số điện thoại");

        $sheetExcel->setCellValue('G' . $i, "Email");
        $sheetExcel->setCellValue('H' . $i, "Số hiệu chuyến bay");
        $sheetExcel->setCellValue('I' . $i, "Mã đặt chỗ");

        $sheetExcel->setCellValue('J' . $i, "Số ghế");
        $sheetExcel->setCellValue('K' . $i, "Thời gian cất cánh dự kiến");
        $sheetExcel->setCellValue('L' . $i, "Thời gian hạ cánh dự kiến");

        $sheetExcel->setCellValue('M' . $i, "Điểm đi dự kiến");
        $sheetExcel->setCellValue('N' . $i, "Điểm đến dự kiến");
        $sheetExcel->setCellValue('O' . $i, "Ngày mua bảo hiểm");

        $sheetExcel->setCellValue('P' . $i, "Sản phẩm bảo hiểm");
        $sheetExcel->setCellValue('Q' . $i, "Mã tag hành lý");
        $sheetExcel->setCellValue('R' . $i, "Phí bảo hiểm (chưa VAT)");

        $sheetExcel->setCellValue('S' . $i, "Thuế GTGT (VAT)");
        $sheetExcel->setCellValue('T' . $i, "Tổng tiền thanh toán");
        $sheetExcel->setCellValue('U' . $i, "Hoa hồng VietJet");

        //style content
        $sheetExcel->getStyle('A' . $i . ':U' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':U' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':U' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CER_STATUS) ? $item->CER_STATUS : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->IDCARD) ? $item->IDCARD : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->PHONE) ? $item->PHONE : '');

            $sheetExcel->setCellValue('G' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->FLI_NO) ? $item->FLI_NO : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->BOOKING_ID) ? $item->BOOKING_ID : '');

            $sheetExcel->setCellValue('J' . $i, isset($item->SEAT) ? $item->SEAT : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->FLI_DATE) ? $item->FLI_DATE : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->FLI_LAND_DATE) ? $item->FLI_LAND_DATE : '');

            $sheetExcel->setCellValue('M' . $i, isset($item->DEP) ? $item->DEP : '');
            $sheetExcel->setCellValue('N' . $i, isset($item->ARR) ? $item->ARR : '');
            $sheetExcel->setCellValue('O' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');

            $sheetExcel->setCellValue('P' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('Q' . $i, isset($item->TAG_CODE_BAG) ? $item->TAG_CODE_BAG : '');
            $sheetExcel->setCellValue('R' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');

            $sheetExcel->setCellValue('S' . $i, isset($item->VAT) ? $item->VAT : '');
            $sheetExcel->setCellValue('T' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');
            $sheetExcel->setCellValue('U' . $i, isset($item->COMMISSION) ? $item->COMMISSION : '');
            $i++;
        }

        /*//tổng tiền
        $sheetExcel->getStyle('A' . $i . ':U' . ($col_next+1))->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FE2E2E'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        $sheetExcel->mergeCells('A' . $i . ':T' . ($col_next+1));
        $sheetExcel->setCellValue('A'.$i, 'Tổng tiền');
        $sheetExcel->setCellValue('U' . $i,isset($dataExten->TOTAL_AMOUNT)?$dataExten->TOTAL_AMOUNT:'');*/

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //kế toán: bảo hiểm hành lý
    public function exportAccountantLOST_BAGGAGE($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];

        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:U2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:U2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Số hợp đồng");
        $sheetExcel->setCellValue('C' . $i, "Tên hợp đồng");

        $sheetExcel->setCellValue('D' . $i, "Ngày bắt đầu hiệu lực");
        $sheetExcel->setCellValue('E' . $i, "Ngày kết thúc hiệu lực");
        $sheetExcel->setCellValue('F' . $i, "Tên khách hàng");

        $sheetExcel->setCellValue('G' . $i, "Tên khác");
        $sheetExcel->setCellValue('H' . $i, "Email");
        $sheetExcel->setCellValue('I' . $i, "Địa chỉ khách hàng");

        $sheetExcel->setCellValue('J' . $i, "Mã số thuế khách hàng");
        $sheetExcel->setCellValue('K' . $i, "Diễn giải");
        $sheetExcel->setCellValue('L' . $i, "Mã ngoại tệ");

        $sheetExcel->setCellValue('M' . $i, "Tiền ngoại tệ");
        $sheetExcel->setCellValue('N' . $i, "Phí (-VAT)");
        $sheetExcel->setCellValue('O' . $i, "VAT");

        $sheetExcel->setCellValue('P' . $i, "Phí (có VAT)");
        $sheetExcel->setCellValue('Q' . $i, "Nghiệp vụ");
        $sheetExcel->setCellValue('R' . $i, "Sản phẩm bảo hiểm");

        $sheetExcel->setCellValue('S' . $i, "Gói bảo hiểm");
        $sheetExcel->setCellValue('T' . $i, "Bộ phận cấp đơn");
        $sheetExcel->setCellValue('U' . $i, "Nhân viên");

        //style content
        $sheetExcel->getStyle('A' . $i . ':U' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':U' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':U' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CONTRACT_NAME) ? $item->CONTRACT_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->FLI_DATE) ? $item->FLI_DATE : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->FLI_LAND_DATE) ? $item->FLI_LAND_DATE : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');

            $sheetExcel->setCellValue('G' . $i, isset($item->CUS_OTHER_NAME) ? $item->CUS_OTHER_NAME : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');

            $sheetExcel->setCellValue('J' . $i, isset($item->TAXCODE) ? $item->TAXCODE : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->DESCIRPTION) ? $item->DESCIRPTION : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->CURRENCY_CODE) ? $item->CURRENCY_CODE : '');

            $sheetExcel->setCellValue('M' . $i, isset($item->CURRENCY_NAME) ? $item->CURRENCY_NAME : '');
            $sheetExcel->setCellValue('N' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');
            $sheetExcel->setCellValue('O' . $i, isset($item->VAT) ? $item->VAT : '');

            $sheetExcel->setCellValue('P' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');
            $sheetExcel->setCellValue('Q' . $i, isset($item->CATEGORY) ? $item->CATEGORY : '');
            $sheetExcel->setCellValue('R' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');

            $sheetExcel->setCellValue('S' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('T' . $i, isset($item->DEPARTMENT) ? $item->DEPARTMENT : '');
            $sheetExcel->setCellValue('U' . $i, isset($item->EMPLOYEE_NAME) ? $item->EMPLOYEE_NAME : '');
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //kế toán common
    public function exportAccountantCOMMON($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];

        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:Y2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:Y2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Số hợp đồng");
        $sheetExcel->setCellValue('C' . $i, "Tên hợp đồng");

        $sheetExcel->setCellValue('D' . $i, "Ngày bắt đầu hiệu lực");
        $sheetExcel->setCellValue('E' . $i, "Ngày kết thúc hiệu lực");
        $sheetExcel->setCellValue('F' . $i, "Tên khách hàng");

        $sheetExcel->setCellValue('G' . $i, "Tên khác");
        $sheetExcel->setCellValue('H' . $i, "Email");
        $sheetExcel->setCellValue('I' . $i, "Số điện thoại");

        $sheetExcel->setCellValue('J' . $i, "Địa chỉ khách hàng");
        $sheetExcel->setCellValue('K' . $i, "Mã số thuế khách hàng");
        $sheetExcel->setCellValue('L' . $i, "Diễn giải");

        $sheetExcel->setCellValue('M' . $i, "Mã ngoại tệ");
        $sheetExcel->setCellValue('N' . $i, "Tiền ngoại tệ");
        $sheetExcel->setCellValue('O' . $i, "Tỉ giá");

        $sheetExcel->setCellValue('P' . $i, "Phí (- VAT)");
        $sheetExcel->setCellValue('Q' . $i, "Thuế xuất");
        $sheetExcel->setCellValue('R' . $i, "VAT");

        $sheetExcel->setCellValue('S' . $i, "Phí có VAT");
        $sheetExcel->setCellValue('T' . $i, "Nghiệp vụ");
        $sheetExcel->setCellValue('U' . $i, "Sản phẩm bảo hiểm");

        $sheetExcel->setCellValue('V' . $i, "Gói bảo hiểm");
        $sheetExcel->setCellValue('W' . $i, "Hệ thống");
        $sheetExcel->setCellValue('X' . $i, "Bộ phận cấp đơn");
        $sheetExcel->setCellValue('Y' . $i, "Nhân viên");

        //style content
        $sheetExcel->getStyle('A' . $i . ':Y' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':Y' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':Y' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CONTRACT_NAME) ? $item->CONTRACT_NAME : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->EFFECTIVE_DATE) ? $item->EFFECTIVE_DATE : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->EXPIRATION_DATE) ? $item->EXPIRATION_DATE : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->CUS_NAME) ? $item->CUS_NAME : '');

            $sheetExcel->setCellValue('G' . $i, isset($item->CUS_OTHER_NAME) ? $item->CUS_OTHER_NAME : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->EMAIL) ? $item->EMAIL : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->PHONE) ? $item->PHONE : '');//phone

            $sheetExcel->setCellValue('J' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->TAXCODE) ? $item->TAXCODE : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->DESCIRPTION) ? $item->DESCIRPTION : '');

            $sheetExcel->setCellValue('M' . $i, isset($item->CURRENCY_CODE) ? $item->CURRENCY_CODE : '');
            $sheetExcel->setCellValue('N' . $i, isset($item->CURRENCY_NAME) ? $item->CURRENCY_NAME : '');
            $sheetExcel->setCellValue('O' . $i, isset($item->RATIO) ? $item->RATIO : '');//tỉ giá

            $sheetExcel->setCellValue('P' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');//Phí (- VAT)
            $sheetExcel->setCellValue('Q' . $i, isset($item->VAT_RATIO) ? $item->VAT_RATIO : '');//Thuế xuất
            $sheetExcel->setCellValue('R' . $i, isset($item->VAT) ? $item->VAT : '');//vat

            $sheetExcel->setCellValue('S' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');//phí có vat
            $sheetExcel->setCellValue('T' . $i, isset($item->CATEGORY) ? $item->CATEGORY : '');//nghiệp vụ
            $sheetExcel->setCellValue('U' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_CODE.' '.$item->PRODUCT_NAME : '');

            $sheetExcel->setCellValue('V' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('W' . $i, isset($item->DATA_SOURCE) ? $item->DATA_SOURCE : '');
            $sheetExcel->setCellValue('X' . $i, isset($item->DEPARTMENT) ? $item->DEPARTMENT : '');
            $sheetExcel->setCellValue('Y' . $i, isset($item->EMPLOYEE_NAME) ? $item->EMPLOYEE_NAME : '');
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //Cấp đơn theo lô
    public function exportOrdersInBatches($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:O2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:O2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Số PLHĐ");
        $sheetExcel->setCellValue('C' . $i, "Số GCN");

        $sheetExcel->setCellValue('D' . $i, "Họ tên khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Ngày sinh");

        $sheetExcel->setCellValue('F' . $i, "Mã sản phẩm");
        $sheetExcel->setCellValue('G' . $i, "Tên sản phẩm");

        $sheetExcel->setCellValue('H' . $i, "Mã gói");
        $sheetExcel->setCellValue('I' . $i, "Tên gói");

        $sheetExcel->setCellValue('J' . $i, "Ngày hiệu lực");
        $sheetExcel->setCellValue('K' . $i, "Số tiền");
        $sheetExcel->setCellValue('L' . $i, "Vat");
        $sheetExcel->setCellValue('M' . $i, "Tổng tiền");
        $sheetExcel->setCellValue('N' . $i, "Tên chương trình");
        $sheetExcel->setCellValue('O' . $i, "URL GCN");

        //style content
        $sheetExcel->getStyle('A' . $i . ':O' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':O' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':O' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->CONTRACT_NO) ? $item->CONTRACT_NO : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->NAME) ? $item->NAME : '');
            $sheetExcel->setCellValue('E' . $i, isset($item->DOB) ? $item->DOB : '');

            $sheetExcel->setCellValue('F' . $i, isset($item->PRODUCT_CODE) ? $item->PRODUCT_CODE : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->PRODUCT_NAME) ? $item->PRODUCT_NAME : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->PACK_CODE) ? $item->PACK_CODE : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');

            $sheetExcel->setCellValue('J' . $i, isset($item->INSUR_TIME) ? $item->INSUR_TIME : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->AMOUNT) ? $item->AMOUNT : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->VAT) ? $item->VAT : '');

            $sheetExcel->setCellValue('M' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');
            $sheetExcel->setCellValue('N' . $i, isset($item->PROG_NAME) ? $item->PROG_NAME : '');

            //link
            $sheetExcel->setCellValue('O' . $i, isset($item->URL_DOWN) ? $item->URL_DOWN : '');
            //$sheetExcel->getCell('O' . $i)->getHyperlink()->setUrl(isset($item->URL_DOWN) ? $item->URL_DOWN : '#');
            //$sheetExcel->getStyle('O' . $i)->applyFromArray($this->link_style);
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    private function _savePropertiesFileExcel(&$spreadsheet, $fileName)
    {
        $userLogin = app(UserSystem::class)->userLogin();
        $userCreated = isset($userLogin['user_name']) ? $userLogin['user_name'] : 'HDI';
        $spreadsheet->getProperties()->setCreator($userCreated)
            ->setLastModifiedBy($userCreated)
            ->setTitle('Office 2007 XLSX Document')
            ->setSubject('Office 2007 XLSX Document')
            ->setDescription('Export ' . $fileName)
            ->setKeywords('office 2007 openxml php');
        //$sheetExcel = $spreadsheet->getActiveSheet();
    }

    public function exportOrdersInBatchesDetail($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:N2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:N2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Số GCN");
        $sheetExcel->setCellValue('C' . $i, "Số serial");

        $sheetExcel->setCellValue('D' . $i, "Họ tên khách hàng");
        $sheetExcel->setCellValue('E' . $i, "Giới tính");

        $sheetExcel->setCellValue('F' . $i, "Ngày sinh");
        $sheetExcel->setCellValue('G' . $i, "CMND/CCCD/Hộ chiếu");
        $sheetExcel->setCellValue('H' . $i, "Số điện thoại");

        $sheetExcel->setCellValue('I' . $i, "Email");
        $sheetExcel->setCellValue('J' . $i, "Địa chỉ");
        $sheetExcel->setCellValue('K' . $i, "Gói bảo hiểm");

        $sheetExcel->setCellValue('L' . $i, "Phí bảo hiểm");
        $sheetExcel->setCellValue('M' . $i, "Thời gian hiệu lực");
        //$sheetExcel->setCellValue('N' . $i, "Ngày hết hiệu lực");
        $sheetExcel->setCellValue('N' . $i, "Link GCN");

        //style content
        $sheetExcel->getStyle('A' . $i . ':N' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':N' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':N' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->CERTIFICATE_NO) ? $item->CERTIFICATE_NO : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->SERIAL) ? $item->SERIAL : '');

            $sheetExcel->setCellValue('D' . $i, isset($item->NAME) ? $item->NAME : '');
            $gioitinh = isset($item->GENDER) ? ($item->GENDER == 'M' ? 'Nam' : 'Nữ') : '';
            $sheetExcel->setCellValue('E' . $i, $gioitinh);
            $sheetExcel->setCellValue('F' . $i, isset($item->DOB) ? $item->DOB : '');

            $sheetExcel->setCellValue('G' . $i, isset($item->IDCARD) ? $item->IDCARD : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->PHONE) ? $item->PHONE : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->EMAIL) ? $item->EMAIL : '');

            $sheetExcel->setCellValue('J' . $i, isset($item->ADDRESS) ? $item->ADDRESS : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->PACK_NAME) ? $item->PACK_NAME : '');
            $sheetExcel->setCellValue('L' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');

            $sheetExcel->setCellValue('M' . $i, isset($item->INSUR_TIME) ? $item->INSUR_TIME : '');

            $sheetExcel->setCellValue('N' . $i, isset($item->URL_DOWN) ? $item->URL_DOWN : '');
            //$sheetExcel->getCell('N' . $i)->getHyperlink()->setUrl(isset($item->URL_DOWN) ? $item->URL_DOWN : '#');
            //$sheetExcel->getStyle('N' . $i)->applyFromArray($this->link_style);

            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);

    }

    //Export data golive
    public function exportDataGolive($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $i = 1;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");

        $sheetExcel->setCellValue('B' . $i, "REF_TABLE");
        $sheetExcel->setCellValue('C' . $i, "REF_ID");
        $sheetExcel->setCellValue('D' . $i, "INFO_SYNCH");

        //style content
        $sheetExcel->getStyle('A' . $i . ':D' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':D' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':D' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'left',
                'vertical' => 'left'
            )
        ));
        foreach ($data as $k => $item) {
            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->REF_TABLE) ? $item->REF_TABLE : '');
            $sheetExcel->setCellValue('C' . $i, isset($item->REF_ID) ? $item->REF_ID : '');
            $sheetExcel->setCellValue('D' . $i, isset($item->INFO_SYNCH) ? $item->INFO_SYNCH : '');
            $i++;
        }

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //đối soát bay AT
    public function exportReconciliationBayAT($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $dataTotal = isset($dataInput['dataTotal']) ? $dataInput['dataTotal'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:J2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:J2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Ngày khởi hành");

        $sheetExcel->setCellValue('C' . $i, "Số hiệu chuyến bay");
        $sheetExcel->setCellValue('D' . $i, "Số lượng hành khách");

        $sheetExcel->setCellValue('E' . $i, "Em bé đi kèm");
        $sheetExcel->setCellValue('F' . $i, "Số khách loại vé C");
        $sheetExcel->setCellValue('G' . $i, "Tổng doanh thu");

        $sheetExcel->setCellValue('H' . $i, "Phí quyền lợi A");
        $sheetExcel->setCellValue('I' . $i, "Phí quyền lợi B, không VAT");
        $sheetExcel->setCellValue('J' . $i, "Thuế quyền lợi B");

        //style content
        $sheetExcel->getStyle('A' . $i . ':J' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':J' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':J' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));

        foreach ($data as $k => $item) {

            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->FLI_R_DATE) ? $item->FLI_R_DATE : '');

            $sheetExcel->setCellValue('C' . $i, isset($item->FLI_NO) ? $item->FLI_NO : '');
            $sheetExcel->setCellValue('D' . $i, isset($item->NUM_OF_CUS) ? $item->NUM_OF_CUS : '');

            $sheetExcel->setCellValue('E' . $i, isset($item->NUM_OF_INFANT) ? $item->NUM_OF_INFANT : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->NUM_CLASS_C) ? $item->NUM_CLASS_C : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');

            $sheetExcel->setCellValue('H' . $i, isset($item->FEES_BENEFIT_A) ? $item->FEES_BENEFIT_A : '');
            $sheetExcel->setCellValue('I' . $i, isset($item->FEES_BENEFIT_B) ? $item->FEES_BENEFIT_B : '');
            $sheetExcel->setCellValue('J' . $i, isset($item->VAT_BENEFIT_B) ? $item->VAT_BENEFIT_B : '');
            $i++;
        }

        //tổng tiền
        /*$sheetExcel->getStyle('A' . $i . ':G' . ($col_next+1))->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FE2E2E'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));
        $sheetExcel->mergeCells('A' . $i . ':C' . ($col_next+1));
        $sheetExcel->setCellValue('A'.$i, 'Tổng tiền');
        $sheetExcel->setCellValue('D' . $i, isset($dataTotal->NUM_OF_CUS) ? $dataTotal->NUM_OF_CUS:'');
        $sheetExcel->setCellValue('E' . $i, isset($dataTotal->NUM_OF_INFANT) ? $dataTotal->NUM_OF_INFANT:'');
        $sheetExcel->setCellValue('F' . $i, isset($dataTotal->NUM_CLASS_C) ? $dataTotal->NUM_CLASS_C:'');
        $sheetExcel->setCellValue('G' . $i, isset($dataTotal->TOTAL_AMOUNT) ? $dataTotal->TOTAL_AMOUNT:'');*/

        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    //đối soát bay SKY_CARE
    public function exportReconciliationSkyCare($dataInput = [], $dataOther = [])
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $dataTotal = isset($dataInput['dataTotal']) ? $dataInput['dataTotal'] : [];
        $arrPackCode = isset($dataInput['arrPackCode']) ? $dataInput['arrPackCode'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:M2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:M2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Ngày khởi hành");
        $sheetExcel->setCellValue('C' . $i, "Hành trình");
        $sheetExcel->setCellValue('D' . $i, "Quốc gia đi");

        $sheetExcel->setCellValue('E' . $i, "Quốc gia đến");
        $sheetExcel->setCellValue('F' . $i, "Số hiệu chuyến bay");
        $sheetExcel->setCellValue('G' . $i, "Số hiệu chuyến bay nối chuyến");
        $sheetExcel->setCellValue('H' . $i, "Gói bảo hiểm");

        $sheetExcel->setCellValue('I' . $i, "Số lượng hành khách");
        $sheetExcel->setCellValue('J' . $i, "Em bé đi kèm");
        $sheetExcel->setCellValue('K' . $i, "Phí bảo hiểm/Hành khách");

        $sheetExcel->setCellValue('L' . $i, "Tổng doanh thu");
        $sheetExcel->setCellValue('M' . $i, "Ngày cấp đơn");

        //style content
        $sheetExcel->getStyle('A' . $i . ':M' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':M' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':M' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));

        foreach ($data as $k => $item) {

            $sheetExcel->setCellValue('A' . $i, $i - 3);// index column
            $sheetExcel->setCellValue('B' . $i, isset($item->DEPATURE_DATE) ? $item->DEPATURE_DATE : '');

            $sheetExcel->setCellValue('C' . $i, isset($item->ROUTE) ? $item->ROUTE : '');
            $sheetExcel->setCellValue('D' . $i, isset($item->DEPARTURE_COUNTRY) ? $item->DEPARTURE_COUNTRY : '');

            $sheetExcel->setCellValue('E' . $i, isset($item->ARRIVE_COUNTRY) ? $item->ARRIVE_COUNTRY : '');
            $sheetExcel->setCellValue('F' . $i, isset($item->FLI_NO) ? $item->FLI_NO : '');
            $sheetExcel->setCellValue('G' . $i, isset($item->CONNECT_FLI_NO) ? $item->CONNECT_FLI_NO : '');
            $sheetExcel->setCellValue('H' . $i, isset($item->PACK_CODE) && isset($arrPackCode[$item->PACK_CODE]) ? $arrPackCode[$item->PACK_CODE] : '');

            $sheetExcel->setCellValue('I' . $i, isset($item->CUSTOMER) ? $item->CUSTOMER : '');
            $sheetExcel->setCellValue('J' . $i, isset($item->INFANT) ? $item->INFANT : '');
            $sheetExcel->setCellValue('K' . $i, isset($item->FEE_INSUR) ? $item->FEE_INSUR : '');

            $sheetExcel->setCellValue('L' . $i, isset($item->TOTAL_AMOUNT) ? $item->TOTAL_AMOUNT : '');
            $sheetExcel->setCellValue('M' . $i, isset($item->MAX_DATE_SIGN) ? convertDateDMY_HI($item->MAX_DATE_SIGN) : '');
            $i++;
        }
        return $this->_saveFileExcel($spreadsheet, $fileName);
    }

    private function _saveFileExcel($spreadsheet, $fileName)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->setPreCalculateFormulas(false);
        $writer->save('php://output');
        exit;
    }

    public function exportDefault($dataInput)
    {
        $data = isset($dataInput['data']) ? $dataInput['data'] : [];
        $dataTotal = isset($dataInput['dataTotal']) ? $dataInput['dataTotal'] : [];
        $fileName = isset($dataInput['file_name']) ? $dataInput['file_name'] . ' ' . getCurrentDateDMYHIS() : 'Danh sách báo cáo ngày ' . getCurrentDateDMYHIS();
        if (empty($data))
            return;
        $total_staff = count($data);
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $this->_savePropertiesFileExcel($spreadsheet, $fileName);
        $sheetExcel = $spreadsheet->getActiveSheet();
        $sheetExcel->mergeCells('A1:G2');
        $sheetExcel->setCellValue('A1', $fileName);
        $sheetExcel->getStyle('A1:G2')->applyFromArray(array(
            'font' => array(
                'size' => 14,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => '47aa5e'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = 3;
        // style header cell of table
        $sheetExcel->setCellValue('A' . $i, "#");
        $sheetExcel->setCellValue('B' . $i, "Ngày khởi hành");

        $sheetExcel->setCellValue('C' . $i, "Số hiệu chuyến bay");
        $sheetExcel->setCellValue('D' . $i, "Số lượng hành khách");

        $sheetExcel->setCellValue('E' . $i, "Em bé đi kèm");
        $sheetExcel->setCellValue('F' . $i, "Số khách loại vé C");

        $sheetExcel->setCellValue('G' . $i, "Tổng doanh thu");

        //style content
        $sheetExcel->getStyle('A' . $i . ':G' . $i)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('4472C4');
        $sheetExcel->getStyle('A' . $i . ':G' . $i)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true,
                'name' => 'Calibri',
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center'
            )
        ));
        $i = $i + 1;
        $col_next = $total_staff + 3;
        $sheetExcel->getStyle('A' . $i . ':G' . $col_next)->applyFromArray(array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                )
            ),
            'font' => array(
                'size' => 10,
                'bold' => false,
                'name' => 'Calibri',
                'color' => array('rgb' => '000000'),
            ),
            'alignment' => array(
                'horizontal' => 'right',
                'vertical' => 'right'
            )
        ));

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
