<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 13/03/2022
* @Version   : 1.0
*/

namespace App\Services;

use App\Models\System\UserSystem;
use Illuminate\Support\Facades\Config;

//use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use Excel;


/**
 * Class ImportExcel
 * @package App\Services
 * Các function liên quan đến ImportExcel
 */
class ImportExcel
{
    /**
     *Gen giấy chứng nhận qua excel
     */
    public function importExcelGenCode($request, $files)
    {
        try {
            if (!isset($files['inputFileExcel']) || !$files['inputFileExcel']['name']) {
                throw new \PDOException('Bad Request: Please select file to import', 406);
            }
            $mimeType = mime_content_type($files['inputFileExcel']['tmp_name']);
            allowedImportMimeType($mimeType);

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($files['inputFileExcel']['tmp_name']);
            $objPHPExcel->setActiveSheetIndex(0);
            $rowsExcel = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            unset($rowsExcel[1]);//mảng tiêu đề

            //col A->E
            $dataInput = $listCodes = $errors = [];
            if (!empty($rowsExcel)) {
                foreach ($rowsExcel as $kr => $valRow) {
                    if (trim($valRow['D']) != '' || trim($valRow['E']) != '' || trim($valRow['K']) != '' || trim($valRow['M']) != '' || trim($valRow['U']) != '') {
                        $dataInput[$kr] = [
                            'I_NAME' => trim($valRow['D']),//name
                            'I_DOB' => trim($valRow['E']),//ngày sinh
                            'I_IDCARD' => trim($valRow['K']),//id card
                            'I_PHONE' => trim($valRow['M']),//Phone
                            'PACK_CODE' => trim($valRow['U']),//pack code
                        ];
                    } else {
                        break;
                    }
                }
            }
            //check dữ liệu
            if (!empty($dataInput)) {
                foreach ($dataInput as $num_row => &$value) {
                    try {
                        if (trim($value['I_NAME']) == '') {
                            throw new \PDOException('Tên đang trống');
                        }
                        if (trim($value['PACK_CODE']) == '') {
                            throw new \PDOException('Pack_code đang trống');
                        }
                        /*if ( trim($value['I_PHONE']) == '') {
                            throw new \PDOException('Số điện thoại đang trống');
                        }
                        if ( trim($value['I_DOB']) == '') {
                            throw new \PDOException('Ngày sinh đang trống');
                        }
                        if ( trim($value['I_IDCARD']) == '') {
                            throw new \PDOException('Idcard đang trống');
                        }*/

                    } catch (\PDOException $e) {
                        $errors[] = "<span class=\"col-sm-2\">DÒNG " . ($num_row) . "</span> <span class=\"col-sm-10\">{$e->getMessage()}</span>";
                    }
                }
                if (!empty($errors)) {
                    throw new \PDOException(implode('<br />', $errors));
                }
                return ['dataExcel' => $dataInput, 'isOk' => STATUS_INT_MOT];
            } else {
                throw new \PDOException('Dữ liệu đầu vào không đúng.', 406);
            }
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function importExcelCreatOrder($request, $files)
    {
        try {
            if (!isset($files['inputFileExcel']) || !$files['inputFileExcel']['name']) {
                return $message = 'Bad Request: Please select file to import';
            }
            $mimeType = mime_content_type($files['inputFileExcel']['tmp_name']);
            allowedImportMimeType($mimeType);

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($files['inputFileExcel']['tmp_name']);
            $objPHPExcel->setActiveSheetIndex(0);
            $rowsExcel = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            //unset($rowsExcel[1]);//mảng tiêu đề

            $dataInput = $arrKey = [];
            if (!empty($rowsExcel)) {
                foreach ($rowsExcel as $kr => $valRow) {
                    foreach ($valRow as $keyExcel => $valueExcel) {
                        if ($kr == 1) {
                            $arrKey[$keyExcel] = $valueExcel;
                        } else {
                            if (in_array($keyExcel, array_keys($arrKey))) {
                                $dataInput[$kr][$arrKey[$keyExcel]] = $valueExcel;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            $strjson = !empty($dataInput) ? json_encode($dataInput, false) : '';
            return $strjson;
        } catch (\PDOException $e) {
            //return $e->getMessage();
            return '';
        }
    }

    public function importExcelDataGolive($request, $files)
    {
        try {
            $message = 'Có lỗi dữ liệu đầu vào';
            if (!isset($files['inputFileExcel']) || !$files['inputFileExcel']['name']) {
                return $message = 'Bad Request: Please select file to import';
            }
            $mimeType = mime_content_type($files['inputFileExcel']['tmp_name']);
            allowedImportMimeType($mimeType);

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($files['inputFileExcel']['tmp_name']);
            $objPHPExcel->setActiveSheetIndex(0);
            $rowsExcel = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            unset($rowsExcel[1]);//mảng tiêu đề

            //col A->E
            $dataInput = $listCodes = $errors = [];
            if (!empty($rowsExcel)) {
                foreach ($rowsExcel as $kr => $valRow) {
                    if (trim($valRow['D']) != '' || trim($valRow['B']) != '' || trim($valRow['C']) != '') {
                        $dataInput[$kr] = [
                            'REF_TABLE' => trim($valRow['B']),//name
                            'REF_ID' => trim($valRow['C']),//ngày sinh
                            'INFO_SYNCH' => trim($valRow['D']),//id card
                        ];
                    } else {
                        break;
                    }
                }
            }
            //check dữ liệu
            if (!empty($dataInput)) {
                foreach ($dataInput as $num_row => &$value) {
                    try {
                        if (trim($value['REF_TABLE']) == '') {
                            throw new \PDOException('REF_TABLE đang trống');
                        }
                        if (trim($value['REF_ID']) == '') {
                            throw new \PDOException('REF_ID đang trống');
                        }
                        if (trim($value['INFO_SYNCH']) == '') {
                            throw new \PDOException('INFO_SYNCH đang trống');
                        }
                    } catch (\PDOException $e) {
                        $errors[] = "<span class=\"col-sm-2\">DÒNG " . ($num_row) . "</span> <span class=\"col-sm-10\">{$e->getMessage()}</span>";
                    }
                }
                if (!empty($errors)) {
                    throw new \PDOException(implode('<br />', $errors));
                }
            } else {
                throw new \PDOException('Dữ liệu đầu vào không đúng.', 406);
            }
            return ['dataExcel' => $dataInput, 'message' => $message];
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }
}
