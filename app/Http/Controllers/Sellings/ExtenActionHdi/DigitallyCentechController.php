<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Sellings\ExtenActionHdi;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\Organization;
use App\Models\System\Province;
use App\Models\Selling\ExtenActionHdi;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class DigitallyCentechController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrOrgAll = array();
    private $arrProvince = array();
    private $arrLoaiCapDon = array();
    private $arrProduct = array();

    private $templateRoot = DIR_PRO_SELLING . '/' . DIR_MODULE_EXTEN_ACTION_HDI . '.DigitallyCentech.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $routerIndex = 'digitallyCentech.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ExtenActionHdi();
    }

    private function _outDataView($request, $data)
    {
        $this->arrProduct = $this->getInforUser('product');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'org_code_user' => $this->user['org_code'],

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route('digitallyCentech.index'),
            'urlCancelOrder' => URL::route('digitallyCentech.ajaxCancelOrder'),
            'urlGetItem' => '',
            'urlPostItem' => '',
            'urlAjaxGetData' => '',
            'urlMovePay' => '',
            'urlSearchAdvanced' => '',
            'urlActionOtherItem' => '',
            'urlSearchOtherItem' => '',
            'urlUpdateStatusOtherItem' => '',
            'urlUpdateStatusItem' => '',
            'functionAction' => '_ajaxGetItemOther',
        ];
    }

    private function _validformdata($id = 0, &$data = array())
    {
        if (!empty($data)) {
            if (isset($data['AMOUNT_ALLOCATE']) && trim($data['AMOUNT_ALLOCATE']) == '') {
                $this->error[] = 'SL cấp phát chưa được nhập';
            }
        }
        return true;
    }

    /*********************************************************************************************************
     * Danh sách ký số
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Danh sách ký số Centech';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);

        $search['p_search'] = trim(addslashes(Request::get('p_search', '')));
        //$search['p_search'] = '21P00F11-000014-00;21P00F11-000013-00';
        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = trim($search['p_search']) != '' ? $this->modelObj->searchDigitallyCentech($search) : [];

        if (isset($result['Success']) && $result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = count($dataList);
        }
        //myDebug($result);
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    /***************************************************
     * Phê duyệt đơn theo list
     * @return \Illuminate\Http\JsonResponse|void
     ***************************************************/
    public function ajaxCancelOrder()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW,PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $arrAjax = array('success' => 0, 'message' => 'Có lỗi khi thao tác');
        $dataRequest = $_POST;
        if (empty($dataRequest['dataId']) || empty($dataRequest['dataTempCode']) || empty($dataRequest['dataOrgCode']) || empty($dataRequest['dataProductCode']) || empty($dataRequest['dataFileCode'])) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }

        $dataCancel = [];$str_file_code = '';
        if (!empty($dataRequest['dataId'])) {
            foreach ($dataRequest['dataId'] as $key => $val) {
                $dataCancel[] = ['BATCH_ID' => $val,
                    'TEMP_CODE' => isset($dataRequest['dataTempCode'][$key]) ? $dataRequest['dataTempCode'][$key] : '',
                    'FILE_CODE' => isset($dataRequest['dataFileCode'][$key]) ? $dataRequest['dataFileCode'][$key] : '',
                    'PRODUCT_CODE' => isset($dataRequest['dataProductCode'][$key]) ? $dataRequest['dataProductCode'][$key] : '',
                    'ORG_CODE' => isset($dataRequest['dataOrgCode'][$key]) ? $dataRequest['dataOrgCode'][$key] : ''];
            }
            $str_file_code = implode(';',$dataRequest['dataFileCode']);
        }
        if (!empty($dataCancel)) {
            $dataUpdate['p_action'] = 'EDIT';
            $dataUpdate['p_search'] = $str_file_code;
            $dataUpdate['p_json'] = $dataCancel;
            $result = $this->modelObj->searchDigitallyCentech($dataUpdate);

            if (isset($result['Success']) && $result['Success'] == STATUS_INT_MOT) {
                $arrAjax['success'] = 1;
                $arrAjax['message'] = 'Cập nhật thành công';
                return Response::json($arrAjax);
            } else {
                return Response::json($arrAjax);
            }
        }
        return Response::json($arrAjax);
    }

}
