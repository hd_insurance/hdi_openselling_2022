<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Sellings\SyncDataCore;

use App\Http\Controllers\BaseAdminController;
use App\Http\Models\Selling\ExtenActionHdi;
use App\Http\Models\Selling\PaymentContract;
use App\Http\Models\Selling\SyncDataCore;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;

use App\Services\ActionExcel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class SyncDataCoreController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;
    private $syncDataCore = false;

    private $arrStatus = array();
    private $arrProduct = array();
    private $templateRoot = DIR_PRO_SELLING . '/' . DIR_MODULE_EXTEN_ACTION_HDI . '.SyncDataCore.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $max_file_size = 10000000;//10.000.000
    private $routerIndex = 'syncDataCore.indexSyncDataCore';

    public function __construct()
    {
        parent::__construct();
        $this->syncDataCore = new SyncDataCore();
        $this->modelObj = new PaymentContract();
        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_PAY_STATUS);
    }

    private function _outDataView($request, $data)
    {
        $this->arrProduct = $this->getInforUser('product');
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['p_status']) ? $data['p_status'] : '');
        $optionOrg = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrgUser, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionProduct = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProductUser, (isset($data['p_product_code']) && trim($data['p_product_code']) != '') ? $data['p_product_code'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionOrg' => $optionOrg,
            'optionProduct' => $optionProduct,
            'org_code_user' => $this->user['org_code'],

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route('syncDataCore.indexSyncDataCore'),
            'urlAjaxGetAction' => URL::route('syncDataCore.ajaxGetAction'),
            'urlAjaxPostAction' => URL::route('syncDataCore.ajaxPostAction'),
            'functionAction' => '_ajaxGetItemOther',
        ];
    }

    /*********************************************************************************************************
     * Danh sách đồng bộ
     *********************************************************************************************************/
    public function indexSyncDataCore()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Đồng bộ dữ liệu sang core';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];

        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 1);
        $arrExport = [STATUS_INT_HAI,STATUS_INT_BA];
        $search['p_from_date'] = trim(addslashes(Request::get('p_from_date', '')));
        $search['p_to_date'] = trim(addslashes(Request::get('p_to_date', '')));

        $search['p_org_code'] = trim(addslashes(Request::get('p_org_code', '')));
        $search['p_product_code'] = trim(addslashes(Request::get('p_product_code', '')));
        $search['p_contract_no'] = trim(addslashes(Request::get('p_contract_no', '')));
        $search['p_from_date'] = ($search['p_from_date'] != '') ? $search['p_from_date'] : date('d/m/Y', strtotime(Carbon::now()->startOfMonth()));
        $search['p_to_date'] = ($search['p_to_date'] != '') ? $search['p_to_date'] : date('d/m/Y', strtotime(Carbon::now()));
        $search['page_no'] = in_array($submit,$arrExport) ? STATUS_INT_KHONG: $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->syncDataCore->searchDataSync($search);
        //myDebug($result);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        if (in_array($submit,$arrExport) && !empty($dataList)) {
            $this->actionExcel = new ActionExcel();
            if($submit == STATUS_INT_HAI){
                $type_export = ActionExcel::EXPORT_ORDERS_IN_BATCHES;
                $file_name = 'Danh sách cấp đơn thu gọn';
            }else{
                $type_export = ActionExcel::EXPORT_ORDERS_IN_BATCHES_DETAIL;
                $file_name = 'Danh sách cấp đơn chi tiết';
            }
            $dataExcel = ['data' => $dataList, 'total' => $total, 'file_name' => $file_name];
            $this->actionExcel->exportExcel($dataExcel, $type_export);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon, $this->dataOutItem));
    }

    public function ajaxGetAction()
    {
        $request = $_POST;
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $funcAction = isset($dataInput['funcAction']) ? $dataInput['funcAction'] : (isset($request['funcAction']) ? $request['funcAction'] : '');
        $objectId = isset($dataInput['objectId']) ? $dataInput['objectId'] : 1; //0 thêm mới, 1//edit
        $formName = isset($request['formName']) ? $request['formName'] : 'formName';
        $titlePopup = isset($request['titlePopup']) ? $request['titlePopup'] : 'formName';
        $htmlView = '';
        switch ($funcAction) {
            case 'getEditContract':
                myDebug($dataInput);
                $htmlView = View::make( $this->templateRoot . 'component._popupHistoryUpdateClaim')
                    ->with(array_merge([
                        'data' => [],

                        'objectId' => $objectId,
                        'formName' => $formName,
                        'formNameOther' => 'formNameTest',
                    ], $this->dataOutCommon))->render();
                break;
            default:
                break;
        }
        return $htmlView;
    }

    public function ajaxPostAction()
    {
        $request = $_POST;
        $arrAjax = array('success' => 0, 'html' => '', 'msg' => '');
        $actionUpdate = 'actionUpdate';
        $actionUpdate = isset($request['dataForm']['actionUpdate']) ? $request['dataForm']['actionUpdate'] : (isset($request['actionUpdate']) ? $request['actionUpdate'] : $actionUpdate);

        switch ($actionUpdate) {
            //cập nhật thông tin liên hệ bồi thường
            case 'syncDataCore':
                $dataForm = isset($request['dataForm']) ? $request['dataForm'] : [];
                myDebug($dataForm);
                $dataClaim = isset($dataForm['data_item']) ? json_decode($dataForm['data_item'], true) : false;
                $itemList = isset($dataForm['itemList']) ? json_decode($dataForm['itemList'], true) : false;
                $dataUpdateContact = [
                    'ORG_CODE' => isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '',
                    'PRODUCT_CODE' => isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '',
                    'CHANNEL' => isset($dataForm['CHANNEL']) ? $dataForm['CHANNEL'] : '',
                    'CLAIM_CODE' => isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '',
                    'RELATIONSHIP' => isset($dataForm['RELATIONSHIP']) ? $dataForm['RELATIONSHIP'] : '',
                    'NAME' => isset($dataForm['REQUIRE_NAME']) ? $dataForm['REQUIRE_NAME'] : '',
                    'IDCARD' => isset($dataForm['IDCARD']) ? $dataForm['IDCARD'] : '',
                    'DOB' => isset($dataForm['DOB']) ? $dataForm['DOB'] : '',
                    'PHONE' => isset($dataForm['PHONE']) ? $dataForm['PHONE'] : '',
                    'EMAIL' => isset($dataForm['EMAIL']) ? $dataForm['EMAIL'] : ''
                ];

                $result = $this->modelObj->updateContactOrFilesAttack(1, $dataUpdateContact);
                if (isset($result['Data'][0][0]->TYPE) && $result['Data'][0][0]->TYPE == 'SUCCESS') {
                    $dataClaim['REQUIRE_NAME'] = $dataUpdateContact['NAME'];

                    $itemList['IDCARD'] = $dataUpdateContact['IDCARD'];
                    $itemList['DOB'] = $dataUpdateContact['DOB'];
                    $itemList['PHONE'] = $dataUpdateContact['PHONE'];
                    $itemList['EMAIL'] = $dataUpdateContact['EMAIL'];

                    $templateOut = $this->templateRoot . 'component.editClaim._editFormItem';
                    $this->_outDataView($request, (array)[]);
                    $htmlView = View::make($templateOut)
                        ->with(array_merge([
                            'data' => (object)$dataClaim,
                            'itemList' => $itemList,//data from list data

                            'objectId' => $dataForm['objectId'],
                            'formName' => $dataForm['formName'],
                            'formNameOther' => 'formNameTest',
                        ], $this->dataOutCommon))->render();

                    $arrAjax['success'] = 1;
                    $arrAjax['html'] = $htmlView;
                    $arrAjax['divShowInfor'] = $dataForm['div_show_edit_success'];
                }
                break;
            default:
                break;
        }
        return Response::json($arrAjax);
    }
}