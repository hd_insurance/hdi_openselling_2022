<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Sellings\ClaimIndemnify;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\Organization;
use App\Models\System\Province;
use App\Models\Selling\ClaimHdi;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Library\AdminFunction\Upload;
use App\Services\ActionExcel;
use App\Services\ServiceCommon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class ClaimHdiController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrOrgAll = array();
    private $arrChannel = array();
    private $arrMoiQuanHe = array();
    private $arrStatusNotEdit = array();
    private $arrStatusHideListBT = array();
    private $arrTypeFiles = array();
    private $arrProductType = array();
    private $arrProvince = array();
    public $arrProCodeVietJet = [PRODUCT_CODE_BAY_AT, PRODUCT_CODE_LOST_BAGGAGE];

    private $templateRoot = DIR_PRO_SELLING . '/' . DIR_MODULE_CLAIM_INDEMNIFY . '.ClaimHdi.';
    private $templateVietJet = DIR_PRO_SELLING . '/' . DIR_MODULE_CLAIM_INDEMNIFY . '.ClaimVietJet.';
    private $templateApprove = DIR_PRO_SELLING . '/' . DIR_MODULE_CLAIM_INDEMNIFY . '.ClaimApprove.';
    private $templateAccountant = DIR_PRO_SELLING . '/' . DIR_MODULE_CLAIM_INDEMNIFY . '.ClaimAccountant.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $routerIndex = 'claimHdi.index';
    private $routerIndexClaimAccountant = 'claimAccountant.indexClaimAccountant';
    private $routerIndexVietJet = 'claimHdi.indexVietJet';
    private $routerIndexReportVietJet = 'claimReport.indexClaimVietJet';
    private $routerIndexClaimCompany = 'claimCompany.indexClaimCompany';
    private $routerIndexClaimDepart = 'claimDepart.indexClaimDepart';

    /* Array
        (
            [TMBT] => Chờ tiếp nhận
            [DYKH] => Đồng ý bồi thường - chờ KH xác nhận
            [TCKH] => Từ chối bồi thường - Chờ KH xác nhận
            [YCBS] => Chờ bổ sung giấy tờ
            [HTHS] => Chờ hoàn tất hồ sơ

            [XLHS] => Đang xử lý
            [KHDY] => KH đồng ý phương án bồi thường
            [KHTC] => KH không đồng ý phương án bồi thường
            [TTBT] => Đã chi trả bồi thường
            [TCBT] => Từ chối bồi thường
            [DATN] => Đã tiếp nhận
            [DABS] => Đã bổ sung giấy tờ
            [DYCC] => Đồng ý bồi thường chờ LĐ Cty duyệt
            [DYCB] => Đồng ý bồi thường chờ LĐ Ban duyệt
            [TCCB] => Từ chối bồi thường chờ LĐ Ban duyệt
            [TCCC] => Từ chối bồi thường chờ LĐ Cty Duyệt
            [TCBB] => Lãnh đạo Ban từ chối bồi thường
            [TCBC] => Lãnh đạo Cty từ chối bồi thường
            [CTBT] => Chờ chi trả bồi thường
        )
     * */

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ClaimHdi();

        $this->arrMoiQuanHe = $this->getArrOptionTypeDefine(DEFINE_MOI_QUAN_HE);
        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_CLAIM_STATUS);
        $this->arrChannel = $this->getArrOptionTypeDefine(DEFINE_CLAIM_CHANNEL);
        $this->arrProvince = app(Province::class)->getOptionProvince();
        $this->arrOrgAll = app(Organization::class)->getArrOptionOrg();
        $this->arrTypeFiles = [0 => 'Bản mềm', 1 => 'Bản cứng'];

        $this->arrStatusNotEdit = [STATUS_CLAIM_TTBT, STATUS_CLAIM_TCBT];
        $this->arrStatusHideListBT = [STATUS_CLAIM_TMBT, STATUS_CLAIM_XLHS, STATUS_CLAIM_YCBS, STATUS_CLAIM_DATN, STATUS_CLAIM_DABS];
        /**
         * - Đồng ý bồi thường chờ LĐ Ban duyệt
         * - Từ chối bồi thường chờ LĐ Ban duyệt
         * - KH đồng ý phương án bồi thường
         * - KH từ chối phương án bồi thường
         * - Chờ hoàn tất hồ sơ
         * - Chờ chi trả bồi thường
         * - Từ chối bồi thường
         * => Đối với trường hợp đồng ý bồi thường thì hiển thị kết quả đồng ý + quyền lợi
         * và số tiền bồi thường. Còn đối với trường hợp Từ chối bồi thường thì hiển thị kết quả từ chối bồi thường
         */
    }

    /*********************************************************************************************************
     * Hồ sơ bồi thường
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Danh sách bồi thường HDI';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 0);

        //tìm kiếm nâng cao
        $dataForm = $_GET;
        $search = $this->modelObj->getParamSearch($dataForm);
        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchClaimHdi($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = isset($dataList[0]->TOTAL) ? $dataList[0]->TOTAL : $total;
        }
        if ($submit == STATUS_INT_HAI) {
            $dataExcel = ['data' => $dataList, 'total' => $total];
            $this->actionExcel = new ActionExcel();
            $this->actionExcel->exportExcel($dataExcel, ActionExcel::EXPORT_EXCEL_CLAIM_HDI);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);

        $dataForm['div_show'] = 'table_show_ajax';
        $dataForm['template_out'] = 'ClaimHDI';
        $dataForm['router_index'] = 'claimHdi.index';

        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'paramSearch' => $dataForm,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    /**
     * @param array $paramCallback : dùng call lại để lấy dữ liệu hiển thị qua ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearchAjax($paramCallback = [])
    {
        $request = $_GET;
        $dataForm = !empty($paramCallback) ? $paramCallback : $request['dataForm'];

        $product_code = isset($dataForm['p_product_code']) ? $dataForm['p_product_code'] : '';
        $dataForm['p_product_code'] = $product_code;

        $div_show = (isset($dataForm['div_show']) && trim($dataForm['div_show']) != '') ? $dataForm['div_show'] : '';
        $template_out = (isset($dataForm['template_out']) && trim($dataForm['template_out']) != '') ? $dataForm['template_out'] : 'ClaimHDI';
        $router_index = (isset($dataForm['router_index']) && trim($dataForm['router_index']) != '') ? $dataForm['router_index'] : 'claimHdi.index';
        $page_no = (isset($dataForm['page_no']) && trim($dataForm['page_no']) != '') ? $dataForm['page_no'] : (int)Request::get('page_no', 1);

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $search = $this->modelObj->getParamSearch($dataForm);
        $result = $this->modelObj->searchClaimHdi($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = isset($dataList[0]->TOTAL) ? $dataList[0]->TOTAL : $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);

        //check quyen theo page with ajax
        $indexPage = $this->routerIndex;
        switch ($template_out) {
            case 'ClaimVietJet':
                $templateOut = $this->templateVietJet . 'component._tableClaimVietJet';
                $indexPage = $this->routerIndexVietJet;
                break;
            case 'ReportClaimVietJet':
                $templateOut = DIR_PRO_REPORT . '.claim._tableClaim_VIETJET';//report
                $indexPage = $this->routerIndexReportVietJet;
                break;
            case 'ClaimAccountant':
                $templateOut = $this->templateAccountant . 'componentAccountant._tableClaim';// dùng cho kế toán
                $indexPage = $this->routerIndexClaimAccountant;
                break;
            case 'ClaimApprove':
                $templateOut = ($router_index == $this->routerIndexClaimCompany) ? $this->templateApprove . 'componentCompany._tableClaim' : $this->templateApprove . 'componentDepart._tableClaim';
                $indexPage = $router_index;
                break;
            default:
                $templateOut = $this->templateRoot . 'component._tableClaim';
                break;
        }

        //add thêm khi load lại dữ liệu ajax
        $this->shareListPermission($indexPage);//lay quyen theo ajax
        $html = View::make($templateOut)
            ->with(array_merge([
                'data' => $dataList,
                'search' => $search,
                'paramSearch' => $dataForm,
                'total' => $total,
                'stt' => ($page_no - 1) * $limit,
                'paging' => $paging,
                'pageTitle' => $this->pageTitle,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowId' => $div_show, 'message' => '');
        return !empty($paramCallback) ? $arrAjax : Response::json($arrAjax);
    }

    /*********************************************************************************************************
     * Duyệt hồ sơ - LĐ Ban
     * componentDepart
     *********************************************************************************************************/
    public function indexClaimDepart()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }

        $this->pageTitle = CGlobal::$pageAdminTitle = 'Duyệt hồ sơ - Lãnh đạo Ban';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 0);
        //tìm kiếm nâng cao
        $dataForm = $_GET;
        $dataForm['p_str_status'] = STATUS_CLAIM_DYCB . ';' . STATUS_CLAIM_TCCB . ';' . STATUS_CLAIM_TCBB;
        $search = $this->modelObj->getParamSearch($dataForm);

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchClaimHdi($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = isset($dataList[0]->TOTAL) ? $dataList[0]->TOTAL : $total;
        }

        if ($submit == STATUS_INT_HAI) {
            $dataExcel = ['data' => $dataList, 'total' => $total];
            $this->actionExcel = new ActionExcel();
            $this->actionExcel->exportExcel($dataExcel, ActionExcel::EXPORT_EXCEL_CLAIM_HDI);
        }
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $arrStatus = [
            STATUS_CLAIM_DYCB => isset($this->arrStatus[STATUS_CLAIM_DYCB]) ? $this->arrStatus[STATUS_CLAIM_DYCB] : 'Trạng thái',//Đồng ý bồi thường chờ LĐ Ban duyệt
            STATUS_CLAIM_TCCB => isset($this->arrStatus[STATUS_CLAIM_TCCB]) ? $this->arrStatus[STATUS_CLAIM_TCCB] : 'Trạng thái',// Từ chối ban
            STATUS_CLAIM_TCBB => isset($this->arrStatus[STATUS_CLAIM_TCBB]) ? $this->arrStatus[STATUS_CLAIM_TCBB] : 'Trạng thái',//Từ chối lãnh đạo ban
        ];
        $optionStatusSearch = FunctionLib::getOptionMultil($arrStatus, (isset($search['p_status']) && trim($search['p_status'])) ? explode(';', $search['p_status']) : []);
        $this->_outDataView($_GET, $search);
        $dataForm['div_show'] = 'table_show_ajax';
        $dataForm['template_out'] = 'ClaimApprove';
        $dataForm['router_index'] = $this->routerIndexClaimDepart;
        return view($this->templateApprove . 'viewIndexDepart', array_merge($this->dataOutCommon, [
            'data' => $dataList,
            'search' => $search,
            'paramSearch' => $dataForm,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
            'optionStatus' => $optionStatusSearch,
        ]));
    }

    /*********************************************************************************************************
     * Duyệt hồ sơ - LĐ Cty
     *********************************************************************************************************/
    public function indexClaimCompany()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }

        $this->pageTitle = CGlobal::$pageAdminTitle = 'Duyệt hồ sơ - Lãnh đạo Công ty';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 0);
        //tìm kiếm nâng cao
        $dataForm = $_GET;
        $dataForm['p_str_status'] = STATUS_CLAIM_DYCC . ';' . STATUS_CLAIM_TCCC . ';' . STATUS_CLAIM_TCBC;
        $search = $this->modelObj->getParamSearch($dataForm);

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchClaimHdi($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = isset($dataList[0]->TOTAL) ? $dataList[0]->TOTAL : $total;
        }

        if ($submit == STATUS_INT_HAI) {
            $dataExcel = ['data' => $dataList, 'total' => $total];
            $this->actionExcel = new ActionExcel();
            $this->actionExcel->exportExcel($dataExcel, ActionExcel::EXPORT_EXCEL_CLAIM_HDI);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $arrStatus = [
            STATUS_CLAIM_DYCC => isset($this->arrStatus[STATUS_CLAIM_DYCC]) ? $this->arrStatus[STATUS_CLAIM_DYCC] : 'Trạng thái',
            STATUS_CLAIM_TCCC => isset($this->arrStatus[STATUS_CLAIM_TCCC]) ? $this->arrStatus[STATUS_CLAIM_TCCC] : 'Trạng thái',
            STATUS_CLAIM_TCBC => isset($this->arrStatus[STATUS_CLAIM_TCBC]) ? $this->arrStatus[STATUS_CLAIM_TCBC] : 'Trạng thái',
        ];
        $optionStatusSearch = FunctionLib::getOptionMultil($arrStatus, (isset($search['p_status']) && trim($search['p_status'])) ? explode(';', $search['p_status']) : []);
        $this->_outDataView($_GET, $search);
        $dataForm['div_show'] = 'table_show_ajax';
        $dataForm['template_out'] = 'ClaimApprove';
        $dataForm['router_index'] = $this->routerIndexClaimCompany;
        return view($this->templateApprove . 'viewIndexCompany', array_merge($this->dataOutCommon, [
            'data' => $dataList,
            'search' => $search,
            'paramSearch' => $dataForm,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
            'optionStatus' => $optionStatusSearch,
        ]));
    }

    /*********************************************************************************************************
     * Thanh toán bồi thường
     *********************************************************************************************************/
    public function indexClaimAccountant()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }

        $this->pageTitle = CGlobal::$pageAdminTitle = 'Thanh toán bồi thường';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 0);
        //tìm kiếm nâng cao
        $dataForm = $_GET;
        $dataForm['p_str_status'] = STATUS_CLAIM_CTBT;
        $search = $this->modelObj->getParamSearch($dataForm);

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchClaimHdi($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = isset($dataList[0]->TOTAL) ? $dataList[0]->TOTAL : $total;
        }

        if ($submit == STATUS_INT_HAI) {
            $dataExcel = ['data' => $dataList, 'total' => $total];
            $this->actionExcel = new ActionExcel();
            $this->actionExcel->exportExcel($dataExcel, ActionExcel::EXPORT_EXCEL_CLAIM_HDI);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $arrStatus = [
            STATUS_CLAIM_CTBT => isset($this->arrStatus[STATUS_CLAIM_CTBT]) ? $this->arrStatus[STATUS_CLAIM_CTBT] : 'Trạng thái',//chờ chi trả
            STATUS_CLAIM_TTBT => isset($this->arrStatus[STATUS_CLAIM_TTBT]) ? $this->arrStatus[STATUS_CLAIM_TTBT] : 'Trạng thái',//đã chi trả
        ];
        $optionStatusSearch = FunctionLib::getOptionMultil($arrStatus, (isset($search['p_status']) && trim($search['p_status'])) ? explode(';', $search['p_status']) : []);
        $this->_outDataView($_GET, $search);
        $dataForm['div_show'] = 'table_show_ajax';
        $dataForm['template_out'] = 'ClaimAccountant';
        $dataForm['router_index'] = $this->routerIndexClaimAccountant;
        return view($this->templateAccountant . 'viewIndexAccountant', array_merge($this->dataOutCommon, [
            'data' => $dataList,
            'search' => $search,
            'paramSearch' => $dataForm,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
            'optionStatus' => $optionStatusSearch,
        ]));
    }

    /*********************************************************************************************************
     * Danh sách bồi thường VietJet
     *********************************************************************************************************/
    public function indexVietJet()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Bồi thường VietJet';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 0);

        if (in_array(PRODUCT_CODE_BAY_AT, array_keys($this->arrProductUser))) {
            $product_code = trim(addslashes(Request::get('p_product_code', PRODUCT_CODE_BAY_AT)));
        } else {
            $product_code = !empty($this->arrProductUser) ? array_key_first($this->arrProductUser) : DATA_SEARCH_NULL;
        }
        //tìm kiếm nâng cao
        $dataForm = $_GET;
        $search = $this->modelObj->getParamSearch($dataForm, ORG_VIETJET_VN, $product_code);

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchClaimHdi($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = isset($dataList[0]->TOTAL) ? $dataList[0]->TOTAL : $total;
        }

        if ($submit == STATUS_INT_HAI && !empty($dataList)) {
            $total = count($dataList);
            $dataExcel = ['data' => $dataList, 'total' => $total];
            $this->actionExcel = new ActionExcel();
            $this->actionExcel->exportExcel($dataExcel, ActionExcel::EXPORT_EXCEL_CLAIM_VIETJET);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);

        //option sản phẩm
        $arrProductOption = getArrChild($this->arrProductUser, $this->arrProCodeVietJet);
        $optionProduct = FunctionLib::getOption(['' => '---Chọn---'] + $arrProductOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');

        $this->dataOutItem = [
            'urlIndex' => URL::route('claimHdi.indexVietJet'),
            'optionProduct' => $optionProduct,
        ];
        return view($this->templateVietJet . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon, $this->dataOutItem));
    }

    private function _outDataView($request, $data)
    {
        $p_status = isset($data['p_status']) ? $data['p_status'] : '';
        $request['p_status_form'] = Request::get('p_status', $p_status);//ko dùng cho param
        $request['p_status'] = Request::get('p_status', $p_status);//ko dùng cho param
        if (is_array($request['p_status_form'])) {
            $request['p_status'] = !empty($request['p_status_form']) ? implode(';', $request['p_status_form']) : '';
        }

        $optionStatus = FunctionLib::getOptionMultil($this->arrStatus, (isset($data['p_status']) && trim($data['p_status'])) ? explode(';', $data['p_status']) : []);
        $optionChannel = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrChannel, isset($data['p_channel']) ? $data['p_channel'] : '');
        $optionOrg = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrOrgAll, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrProductUser, (isset($data['p_product_code']) && trim($data['p_product_code']) != '') ? $data['p_product_code'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionChannel' => $optionChannel,
            'optionOrg' => $optionOrg,
            'optionProduct' => $optionProduct,

            'arrProductType' => $this->arrProductType,
            'arrStatusHideListBT' => $this->arrStatusHideListBT,
            'arrStatus' => $this->arrStatus,
            'arrMoiQuanHe' => $this->arrMoiQuanHe,
            'org_code_user' => $this->user['org_code'],

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route(Route::currentRouteName()),
            'urlGetItem' => '',//URL::route('claimHdi.ajaxGetItem'),
            'urlPostItem' => URL::route('claimHdi.ajaxPostItem'),
            'urlUpdateData' => URL::route('claimHdi.ajaxUpdateData'),
            'urlAjaxGetData' => URL::route('claimHdi.ajaxGetData'),
            'urlChangeProcess' => URL::route('claimHdi.ajaxChangeProcess'),
            'userAction' => $this->user,
            'urlServiceFile' => Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/',
            'functionAction' => '_ajaxGetItemOther',
            'urlSearchAjax' => URL::route('claimHdi.getSearchAjax'),
            'formSeachIndex' => 'formSeachIndex',
        ];
    }

    /*
     * Get data with function common
     * */
    private function _ajaxFunctionAction($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $funcAction = isset($dataInput['funcAction']) ? $dataInput['funcAction'] : (isset($request['funcAction']) ? $request['funcAction'] : '');
        $objectId = isset($dataInput['objectId']) ? $dataInput['objectId'] : 1; //0 thêm mới, 1//edit
        $formName = isset($request['formName']) ? $request['formName'] : 'formName';
        $titlePopup = isset($request['titlePopup']) ? $request['titlePopup'] : 'formName';
        $paramSearch = isset($dataInput['paramSearch']) ? $dataInput['paramSearch'] : [];
        $indexPageInput = isset($dataInput['indexPage']) ? $dataInput['indexPage'] : '';

        $htmlView = '';
        switch ($funcAction) {
            case 'getEditClaim':
                $this->_outDataView($request, (array)[]);
                $dataClaim = isset($dataInput['dataClaim']) ? $dataInput['dataClaim'] : false;

                //myDebug($dataInput,false);
                $p_org_code = isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '';
                $p_product_code = isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '';
                $p_claim_code = isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '';
                $inforClaim = $this->_getDataClaim($p_claim_code, $p_product_code, $p_org_code);

                $detailClaim = $inforClaim['detailClaim'];
                $listFileAttack = $inforClaim['listFileAttack'];
                $inforDetailExten = $inforClaim['inforDetailExten'];
                $desRequestClaim = $inforClaim['desRequestClaim'];
                $listTimeLine = $inforClaim['listTimeLine'];
                $listDuocBoiThuong = $inforClaim['listDuocBoiThuong'];
                $listFileBoSung = $inforClaim['listFileBoSung'];

                $arrEditStatusPopup = $arrStatusAll = $this->arrStatus;
                $templateDetailItem = isset($dataInput['templateDetailItem']) ? $dataInput['templateDetailItem'] : '';
                $indexPage = $this->routerIndex;
                $isEdit = STATUS_INT_KHONG;
                switch ($templateDetailItem) {
                    case 'ClaimAccountant':
                        $templateDetail = $this->templateAccountant . 'componentAccountant.popupDetail';
                        $arrEditStatusPopup = [STATUS_CLAIM_TTBT => isset($arrStatusAll[STATUS_CLAIM_TTBT]) ? $arrStatusAll[STATUS_CLAIM_TTBT] : 'Trạng thái'];
                        $indexPage = $this->routerIndexClaimAccountant;
                        break;
                    case 'ClaimDepart':
                        $templateDetail = $this->templateApprove . 'componentDepart.popupDetail';
                        $indexPage = $this->routerIndexClaimDepart;
                        break;
                    case 'ClaimCompany':
                        $templateDetail = $this->templateApprove . 'componentCompany.popupDetail';
                        $indexPage = $this->routerIndexClaimCompany;
                        break;
                    default:
                        $templateDetail = $this->templateRoot . 'component.editClaim.popupEdit';
                        $isEdit = STATUS_INT_MOT;
                        $arrEditStatusPopup = [
                            STATUS_CLAIM_DATN => isset($arrStatusAll[STATUS_CLAIM_DATN]) ? $arrStatusAll[STATUS_CLAIM_DATN] : 'Trạng thái',//Đã tiếp nhận
                            STATUS_CLAIM_XLHS => isset($arrStatusAll[STATUS_CLAIM_XLHS]) ? $arrStatusAll[STATUS_CLAIM_XLHS] : 'Trạng thái',//Đang xử lý
                            STATUS_CLAIM_YCBS => isset($arrStatusAll[STATUS_CLAIM_YCBS]) ? $arrStatusAll[STATUS_CLAIM_YCBS] : 'Trạng thái',//Chờ bổ sung giấy tờ
                            STATUS_CLAIM_DABS => isset($arrStatusAll[STATUS_CLAIM_DABS]) ? $arrStatusAll[STATUS_CLAIM_DABS] : 'Trạng thái',//Đã bổ sung giấy tờ
                            STATUS_CLAIM_DYCB => isset($arrStatusAll[STATUS_CLAIM_DYCB]) ? $arrStatusAll[STATUS_CLAIM_DYCB] : 'Trạng thái',//Đồng ý bồi thường chờ LĐ Ban duyệt
                            STATUS_CLAIM_TCCB => isset($arrStatusAll[STATUS_CLAIM_TCCB]) ? $arrStatusAll[STATUS_CLAIM_TCCB] : 'Trạng thái',// Từ chối ban
                            STATUS_CLAIM_KHDY => isset($arrStatusAll[STATUS_CLAIM_KHDY]) ? $arrStatusAll[STATUS_CLAIM_KHDY] : 'Trạng thái',//Khách hàng đồng ý
                            STATUS_CLAIM_KHTC => isset($arrStatusAll[STATUS_CLAIM_KHTC]) ? $arrStatusAll[STATUS_CLAIM_KHTC] : 'Trạng thái',// KH từ chối
                            STATUS_CLAIM_HTHS => isset($arrStatusAll[STATUS_CLAIM_HTHS]) ? $arrStatusAll[STATUS_CLAIM_HTHS] : 'Trạng thái',// Chờ hoàn tất hồ so
                            STATUS_CLAIM_CTBT => isset($arrStatusAll[STATUS_CLAIM_CTBT]) ? $arrStatusAll[STATUS_CLAIM_CTBT] : 'Trạng thái',// Chờ chi trả bồi thường
                            STATUS_CLAIM_TCBT => isset($arrStatusAll[STATUS_CLAIM_TCBT]) ? $arrStatusAll[STATUS_CLAIM_TCBT] : 'Trạng thái',// Từ chối bồi thường
                        ];
                        break;
                }

                $this->shareListPermission($indexPage);//lay quyen theo ajax
                $relationship = isset($dataClaim['RELATIONSHIP_CODE']) ? $dataClaim['RELATIONSHIP_CODE'] : '';
                $optionMoiQuanHe = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrMoiQuanHe, $relationship);
                $htmlView = View::make($templateDetail)
                    ->with(array_merge([
                        'data' => $detailClaim,
                        'itemList' => $dataClaim,//data from list data
                        'listFileAttack' => $listFileAttack,
                        'inforDetailExten' => $inforDetailExten,
                        'desRequestClaim' => $desRequestClaim,
                        'listTimeLine' => $listTimeLine,
                        'listDuocBoiThuong' => $listDuocBoiThuong,
                        'listFileBoSung' => $listFileBoSung,
                        'arrStatusNotEdit' => $this->arrStatusNotEdit,

                        'arrEditStatusPopup' => $arrEditStatusPopup,
                        'optionMoiQuanHe' => $optionMoiQuanHe,
                        'isEdit' => $isEdit,
                        'indexPage' => $indexPage,
                        'paramSearch' => $paramSearch,
                        'objectId' => $objectId,
                        'formName' => $formName,
                        'formNameOther' => 'formNameTest',
                    ], $this->dataOutCommon))->render();
                break;
            case 'getHistoryUpdateClaim':
                $this->_outDataView($request, (array)[]);
                $dataClaim = isset($dataInput['dataClaim']) ? $dataInput['dataClaim'] : false;

                $p_org_code = isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '';
                $p_product_code = isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '';
                $p_claim_code = isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '';
                //$p_claim_code =  'CD48A7CF43133D5FE0530100007F236A';
                $dataHistory = $this->modelObj->getHistoryUpdateClaim($p_claim_code, $p_product_code, $p_org_code);

                $templateDetail = $this->templateRoot . 'component._popupHistoryUpdateClaim';
                $htmlView = View::make($templateDetail)
                    ->with(array_merge([
                        'dataHistoryUpdate' => $dataHistory,
                        'objectId' => $objectId,
                        'formName' => $formName,
                        'formNameOther' => 'formNameTest',
                    ], $this->dataOutCommon))->render();
                break;
            case 'getUploadFileClaim':
                $this->_outDataView($request, (array)[]);
                $templateOut = $this->templateRoot . 'component._popupUploadFilesClaim';
                $htmlView = View::make($templateOut)
                    ->with(array_merge([
                        'data' => [],
                        'file_key' => $dataInput['file_key'],
                        'relationship' => $dataInput['relationship'],
                        'detailClaim' => $dataInput['detailClaim'],

                        'objectId' => $objectId,
                        'formName' => $formName,
                        'title_popup' => $titlePopup,
                    ], $this->dataOutCommon))->render();
                break;
            case 'showListFileClaim':
                $listFiles = $dataInput['listFiles'];
                $service = new ServiceCommon();
                foreach ($listFiles as $key => &$val_files) {
                    if (trim($val_files['FILE_ID']) != '') {
                        $val_files['FILE_TYPE'] = $service->getTypeFileId($val_files['FILE_ID']);
                    }
                }

                $this->_outDataView($request, (array)[]);
                $templateOut = $this->templateRoot . 'component._popupShowFilesClaim';
                $htmlView = View::make($templateOut)
                    ->with(array_merge([
                        'data' => [],
                        'listFiles' => $listFiles,
                        'detailClaim' => $dataInput['detailClaim'],
                        'itemList' => $dataInput['itemList'],

                        'objectId' => $objectId,
                        'formName' => $formName,
                        'title_popup' => $titlePopup,
                    ], $this->dataOutCommon))->render();
                break;
            case 'getApproveClaim':
                $this->shareListPermission($indexPageInput);//lay quyen theo ajax
                $this->_outDataView($request, (array)[]);
                $templateOut = $this->templateApprove . 'popupApproveClaim';
                $htmlView = View::make($templateOut)
                    ->with(array_merge([
                        'data' => [],
                        'actionApprove' => $dataInput['actionApprove'],
                        'detailClaim' => $dataInput['detailClaim'],
                        'dataItem' => $dataInput['dataItem'],
                        'paramSearch' => $paramSearch,
                        'objectId' => $objectId,
                        'formName' => $formName,
                        'title_popup' => $titlePopup,
                    ], $this->dataOutCommon))->render();
                break;
            default:
                break;
        }
        return $htmlView;
    }

    /*
     * url update common
     * actionUpdate:
     * */
    public function ajaxUpdateData()
    {
        $request = $_POST;
        $arrAjax = array('success' => 0, 'html' => '', 'msg' => '');
        $actionUpdate = 'actionUpdate';
        $actionUpdate = isset($request['dataForm']['actionUpdate']) ? $request['dataForm']['actionUpdate'] : (isset($request['actionUpdate']) ? $request['actionUpdate'] : $actionUpdate);

        switch ($actionUpdate) {
            //cập nhật thông tin liên hệ bồi thường
            case 'updateContact':
                $dataForm = isset($request['dataForm']) ? $request['dataForm'] : [];
                $dataClaim = isset($dataForm['data_item']) ? json_decode($dataForm['data_item'], true) : false;
                $itemList = isset($dataForm['itemList']) ? json_decode($dataForm['itemList'], true) : false;
                $dataUpdateContact = [
                    'ORG_CODE' => isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '',
                    'PRODUCT_CODE' => isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '',
                    'CHANNEL' => isset($dataForm['CHANNEL']) ? $dataForm['CHANNEL'] : '',
                    'CLAIM_CODE' => isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '',
                    'RELATIONSHIP' => isset($dataForm['RELATIONSHIP']) ? $dataForm['RELATIONSHIP'] : '',
                    'NAME' => isset($dataForm['REQUIRE_NAME']) ? $dataForm['REQUIRE_NAME'] : '',
                    'IDCARD' => isset($dataForm['IDCARD']) ? $dataForm['IDCARD'] : '',
                    'DOB' => isset($dataForm['DOB']) ? $dataForm['DOB'] : '',
                    'PHONE' => isset($dataForm['PHONE']) ? $dataForm['PHONE'] : '',
                    'EMAIL' => isset($dataForm['EMAIL']) ? $dataForm['EMAIL'] : ''
                ];

                $result = $this->modelObj->updateContactOrFilesAttack(1, $dataUpdateContact);
                if (isset($result['Data'][0][0]->TYPE) && $result['Data'][0][0]->TYPE == 'SUCCESS') {
                    $dataClaim['REQUIRE_NAME'] = $dataUpdateContact['NAME'];

                    $itemList['IDCARD'] = $dataUpdateContact['IDCARD'];
                    $itemList['DOB'] = $dataUpdateContact['DOB'];
                    $itemList['PHONE'] = $dataUpdateContact['PHONE'];
                    $itemList['EMAIL'] = $dataUpdateContact['EMAIL'];
                    $itemList['RELATIONSHIP'] = $dataUpdateContact['RELATIONSHIP'];

                    $templateOut = $this->templateRoot . 'component.editClaim._editFormItem';
                    $this->_outDataView($request, (array)[]);

                    $relationship = isset($itemList['RELATIONSHIP']) ? $itemList['RELATIONSHIP'] : '';
                    $optionMoiQuanHe = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrMoiQuanHe, $relationship);
                    $htmlView = View::make($templateOut)
                        ->with(array_merge([
                            'data' => (object)$dataClaim,
                            'itemList' => $itemList,//data from list data

                            'objectId' => $dataForm['objectId'],
                            'formName' => $dataForm['formName'],
                            'isEdit' => $dataForm['isEdit'],
                            'arrStatusNotEdit' => $this->arrStatusNotEdit,
                            'optionMoiQuanHe' => $optionMoiQuanHe,
                            'formNameOther' => 'formNameTest',
                        ], $this->dataOutCommon))->render();

                    $arrAjax['success'] = 1;
                    $arrAjax['html'] = $htmlView;
                    $arrAjax['divShowInfor'] = $dataForm['div_show_edit_success'];
                }
                break;
            case 'uploadFilesClaim':
                $file_key = $request['FILE_KEY'];
                $relationship = $request['RELATIONSHIP'];
                $type_of_paper = isset($request['TYPE_OF_PAPER']) ? 1 : 0;
                $dataClaim = isset($request['detailClaim']) ? json_decode($request['detailClaim'], true) : false;
                $itemList = isset($request['itemList']) ? json_decode($request['itemList'], true) : false;

                $dataUpdateFile = [
                    'ORG_CODE' => isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '',
                    'PRODUCT_CODE' => isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '',
                    'CHANNEL' => isset($dataClaim['CHANNEL']) ? $dataClaim['CHANNEL'] : '',
                    'CLAIM_CODE' => isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '',
                    'RELATIONSHIP' => $relationship,
                    'ARR_FILES' => [],
                ];

                $arrFilesUpdate = [];
                $folder = FOLDER_FILE_CLAIM;
                $ext_file = 'jfif,jpeg,png,tif,psd,pdf,eps,ai,heic,raw,svg';
                $max_file_size = 10000000;//10.000.000
                $arrFile = app(Upload::class)->uploadMultipleFile('filesClaim', $folder, $ext_file, $max_file_size);
                if (!empty($arrFile)) {
                    foreach ($arrFile as $ky => $fileName) {
                        if (trim($fileName) != '') {
                            $pathFileUpload = getDirFile($fileName);
                            $file_id = app(ServiceCommon::class)->moveFileToServerStore($pathFileUpload, false);
                            app(Upload::class)->removeFile('', $fileName);
                            $inforFiles = [
                                'FILE_KEY' => $file_key,
                                'FILE_NAME' => $fileName,
                                'FILE_ID' => $file_id,
                                'TYPE_OF_PAPER' => $type_of_paper,
                            ];
                            $arrFilesUpdate[$ky] = $inforFiles;
                        }
                        sleep(2);
                    }
                } else {
                    return Response::json(returnError(viewLanguage('Upload file ko đúng định dạng: ' . $ext_file)));
                }

                $dataUpdateFile['ARR_FILES'] = $arrFilesUpdate;
                $result = $this->modelObj->updateContactOrFilesAttack(2, $dataUpdateFile);

                if (isset($result['Success']) && $result['Success'] == 1) {
                    $p_org_code = isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '';
                    $p_product_code = isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '';
                    $p_claim_code = isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '';

                    $inforClaim = $this->_getDataClaim($p_claim_code, $p_product_code, $p_org_code);
                    $detailClaim = $inforClaim['detailClaim'];
                    $listFileAttack = $inforClaim['listFileAttack'];

                    $this->_outDataView($request, (array)[]);
                    $templateOut = $this->templateRoot . 'component._tableFilesAttack';
                    $htmlView = View::make($templateOut)
                        ->with(array_merge([
                            'data' => $detailClaim,
                            'itemList' => $itemList,//data from list data
                            'listFileAttack' => $listFileAttack,
                        ], $this->dataOutCommon))->render();
                    $arrAjax['success'] = 1;
                    $arrAjax['html'] = $htmlView;
                    $arrAjax['divShowInfor'] = $request['divShowDataSuccess'];
                }
                break;
            case 'updateApproveStatus':
                $dataForm = isset($request['dataForm']) ? $request['dataForm'] : [];
                $actionApprove = isset($dataForm['actionApprove']) ? $dataForm['actionApprove'] : [];
                $reasonApprove = isset($dataForm['REASON_APPROVE']) ? $dataForm['REASON_APPROVE'] : [];
                $dataClaim = isset($dataForm['dataClaim']) ? json_decode($dataForm['dataClaim'], true) : false;
                $dataItem = isset($dataForm['dataItem']) ? json_decode($dataForm['dataItem'], false) : false;

                $claim_status_first = $claim_status = $dataClaim['STATUS'];
                switch ($actionApprove) {
                    /**
                     * Đồng ý bồi thường chờ LĐ ban duyệt
                     * Duyệt : DYCB => duyệt =>DYCC
                     * Từ chối: DYCB => từ chối =>TCBB
                     *
                     * Từ chối bồi thường chờ LĐ ban duyệt
                     * Duyệt : TCCB => duyệt =>TCCC
                     * Từ chối: TCCB => từ chối =>TCBB
                     */
                    case 'approveSuccessDepart':
                        $claim_status = ($claim_status_first == STATUS_CLAIM_DYCB) ? STATUS_CLAIM_DYCC : STATUS_CLAIM_TCCC;
                        break;
                    case 'approveCancelDepart':
                        $claim_status = STATUS_CLAIM_TCBB;
                        break;
                    /**
                     * Đồng ý bồi thường chờ LĐ Cty duyệt
                     * Duyệt : DYCC => duyệt =>DYKH
                     * Từ chối: DYCC => từ chối =>TCBC
                     *
                     * Từ chối bồi thường chờ LĐ Cty duyệt
                     * Duyệt : TCCC => duyệt =>TCKH
                     * Từ chối: TCCC => từ chối =>TCBC
                     */
                    case 'approveSuccessCompany':
                        $claim_status = ($claim_status_first == STATUS_CLAIM_DYCC) ? STATUS_CLAIM_DYKH : STATUS_CLAIM_TCKH;
                        break;
                    case 'approveCancelCompany':
                        $claim_status = STATUS_CLAIM_TCBC;
                        break;
                    default:
                        break;
                }

                $dataApprovel['p_org_code'] = $dataClaim['ORG_CODE'];
                $dataApprovel['p_prodcode'] = $dataClaim['PRODUCT_CODE'];
                $dataApprovel['p_claim_code'] = $dataClaim['CLAIM_CODE'];
                $dataApprovel['p_claim_status'] = $claim_status;
                $dataApprovel['p_staffcode'] = $this->user['user_id'];
                $dataApprovel['p_staffname'] = $this->user['user_full_name'];
                $dataApprovel['p_content'] = $reasonApprove;
                $dataApprovel['p_pay_date'] = '';
                $dataApprovel['p_pay_claim'] = '';

                $result = $this->modelObj->updateChangeProcess($dataApprovel);
                if (isset($result['Data'][0][0]->TYPE) && $result['Data'][0][0]->TYPE == 'SUCCESS') {

                    //gửi mail nếu có
                    $this->_sendEmailClaim($claim_status, $dataItem, ['note' => $reasonApprove]);

                    $arrAjax['success'] = 1;
                    $arrAjax['html'] = '';
                    $arrAjax['loadPage'] = 1;
                }
                break;
            default:
                break;
        }
        return Response::json($arrAjax);
    }

    private function _getDataClaim($p_claim_code = '', $p_product_code = '', $p_org_code = '')
    {
        $detailClaim = $inforDetailExten = $desRequestClaim = $listFileAttack = $listTimeLine = $listDuocBoiThuong = $listFileBoSung = [];
        if (trim($p_claim_code) != '' && trim($p_claim_code) != '' && trim($p_claim_code) != '') {
            $search['p_org_code'] = $p_org_code;
            $search['p_product_code'] = $p_product_code;
            $search['p_claim_code'] = $p_claim_code;
            $search['p_type'] = 'DETAIL';
            $dataGet = $this->modelObj->searchClaimHdi($search);
            //myDebug($dataGet);
            if (isset($dataGet['Success']) && $dataGet['Success'] == STATUS_INT_MOT) {
                //0--Thong tin claim chi tiet
                $detailClaim = isset($dataGet['Data'][0][0]) ? $dataGet['Data'][0][0] : [];

                //1--Thong tin claim chi tiet exten
                $inforDetailExten = isset($dataGet['Data'][1]) ? $dataGet['Data'][1] : [];

                //2--Thong tin mo ta yeu cau boi thuong
                $resultRequest = isset($dataGet['Data'][2]) ? $dataGet['Data'][2] : [];
                foreach ($resultRequest as $key => $valu) {
                    if (isset($desRequestClaim[$valu->BEN_CODE]['ARR_LIST'])) {
                        $desRequestClaim[$valu->BEN_CODE]['ARR_LIST'][] = ['DEC_DESC' => $valu->DEC_DESC, 'DEC_VALUE' => $valu->DEC_VALUE];
                    } else {
                        $desRequestClaim[$valu->BEN_CODE]['BEN_NAME'] = $valu->BEN_NAME;
                        $desRequestClaim[$valu->BEN_CODE]['ARR_LIST'][] = ['DEC_DESC' => $valu->DEC_DESC, 'DEC_VALUE' => $valu->DEC_VALUE];
                    }
                }

                //3--Thong tin giay to di kem
                $resultFiles = isset($dataGet['Data'][3]) ? $dataGet['Data'][3] : [];
                foreach ($resultFiles as $key2 => $valu2) {
                    if (isset($listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['ITEM_FILE'])) {
                        if ($valu2->TYPE_OF_PAPER == 1) {
                            $listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['TYPE_OF_PAPER'] = $valu2->TYPE_OF_PAPER;
                        }
                        $listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['ITEM_FILE'][] = (array)$valu2;
                    } else {
                        $listFileAttack[$valu2->GROUP_ID]['GROUP_NAME'] = $valu2->GROUP_NAME;
                        $listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['TYPE_OF_PAPER'] = $valu2->TYPE_OF_PAPER;
                        $listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['FILE_KEY'] = $valu2->FILE_KEY;
                        $listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['GROUP_FILE_NAME'] = $valu2->GROUP_FILE_NAME;
                        $listFileAttack[$valu2->GROUP_ID]['ARR_LIST'][$valu2->FILE_KEY]['ITEM_FILE'][] = (array)$valu2;
                    }
                }
                if (!empty($listFileAttack)) {
                    ksort($listFileAttack);
                }

                //4--Thong tin tien trinh xu ly
                $listTimeLine = isset($dataGet['Data'][4]) ? $dataGet['Data'][4] : [];

                //5--list danh sách bồi thường nếu có
                $listDuocBoiThuong = isset($dataGet['Data'][5]) ? $dataGet['Data'][5] : [];

                //6--list danh sách file bổ sung nếu có
                $listFileBoSung = isset($dataGet['Data'][6]) ? $dataGet['Data'][6] : [];
            }
        }
        //myDebug($listFileAttack);
        return ['detailClaim' => $detailClaim,
            'inforDetailExten' => $inforDetailExten,
            'desRequestClaim' => $desRequestClaim,
            'listFileAttack' => $listFileAttack,
            'listTimeLine' => $listTimeLine,
            'listDuocBoiThuong' => $listDuocBoiThuong,
            'listFileBoSung' => $listFileBoSung,];
    }

    private function _ajaxActionOther($request)
    {
        $data = $inforItem = $listNotPayment = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;

        $listBoiThuongChose = isset($dataInput['listDuocBoiThuong']) ? $dataInput['listDuocBoiThuong'] : [];
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : (isset($request['type']) ? $request['type'] : '');
        $dataClaim = isset($dataInput['dataClaim']) ? $dataInput['dataClaim'] : [];
        $paramSearch = isset($dataInput['paramSearch']) ? $dataInput['paramSearch'] : [];
        $dataItem = isset($dataInput['dataItem']) ? $dataInput['dataItem'] : [];
        $objectId = isset($request['objectId']) ? $request['objectId'] : '';
        $templateOut = $this->templateRoot . 'component._popupChangeStatus';

        switch ($typeTab) {
            //get popup chuyển đổi  trạng thái
            case 'getChangeStatus':
                $search['p_org_code'] = isset($dataClaim['ORG_CODE']) ? $dataClaim['ORG_CODE'] : '';
                $search['p_product_code'] = isset($dataClaim['PRODUCT_CODE']) ? $dataClaim['PRODUCT_CODE'] : '';
                $search['p_claim_code'] = isset($dataClaim['CLAIM_CODE']) ? $dataClaim['CLAIM_CODE'] : '';
                $search['p_type'] = 'LIST_BEN';
                $dataGet = $this->modelObj->searchClaimHdi($search);

                //arr list file cần bổ sung
                $arrLisDocument = $this->_getListDocument($search);
                $optionLisDocument = FunctionLib::getOption(['' => '---Chọn---'] + $arrLisDocument, '');
                $listBoiThuong = $arrOptionBoiThuong = [];
                if (isset($dataGet['Success']) && $dataGet['Success'] == STATUS_INT_MOT) {
                    //get list bồi thường table
                    foreach ($dataGet['Data'][0] as $kk => $val_option) {
                        $arrOptionBoiThuong[$val_option->BEN_CODE] = $val_option->BEN_NAME;
                    }
                    $listBoiThuong = isset($dataGet['Data'][0]) ? $dataGet['Data'][0] : [];
                }
                $arrEditStatusPopup = isset($dataInput['arrEditStatusPopup']) ? $dataInput['arrEditStatusPopup'] : $this->arrStatus;
                $optionStatusPopup = FunctionLib::getOption(['' => '---Chọn---'] + $arrEditStatusPopup, isset($dataClaim['STATUS']) ? $dataClaim['STATUS'] : '');
                $optionBoiThuong = FunctionLib::getOption(['' => '---Chọn---'] + $arrOptionBoiThuong, '');

                //list file bổ sung nếu có
                $filesBoSung = isset($dataInput['listFileBoSung']) ? $dataInput['listFileBoSung'] : [];
                $listFilesAdd = $dataFileExit = [];
                if (!empty($filesBoSung)) {
                    foreach ($filesBoSung as $ks => $fileBS) {
                        $FILE_KEY = $fileBS['FILE_KEY'];
                        $listFilesAdd[$FILE_KEY]['NAME_FILE'] = $fileBS['FILE_NAME'];
                        $listFilesAdd[$FILE_KEY]['NOTE_FILE'] = $fileBS['NOTE'];
                        $listFilesAdd[$FILE_KEY]['TYPE_FILE'] = $fileBS['TYPE_OF_PAPER'];
                        $dataFileExit[] = $FILE_KEY;
                    }
                }

                // myDebug($listBoiThuong);
                $this->dataOutItem = [
                    'listBoiThuongChose' => $listBoiThuongChose,
                    'listBoiThuong' => $listBoiThuong,
                    'optionBoiThuong' => $optionBoiThuong,

                    'listFilesAdd' => $listFilesAdd,
                    'optionStatusPopup' => $optionStatusPopup,
                    'optionLisDocument' => $optionLisDocument,
                    'arrLisDocument' => $arrLisDocument,
                    'arrTypeFiles' => $this->arrTypeFiles,
                    'strDataFileExit' => implode(',', $dataFileExit),

                    'urlAddInforClaim' => URL::route('claimHdi.ajaxChangeProcess'),
                ];
                $templateOut = $this->templateRoot . 'component._popupChangeStatus';
                break;
            // add item file bổ sung
            case 'addFilesClaim':
                $listFilesAdd = [];
                $dataFileExit = isset($request['dataFileExit']) ? $request['dataFileExit'] : [];
                $arrLisDocument = isset($request['arrLisDocument']) ? json_decode($request['arrLisDocument'], true) : [];
                $key_file_chose = $request['key_file_chose'];

                if (!empty($dataFileExit) && in_array($key_file_chose, $dataFileExit)) {
                    $arrAjax = array('success' => 0, 'message' => 'File bổ sung này đã tồn tại');
                    return $arrAjax;
                }
                $dataFileExit[] = $key_file_chose;
                $listFilesAddInput = isset($request['listFilesAdd']) ? json_decode($request['listFilesAdd'], false) : false;
                if (!empty($listFilesAddInput)) {
                    foreach ($listFilesAddInput as $kk => $inforClaim) {
                        $listFilesAdd[$key_file_chose]['NAME_FILE'] = isset($arrLisDocument[$key_file_chose]) ? $arrLisDocument[$key_file_chose] : '';
                        $listFilesAdd[$key_file_chose]['NOTE_FILE'] = '';
                        $listFilesAdd[$key_file_chose]['TYPE_FILE'] = '';
                    }
                } else {
                    $listFilesAdd[$key_file_chose]['NAME_FILE'] = isset($arrLisDocument[$key_file_chose]) ? $arrLisDocument[$key_file_chose] : '';
                    $listFilesAdd[$key_file_chose]['NOTE_FILE'] = '';
                    $listFilesAdd[$key_file_chose]['TYPE_FILE'] = '';
                }
                $this->dataOutItem = [
                    'listFilesAdd' => $listFilesAdd,
                    'arrTypeFiles' => $this->arrTypeFiles,
                    'strDataFileExit' => implode(',', $dataFileExit),
                ];
                $templateOut = $this->templateRoot . 'component._tr_add_file_claim';
                break;
            case 'addInforClaim':
                $listBoiThuong = [];
                $dataClaimChose = isset($request['dataClaimChose']) ? $request['dataClaimChose'] : [];
                $valu_claim_chose = $request['valu_claim_chose'];
                if ($valu_claim_chose == 'KHAC') {
                    $inforClaim = [];
                    $inforClaim['BEN_CODE'] = 'KHAC_' . (count($dataClaimChose)+1);
                    $inforClaim['BEN_NAME'] = '';
                    $listBoiThuong[] = (object)$inforClaim;
                } else {
                    if (!empty($dataClaimChose) && in_array($valu_claim_chose, $dataClaimChose)) {
                        $arrAjax = array('success' => 0, 'message' => 'Quyền lợi bồi thường này đã tồn tại');
                        return $arrAjax;
                    }

                    $listBoiThuongInput = isset($request['listBoiThuong']) ? json_decode($request['listBoiThuong'], false) : false;
                    $arrExits = [];
                    if (!empty($listBoiThuongInput)) {
                        foreach ($listBoiThuongInput as $kk => $inforClaim) {
                            if ($inforClaim->BEN_CODE == $valu_claim_chose) {
                                if (empty($arrExits) || !in_array($valu_claim_chose, $arrExits)) {
                                    $listBoiThuong[] = $inforClaim;
                                    $arrExits[$valu_claim_chose] = $valu_claim_chose;
                                }
                            }
                        }
                    }
                }
                $this->dataOutItem = [
                    'listBoiThuong' => $listBoiThuong,
                ];
                $templateOut = $this->templateRoot . 'component._tr_infor_claim';
                break;
            case 'getHistory':
                $templateOut = $this->templateRoot . 'component._popupHistoryTimeLine';
                break;
            case 'getTimeLine':
                $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];
                $listTimeLine = [];
                if (!empty($arrKey)) {
                    $search['p_org_code'] = isset($arrKey['ORG_CODE']) ? $arrKey['ORG_CODE'] : '';
                    $search['p_product_code'] = isset($arrKey['PRODUCT_CODE']) ? $arrKey['PRODUCT_CODE'] : '';
                    $search['p_claim_code'] = isset($arrKey['CLAIM_CODE']) ? $arrKey['CLAIM_CODE'] : '';
                    $search['p_type'] = 'DETAIL';
                    $dataGet = $this->modelObj->searchClaimHdi($search);
                    if (isset($dataGet['Success']) && $dataGet['Success'] == STATUS_INT_MOT) {
                        //4--Thong tin tien trinh xu ly
                        $resultTimeLine = isset($dataGet['Data'][4]) ? $dataGet['Data'][4] : [];
                        foreach ($resultTimeLine as $key => $valu) {
                            $arrDate = explode(' ', $valu->PROCESSING_DATE);
                            $dateTime = $arrDate[0];
                            $valu->HOURS = $arrDate[1];
                            $valu->WORK_NAME = isset($this->arrStatus[$valu->WORK_ID]) ? $this->arrStatus[$valu->WORK_ID] : $valu->WORK_ID;
                            $listTimeLine[$dateTime][] = $valu;
                        }
                    }
                }
                $this->dataOutItem = [
                    'listTimeLine' => $listTimeLine,
                ];
                $templateOut = $this->templateVietJet . 'component._popupHistoryTimeLine';
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$dataClaim);
        $html = View::make($templateOut)
            ->with(array_merge([
                'data' => $data,
                'objectId' => $objectId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
                'dataClaim' => $dataClaim,
                'dataItem' => $dataItem,
                'paramSearch' => $paramSearch,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    /***************************************************
     * Đổi trạng thái của đơn
     * @return \Illuminate\Http\JsonResponse|void
     ***************************************************/
    public function ajaxChangeProcess()
    {
        $arrAjax = array('success' => 0, 'message' => 'Có lỗi khi thao tác');
        $dataRequest = $_POST;
        $dataForm = isset($dataRequest['dataForm']) ? $dataRequest['dataForm'] : [];
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }

        //fix trường hợp có quyền lợi bồi thường rồi, chỉ chuyển trạng thái, nhưng vẫn phải
        // giữ nguyên các quyền lợi BT trước đó
        //check xem quyền lợi có thay đổi khi chuyển trạng thái ko?
        $arrClaimChose = $dataBoiThuongExit = [];
        $listBoiThuongChose = isset($dataForm['listBoiThuongChose']) ? json_decode($dataForm['listBoiThuongChose']) : [];
        if(!empty($listBoiThuongChose)){
            foreach ($listBoiThuongChose as $keyChose => $objChose) {
                if(isset($dataForm[$objChose->BEN_CODE]) && isset($dataForm['money_'.$objChose->BEN_CODE]) && $dataForm['money_'.$objChose->BEN_CODE] > 0){
                    $arrTemChose = [
                        'SORT' => $objChose->SORT,
                        'BEN_CODE' => $objChose->BEN_CODE,
                        'BEN_NAME' => $objChose->BEN_NAME,
                        'AMOUNT' => $objChose->AMOUNT
                        ];
                    $arrClaimChose[] = $arrTemChose;
                }
            }
        }

        $listBoiThuong = isset($dataForm['listBoiThuong']) ? json_decode($dataForm['listBoiThuong']) : [];

        $dataItem = isset($dataForm['dataItem']) ? json_decode($dataForm['dataItem'], false) : false;
        $dataClaim = isset($dataForm['dataClaim']) ? json_decode($dataForm['dataClaim'], false) : false;
        $paramSearch = isset($dataForm['paramSearch']) ? json_decode($dataForm['paramSearch'], true) : false;

        $claim_status = isset($dataForm['CLAIM_STATUS']) ? $dataForm['CLAIM_STATUS'] : '';
        $note_status = isset($dataForm['NOTE_STATUS']) ? $dataForm['NOTE_STATUS'] : '';
        $pay_date = isset($dataForm['PAY_DATE']) ? $dataForm['PAY_DATE'] : '';

        /*if (trim($claim_status) == '' || trim($note_status) == '') {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không chính xác! Hãy nhập lại')));
        }*/
        if ($claim_status == $dataClaim->STATUS) {
            return Response::json(returnError(viewLanguage('Trạng thái trùng với trạng thái hiện tại!')));
        }
        if ($claim_status == STATUS_CLAIM_TTBT && $pay_date == '') {
            return Response::json(returnError(viewLanguage('Ngày thanh toán phải được chọn!')));
        }

        //get list infor claim từ form mới
        $dataBoiThuong = [];
        $totalBoiThuong = 0;
        $arrStatusAddBoiThuong = [STATUS_CLAIM_DYCB, STATUS_CLAIM_TCCB];
        $indexSort = 1;
        foreach ($dataForm as $keyForm => $valueForm) {
            $keyBenCode = $keyForm;
            if(isset($dataForm['money_'.$keyBenCode]) && isset($dataForm['name_ben_code_'.$keyBenCode])){
                if (empty($dataBoiThuongExit) || !in_array($keyBenCode, $dataBoiThuongExit)) {
                    $money = (isset($dataForm['money_' . $keyBenCode]) && trim($dataForm['money_' . $keyBenCode]) != '') ? returnFormatMoney($dataForm['money_' . $keyBenCode]) : 0;
                    $totalBoiThuong = $totalBoiThuong + returnFormatMoney($money);
                    if ($money > 0) {
                        $arrTem = ['SORT' => $indexSort,
                            'BEN_CODE' => $keyBenCode,
                            'BEN_NAME' => $dataForm['name_ben_code_'.$keyBenCode],
                            'AMOUNT' => returnFormatMoney($money)
                        ];
                        $indexSort++;
                        $dataBoiThuong[] = $arrTem;
                        $dataBoiThuongExit[$keyBenCode] = $keyBenCode;
                    }
                }
            }
        }

        //list bồi thường
        /*$dataBoiThuong = [];
        $totalBoiThuong = 0;
        $arrStatusAddBoiThuong = [STATUS_CLAIM_DYCB, STATUS_CLAIM_TCCB];
        if (!empty($listBoiThuong) && in_array($claim_status, $arrStatusAddBoiThuong)) {
            foreach ($listBoiThuong as $keybt => $boithuong) {
                if (empty($dataBoiThuongExit) || !in_array($boithuong->BEN_CODE, $dataBoiThuongExit)) {
                    if (in_array($claim_status, [STATUS_CLAIM_TTBT])) {
                        $money = (isset($dataForm['money_' . $boithuong->BEN_CODE]) && trim($dataForm['money_' . $boithuong->BEN_CODE]) != '') ? returnFormatMoney($dataForm['money_' . $boithuong->BEN_CODE]) : 0;
                    } else {
                        $money = (isset($dataForm[$boithuong->BEN_CODE]) && trim($dataForm[$boithuong->BEN_CODE]) != '') ? returnFormatMoney($dataForm[$boithuong->BEN_CODE]) : 0;
                    }
                    $totalBoiThuong = $totalBoiThuong + returnFormatMoney($money);
                    if ($money > 0) {
                        $arrTem = ['SORT' => $boithuong->SORT,
                            'BEN_CODE' => $boithuong->BEN_CODE,
                            'BEN_NAME' => $boithuong->BEN_NAME,
                            'AMOUNT' => $money
                        ];
                        $dataBoiThuong[] = $arrTem;
                        $dataBoiThuongExit[$boithuong->BEN_CODE] = $boithuong->BEN_CODE;
                    }
                }
            }
        }
        //quyền lợi khác
        //quyền lợi bồi thường add thêm
        $arrClaimDiff = [];
        foreach ($dataForm as $ki => $val_i) {
            $k_name = 'name_ben_code_' . $ki;
            if (isset($dataForm[$k_name]) && isset($dataForm[$ki]) && $dataForm[$ki] > 0) {
                $arrClaimDiff[] = ['BEN_CODE' => $ki, 'BEN_NAME' => $dataForm[$k_name], 'SORT' => 1, 'AMOUNT' => $dataForm[$ki]];
            }
        }
        if (!empty($arrClaimDiff) && in_array($claim_status, $arrStatusAddBoiThuong)) {
            foreach ($arrClaimDiff as $kkk => $val_clai) {
                if (empty($dataBoiThuongExit) || !in_array($val_clai['BEN_CODE'], $dataBoiThuongExit)) {
                    $money = returnFormatMoney($val_clai['AMOUNT']);
                    $totalBoiThuong = $totalBoiThuong + $money;
                    if ($money > 0) {
                        $arrTem = ['SORT' => $val_clai['SORT'],
                            'BEN_CODE' => $val_clai['BEN_CODE'],
                            'BEN_NAME' => $val_clai['BEN_NAME'],
                            'AMOUNT' => $money];
                        $dataBoiThuong[] = $arrTem;
                        $dataBoiThuongExit[$val_clai['BEN_CODE']] = $val_clai['BEN_CODE'];
                    }
                }
            }
        }*/

        /*myDebug('$dataForm',false);
        myDebug($dataForm,false);

        myDebug('$dataBoiThuong',false);
        myDebug($dataBoiThuong,true);*/

        //có file bổ sung với các trạng thái: Chờ bổ sung giấy tờ hoặc Chờ hoàn tất hồ sơ
        $dataFilesAdd = [];
        if (in_array($claim_status, [STATUS_CLAIM_YCBS, STATUS_CLAIM_HTHS])) {
            $type_files_add = isset($dataForm['TYPE_FILES_ADD']) ? $dataForm['TYPE_FILES_ADD'] : '';
            if (trim($type_files_add) == '') {
                return Response::json(returnError(viewLanguage('Dữ liệu chưa có file bổ sung! Hãy nhập lại')));
            }
            $arrFilesAdd = explode(',', $dataForm['strDataFileExit']);
            foreach ($arrFilesAdd as $k => $key_file_name) {
                $inforFiles = [
                    'FILE_KEY' => $key_file_name,
                    'FILE_NAME' => $dataForm['NAME_FILE_' . $key_file_name],
                    'NOTE' => $dataForm['NOTE_FILE_' . $key_file_name],
                    'TYPE_OF_PAPER' => $dataForm['TYPE_FILE_' . $key_file_name],
                    'FILE_ID' => '',
                ];
                $dataFilesAdd[$k] = $inforFiles;
            }
        }

        $dataApprovel['p_org_code'] = $dataClaim->ORG_CODE;
        $dataApprovel['p_prodcode'] = $dataClaim->PRODUCT_CODE;
        $dataApprovel['p_claim_code'] = $dataClaim->CLAIM_CODE;
        $dataApprovel['p_claim_status'] = $claim_status;
        $dataApprovel['p_staffcode'] = (isset($this->user['emp_code']) && !empty($this->user['emp_code'])) ? $this->user['emp_code'] : $this->user['user_id'];
        $dataApprovel['p_staffname'] = $this->user['user_full_name'];
        $dataApprovel['p_content'] = $note_status;
        $dataApprovel['p_pay_date'] = (in_array($claim_status, [STATUS_CLAIM_TTBT])) ? $pay_date : '';
        $dataApprovel['p_pay_claim'] = !empty($dataBoiThuong) ? $dataBoiThuong : (!empty($arrClaimChose) ? $arrClaimChose : '');//thông tin bồi thường
        $dataApprovel['p_file_attach'] = !empty($dataFilesAdd) ? $dataFilesAdd : '';//file bổ sung

        $result = $this->modelObj->updateChangeProcess($dataApprovel);

        if ($result['Success'] == STATUS_INT_MOT && isset($result['Data'][0][0]->TYPE) && $result['Data'][0][0]->TYPE == 'SUCCESS') {
            //Gửi mail cho KH nếu có
            //1:có gửi mail: 0: không gửi
            if (trim($note_status) != ''){
                $this->_sendEmailClaim($claim_status, $dataItem, ['note' => $note_status, 'is_send_mail' => isset($dataForm['IS_SEND_MAIL_ACTION']) ? $dataForm['IS_SEND_MAIL_ACTION'] : STATUS_INT_KHONG]);
            }

            //lấy lại dữ liệu list sau khi cập nhật thành công
            $dataHtml = $this->getSearchAjax($paramSearch);
            $dataHtml['loadPage'] = 3;//load lai page list
            return Response::json($dataHtml);
        } else {
            $arrAjax['success'] = 0;
            $arrAjax['loadPage'] = 0;//load lai page
            $arrAjax['message'] = isset($result['Data'][0][0]->RESULT) ? $result['Data'][0][0]->RESULT : 'Có lỗi cập nhật';
            return Response::json($arrAjax);
        }
    }

    private function _sendEmailClaim($claim_status = '', $dataItem = [], $dataOther = [])
    {
        if (empty($dataItem) || trim($claim_status) == '')
            return;
        $arrStatusSendMail = [STATUS_CLAIM_TMBT, STATUS_CLAIM_DYKH, STATUS_CLAIM_TCKH, STATUS_CLAIM_YCBS, STATUS_CLAIM_HTHS];
        if (in_array($claim_status, $arrStatusSendMail)) {
            $p_org_code = isset($dataItem->ORG_CODE) ? $dataItem->ORG_CODE : '';
            $p_product_code = isset($dataItem->PRODUCT_CODE) ? $dataItem->PRODUCT_CODE : '';
            $p_claim_code = isset($dataItem->CLAIM_CODE) ? $dataItem->CLAIM_CODE : '';
            $inforClaim = $this->_getDataClaim($p_claim_code, $p_product_code, $p_org_code);

            $dataClaim = $inforClaim['detailClaim'];
            $listDuocBoiThuong = $inforClaim['listDuocBoiThuong'];
            $listFileBoSung = $inforClaim['listFileBoSung'];

            //$url_search_eclaim = Config::get('config.URL_ECLAIM_' . Config::get('config.ENVIRONMENT'));
            $url_search_eclaim = isset($dataClaim->URL_CLAIM) ? $dataClaim->URL_CLAIM : '';

            //link tra cứu claim: LINK_CLAIM_DETAIL
            //HDOSP: https://cus-portal-uat.hdinsurance.com.vn/buying/claim/find?returnURL=https:%2F%2Fhomepage-uat.hdinsurance.com.vn%2F&buyNow=true
            //VIETJET: https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi
            //còn lại: https://www.hdinsurance.com.vn/#page-5
            $link_claim = 'https://www.hdinsurance.com.vn/#page-5';
            switch ($claim_status) {
                //[TMBT] => Chờ tiếp nhận
                //TMBT	    DATE_REQUIRED, CERTIFICATE_NO,INSURED_NAME,REQUIRED_AMOUNT, link tra cứu e-claim
                //TMBT	    LINK_CLAIM_DETAIL, DATE_REQUIRED, DECLARE_NAME, PRODUCT_NAME, CERTIFICATE_NO, INSURED_NAME - Giamdinhboithuong@hdinsurance.com.vn
                case STATUS_CLAIM_TMBT:
                    $dataSendMail = [
                        "DATE_REQUIRED" => isset($dataClaim->CLAIM_DATE) ? $dataClaim->CLAIM_DATE : '',
                        "CERTIFICATE_NO" => isset($dataClaim->CERTIFICATE_NO) ? $dataClaim->CERTIFICATE_NO : '',
                        "PRODUCT_NAME" => isset($dataClaim->PRODUCT_NAME) ? $dataClaim->PRODUCT_NAME : '',
                        "DECLARE_NAME" => isset($dataItem->NAME) ? $dataItem->NAME : '',// tên từ danh sách list truyền vào
                        "INSURED_NAME" => isset($dataItem->INSURED_NAME) ? $dataItem->INSURED_NAME : '',
                        "CLAIM_NO" => isset($dataItem->CLAIM_NO) ? $dataItem->CLAIM_NO : '',
                        "REQUIRED_AMOUNT" => isset($dataItem->REQUIRED_AMOUNT) ? numberFormat($dataItem->REQUIRED_AMOUNT) : '',
                        "LINK_CLAIM_DETAIL" => $link_claim,
                        "URL_CLAIM" => $url_search_eclaim,
                    ];
                    $emailSend = isset($dataItem->EMAIL) ? $dataItem->EMAIL : '';
                    $emailCC = isset($dataClaim->EMAIL) ? $dataClaim->EMAIL : '';
                    if (trim($emailSend) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => $claim_status,
                            "PRODUCT_CODE" => $claim_status,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $emailSend,//$emailSend,
                            "CC" => $emailCC,
                            "BCC" => CGlobal::mail_test];
                        $this->modelObj->sendMailClaim($dataSendMail);
                    }
                    //gửi cho giám định bồi thường
                    $dataSendMail['TEMP'][] = [
                        "TEMP_CODE" => $claim_status,
                        "PRODUCT_CODE" => $claim_status,
                        "ORG_CODE" => ORG_HDI,
                        "TYPE" => "EMAIL",
                        "TO" => 'giamdinhboithuong@hdinsurance.com.vn',
                        "CC" => '',
                        "BCC" => CGlobal::mail_test];
                    $this->modelObj->sendMailClaim($dataSendMail);
                    break;

                //[DYKH] => Đồng ý bồi thường - chờ KH xác nhận
                //DYKH	    DATE_REQUIRED, CERTIFICATE_NO,INSURED_NAME,REQUIRED_AMOUNT, REFUSED_AMOUNT, AMOUNT, REFUSED_CONTENT,
                // ACCOUNT_NAME, BANK_ACCOUNT, BANK_NAME, chi nhánh ngân hàng,  swift code của ngân hàng thụ hưởng,
                // danh sách chi tiết quyền lợi được bồi thường (STT, Tên quyền lợi, số tiền được bồi thường của quyền lợi), link tra cứu e-claim
                case STATUS_CLAIM_DYKH:

                    $soTienYeuCau = isset($dataItem->REQUIRED_AMOUNT) ? $dataItem->REQUIRED_AMOUNT : 0;
                    $tongBoiThuong = $soTienTuChoi = 0;
                    $inforSendMail = [];
                    if (!empty($listDuocBoiThuong)) {
                        foreach ($listDuocBoiThuong as $k => $boi_thuong) {
                            $tongBoiThuong = $tongBoiThuong + $boi_thuong->AMOUNT;
                            $inforSendMail[] = [
                                'NAME' => $boi_thuong->BEN_NAME,
                                'VALUE' => numberFormat($boi_thuong->AMOUNT) . MONEY_VND
                            ];
                        }
                    }
                    //$soTienTuChoi = ($tongBoiThuong < $soTienYeuCau) ? ($soTienYeuCau - $tongBoiThuong) : $soTienYeuCau;
                    $soTienTuChoi = ($tongBoiThuong < $soTienYeuCau) ? ($soTienYeuCau - $tongBoiThuong) : STATUS_INT_KHONG;
                    $dataSendMail = [
                        "DATE_REQUIRED" => isset($dataClaim->CLAIM_DATE) ? $dataClaim->CLAIM_DATE : '',
                        "CERTIFICATE_NO" => isset($dataClaim->CERTIFICATE_NO) ? $dataClaim->CERTIFICATE_NO : '',
                        "INSURED_NAME" => isset($dataItem->INSURED_NAME) ? $dataItem->INSURED_NAME : '',
                        "REQUIRED_AMOUNT" => isset($dataItem->REQUIRED_AMOUNT) ? numberFormat($dataItem->REQUIRED_AMOUNT) : '',
                        "URL_CLAIM" => $url_search_eclaim,
                        "AMOUNT" => numberFormat($tongBoiThuong),//số tiền bồi thường
                        "REFUSED_AMOUNT" => numberFormat($soTienTuChoi),//số tiền từ chối
                        "REFUSED_CONTENT" => ($soTienTuChoi == STATUS_INT_KHONG) ? '' : (isset($dataOther['note']) ? $dataOther['note'] : ''),// lý do từ chối
                        "ACCOUNT_NAME" => isset($dataItem->ACCOUNT_NAME) ? $dataItem->ACCOUNT_NAME : '',
                        "BANK_ACCOUNT" => isset($dataItem->ACCOUNT_NO) ? $dataItem->ACCOUNT_NO : '',
                        "BANK_NAME" => isset($dataItem->ACCOUNT_BANK) ? $dataItem->ACCOUNT_BANK : '',
                        "CLAIM_NO" => isset($dataItem->CLAIM_NO) ? $dataItem->CLAIM_NO : '',
                        "BRANCH_BANK" => isset($dataClaim->BRANCH_BANK) ? $dataClaim->BRANCH_BANK : '',//chi nhánh
                        "SWIFT_CODE" => isset($dataClaim->SWIFT_CODE) ? $dataClaim->SWIFT_CODE : '',//swift_code
                        "LINK_CLAIM_DETAIL" => $link_claim,
                        "DS_DK" => $inforSendMail
                    ];
                    $emailSend = isset($dataItem->EMAIL) ? $dataItem->EMAIL : '';
                    $emailCC = isset($dataClaim->EMAIL) ? $dataClaim->EMAIL : '';
                    if (trim($emailSend) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => $claim_status,
                            "PRODUCT_CODE" => $claim_status,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $emailSend,//$emailSend,
                            "CC" => $emailCC,
                            "BCC" => CGlobal::mail_test];
                        $this->modelObj->sendMailClaim($dataSendMail);
                    }
                    break;

                //[TCKH] => Từ chối bồi thường - Chờ KH xác nhận
                //TCKH	    DATE_REQUIRED, CERTIFICATE_NO,INSURED_NAME,REQUIRED_AMOUNT, REFUSED_AMOUNT, REFUSED_CONTENT, link tra cứu e-claim
                case STATUS_CLAIM_TCKH:
                    $soTienYeuCau = isset($dataItem->REQUIRED_AMOUNT) ? $dataItem->REQUIRED_AMOUNT : 0;
                    $tongBoiThuong = $soTienTuChoi = 0;
                    if (!empty($listDuocBoiThuong)) {
                        foreach ($listDuocBoiThuong as $k => $boi_thuong) {
                            $tongBoiThuong = $tongBoiThuong + $boi_thuong->AMOUNT;
                        }
                    }
                    $soTienTuChoi = ($tongBoiThuong < $soTienYeuCau) ? ($soTienYeuCau - $tongBoiThuong) : $soTienYeuCau;
                    $dataSendMail = [
                        "DATE_REQUIRED" => isset($dataClaim->CLAIM_DATE) ? $dataClaim->CLAIM_DATE : '',
                        "CERTIFICATE_NO" => isset($dataClaim->CERTIFICATE_NO) ? $dataClaim->CERTIFICATE_NO : '',
                        "INSURED_NAME" => isset($dataItem->INSURED_NAME) ? $dataItem->INSURED_NAME : '',
                        "CLAIM_NO" => isset($dataItem->CLAIM_NO) ? $dataItem->CLAIM_NO : '',
                        "REQUIRED_AMOUNT" => isset($dataItem->REQUIRED_AMOUNT) ? numberFormat($dataItem->REQUIRED_AMOUNT) : '',
                        "URL_CLAIM" => $url_search_eclaim,
                        "LINK_CLAIM_DETAIL" => $link_claim,
                        "REFUSED_AMOUNT" => numberFormat($soTienTuChoi),//số tiền từ chối
                        "REFUSED_CONTENT" => isset($dataOther['note']) ? $dataOther['note'] : '',// lý do từ chối
                    ];
                    $emailSend = isset($dataItem->EMAIL) ? $dataItem->EMAIL : '';
                    $emailCC = isset($dataClaim->EMAIL) ? $dataClaim->EMAIL : '';
                    if (trim($emailSend) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => $claim_status,
                            "PRODUCT_CODE" => $claim_status,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $emailSend,//$emailSend,
                            "CC" => $emailCC,
                            "BCC" => CGlobal::mail_test];
                        $this->modelObj->sendMailClaim($dataSendMail);
                    }
                    break;

                //yêu cầu bổ sung, Hoàn thành hồ sơ
                //YCBS	    DATE_REQUIRED, CERTIFICATE_NO, danh sách bổ sung (bao gồm:  STT, Loại hồ sơ, Yêu cầu, Ghi chú), link tra cứu e-claim
                //HTHS      DATE_REQUIRED, CERTIFICATE_NO, danh sách bổ sung (bao gồm:  STT, Loại hồ sơ, Yêu cầu, Ghi chú), link tra cứu e-claim
                case STATUS_CLAIM_YCBS:
                case STATUS_CLAIM_HTHS:
                    $inforFileBoSung = [];
                    if (!empty($listFileBoSung)) {
                        $stt = 1;
                        foreach ($listFileBoSung as $k => $file_bosung) {
                            $inforFileBoSung[] = [
                                'STT' => $stt++,//số thứ tụ
                                'FILE_NAME' => isset($file_bosung->FILE_NAME) ? $file_bosung->FILE_NAME : '',//yêu cầu
                                'FILE_TYPE' => (isset($file_bosung->TYPE_OF_PAPER) && $file_bosung->TYPE_OF_PAPER == 1) ? 'Bản cứng' : 'Bản mềm',//loại hồ sơ
                                'FILE_NOTE' => isset($file_bosung->NOTE) ? $file_bosung->NOTE : ''//Ghi chú
                            ];
                        }
                    }
                    $dataSendMail = [
                        "DATE_REQUIRED" => isset($dataClaim->CLAIM_DATE) ? $dataClaim->CLAIM_DATE : '',
                        "CERTIFICATE_NO" => isset($dataClaim->CERTIFICATE_NO) ? $dataClaim->CERTIFICATE_NO : '',
                        "DS_DL" => $inforFileBoSung,
                        "URL_CLAIM" => $url_search_eclaim,
                        "LINK_CLAIM_DETAIL" => $link_claim,
                        "CLAIM_NO" => isset($dataItem->CLAIM_NO) ? $dataItem->CLAIM_NO : '',
                    ];
                    $emailSend = isset($dataItem->EMAIL) ? $dataItem->EMAIL : '';
                    $emailCC = isset($dataClaim->EMAIL) ? $dataClaim->EMAIL : '';
                    if (trim($emailSend) != '') {
                        if (isset($dataOther['is_send_mail']) && $dataOther['is_send_mail'] == 1 && $claim_status == STATUS_CLAIM_YCBS) {
                            $dataSendMail['TEMP'][] = [
                                "TEMP_CODE" => $claim_status,
                                "PRODUCT_CODE" => $claim_status,
                                "ORG_CODE" => ORG_HDI,
                                "TYPE" => "EMAIL",
                                "TO" => $emailSend,//$emailSend,
                                "CC" => $emailCC,
                                "BCC" => CGlobal::mail_test];
                            $this->modelObj->sendMailClaim($dataSendMail);
                        } elseif ($claim_status == STATUS_CLAIM_HTHS) {
                            $dataSendMail['TEMP'][] = [
                                "TEMP_CODE" => $claim_status,
                                "PRODUCT_CODE" => $claim_status,
                                "ORG_CODE" => ORG_HDI,
                                "TYPE" => "EMAIL",
                                "TO" => $emailSend,//$emailSend,
                                "CC" => $emailCC,
                                "BCC" => CGlobal::mail_test];
                            $this->modelObj->sendMailClaim($dataSendMail);
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }

    public function ajaxChangeProcessOld()
    {
        $arrAjax = array('success' => 0, 'message' => 'Có lỗi khi thao tác');
        $dataRequest = $_POST;

        $dataForm = isset($dataRequest['dataForm']) ? $dataRequest['dataForm'] : [];
        //quyền lợi bồi thường add thêm
        $arrClaimDiff = [];
        foreach ($dataForm as $ki => $val_i) {
            $k_name = 'name_ben_code_' . $ki;
            if (isset($dataForm[$k_name]) && isset($dataForm[$ki]) && $dataForm[$ki] > 0) {
                $arrClaimDiff[] = ['BEN_CODE' => $ki, 'BEN_NAME' => $dataForm[$k_name], 'SORT' => 1, 'AMOUNT' => $dataForm[$ki]];
            }
        }

        $listBoiThuong = isset($dataForm['listBoiThuong']) ? json_decode($dataForm['listBoiThuong']) : [];
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        $dataItem = isset($dataForm['dataItem']) ? json_decode($dataForm['dataItem'], false) : false;
        $dataClaim = isset($dataForm['dataClaim']) ? json_decode($dataForm['dataClaim'], false) : false;

        $claim_status = isset($dataForm['CLAIM_STATUS']) ? $dataForm['CLAIM_STATUS'] : '';
        $note_status = isset($dataForm['NOTE_STATUS']) ? $dataForm['NOTE_STATUS'] : '';
        $pay_date = isset($dataForm['PAY_DATE']) ? $dataForm['PAY_DATE'] : '';

        /*if (trim($claim_status) == '' || trim($note_status) == '') {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không chính xác! Hãy nhập lại')));
        }*/
        if ($claim_status == $dataClaim->STATUS) {
            return Response::json(returnError(viewLanguage('Trạng thái trùng với trạng thái hiện tại!')));
        }
        if ($claim_status == STATUS_CLAIM_TTBT && $pay_date == '') {
            return Response::json(returnError(viewLanguage('Ngày thanh toán phải được chọn!')));
        }
        //list bồi thường
        $dataBoiThuong = [];
        $inforSendMail = [];
        $totalBoiThuong = 0;
        $arrStatusSendMail = [STATUS_CLAIM_DYCB, STATUS_CLAIM_TCBT, STATUS_CLAIM_TTBT, STATUS_CLAIM_TCKH, STATUS_CLAIM_DYKH];
        if (in_array($claim_status, $arrStatusSendMail)) {
            if (!empty($listBoiThuong)) {
                foreach ($listBoiThuong as $keybt => $boithuong) {
                    if (in_array($claim_status, [STATUS_CLAIM_TTBT])) {
                        $money = (isset($dataForm['money_' . $boithuong->BEN_CODE]) && trim($dataForm['money_' . $boithuong->BEN_CODE]) != '') ? returnFormatMoney($dataForm['money_' . $boithuong->BEN_CODE]) : 0;
                    } else {
                        $money = (isset($dataForm[$boithuong->BEN_CODE]) && trim($dataForm[$boithuong->BEN_CODE]) != '') ? returnFormatMoney($dataForm[$boithuong->BEN_CODE]) : 0;
                    }
                    $totalBoiThuong = $totalBoiThuong + $money;
                    if ($money > 0) {
                        $arrTem = ['SORT' => $boithuong->SORT,
                            'BEN_CODE' => $boithuong->BEN_CODE,
                            'BEN_NAME' => $boithuong->BEN_NAME,
                            'AMOUNT' => returnFormatMoney($money)
                        ];
                        $arrTemEmail = [
                            'NAME' => $boithuong->BEN_NAME,
                            'VALUE' => numberFormat($money) . MONEY_VND
                        ];
                        $inforSendMail[] = $arrTemEmail;
                        $dataBoiThuong[] = $arrTem;
                    }
                }
            }
        }
        //quyền lợi khác
        if (!empty($arrClaimDiff)) {
            foreach ($arrClaimDiff as $kkk => $val_clai) {
                $money = $val_clai['AMOUNT'];
                $totalBoiThuong = $totalBoiThuong + $money;
                if ($money > 0) {
                    $arrTem = ['SORT' => $val_clai['SORT'],
                        'BEN_CODE' => $val_clai['BEN_CODE'],
                        'BEN_NAME' => $val_clai['BEN_NAME'],
                        'AMOUNT' => returnFormatMoney($money)
                    ];
                    $arrTemEmail = [
                        'NAME' => $val_clai['BEN_NAME'],
                        'VALUE' => numberFormat($money) . MONEY_VND
                    ];
                    $inforSendMail[] = $arrTemEmail;
                    $dataBoiThuong[] = $arrTem;
                }
            }
        }

        //có file bổ sung với các trạng thái: Chờ bổ sung giấy tờ hoặc Chờ hoàn tất hồ sơ
        $dataFilesAdd = [];
        if (in_array($claim_status, [STATUS_CLAIM_YCBS, STATUS_CLAIM_HTHS])) {
            $type_files_add = isset($dataForm['TYPE_FILES_ADD']) ? $dataForm['TYPE_FILES_ADD'] : '';
            if (trim($type_files_add) == '') {
                return Response::json(returnError(viewLanguage('Dữ liệu chưa có file bổ sung! Hãy nhập lại')));
            }
            $arrFilesAdd = explode(',', $dataForm['strDataFileExit']);
            foreach ($arrFilesAdd as $k => $key_file_name) {
                $inforFiles = [
                    'FILE_KEY' => $key_file_name,
                    'FILE_NAME' => $dataForm['NAME_FILE_' . $key_file_name],
                    'NOTE' => $dataForm['NOTE_FILE_' . $key_file_name],
                    'TYPE_OF_PAPER' => $dataForm['TYPE_FILE_' . $key_file_name],
                    'FILE_ID' => '',
                ];
                $dataFilesAdd[$k] = $inforFiles;
            }
        }

        $dataApprovel['p_org_code'] = $dataClaim->ORG_CODE;
        $dataApprovel['p_prodcode'] = $dataClaim->PRODUCT_CODE;
        $dataApprovel['p_claim_code'] = $dataClaim->CLAIM_CODE;
        $dataApprovel['p_claim_status'] = $claim_status;
        $dataApprovel['p_staffcode'] = $this->user['user_id'];
        $dataApprovel['p_staffname'] = $this->user['user_full_name'];
        $dataApprovel['p_content'] = $note_status;
        $dataApprovel['p_pay_date'] = (in_array($claim_status, [STATUS_CLAIM_TCBT, STATUS_CLAIM_TTBT])) ? $pay_date : '';
        $dataApprovel['p_pay_claim'] = !empty($dataBoiThuong) ? $dataBoiThuong : '';//thông tin bồi thường
        $dataApprovel['p_file_attach'] = !empty($dataFilesAdd) ? $dataFilesAdd : '';//file bổ sung

        $result = $this->modelObj->updateChangeProcess($dataApprovel);
        if ($result['Success'] == STATUS_INT_MOT && isset($result['Data'][0][0]->TYPE) && $result['Data'][0][0]->TYPE == 'SUCCESS') {
            //Gửi mail cho KH nếu có
            //1:có gửi mail: 0: không gửi
            if ($dataForm['IS_SEND_MAIL_ACTION'] == 1 && in_array($claim_status, $arrStatusSendMail)) {
                /*
                 *
                 * Array
                    (
                        [TMBT] => Chờ tiếp nhận
                        [DYKH] => Đồng ý bồi thường - chờ KH xác nhận
                        [TCKH] => Từ chối bồi thường - Chờ KH xác nhận
                        [YCBS] => Chờ bổ sung giấy tờ

                        [XLHS] => Đang xử lý
                        [KHDY] => KH đồng ý phương án bồi thường
                        [KHTC] => KH không đồng ý phương án bồi thường
                        [TTBT] => Đã chi trả bồi thường
                        [TCBT] => Từ chối bồi thường
                        [DATN] => Đã tiếp nhận
                        [DABS] => Đã bổ sung giấy tờ
                        [DYCC] => Đồng ý bồi thường chờ LĐ Cty duyệt
                        [DYCB] => Đồng ý bồi thường chờ LĐ Ban duyệt
                        [TCCB] => Từ chối bồi thường chờ LĐ Ban duyệt
                        [TCCC] => Từ chối bồi thường chờ LĐ Cty Duyệt
                        [TCBB] => Lãnh đạo Ban từ chối bồi thường
                        [TCBC] => Lãnh đạo Cty từ chối bồi thường
                        [HTHS] => Chờ hoàn tất hồ sơ
                        [CTBT] => Chờ chi trả bồi thường
                    )
                 *
                 *  TMBT	    DATE_REQUIRED, CERTIFICATE_NO,INSURED_NAME,REQUIRED_AMOUNT, link tra cứu e-claim
                    TMBT	    DATE_REQUIRED, DECLARE_NAME, PRODUCT_NAME, CERTIFICATE_NO, INSURED_NAME - Giamdinhboithuong@hdinsurance.com.vn
                    DYKH	    DATE_REQUIRED, CERTIFICATE_NO,INSURED_NAME,REQUIRED_AMOUNT, REFUSED_AMOUNT, AMOUNT, REFUSED_CONTENT, ACCOUNT_NAME, BANK_ACCOUNT, BANK_NAME,  chi nhánh ngân hàng,  swift code của ngân hàng thụ hưởng, danh sách chi tiết quyền lợi được bồi thường (STT, Tên quyền lợi, số tiền được bồi thường của quyền lợi), link tra cứu e-claim
                    TCKH	    DATE_REQUIRED, CERTIFICATE_NO,INSURED_NAME,REQUIRED_AMOUNT, REFUSED_AMOUNT, REFUSED_CONTENT, link tra cứu e-claim
                    YCBS	    DATE_REQUIRED, CERTIFICATE_NO, danh sách bổ sung (bao gồm:  STT, Loại hồ sơ, Yêu cầu, Ghi chú), link tra cứu e-claim
                 * */
                $so_tien_tu_choi = (in_array($claim_status, [STATUS_CLAIM_TCBT, STATUS_CLAIM_TCKH])) ? $dataItem->REQUIRED_AMOUNT : abs($dataItem->REQUIRED_AMOUNT - $totalBoiThuong);
                $so_tien_tu_choi = ($so_tien_tu_choi > $dataItem->REQUIRED_AMOUNT) ? STATUS_INT_KHONG : $so_tien_tu_choi;
                $dataSendMail = [
                    "DATE" => $pay_date,
                    "CONTRACT_NO" => isset($dataItem->CONTRACT_NO) ? $dataItem->CONTRACT_NO : '',
                    "NAME" => isset($dataItem->NAME) ? $dataItem->NAME : '',
                    "INSURED_NAME" => isset($dataItem->INSURED_NAME) ? $dataItem->INSURED_NAME : '',
                    "REQUIRED_AMOUNT" => isset($dataItem->REQUIRED_AMOUNT) ? numberFormat($dataItem->REQUIRED_AMOUNT) : '',
                    "AMOUNT" => numberFormat($totalBoiThuong),
                    "REFUSED_AMOUNT" => ($so_tien_tu_choi >= 0) ? numberFormat($so_tien_tu_choi) : 0,//số tiền từ chối
                    "REFUSED_CONTENT" => $note_status,//lý do từ chối
                    "ACCOUNT_NAME" => isset($dataItem->ACCOUNT_NAME) ? $dataItem->ACCOUNT_NAME : '',
                    "BANK_ACCOUNT" => isset($dataItem->ACCOUNT_NO) ? $dataItem->ACCOUNT_NO : '',
                    "BANK_NAME" => isset($dataItem->ACCOUNT_BANK) ? $dataItem->ACCOUNT_BANK : '',
                    "DS_DK" => $inforSendMail
                ];
                //myDebug($dataSendMail);
                $emailSend = isset($dataItem->EMAIL) ? $dataItem->EMAIL : '';
                $emailCC = isset($dataClaim->EMAIL) ? $dataClaim->EMAIL : '';
                if (trim($emailSend) != '') {
                    $dataSendMail['TEMP'][] = [
                        "TEMP_CODE" => $claim_status,
                        "PRODUCT_CODE" => $claim_status,
                        "ORG_CODE" => ORG_VIETJET_VN,
                        "TYPE" => "EMAIL",
                        "TO" => $emailSend,//$emailSend,
                        "CC" => $emailCC,
                        "BCC" => CGlobal::mail_test];
                    $this->modelObj->sendMailClaim($dataSendMail);
                }
            }
            $arrAjax['success'] = 1;
            $arrAjax['loadPage'] = 1;//load lai page
            $arrAjax['message'] = 'Cập nhật thành công';
            return Response::json($arrAjax);
        } else {
            $arrAjax['success'] = 0;
            $arrAjax['loadPage'] = 0;//load lai page
            $arrAjax['message'] = isset($result['Data'][0][0]->RESULT) ? $result['Data'][0][0]->RESULT : 'Có lỗi cập nhật';
            return Response::json($arrAjax);
        }
    }

    /**************************************************
     * Get data Common từ form
     * ************************************************/
    public function ajaxGetData()
    {
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            if (is_array($html)) {
                return Response::json($html);
            } else {
                $success = STATUS_INT_MOT;
            }
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }

    private function _getListDocument($search)
    {
        $dataListDocument = $this->modelObj->getListDocument($search);
        $arrListDoc = [];
        if (isset($dataListDocument['Data'][0])) {
            foreach ($dataListDocument['Data'][0] as $k => $val) {
                $arrListDoc[$val->FILE_KEY] = $val->FILE_NAME;
            }
        }
        return $arrListDoc;
    }

    /**************************************************
     * Tạm thời đang ko dùng tới, ổn ổn thì xóa
     * Chi tiết hợp đồng thanh toán
     * ************************************************/
    public function ajaxGetItem()
    {
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    private function _getInfoItem($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        //myDebug($request);
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];
        $dataItem = isset($dataInput['item']) ? $dataInput['item'] : [];
        $detailClaim = $inforDetailExten = $desRequestClaim = $listFileAttack = $listTimeLine = $listDuocBoiThuong = [];
        if (!empty($arrKey)) {
            $p_org_code = isset($arrKey['ORG_CODE']) ? $arrKey['ORG_CODE'] : '';
            $p_product_code = isset($arrKey['PRODUCT_CODE']) ? $arrKey['PRODUCT_CODE'] : '';
            $p_claim_code = isset($arrKey['CLAIM_CODE']) ? $arrKey['CLAIM_CODE'] : '';

            $inforClaim = $this->_getDataClaim($p_claim_code, $p_product_code, $p_org_code);
            $detailClaim = $inforClaim['detailClaim'];
            $inforDetailExten = $inforClaim['inforDetailExten'];
            $desRequestClaim = $inforClaim['desRequestClaim'];
            $listFileAttack = $inforClaim['listFileAttack'];
            $listTimeLine = $inforClaim['listTimeLine'];
            $listDuocBoiThuong = $inforClaim['listDuocBoiThuong'];
        }

        $templateDetailItem = isset($dataInput['templateDetailItem']) ? $dataInput['templateDetailItem'] : '';
        switch ($templateDetailItem) {
            case 'ClaimAccountant':
                $templateDetail = $this->templateAccountant . 'componentAccountant.popupDetail';
                break;
            case 'ClaimDepart':
                $templateDetail = $this->templateApprove . 'componentDepart.popupDetail';
                break;
            case 'ClaimCompany':
                $templateDetail = $this->templateApprove . 'componentCompany.popupDetail';
                break;
            default:
                $templateDetail = $this->templateRoot . 'component.popupDetail';
                break;
        }

        $this->_outDataView($request, (array)$detailClaim);
        $html = View::make($templateDetail)
            ->with(array_merge([
                'data' => $detailClaim,
                'dataItem' => $dataItem,
                'inforDetailExten' => $inforDetailExten,
                'desRequestClaim' => $desRequestClaim,
                'listFileAttack' => $listFileAttack,
                'listTimeLine' => $listTimeLine,
                'listDuocBoiThuong' => $listDuocBoiThuong,
                'arrKeyDetail' => $arrKey,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }
}
