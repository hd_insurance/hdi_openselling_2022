<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Sellings\DigitalSignatureRequest;

use App\Http\Controllers\BaseAdminController;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Models\Selling\DigitalSignatureRequest;
use App\Services\ActionExcel;
use App\Services\ServiceCommon;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class DigitalSignatureRequestController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrProduct = array();
    private $templateRoot = DIR_PRO_SELLING . '/' . DIR_MODULE_DIGITAL_SIGNATURE_REQUEST . '/';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $max_file_size = 10000000;//10.000.000
    private $routerIndex = 'signatureRequest.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new DigitalSignatureRequest();
        $this->arrStatus = DigitalSignatureRequest::$arrStatus;
    }

    private function _outDataView($request, $data)
    {
        $this->arrProduct = $this->getInforUser('product');
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['p_status']) ? $data['p_status'] : '');
        $optionOrg = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrgUser, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionProduct = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProductUser, (isset($data['p_product_code']) && trim($data['p_product_code']) != '') ? $data['p_product_code'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionOrg' => $optionOrg,
            'optionProduct' => $optionProduct,
            'org_code_user' => $this->user['org_code'],
            'arrStatus' => $this->arrStatus,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,

            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route($this->routerIndex),
            'urlAjaxGetAction' => URL::route('signatureRequest.ajaxGetAction'),
            'urlAjaxPostAction' => URL::route('signatureRequest.ajaxPostAction'),
            'urlServiceFile' => Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/',
            'functionAction' => '_ajaxGetItemOther',
        ];
    }

    /*********************************************************************************************************
     * Danh sách đồng bộ
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Danh sách yêu cầu cập nhật đơn bảo hiểm';
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Cấp đơn ' . CGlobal::$arrTitleProject[$this->tab_top];

        $page_no = (int)Request::get('page_no', 1);
        $search['p_from_date'] = trim(addslashes(Request::get('p_from_date', '')));
        $search['p_to_date'] = trim(addslashes(Request::get('p_to_date', '')));

        $search['p_org_code'] = trim(addslashes(Request::get('p_org_code', '')));
        $search['p_product_code'] = trim(addslashes(Request::get('p_product_code', '')));
        //$search['p_product_code'] = (trim($search['p_product_code']) != '')?$search['p_product_code']: PRODUCT_CODE_XCG_TNDSBB_NEW;

        $search['p_keyword'] = trim(addslashes(Request::get('p_keyword', '')));
        $search['p_status'] = trim(addslashes(Request::get('p_status', '')));
        $search['p_from_date'] = ($search['p_from_date'] != '') ? $search['p_from_date'] : date('d/m/Y', strtotime(Carbon::now()->startOfMonth()));
        $search['p_to_date'] = ($search['p_to_date'] != '') ? $search['p_to_date'] : date('d/m/Y', strtotime(Carbon::tomorrow()));
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchList($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon, $this->dataOutItem));
    }

    public function ajaxGetAction()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW, PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $result = [];
        $success = STATUS_INT_KHONG;
        $message = 'Có lỗi thao tác';
        $arrAjax = ['success' => $success, 'html' => $result, 'message' => $message];
        if (trim($functionAction) != '') {
            $result = $this->$functionAction($dataRequest);
            if (is_array($result)) {
                $arrAjax = array_merge($arrAjax, $result);
            }
        }
        return Response::json($arrAjax);
    }

    private function _funcViewImage($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $this->dataOutItem = [
            'nameImage' => $dataInput['nameImage'] ?? '',
            'fileExtension' => $dataInput['fileExtension'] ?? '',
            'urlImage' => $dataInput['urlImage'] ?? '',
        ];

        $this->_outDataView($request, (array)[]);
        $html = View::make($this->templateRoot . 'component._popupShowImage')
            ->with(array_merge([
                'objectId' => 0,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $arrAjax = array('success' => 1, 'loadPage' => 0, 'viewHtml' => 2, 'html' => $html, 'divShowInfor' => '');
        return $arrAjax;
    }

    /**
     * @param $request
     * viewHtml: 2: div right show, 1: popup big, 0: popup small
     * @return array
     */
    private function _funcGetDetailItem($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
        $divShowInfor = isset($request['divShow']) ? json_decode($request['divShow']) : 'content-page-right';

        $dataItem = isset($dataInput->dataItem) ? $dataInput->dataItem : false;
        $arrInforChange = (isset($dataItem->DATA_CHANGE) && !empty($dataItem->DATA_CHANGE)) ? json_decode($dataItem->DATA_CHANGE, true) : [];
        $arrFileAttach = (isset($dataItem->FILE_ATTACH) && !empty($dataItem->FILE_ATTACH)) ? json_decode($dataItem->FILE_ATTACH) : [];
        $arrChangeKeys = isset($dataItem->CHANGE_KEYS) ? explode(';', $dataItem->CHANGE_KEYS) : [];

        $dataChange = [];
        $arrKey = DigitalSignatureRequest::$arrKeyRequest;
        if (!empty($arrInforChange)) {
            foreach ($arrInforChange as $k_change => &$val_change) {
                if (in_array($k_change, array_keys($arrKey))) {
                    $val_change['NAME_ALIAS'] = $arrKey[$k_change];
                } else {
                    unset($arrInforChange[$k_change]);//xóa key ko đc phép show ra
                }
            }
        }
        unset($this->arrStatus[STATUS_INT_HAI]);
        unset($this->arrStatus[STATUS_INT_BON]);
        $optionStatusEdit = FunctionLib::getOption($this->arrStatus, isset($dataItem->STATUS) ? $dataItem->STATUS : '');
        $this->dataOutItem = [
            'dataItem' => $dataItem,
            'arrKey' => $arrKey,
            'dataChange' => $dataChange,
            'arrChangeKeys' => $arrChangeKeys,
            'arrInforChange' => $arrInforChange,
            'arrFileAttach' => $arrFileAttach,
            'optionStatusEdit' => $optionStatusEdit,
        ];

        $this->_outDataView($request, (array)$dataItem);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'objectId' => 0,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $arrAjax = array('success' => 1, 'loadPage' => 0, 'viewHtml' => 2, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    private function _funcUpdateRequestChange($request)
    {
        $dataInput = $request;
        $arrAjax = array('success' => 0, 'message' => 'Lỗi chưa cập nhật được dữ liệu');

        $dataRequest = isset($dataInput['dataRequest']) ? json_decode($dataInput['dataRequest']) : false;
        $dataKeyChange = isset($dataInput['dataKeyChange']) ? $dataInput['dataKeyChange'] : [];
        $status = isset($dataInput['status']) ? $dataInput['status'] : STATUS_INT_KHONG;
        $note_reply = isset($dataInput['note_reply']) ? $dataInput['note_reply'] : '';

        $updateRequest = [];
        $request_code = isset($dataRequest->REQUEST_CODE) ? $dataRequest->REQUEST_CODE : '';
        $certificate_no = isset($dataRequest->CERTIFICATE_NO) ? $dataRequest->CERTIFICATE_NO : '';
        $product_code = isset($dataRequest->PRODUCT_CODE) ? $dataRequest->PRODUCT_CODE : '';
        $dataChange = isset($dataRequest->DATA_CHANGE) ? json_decode($dataRequest->DATA_CHANGE, true) : [];
        $changeDetail = isset($dataRequest->CHANGE_DETAIL) ? json_decode($dataRequest->CHANGE_DETAIL, true) : [];
        $changeExtend = isset($dataRequest->CHANGE_EXTEND) ? json_decode($dataRequest->CHANGE_EXTEND, true) : [];
        $urlServiceFile = Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/';


        if ($status == STATUS_INT_BA) {//Từ chối xử lý
            //bien hien thị email
            $email_to = isset($changeDetail['EMAIL']) ? $changeDetail['EMAIL'] : '';
            $name_customer = isset($changeDetail['NAME']) ? $changeDetail['NAME'] : '';
            $file_code = isset($dataRequest->FILE_CODE) ? $dataRequest->FILE_CODE : '';
            $urlFile = (trim($file_code) != '') ? $urlServiceFile . $file_code : '';
            $email_cc = (isset($dataRequest->CONTACT_EMAIL) && trim($dataRequest->CONTACT_EMAIL) != '') ? trim($dataRequest->CONTACT_EMAIL) : '';

            $arrEmail[$email_to] = [
                'CERTIFICATE_NO' => $certificate_no,
                'NAME_CUSTOMER' => $name_customer,
                'URL_FILE' => $urlFile,
                'EMAIL_CC' => $email_cc,
                'PRODUCT_CODE' => $product_code,
                'REQUEST_CODE' => $request_code,
            ];

            $updateRequest['PRODUCT_CODE'] = $product_code;
            $updateRequest['REQUEST_CODE'] = isset($dataRequest->REQUEST_CODE) ? $dataRequest->REQUEST_CODE : '';
            $updateRequest['FILE_ATTACH'] = isset($dataRequest->FILE_ATTACH) ? $dataRequest->FILE_ATTACH : '';
            $updateRequest['CHANGE_DETAIL'] = json_encode($changeDetail);
            $updateRequest['CHANGE_EXTEND'] = json_encode($changeExtend);
            $updateRequest['CHANGE_KEYS'] = isset($dataRequest->CHANGE_KEYS) ? $dataRequest->CHANGE_KEYS : '';
            $updateRequest['USER_ACTION'] = $this->user_name;
            $updateRequest['IS_RUN'] = STATUS_INT_MOT;//làm thủ công
            $updateRequest['STATUS'] = $status;
            $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_KHONG;//Chưa gửi email
            $updateRequest['NOTE_REPLY'] = $note_reply;
            $arrUpdateRequest[$email_to][$request_code] = $updateRequest;

            //gửi mai theo nhóm yêu cầu ký số
            if (!empty($arrEmail) && !empty($arrUpdateRequest)) {
                foreach ($arrEmail as $email_to => $dataEmail) {
                    //NAME_EN;NAME;CERTIFICATE_NO;URL_FILE;REASON_CANCEL
                    $dataSendMail = [
                        "NAME_EN" => $dataEmail['NAME_CUSTOMER'],
                        "NAME" => $dataEmail['NAME_CUSTOMER'],
                        "CERTIFICATE_NO" => $dataEmail['CERTIFICATE_NO'],
                        "URL_FILE" => $dataEmail['URL_FILE'],
                        "REASON_CANCEL" => $note_reply,
                    ];
                    if (trim($email_to) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => DigitalSignatureRequest::STATUS_EMAIL_TCYC_SDBS,
                            "PRODUCT_CODE" => DigitalSignatureRequest::STATUS_EMAIL_TCYC_SDBS,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $email_to,//$emailSend,
                            "CC" => $dataEmail['EMAIL_CC'],
                            "BCC" => CGlobal::mail_test];
                        $sendEmail = app(ServiceCommon::class)->sendMailWithTemplate($dataSendMail);
                        if (isset($sendEmail->Success) && $sendEmail->Success == 1) {
                            foreach ($arrUpdateRequest[$email_to] as $request_code => $updateInforRequest) {
                                $updateInforRequest['IS_SEND_EMAIL'] = STATUS_INT_MOT;
                                $this->modelObj->editRequest($updateInforRequest, 'EDIT_REQUEST');
                            }
                        }
                    }
                }
            }
            $arrAjax['success'] = STATUS_INT_MOT;
            $arrAjax['message'] = 'Bạn đã cập nhật thành công';
        } //đồng ý ký số
        elseif (!empty($dataKeyChange) && $status == STATUS_INT_MOT) {
            //lấy key thay đổi giá trị
            $temChange = $changeKeys = [];
            foreach ($dataKeyChange as $ky => $va_key) {
                if (isset($dataChange[$va_key])) {
                    $temChange[$va_key] = $dataChange[$va_key];
                    $changeKeys[] = $va_key;
                }
            }

            //đẩy dữ liệu thay đổi vào
            if (!empty($temChange)) {
                $fieldChange = DigitalSignatureRequest::$arrKeyMergerDB;
                foreach ($temChange as $kys_field => $infor_chage) {
                    if (isset($fieldChange[$kys_field])) {
                        $field_db = $fieldChange[$kys_field];
                        //if (isset($changeDetail[$field_db])) {
                        $changeDetail[$field_db] = $infor_chage['VALUE_NEW'];
                        //}
                        //if (isset($changeExtend[$field_db])) {
                        $changeExtend[$field_db] = $infor_chage['VALUE_NEW'];
                        //}
                    }
                }
            }
            //push data update Request
            if (!empty($changeKeys)) {
                $updateRequest['PRODUCT_CODE'] = $product_code;
                $updateRequest['REQUEST_CODE'] = $request_code;
                $updateRequest['FILE_ATTACH'] = isset($dataRequest->FILE_ATTACH) ? $dataRequest->FILE_ATTACH : '';
                $updateRequest['NOTE_REPLY'] = $note_reply;
                $updateRequest['STATUS'] = $status;//1:đã xử lý
                $updateRequest['IS_RUN'] = STATUS_INT_MOT;//làm thủ công
                $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_KHONG;//chưa gửi
                $updateRequest['CHANGE_DETAIL'] = json_encode($changeDetail);
                $updateRequest['CHANGE_EXTEND'] = json_encode($changeExtend);
                $updateRequest['CHANGE_KEYS'] = join(';', $changeKeys);
                $updateRequest['USER_ACTION'] = $this->user_name;

                $result = $this->modelObj->editRequest($updateRequest);//data ký thỏa mãn
                if ($result['Success'] == STATUS_INT_MOT) {

                    $dataResign['CERTIFICATE_NO'] = $certificate_no;
                    $dataResign['PRODUCT_CODE'] = $product_code;
                    $service = new ServiceCommon();
                    $resign = $service->resignCertificate($dataResign);

                    //bien hien thị email
                    $email_to = isset($changeDetail['EMAIL']) ? $changeDetail['EMAIL'] : '';
                    $name_customer = isset($changeDetail['NAME']) ? $changeDetail['NAME'] : '';
                    $file_code = isset($dataRequest->FILE_CODE) ? $dataRequest->FILE_CODE : '';
                    $file_code_new = (isset($resign['file_signed']) && trim($resign['file_signed']) != '') ? trim($resign['file_signed']) : $file_code;
                    $urlFile = (trim($file_code_new) != '') ? $urlServiceFile . $file_code_new : '';
                    $email_cc = (isset($dataRequest->CONTACT_EMAIL) && trim($dataRequest->CONTACT_EMAIL) != '') ? trim($dataRequest->CONTACT_EMAIL) : '';

                    $dataSendMail = [
                        "NAME_EN" => $name_customer,
                        "NAME" => $name_customer,
                        "CERTIFICATE_NO" => $certificate_no,
                        "URL_FILE" => $urlFile,
                    ];
                    if (trim($email_to) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => DigitalSignatureRequest::STATUS_EMAIL_DYYC_SDBS,
                            "PRODUCT_CODE" => DigitalSignatureRequest::STATUS_EMAIL_DYYC_SDBS,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $email_to,//$emailSend,
                            "CC" => $email_cc,
                            "BCC" => CGlobal::mail_test];
                        $sendEmail = app(ServiceCommon::class)->sendMailWithTemplate($dataSendMail);

                        if (isset($resign['file_signed']) && trim($resign['file_signed']) == '') {
                            $updateRequest['STATUS'] = STATUS_INT_BON;//xử lý lỗi
                            $updateRequest['CHANGE_KEYS'] = '';
                        }
                        if (isset($sendEmail->Success) && $sendEmail->Success == 1) {
                            $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_MOT;
                        }
                        $this->modelObj->editRequest($updateRequest, 'EDIT_REQUEST');
                    }

                    if ($resign['success'] == STATUS_INT_MOT) {
                        $arrAjax['success'] = STATUS_INT_MOT;
                        $arrAjax['message'] = 'Bạn đã cập nhật thành công';
                    } else {
                        $arrAjax = $resign;
                    }
                }
            }
        } else {
            $arrAjax['message'] = 'Bạn chưa chọn thông tin cập nhập';
        }
        return $arrAjax;
    }

    public function _funcPushDataTest($request)
    {
        $arrInfor = [
            'customer_name' => 'Chu Van An bug222',
            'customer_address' => 'Dia chi moi',
            'vehicle_number_plate' => 'BKSSDBS01',//biển kiểm soát
            'vehicle_chassis_no' => 'SKSDBS01',//số khung
            'vehicle_engine_no' => 'SMSDBS01',//số máy
            'vehicle_mfg' => '2022',//năm sản xuất
            'customer_phone' => '',
            'customer_email' => '',
        ];
        $arrFile = [
            0 => ['file_name' => 'CMT mat truoc', 'file_base64' => 'https://scontent.fsgn5-11.fna.fbcdn.net/v/t39.30808-6/305477988_3300543313548301_2326428246413051590_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=ra3f4ctnDyAAX8M0DtD&tn=w3YWoHhgB0Ts9F_Q&_nc_ht=scontent.fsgn5-11.fna&oh=00_AT-DCpmCGRbtV_grAdAz0N2ePcW84iIuQ3p-pzIRCeHWRQ&oe=631C9F34', 'file_extension' => 'png'],
            1 => ['file_name' => 'CMT mat sau', 'file_base64' => '', 'file_extension' => 'png']
        ];
        $dataRequest['p_certificate_code'] = 'S22V01FWQVI4';
        $dataRequest['p_note'] = 'KH nay nhap sai thong tin can ky lai';
        $dataRequest['p_org_code'] = 'FPT';
        $dataRequest['p_contact_email'] = '';//đối tác
        $dataRequest['p_contact_phone'] = '';
        $dataRequest['p_data_request'] = $arrInfor;
        $dataRequest['p_file_attach'] = $arrFile;
        $result = $this->modelObj->pushRequest($dataRequest);
    }

}
