<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2018
* @Version   : 1.0
*/
namespace App\Http\Controllers;

use App\Models\System\DepartmentOrg;
use App\Models\System\MenuSystem;
use App\Models\System\Province;
use App\Models\Selling\Campaigns;
use App\Library\AdminFunction\FunctionLib;
use Illuminate\Support\Facades\Response;

class AjaxCommonController extends BaseAdminController
{
    public function getOptionCommon(){
        $dataRequest = $_POST;
        $object = $dataRequest['object'] ?? '';
        $type = $dataRequest['type'] ?? '';
        $optionOut = '';
        $success = STATUS_INT_KHONG;
        if(trim($object) != '' && trim($type) != ''){
            switch ($type){
                case 'DEPART':
                    $data = app(DepartmentOrg::class)->getArrOptionDepartByOrgCode($object);
                    $optionOut = FunctionLib::getOption(['' => '---Chọn---'] + $data, '');
                    $success = STATUS_INT_MOT;
                    break;
                case 'ORG_BY_CAMPAIGN_CODE':
                    $data = app(Campaigns::class)->getArrOptionOrgByCampaignCode($object);
                    $optionOut = FunctionLib::getOption(['' => '---Chọn---'] + $data, '');
                    $success = STATUS_INT_MOT;
                    break;
                case 'OPTION_MENU_PARENT':
                    $data = app(MenuSystem::class)->getOptionMenuParent($object);
                    $optionOut = FunctionLib::getOption(['' => '---Chọn---'] + $data, '');
                    $success = STATUS_INT_MOT;
                    break;
                case 'OPTION_DISTRICT_CODE':
                    $data = app(Province::class)->getOptionDistrict($object);
                    $optionOut = FunctionLib::getOption(['' => '---Chọn---'] + $data, '');
                    $success = STATUS_INT_MOT;
                    break;
                case 'OPTION_WARD_CODE':
                    $data = app(Province::class)->getOptionWard($object);
                    $optionOut = FunctionLib::getOption(['' => '---Chọn---'] + $data, '');
                    $success = STATUS_INT_MOT;
                    break;
                case 'CHANGE_STATUS_CHOOSE_DATE_BUY':
                    $arrStatus = $this->getArrOptionTypeDefine(DEFINE_ORDER_SKU_STATUS);
                    switch ($object){
                        case 'BUYINGDATE'://ngày mua
                            break;
                        case 'EFFECTIVEDATE'://ngày hiệu lực
                            if($arrStatus['CANCEL_HDI']){
                                unset($arrStatus['CANCEL_HDI']);
                            }
                            if($arrStatus['CANCEL']){
                                unset($arrStatus['CANCEL']);
                            }
                            if($arrStatus['NONE_INFO']){
                                unset($arrStatus['NONE_INFO']);
                            }
                            break;
                        case 'EXPIRATIONDATE'://ngày hết hiệu lực
                            if($arrStatus['ADDED_INFO']){
                                unset($arrStatus['ADDED_INFO']);
                            }
                            if($arrStatus['NONE_INFO']){
                                unset($arrStatus['NONE_INFO']);
                            }
                            break;
                        default:
                            break;
                    }
                    $optionOut = FunctionLib::getOption(['' => '---Chọn---'] + $arrStatus, '');
                    $success = STATUS_INT_MOT;
                    break;
                default;
                    break;
            }
        }
        $arrAjax = array('success' => $success, 'optionOut' => $optionOut);
        return Response::json($arrAjax);
    }

    public function ajaxGetData(){
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if(trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            $success = STATUS_INT_MOT;
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }
}
