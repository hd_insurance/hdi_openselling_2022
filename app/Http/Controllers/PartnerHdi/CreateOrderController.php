<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\PartnerHdi;

use App\Http\Controllers\BasePartnerController;
use App\Models\BContracts\Products;
use App\Models\Partner\PartnerModel;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class CreateOrderController extends BasePartnerController
{
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();

    private $templateRoot = DIR_PRO_PARTNER_HDI . '/' . '.CreateOrder.';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new PartnerModel();
    }

    private function _outDataView($request, $data)
    {
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = $request['objectId'] ?? 0;
        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'arrStatus' => $this->arrStatus,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'urlAjaxGetData' => '',
        ];
    }

    /*********************************************************************************************************
     * Page lỗi
     *********************************************************************************************************/
    public function pagePartnerError(){
        $param = $_GET;
        $this->pageTitle = CGlobal::$pageAdminTitle = ' Không tồn tại trang này';
        return view(DIR_PRO_PARTNER_HDI. '.pageError', [
            'pageTitle' => $this->pageTitle,
            'msg' => isset($param['msg']) ? $param['msg'] : '',
        ]);
    }
    /*********************************************************************************************************
     * Cấp đơn cho đối tác: partner/indexCreaterOrder?pt=21B44B42E5D672C5583BB57B89246987
     *********************************************************************************************************/
    public function indexCreaterOrder()
    {
        $partner = $this->checkValidatePartner();
        if (empty($partner)) {
            return Redirect::route('partner.pagePartnerError', array('msg' => 'User đã hết hạn đăng nhập'));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Cấp đơn bảo hiểm HDI';
        $this->_outDataView($_GET, []);
        return view($this->templateRoot . 'view', array_merge([
            'pageTitle' => $this->pageTitle,
            'category' => isset($partner->category) ? $partner->category : '',
            'tilte' => isset($partner->tilte) ? $partner->tilte : '',
            'action' => isset($partner->action) ? $partner->action : '',
            'contract_code' => isset($partner->contract_code) ? $partner->contract_code : '',
            'detail_code' => isset($partner->detail_code) ? $partner->detail_code : '',
            'product_code' => isset($partner->product_code) ? $partner->product_code : '',
            'channel' => isset($partner->channel) ? $partner->channel : '',
            'org_code' => isset($partner->org_code) ? $partner->org_code : '',
            'user_name' => isset($partner->user_name) ? $partner->user_name : '',
            'user_permission' => isset($partner->user_permission) ? json_encode($partner->user_permission) : json_encode(['APPROVE'=>0,'CREATE_ORDER'=>1,'INSPECTION'=>0]),
            'partner' => isset($partner->partner) ? $partner->partner : '',
        ], $this->dataOutCommon));
    }

    public function checkValidatePartner()
    {
        $param = $_GET;
        $token = isset($param['pt']) ? $param['pt'] : '';
        $data = (trim($token) != '') ? $this->modelObj->checkTokenPartnerCreatOrder(['token' => $token]) : [];
        return $data;
    }
}
