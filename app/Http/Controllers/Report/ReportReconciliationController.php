<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Report;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\Organization;
use App\Models\System\PackageCore;
use App\Models\Report\ReportProduct;

use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Services\ActionExcel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class ReportReconciliationController extends BaseAdminController
{
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrOrgAll = array();
    private $arrOrg = array();
    private $arrProduct = array();
    private $arrHours = array();
    private $arrMinute = array();
    private $arrMonth = array();
    private $arrYear = array();
    private $arrPackCode = array();
    private $arrPackCode2 = array();
    private $arrTypeDateSkyCare = array('RECEIVED_DATE'=>'Ngày nhận dữ liệu','ETD1'=>'Thời gian khởi hành dự kiến');

    private $templateRoot = DIR_PRO_REPORT . '/';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ReportProduct();
    }

    public function _getDataUse()
    {
        $this->arrHours = CGlobal::$arrHours;
        $this->arrMinute = CGlobal::$arrMinute;

        $this->arrMonth = CGlobal::$arrMonth;
        $this->arrYear = getArrYear();
        $this->arrOrgAll = app(Organization::class)->getArrOptionOrg();

        $this->arrPackCode = app(PackageCore::class)->getOptionPackage(PRODUCT_CODE_SKY_CARE);
        $this->arrPackCode2 = app(PackageCore::class)->getOptionPackage(PRODUCT_CODE_SKY_CARE_DOMESTIC);
    }

    private function _outDataView($request, $data)
    {
        $this->_getDataUse();
        //myDebug($request,false);
        //myDebug($data);
        $this->arrProduct = $this->getInforUser('product');
        $this->arrSeller = $this->getInforUser('org');
        $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrProduct, isset($data['p_product_code']) ? $data['p_product_code'] : '');
        $optionSeller = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrSeller, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionMonth = FunctionLib::getOption(['' => 'Tháng'] + $this->arrMonth, isset($data['p_month']) ? $data['p_month'] : '');
        $optionYear = FunctionLib::getOption(['' => 'Năm'] + $this->arrYear, isset($data['p_year']) ? $data['p_year'] : '');
        $optionTypeDateSkyCare = FunctionLib::getOption($this->arrTypeDateSkyCare, isset($data['p_type_date_sky_care']) ? $data['p_type_date_sky_care'] : 'RD');

        $pack_default = !empty($this->arrPackCode) ? implode(';', array_keys($this->arrPackCode)) : '';
        $optionPackCode = FunctionLib::getOptionMultil($this->arrPackCode, (isset($data['p_pack_code_SKY_CARE']) && trim($data['p_pack_code_SKY_CARE'])  != '') ? explode(';', $data['p_pack_code_SKY_CARE']) : explode(';', $pack_default));
        $pack_default2 = !empty($this->arrPackCode2) ? implode(';', array_keys($this->arrPackCode2)) : '';
        $optionPackCode2 = FunctionLib::getOptionMultil($this->arrPackCode2, (isset($data['p_pack_code_SKY_CARE_DOMESTIC']) && trim($data['p_pack_code_SKY_CARE_DOMESTIC'])  != '') ? explode(';', $data['p_pack_code_SKY_CARE_DOMESTIC']) : explode(';', $pack_default2));

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionMonth' => $optionMonth,
            'optionYear' => $optionYear,
            'optionProduct' => $optionProduct,
            'optionSeller' => $optionSeller,
            'optionTypeDateSkyCare' => $optionTypeDateSkyCare,

            'optionPackCode' => $optionPackCode,
            'optionPackCode2' => $optionPackCode2,
            'arrPackCode' => $this->arrPackCode,
            'arrPackCode2' => $this->arrPackCode2,

            'arrStatus' => $this->arrStatus,
            'arrOrg' => $this->arrOrg,
            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlGetItem' => '',
            'urlPostItem' => '',
            'urlAjaxGetData' => '',
            'functionAction' => '_ajaxGetItemOther',
        ];
    }

    /********************************************************************************************
     * Đối soát dữ liệu
     ********************************************************************************************/
    public function indexDataReconciliation()
    {
        return $this->_getDataReconciliation();
    }

    //bay an toàn
    public function indexReconciliationFlySafe()
    {
        return $this->_getDataReconciliation(PRODUCT_CODE_BAY_AT);
    }

    //Đối soát Sky Care
    ///manager/report/indexReconciliationSkyCare
    public function indexReconciliationSkyCare()
    {
        $product_code = trim(Request::get('p_product_code', PRODUCT_CODE_SKY_CARE));
        $this->_getDataUse();
        return $this->_getDataReconciliation($product_code);
    }

    /************************************************************************************************************************************************/
    private function _getDataReconciliation($product_code = '')
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }

        CGlobal::$pageAdminTitle = $this->pageTitle . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 1);
        $search = $this->_buildParamSearchReconciliation($product_code);

        //myDebug($search);
        $dataList = $dataTotal = [];
        $total = 0;
        $totalMoney = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchReportDataReconciliation($search);
        $this->_outDataView($_POST, $search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $total = $result['Data'][0][0]->TOTAL_ITEM ?? $total;
            $dataTotal = $result['Data'][1][0] ?? $dataTotal;
        }

        $pro_code = $search['p_product_code'];
        $list_template = 'listDataReconciliation';
        $arrPackCodeExcel = $this->arrPackCode;
        switch ($pro_code) {
            case PRODUCT_CODE_BAY_AT:
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo đối soát bay an toàn';
                $type_export = ActionExcel::EXPORT_RECONCILIATION_BAY_AT;
                $table_view = '_table_BAY_AT';
                break;
            case PRODUCT_CODE_SKY_CARE:
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo đối soát Sky Care';
                $type_export = ActionExcel::EXPORT_RECONCILIATION_SKY_CARE;
                $arrPackCodeExcel = $this->arrPackCode;
                $table_view = '_table_SKY_CARE';
                $list_template = 'listDataSkyCare';
                break;
            case PRODUCT_CODE_SKY_CARE_DOMESTIC:
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo đối soát Sky Care Nội địa';
                $type_export = ActionExcel::EXPORT_RECONCILIATION_SKY_CARE_DOMESTIC;
                $arrPackCodeExcel = $this->arrPackCode2;
                $table_view = '_table_SKY_CARE';
                $list_template = 'listDataSkyCare';
                break;
            default:
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo đối soát bay an toàn';
                $type_export = ActionExcel::EXPORT_RECONCILIATION_BAY_AT;
                $table_view = '_table_BAY_AT';
                break;
        }
        //export excel
        if ($submit == STATUS_INT_HAI && trim($type_export) != '') {
            $file_name = 'Báo cáo đối soát ' . (isset($this->arrProduct[$pro_code]) ? $this->arrProduct[$pro_code] : '');
            $this->actionExcel = new ActionExcel();
            $dataExcel = ['data' => $dataList, 'arrPackCode' => $arrPackCodeExcel, 'dataTotal' => $dataTotal, 'total' => $total, 'file_name' => $file_name];
            $this->actionExcel->exportExcel($dataExcel, $type_export);
        }
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';


        $arrProPage = [PRODUCT_CODE_BAY_AT];
        $arrProSkyCare = [PRODUCT_CODE_SKY_CARE, PRODUCT_CODE_SKY_CARE_DOMESTIC];
        if (in_array($product_code, $arrProPage)) {
            $arrOption = [];
            foreach ($this->arrProductUser as $key => $name) {
                if (in_array($key, $arrProPage)) {
                    $arrOption[$key] = $name;
                }
            }
            $optionProduct = FunctionLib::getOptionKey($arrOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
        } else {
            //Sky care
            //sản phẩm Sky care
            if (in_array($product_code, $arrProSkyCare)) {
                $arrOption = [];
                foreach ($this->arrProductUser as $key => $name) {
                    if (in_array($key, $arrProSkyCare)) {
                        $arrOption[$key] = $name;
                    }
                }
                $optionProduct = FunctionLib::getOptionKey($arrOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
            }else{
                $arrProductOption = getArrChild($this->arrProductUser, $arrProPage);
                $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $arrProductOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
            }
        }

        return view($this->templateRoot . 'indexDataReconciliation', array_merge($this->dataOutCommon, [
            'data' => $dataList,
            'product_code' => $product_code,

            'table_view' => $table_view,
            'list_template' => $list_template,

            'dataTotal' => $dataTotal,
            'search' => $search,
            'total' => $total,
            'optionProduct' => $optionProduct,
            'totalMoney' => $totalMoney,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
            'urlIndex' => URL::route(Route::currentRouteName()),
        ]));
    }

    private function _buildParamSearchReconciliation($product_code = '')
    {
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 1);

        if (in_array($product_code, [PRODUCT_CODE_SKY_CARE, PRODUCT_CODE_SKY_CARE_DOMESTIC])) {
            $pack_default1 = !empty($this->arrPackCode) ? implode(';', array_keys($this->arrPackCode)) : '';
            $pack_default2 = !empty($this->arrPackCode2) ? implode(';', array_keys($this->arrPackCode2)) : '';
            $pack_default = $product_code == PRODUCT_CODE_SKY_CARE ? $pack_default1 : $pack_default2;

            $param_pack_code = 'p_pack_code_'.$product_code;
            $param_str_pack_code = 'p_str_pack_code_'.$product_code;

            $p_pack_code = trim(addslashes(Request::get($param_pack_code, $pack_default)));
            $p_str_pack_code = str_replace(',',';',trim(addslashes(Request::get($param_str_pack_code, $pack_default))));

            //fix chuyển sản phẩm
            if($product_code == PRODUCT_CODE_SKY_CARE){
                $param_str_pack_code_hiden = 'p_str_pack_code_'.PRODUCT_CODE_SKY_CARE_DOMESTIC;
                $search[$param_str_pack_code_hiden] = implode(';', array_keys($this->arrPackCode2));
            }else{
                $param_str_pack_code_hiden = 'p_str_pack_code_'.PRODUCT_CODE_SKY_CARE;
                $search[$param_str_pack_code_hiden] = implode(';', array_keys($this->arrPackCode));
            }

            $search['p_pack_code'] = $search[$param_str_pack_code] = $search["p_str_pack_code"] = $search[$param_pack_code] = (trim($p_str_pack_code) != '') ? trim($p_str_pack_code) : $p_str_pack_code;
        }

        $search['p_from_date'] = trim(addslashes(Request::get('p_from_date', getDateStartOfMonth())));
        $search['p_to_date'] = trim(addslashes(Request::get('p_to_date', getDateNow())));
        $search['p_type_date_sky_care'] = trim(addslashes(Request::get('p_type_date_sky_care', 'RECEIVED_DATE')));//Loại ngày tìm kiếm
        $search['p_fight_number'] = trim(addslashes(Request::get('p_fight_number', '')));//Số hiệu chuyến bay
        $search['p_org_code'] = trim(addslashes(Request::get('p_org_code', $this->user['org_code'])));
        $search['p_product_code'] = trim(addslashes(Request::get('p_product_code', $product_code)));
        $search['p_month'] = trim(addslashes(Request::get('p_month', getTimeCurrent('m'))));
        $search['p_year'] = trim(addslashes(Request::get('p_year', getTimeCurrent('y'))));
        $search['is_accumulated_defaul'] = trim(addslashes(Request::get('is_accumulated_defaul', STATUS_INT_KHONG)));
        $search['page_no'] = ($submit == STATUS_INT_MOT) ? $page_no : STATUS_INT_KHONG;
        //myDebug($search);
        return $search;
    }

}
