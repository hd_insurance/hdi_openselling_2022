<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Report;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\Organization;
use App\Models\Report\ReportProduct;

use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Models\System\PackageCore;
use App\Services\ActionExcel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class ReportProductDetailController extends BaseAdminController
{
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrOrgAll = array();
    private $arrOrg = array();
    private $arrPackCode = array();
    private $arrPackCode2 = array();
    private $arrMonth = array();
    private $arrYear = array();
    private $arrHours = array();
    private $arrMinute = array();
    private $arrTypeDateSearch = array();
    private $arrTypeDateOrder = array('DATE_SIGN' => 'Ngày nhận dữ liệu', 'FLY_DATE' => 'Ngày bay');
    private $arrTypeCutomer = array('ADL' => 'Người lớn', 'CHD' => 'Trẻ em', 'INFANT' => 'Trẻ em đi kèm');

    private $templateRoot = DIR_PRO_REPORT . '/';
    private $ngayKy = 'SIGNDATE';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ReportProduct();
    }

    public function _getDataUse()
    {
        $this->arrHours = CGlobal::$arrHours;
        $this->arrMinute = CGlobal::$arrMinute;

        $this->arrMonth = CGlobal::$arrMonth;
        $this->arrYear = getArrYear();
        $this->arrOrgAll = app(Organization::class)->getArrOptionOrg();
        $this->arrTypeDateSearch = $this->getArrOptionTypeDefine(DEFINE_TYPE_DATE_SEARCH);

        $this->arrPackCode = app(PackageCore::class)->getOptionPackage(PRODUCT_CODE_SKY_CARE);
        $this->arrPackCode2 = app(PackageCore::class)->getOptionPackage(PRODUCT_CODE_SKY_CARE_DOMESTIC);
    }

    private function _outDataView($request, $data)
    {
        $this->_getDataUse();
        $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrProductUser, isset($data['p_product_code']) ? $data['p_product_code'] : '');
        $optionSeller = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrOrgUser, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionTypeDateSearch = FunctionLib::getOption($this->arrTypeDateSearch, isset($data['p_type_date_search']) ? $data['p_type_date_search'] : $this->ngayKy);

        $optionTypeDateOrder = FunctionLib::getOption($this->arrTypeDateOrder, isset($data['p_type_date_search']) ? $data['p_type_date_search'] : 'DATE_SIGN');
        $optionTypeCutomer = FunctionLib::getOptionMultil($this->arrTypeCutomer, (isset($data['p_type_customer']) && trim($data['p_type_customer'])) ? explode(';', $data['p_type_customer']) : explode(';', implode(';', array_keys($this->arrTypeCutomer))));

        $pack_default = !empty($this->arrPackCode) ? implode(';', array_keys($this->arrPackCode)) : '';
        $optionPackCode = FunctionLib::getOptionMultil($this->arrPackCode, (isset($data['p_pack_code_SKY_CARE']) && trim($data['p_pack_code_SKY_CARE']) != '') ? explode(';', $data['p_pack_code_SKY_CARE']) : explode(';', $pack_default));

        $pack_default2 = !empty($this->arrPackCode2) ? implode(';', array_keys($this->arrPackCode2)) : '';
        $optionPackCode2 = FunctionLib::getOptionMultil($this->arrPackCode2, (isset($data['p_pack_code_SKY_CARE_DOMESTIC']) && trim($data['p_pack_code_SKY_CARE_DOMESTIC']) != '') ? explode(';', $data['p_pack_code_SKY_CARE_DOMESTIC']) : explode(';', $pack_default2));

        $optionMonth = FunctionLib::getOption(['' => 'Tháng'] + $this->arrMonth, isset($data['p_month']) ? $data['p_month'] : '');
        $optionYear = FunctionLib::getOption(['' => 'Năm'] + $this->arrYear, isset($data['p_year']) ? $data['p_year'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionMonth' => $optionMonth,
            'optionYear' => $optionYear,
            'optionProduct' => $optionProduct,
            'optionSeller' => $optionSeller,
            'optionTypeDateSearch' => $optionTypeDateSearch,

            'optionTypeDateOrder' => $optionTypeDateOrder,
            'optionTypeCutomer' => $optionTypeCutomer,
            'optionPackCode' => $optionPackCode,
            'arrPackCode' => $this->arrPackCode,

            'optionPackCode2' => $optionPackCode2,
            'arrPackCode2' => $this->arrPackCode2,

            'arrStatus' => $this->arrStatus,
            'arrOrg' => $this->arrOrg,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlGetItem' => '',
            'urlPostItem' => '',
            'urlAjaxGetData' => '',
            'functionAction' => '_ajaxGetItemOther',
        ];
    }

    /********************************************************************************************
     * báo cáo sản phẩm chi tiết
     ********************************************************************************************/
    public function indexReportDetailProduct()
    {
        return $this->_getDataProductDetail();
    }

    //bay an toàn
    public function indexProductDetailFlySafe()
    {
        return $this->_getDataProductDetail(PRODUCT_CODE_BAY_AT, ORG_VIETJET_VN);
    }

    //bay Sky care
    public function indexProductDetailSkyCare()
    {
        $product_code = trim(Request::get('p_product_code', PRODUCT_CODE_SKY_CARE));
        return $this->_getDataProductDetail($product_code, ORG_HDI);
    }

    public function getMemory(){
        /*if($this->is_root && $this->user['user_name'] =='QUYNHTM'){
            $memory_limit = ini_get('memory_limit');
            echo "Current memory limit is: " . $memory_limit . "\n";
            echo "Peak memory usage: " . memory_get_peak_usage() . "\n";
            ini_set('memory_limit', '50G');
            $memory_limit2 = ini_get('memory_limit');
            echo "New memory limit is: " . $memory_limit2 . "\n";
            //phpinfo();
        }*/
    }
    //bảo hiểm hành lý
    public function indexProductDetailLostBaggage()
    {
        return $this->_getDataProductDetail(PRODUCT_CODE_LOST_BAGGAGE, ORG_VIETJET_VN);
    }

    private function _getDataProductDetail($product_code = '', $org_code = '')
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }

        $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo chi tiết sản phẩm: ' . $product_code;
        CGlobal::$pageAdminTitle = $this->pageTitle . ' - Thống kê ' . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', STATUS_INT_KHONG);

        if (!in_array($product_code, array_keys($this->arrProductUser))) {
            $product_code = !empty($this->arrProductUser) ? array_key_first($this->arrProductUser) : DATA_SEARCH_NULL;
        }

        $dataList = $inforTotal = $dataTotal = [];
        $total = $totalList = 0;
        $totalMoney = 0;
        $limit = CGlobal::number_show_10;

        $search = $this->_buildParamSearchDetail($product_code, $org_code);
        //myDebug($search);
        if ($submit > 0) {
            set_time_limit(500000);
            $result = $this->modelObj->searchReportProductDetail($search);
        }

        $this->_outDataView($_POST, $search);

        if (isset($result['Success']) && $result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $inforTotal = $result['Data'][1][0] ?? $inforTotal;
            $total = $result['Data'][0][0]->TOTAL ?? $total;
            $dataTotal = $result['Data'][1][0] ?? $dataTotal;
            $totalList = !empty($dataList) ? array_key_last($dataList) + 1 : $total;
        }

        $pro_code = $search['p_product_code'];
        $file_name = 'Báo cáo chi tiết SP ' . (isset($this->arrProductUser[$pro_code]) ? $this->arrProductUser[$pro_code] : '');
        $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_COMMON;
        $list_template = 'listDataReportProductDetail';
        $searchExcel = [];
        switch ($pro_code) {
            case PRODUCT_CODE_XCG_TNDSBB:
            case PRODUCT_CODE_XCG_TNDSBB_NEW:
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_XCG_TNDSBB;
                $table_view = '_tableDetail_XCG_TNDSBB';
                break;
            case PRODUCT_CODE_XCG_VCX:
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_XCG_VCX_NEW;
                $table_view = '_tableDetail_XCG_VCX_NEW';
                break;
            case PRODUCT_CODE_LOST_BAGGAGE:
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_LOST_BAGGAGE;
                $table_view = '_tableDetail_LOST_BAGGAGE';
                break;
            case PRODUCT_CODE_BAY_AT:
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_BAY_AT;
                $table_view = '_tableDetail_BAY_AT';
                break;
            case PRODUCT_CODE_ATTD_NEW://bình an cá nhân
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_ATTD_NEW;
                $table_view = '_tableDetail_VISA_CARE';
                break;

            case PRODUCT_CODE_CSSK_NV:
                $table_view = '_tableDetail_CSKH_NV';
                break;
            case PRODUCT_CODE_ATTD:
            case PRODUCT_CODE_ATTD_HDB:
                $table_view = '_tableDetail_ATTD';
                break;
            case PRODUCT_CODE_CSVX:
                $table_view = '_tableDetail_CSVX';
                break;
            case PRODUCT_CODE_TRAU:
                $table_view = '_tableDetail_TRAU';
                break;
            case PRODUCT_CODE_SKY_CARE:
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_SKY_CARE;
                $searchExcel = $search;
                $searchExcel['arrTypeDateOrder'] = $this->arrTypeDateOrder;
                $searchExcel['arrTypeCutomer'] = $this->arrTypeCutomer;
                $searchExcel['arrPackCode'] = $this->arrPackCode;
                $table_view = '_tableDetail_SKY_CARE';
                $list_template = 'listDataReportProductDetailSkyCare';
                break;
            case PRODUCT_CODE_SKY_CARE_DOMESTIC:
                $type_export = ActionExcel::EXPORT_PRODUCT_DETAIL_SKY_CARE_DOMESTIC;
                $searchExcel = $search;
                $searchExcel['arrTypeDateOrder'] = $this->arrTypeDateOrder;
                $searchExcel['arrTypeCutomer'] = $this->arrTypeCutomer;
                $searchExcel['arrPackCode'] = $this->arrPackCode2;
                $table_view = '_tableDetail_SKY_CARE';
                $list_template = 'listDataReportProductDetailSkyCare';
                break;
            default:
                $table_view = '_tableDetail_VISA_CARE';
                break;
        }
        //export excel
        if ($submit == STATUS_INT_HAI && trim($type_export) != '') {
            $this->actionExcel = new ActionExcel();
            $dataExcel = ['data' => $dataList, 'dataExten' => $inforTotal, 'total' => $totalList, 'file_name' => $file_name, 'arrPackCode' => $this->arrPackCode,
                'dataSearch' => $searchExcel];
            $this->actionExcel->exportExcel($dataExcel, $type_export);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $arrProPage = [PRODUCT_CODE_BAY_AT, PRODUCT_CODE_LOST_BAGGAGE];
        $arrProSkyCare = [PRODUCT_CODE_SKY_CARE, PRODUCT_CODE_SKY_CARE_DOMESTIC];
        if (!in_array($product_code, $arrProPage) && !in_array($product_code, $arrProSkyCare)) {
            unset($this->arrProductUser[PRODUCT_CODE_BAY_AT]);
            unset($this->arrProductUser[PRODUCT_CODE_LOST_BAGGAGE]);

            unset($this->arrProductUser[PRODUCT_CODE_SKY_CARE]);
            unset($this->arrProductUser[PRODUCT_CODE_SKY_CARE_DOMESTIC]);
            $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrProductUser, isset($search['p_product_code']) ? $search['p_product_code'] : '');
        } else {
            //Sky care
            //sản phẩm Sky care
            if (in_array($product_code, $arrProSkyCare)) {
                $arrOption = [];
                foreach ($this->arrProductUser as $key => $name) {
                    if (in_array($key, $arrProSkyCare)) {
                        $arrOption[$key] = $name;
                    }
                }
                $optionProduct = FunctionLib::getOptionKey($arrOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
            }else{
                $arrProductOption = getArrChild($this->arrProductUser, $arrProPage);
                $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $arrProductOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
            }
        }

        return view($this->templateRoot . 'indexReportProductDetail', array_merge($this->dataOutCommon, [
            'data' => $dataList,
            'dataTotal' => $dataTotal,
            'inforTotal' => $inforTotal,
            'product_code' => $product_code,
            'table_view' => $table_view,
            'list_template' => $list_template,
            'search' => $search,
            'arrProPage' => $arrProPage,
            'total' => $total,
            'totalList' => $totalList,
            'optionProduct' => $optionProduct,
            'totalMoney' => $totalMoney,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
            'urlIndex' => URL::route(Route::currentRouteName()),
        ]));
    }

    private function _buildParamSearchDetail($product_code = '', $org_code = '')
    {
        $this->_getDataUse();
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 1);
        $search['p_org_code'] = trim(addslashes(Request::get('p_org_code', $org_code)));
        $product_form = trim(addslashes(Request::get('p_product_code', (trim($product_code) != '') ? $product_code : '')));

        if (in_array($product_code, [PRODUCT_CODE_SKY_CARE, PRODUCT_CODE_SKY_CARE_DOMESTIC])) {
            $pack_default1 = !empty($this->arrPackCode) ? implode(';', array_keys($this->arrPackCode)) : '';
            $pack_default2 = !empty($this->arrPackCode2) ? implode(';', array_keys($this->arrPackCode2)) : '';
            $pack_default = $product_code == PRODUCT_CODE_SKY_CARE ? $pack_default1 : $pack_default2;

            $param_pack_code = 'p_pack_code_'.$product_code;
            $param_str_pack_code = 'p_str_pack_code_'.$product_code;
            $p_str_pack_code = str_replace(',',';',trim(addslashes(Request::get($param_str_pack_code, $pack_default))));

            //fix chuyển sản phẩm
            if($product_code == PRODUCT_CODE_SKY_CARE){
                $param_str_pack_code_hiden = 'p_str_pack_code_'.PRODUCT_CODE_SKY_CARE_DOMESTIC;
                $search[$param_str_pack_code_hiden] = implode(';', array_keys($this->arrPackCode2));
            }else{
                $param_str_pack_code_hiden = 'p_str_pack_code_'.PRODUCT_CODE_SKY_CARE;
                $search[$param_str_pack_code_hiden] = implode(';', array_keys($this->arrPackCode));
            }

            $search['p_pack_code'] = $search[$param_str_pack_code] = $search["p_str_pack_code"] = $search[$param_pack_code] = (trim($p_str_pack_code) != '') ? trim($p_str_pack_code) : $p_str_pack_code;
           // $search['p_pack_code'] = $search[$param_str_pack_code] = (trim($p_pack_code) != '') ? implode(';', explode(';', trim($p_pack_code))) : '';

            $p_type_customer = trim(addslashes(Request::get('p_type_customer', '')));
            $p_str_type_customer = trim(addslashes(Request::get('p_str_type_customer', '')));
            $search["p_str_type_customer"] = (trim($p_str_type_customer) != '' && trim($p_str_type_customer) != '') ? trim($p_str_type_customer) : $p_type_customer;
            $search['p_type_customer'] = (trim($search["p_str_type_customer"]) != '' && $search["p_str_type_customer"] != '') ? implode(';', explode(',', trim($search["p_str_type_customer"]))) : '';

            $search['p_type_date_search'] = trim(addslashes(Request::get('p_type_date_search', 'DATE_SIGN')));
            $p_date_search = trim(addslashes(Request::get('p_date_search', '')));
            $is_date = validateFormatDate($p_date_search);

            $search['p_date_search'] = (trim($p_date_search) != '' && $is_date) ? $p_date_search : getDateNow();
            $search['p_fight_number'] = trim(addslashes(Request::get('p_fight_number', '')));
            $search['p_pnr_no'] = trim(addslashes(Request::get('p_pnr_no', '')));//mã đặt chỗ
        } else {
            $search['p_pack_code'] = trim(addslashes(Request::get('p_pack_code', 'GOI_1')));
            $search['p_type_date_search'] = trim(addslashes(Request::get('p_type_date_search', $this->ngayKy)));
        }

        $search['p_product_code'] = trim($product_form) != '' ? $product_form : (!empty($this->arrProductUser) ? array_key_first($this->arrProductUser) : DATA_SEARCH_NULL);
        $search['p_month'] = trim(addslashes(Request::get('p_month', getTimeCurrent('m'))));
        $search['p_year'] = trim(addslashes(Request::get('p_year', getTimeCurrent('y'))));
        $search['is_accumulated_defaul'] = trim(addslashes(Request::get('is_accumulated_defaul', STATUS_INT_KHONG)));
        $search['type_excel'] = STATUS_INT_MOT;
        $search['submit'] = $submit;
        $search['page_no'] = ($submit == STATUS_INT_MOT) ? $page_no : STATUS_INT_KHONG;

        return $search;
    }

}
