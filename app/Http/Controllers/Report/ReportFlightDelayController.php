<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Report;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\Organization;
use App\Models\System\PackageCore;
use App\Models\Report\ReportProduct;

use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Services\ActionExcel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class ReportFlightDelayController extends BaseAdminController
{
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrOrgAll = array();
    private $arrOrg = array();
    private $arrProduct = array();
    private $arrHours = array();
    private $arrMinute = array();
    private $arrMonth = array();
    private $arrYear = array();
    private $arrPackCode = array();
    private $arrPackCode2 = array();
    //private $arrTypeDateSkyCare = array('RECEIVED_DATE' => 'Ngày nhận dữ liệu', 'ETD1' => 'Thời gian khởi hành dự kiến');
    private $arrGroupDelay = array('GROUP_1' => '120-239 phút', 'GROUP_2' => '240-359 phút', 'GROUP_3' => 'Từ 360 phút trở lên');
    private $arrTypeDateSkyCare = array('DATE_SIGN' => 'Ngày nhận dữ liệu', 'FLY_DATE' => 'Ngày bay');
    private $templateRoot = DIR_PRO_REPORT . '/';
    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ReportProduct();
    }

    public function _getDataUse()
    {
        $this->arrHours = CGlobal::$arrHours;
        $this->arrMinute = CGlobal::$arrMinute;

        $this->arrMonth = CGlobal::$arrMonth;
        $this->arrYear = getArrYear();
        $this->arrOrgAll = app(Organization::class)->getArrOptionOrg();

        $this->arrPackCode = app(PackageCore::class)->getOptionPackage(PRODUCT_CODE_SKY_CARE);
        $this->arrPackCode2 = app(PackageCore::class)->getOptionPackage(PRODUCT_CODE_SKY_CARE_DOMESTIC);
    }

    private function _outDataView($request, $data)
    {
        $this->_getDataUse();
        //myDebug($request,false);
        //myDebug($data);
        $this->arrProduct = $this->getInforUser('product');
        $this->arrSeller = $this->getInforUser('org');
        $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrProduct, isset($data['p_product_code']) ? $data['p_product_code'] : '');
        $optionSeller = FunctionLib::getOptionKey(['' => '---Chọn---'] + $this->arrSeller, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionMonth = FunctionLib::getOption(['' => 'Tháng'] + $this->arrMonth, isset($data['p_month']) ? $data['p_month'] : '');
        $optionYear = FunctionLib::getOption(['' => 'Năm'] + $this->arrYear, isset($data['p_year']) ? $data['p_year'] : '');
        $optionTypeDateSkyCare = FunctionLib::getOption($this->arrTypeDateSkyCare, isset($data['p_type_date_sky_care']) ? $data['p_type_date_sky_care'] : 'DATE_SIGN');
        $optionGroupDelay = FunctionLib::getOption(['' => 'Tất cả'] +$this->arrGroupDelay, isset($data['p_group_time_delay']) ? $data['p_group_time_delay'] : '');

        $pack_default = !empty($this->arrPackCode) ? implode(';', array_keys($this->arrPackCode)) : '';
        $optionPackCode = FunctionLib::getOptionMultil($this->arrPackCode, (isset($data['p_pack_code_SKY_CARE']) && trim($data['p_pack_code_SKY_CARE']) != '') ? explode(';', $data['p_pack_code_SKY_CARE']) : explode(';', $pack_default));
        $pack_default2 = !empty($this->arrPackCode2) ? implode(';', array_keys($this->arrPackCode2)) : '';
        $optionPackCode2 = FunctionLib::getOptionMultil($this->arrPackCode2, (isset($data['p_pack_code_SKY_CARE_DOMESTIC']) && trim($data['p_pack_code_SKY_CARE_DOMESTIC']) != '') ? explode(';', $data['p_pack_code_SKY_CARE_DOMESTIC']) : explode(';', $pack_default2));

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionMonth' => $optionMonth,
            'optionYear' => $optionYear,
            'optionProduct' => $optionProduct,
            'optionSeller' => $optionSeller,
            'optionTypeDateSkyCare' => $optionTypeDateSkyCare,
            'optionGroupDelay' => $optionGroupDelay,

            'optionPackCode' => $optionPackCode,
            'optionPackCode2' => $optionPackCode2,
            'arrPackCode' => $this->arrPackCode,
            'arrPackCode2' => $this->arrPackCode2,

            'arrStatus' => $this->arrStatus,
            'arrOrg' => $this->arrOrg,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlGetItem' => '',
            'urlPostItem' => '',
            'urlAjaxGetData' => '',
            'functionAction' => '_ajaxGetItemOther',
        ];
    }

    /********************************************************************************************
     * Báo cáo tổng hợp chễ chuyến bay
     ********************************************************************************************/
    public function indexSalesFlightDelay()
    {
        $type_report = 'SALES';
        $this->_getDataUse();
        return $this->_getDataFlightDelay($type_report);
    }

    /********************************************************************************************
     * Báo cáo chi tiết chễ chuyến bay
     ********************************************************************************************/
    public function indexDetailFlightDelay()
    {
        $type_report = 'DETAIL';
        $this->_getDataUse();
        return $this->_getDataFlightDelay($type_report);
    }

    /************************************************************************************************************************************************/
    private function _getDataFlightDelay($type_report = '')
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }

        $product_code = trim(Request::get('p_product_code', PRODUCT_CODE_SKY_CARE));
        CGlobal::$pageAdminTitle = $this->pageTitle . CGlobal::$arrTitleProject[$this->tab_top];
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 1);
        $search = $this->_buildParamFlightDelay($type_report);

        //myDebug($search,true);
        $dataList = $dataTotal = [];
        $total = 0;
        $totalMoney = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchReportDataFlightDelay($search);
        //myDebug($result);

        $this->_outDataView($_POST, $search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data'][0] ?? $dataList;
            $dataTotal = $result['Data'][1][0] ?? $dataTotal;
            if (!empty($dataList)) {
                foreach ($dataList as $keys => $val) {
                    $total = $val->TOTAL;
                    break;
                }
            }
        }
        //myDebug($dataList);
        $list_template = 'listDataSales';
        switch ($type_report) {
            case 'SALES':
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo tổng hợp trễ chuyến bay';
                $type_export = ActionExcel::EXPORT_SALES_FLIGHT_DELAY;
                $search['arrPackCode'] = ($search['p_product_code'] == PRODUCT_CODE_SKY_CARE) ? $this->arrPackCode:$this->arrPackCode2;
                $table_view = '_table_sales';
                $list_template = 'listDataSales';
                break;
            case 'DETAIL':
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo chi tiết trễ chuyến bay';
                $type_export = ActionExcel::EXPORT_DETAIL_FLIGHT_DELAY;
                $search['arrPackCode'] = ($search['p_product_code'] == PRODUCT_CODE_SKY_CARE) ? $this->arrPackCode:$this->arrPackCode2;
                $search['arrGroupDelay'] = ['' => 'Tất cả'] +$this->arrGroupDelay;
                $table_view = '_table_detail';
                $list_template = 'listDataDetail';
                break;
            default:
                $this->pageTitle = CGlobal::$pageAdminTitle = 'Báo cáo tổng hợp trễ chuyến bay';
                $type_export = ActionExcel::EXPORT_SALES_FLIGHT_DELAY;
                $table_view = '_table_sales';
                break;
        }
        //export excel
        if ($submit == STATUS_INT_HAI && trim($type_export) != '') {
            $search['arrTypeDateOrder'] = $this->arrTypeDateSkyCare;
            $search['arrProductOrder'] = $this->arrProduct;

            $file_name = $this->pageTitle;
            $this->actionExcel = new ActionExcel();
            $dataExcel = ['data' => $dataList, 'dataExten' => $dataTotal, 'total' => $total, 'file_name' => $file_name, 'arrPackCode' => $this->arrPackCode,'dataSearch' => $search];
            $this->actionExcel->exportExcel($dataExcel, $type_export);
        }
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $arrProPage = [PRODUCT_CODE_BAY_AT];
        $arrProSkyCare = [PRODUCT_CODE_SKY_CARE, PRODUCT_CODE_SKY_CARE_DOMESTIC];
        if (in_array($product_code, $arrProPage)) {
            $arrOption = [];
            foreach ($this->arrProductUser as $key => $name) {
                if (in_array($key, $arrProPage)) {
                    $arrOption[$key] = $name;
                }
            }
            $optionProduct = FunctionLib::getOptionKey($arrOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
        } else {
            //Sky care
            //sản phẩm Sky care
            if (in_array($product_code, $arrProSkyCare)) {
                $arrOption = [];
                foreach ($this->arrProductUser as $key => $name) {
                    if (in_array($key, $arrProSkyCare)) {
                        $arrOption[$key] = $name;
                    }
                }
                $optionProduct = FunctionLib::getOptionKey($arrOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
            } else {
                $arrProductOption = getArrChild($this->arrProductUser, $arrProPage);
                $optionProduct = FunctionLib::getOptionKey(['' => '---Chọn---'] + $arrProductOption, isset($search['p_product_code']) ? $search['p_product_code'] : '');
            }
        }

        return view($this->templateRoot . 'indexReportFlightDelay', array_merge($this->dataOutCommon, [
            'data' => $dataList,
            'product_code' => $product_code,

            'table_view' => $table_view,
            'list_template' => $list_template,

            'dataTotal' => $dataTotal,
            'search' => $search,
            'total' => $total,
            'optionProduct' => $optionProduct,
            'totalMoney' => $totalMoney,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
            'urlIndex' => URL::route(Route::currentRouteName()),
        ]));
    }

    private function _buildParamFlightDelay($type_report = '')
    {
        $page_no = (int)Request::get('page_no', 1);
        $submit = (int)Request::get('submit', 1);
        $product_code = trim(addslashes(Request::get('p_product_code',)));
        if (in_array($product_code, [PRODUCT_CODE_SKY_CARE, PRODUCT_CODE_SKY_CARE_DOMESTIC])) {
            $pack_default1 = !empty($this->arrPackCode) ? implode(';', array_keys($this->arrPackCode)) : '';
            $pack_default2 = !empty($this->arrPackCode2) ? implode(';', array_keys($this->arrPackCode2)) : '';
            $pack_default = $product_code == PRODUCT_CODE_SKY_CARE ? $pack_default1 : $pack_default2;

            $param_pack_code = 'p_pack_code_' . $product_code;
            $param_str_pack_code = 'p_str_pack_code_' . $product_code;

            $p_pack_code = trim(addslashes(Request::get($param_pack_code, $pack_default)));
            $p_str_pack_code = str_replace(',', ';', trim(addslashes(Request::get($param_str_pack_code, $pack_default))));

            //fix chuyển sản phẩm
            if ($product_code == PRODUCT_CODE_SKY_CARE) {
                $param_str_pack_code_hiden = 'p_str_pack_code_' . PRODUCT_CODE_SKY_CARE_DOMESTIC;
                $search[$param_str_pack_code_hiden] = implode(';', array_keys($this->arrPackCode2));
            } else {
                $param_str_pack_code_hiden = 'p_str_pack_code_' . PRODUCT_CODE_SKY_CARE;
                $search[$param_str_pack_code_hiden] = implode(';', array_keys($this->arrPackCode));
            }

            $search['p_pack_code'] = $search[$param_str_pack_code] = $search["p_str_pack_code"] = $search[$param_pack_code] = (trim($p_str_pack_code) != '') ? trim($p_str_pack_code) : $p_str_pack_code;
        }

        $search['p_type_report'] = $type_report;
        $search['p_from_date'] = trim(addslashes(Request::get('p_from_date', getDateStartOfMonth())));
        $search['p_to_date'] = trim(addslashes(Request::get('p_to_date', getDateNow())));
        $search['p_type_date_sky_care'] = trim(addslashes(Request::get('p_type_date_sky_care', 'DATE_SIGN')));//Loại ngày tìm kiếm
        $search['p_org_code'] = trim(addslashes(Request::get('p_org_code', $this->user['org_code'])));
        $search['p_product_code'] = trim(addslashes(Request::get('p_product_code', $product_code)));
        $search['p_month'] = trim(addslashes(Request::get('p_month', getTimeCurrent('m'))));
        $search['p_year'] = trim(addslashes(Request::get('p_year', getTimeCurrent('y'))));
        $search['p_group_time_delay'] = trim(addslashes(Request::get('p_group_time_delay', '')));//nhóm trễ chuyến bay
        $search['p_fight_number'] = trim(addslashes(Request::get('p_fight_number', '')));//Số hiệu chuyến bay
        $search['is_accumulated_defaul'] = trim(addslashes(Request::get('is_accumulated_defaul', STATUS_INT_KHONG)));
        $search['page_no'] = ($submit == STATUS_INT_MOT) ? $page_no : STATUS_INT_KHONG;
        //myDebug($search);
        return $search;
    }

}
