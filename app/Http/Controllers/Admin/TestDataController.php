<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseAdminController;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Services\ServiceCommon;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class  TestDataController extends BaseAdminController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function viewLogDebug()
    {
        if (!$this->checkMultiPermiss()) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $pathDir = public_path() . '/logDebug';
        $this->_getFilesDebug($pathDir,$arrFiles);
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Show Debug Data';
        myDebug('$arrFiles',false);
        myDebug($arrFiles,false);
        return view('admin.TestData.listLogDebug.index', array_merge([
            'arrFiles' => $arrFiles,
            'stt' => 1,
            'pageTitle' => $this->pageTitle,
            'urlIndex' => URL::route('debugData.indexDebug'),
            'urlAjaxGetAction' => URL::route('debugData.ajaxGetAction'),
        ],[]));
    }

    //http://local.hdinew2022.com.vn/manager/debugData/detaiDebug
    public function detaiDebug(){
        if (!$this->checkMultiPermiss()) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        //$str_root = '{"p_in1":"VARCHAR2","p_in2":"VARCHAR2","p_in3":"VARCHAR2"}';
       // myDebug('xxxxx');
        $path = Request::get('path', '');
        $pathDir = public_path() . '/logDebug';
        if(trim($path) != ''){
            $contentDebug = file_get_contents($pathDir.'/'.$path, FILE_USE_INCLUDE_PATH);
            myDebug($contentDebug);
        }
        myDebug('Không có gì để xem cả :D :D ');
    }

    private function _funcGetDetailItem($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
        $contentDebug = file_get_contents($dataInput->pathDetail, FILE_USE_INCLUDE_PATH);

        myDebug($contentDebug);
        /*$html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'objectId' => 0,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $arrAjax = array('success' => 1, 'loadPage' => 0, 'viewHtml' => 2, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;*/
    }
    private function _getFilesDebug($path, &$files = array())
    {
        $path2 = 'logDebug';
        $dir = opendir($path."/.");
        while($item = readdir($dir))
            if(is_file($sub = $path."/".$item)){
                $arrTem = explode('/logDebug/',$sub);
                if(isset($arrTem[1])){
                    $files[] = [
                        'name' => $item,
                        'path' => $arrTem[1],
                        'url' => Config::get('config.WEB_ROOT').$path2.'/'.$arrTem[1]];
                }
            }
            else
                if($item != "." and $item != "..")
                    $this->_getFilesDebug($sub,$files[$item]);
        return($files);
    }

    //common
    public function ajaxGetAction()
    {
        if (!$this->checkMultiPermiss()) {
            return Response::json(returnError(1));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $result = [];
        $success = STATUS_INT_KHONG;
        $message = 'Có lỗi thao tác';
        $arrAjax = ['success' => $success, 'html' => $result, 'message' => $message];
        if (trim($functionAction) != '') {
            $result = $this->$functionAction($dataRequest);
            if (is_array($result)) {
                $arrAjax = array_merge($arrAjax, $result);
            }
        }
        return Response::json($arrAjax);
    }

    public function viewLogError()
    {
        if (!$this->checkMultiPermiss()) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $search['p_from_date'] = trim(addslashes(Request::get('p_from_date', '')));
        $search['p_from_date'] = ($search['p_from_date'] != '') ? $search['p_from_date'] : date('d/m/Y', strtotime(Carbon::now()));

        // Create a stream
        $opts = array(
            'https'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            )
        );
        $dateStr = date('dmY', strtotime(Carbon::now()));//time now

        $arrayDate = explode('/', $search['p_from_date']);
        $yearStr = isset($arrayDate[2])?$arrayDate[2]:2022;
        $monthStr = isset($arrayDate[1])?$arrayDate[1]:12;
        $dayStr = isset($arrayDate[0])?$arrayDate[0]:30;

        $param = 'OpenApi/logs/get?key='.$dateStr.'HDI-Logs&nam='.$yearStr.'&thang='.$monthStr.'&ngay='.$dayStr;
        $urlApi = Config::get('config.URL_API_' . Config::get('config.ENVIRONMENT')) . $param;

        $context = stream_context_create($opts);
        $contextFile = file_get_contents($urlApi, false, $context);

        //$arrayContent = explode('2022-05-30 ', $contextFile);
        $arrayContent = explode($yearStr.'-'.$monthStr.'-'.$dayStr.' ', $contextFile);
        if(!empty($arrayContent)){
            rsort($arrayContent);
            foreach ($arrayContent as $key => &$stringFile){
                $strSearch1 = 'INFO  TokenAuthorization';
                $pos1 = stripos($stringFile,$strSearch1);
                $strSearch2 = 'INFO  LoginAuthorization';
                $pos2 = stripos($stringFile,$strSearch2);
                if($pos1 || $pos2){
                    unset($arrayContent[$key]);
                }
            }
        }

        $this->pageTitle = CGlobal::$pageAdminTitle = 'Show Error log';
        $total = count($arrayContent);
        return view('admin.TestData.logError.index', array_merge([
            'arrayContent' => $arrayContent,
            'search' => $search,
            'stt' => 1,
            'total' => $total,
            'pageTitle' => $this->pageTitle,
            'urlIndex' => URL::route('admin.viewLogError'),
        ],[]));
    }
    ///manager/runCronjob?job=CronExcelExportInsmart
    public function runCronjob()
    {
        //Check phan quyen.
        if (!$this->checkMultiPermiss()) {
            return Redirect::route('admin.dashboard', array('error' => ERROR_PERMISSION));
        }
        $nameCronjob = trim(Request::get('job', ''));
        $taskAction = trim(Request::get('action', ''));
        if ($nameCronjob != '' && $this->is_root) {
            myDebug('$nameCronjob',false);
            myDebug($nameCronjob,false);
            Artisan::call('cronjob:' . $nameCronjob);
            echo 'Đã chạy thành công';
        }/* else {
            if(trim($taskAction) != ''){
                $this->$taskAction();
            }
            echo 'Chưa nhập tên cronjob để chạy, hãy nhập lại';
        }*/
        echo 'Chưa nhập tên cronjob để chạy, hãy nhập lại';
    }

    public function testData()
    {
        //Check phan quyen.
        if (!$this->checkMultiPermiss()) {
            return Redirect::route('admin.dashboard', array('error' => ERROR_PERMISSION));
        }
        $functionAction = trim(Request::get('func', ''));
        if ($functionAction != '' && $this->is_root) {
            return $this->$functionAction();
        } else {
            echo 'Chưa nhập tên function để chạy, hãy nhập lại';
        }
    }

    public function sendEmail()
    {
        $dataSend['PASSWORD'] = 'PASSWORD';
        $dataSend['OLD_PASSWORD'] = 'OLD_PASSWORD';
        $dataSend['IS_CHANGE_PWD'] = 1;

        $dataSend['EMAIL'] = CGlobal::mail_test;
        $dataSend['USER_NAME'] = 'USER_NAME';
        $dataSend['FULL_NAME'] = 'FULL_NAME';
        $dataSend['PASSWORD_NEW'] = 'PASSWORD_NEW';
        $dataSend['URL_LOGIN'] = Config::get('config.WEB_ROOT');

        $content = View::make('mail.mailForgotPassword')->with(['data' => $dataSend])->render();
        $dataSenmail['CONTENT'] = $content;
        $dataSenmail['TO'] = CGlobal::mail_test;
        $dataSenmail['CC'] = CGlobal::mail_test;
        $dataSenmail['TYPE'] = 'MAT_KHAU';
        $sendEmail = app(ServiceCommon::class)->sendMailWithContent($dataSenmail);
        myDebug($sendEmail);
    }

}
