<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\TemplateSystem;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Library\AdminFunction\Upload;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class TemplateSystemController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;
    private $arrStatus = array();

    private $templateRoot = DIR_PRO_SYSTEM . '/' . '.templateSystem.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $routerIndex = 'templateConfig.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new TemplateSystem();
        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
    }

    private function _outDataView($request, $data)
    {
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);
        $optionProduct = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProductUser, isset($data['p_product_code']) ? $data['p_product_code'] : '');
        $optionOrg = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrgUser, isset($data['p_org_code']) ? $data['p_org_code'] : '');
        $optionPack = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrPackUser, isset($data['p_pack_code']) ? $data['p_pack_code'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionProduct' => $optionProduct,
            'optionOrg' => $optionOrg,
            'optionPack' => $optionPack,
            'arrStatus' => $this->arrStatus,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route('templateConfig.index'),
            'urlGetItem' => URL::route('templateConfig.ajaxGetItem'),
            'urlPostItem' => URL::route('templateConfig.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('templateConfig.ajaxGetData'),
            'urlAjaxGetAction' => URL::route('templateConfig.ajaxGetAction'),
            'urlActionOtherItem' => URL::route('templateConfig.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',
            'urlDeleteItem' => '',
            'urlAjaxChangePass' => '',
        ];
    }

    private function _validformdata($id = 0, &$data = array())
    {
        if (!empty($data)) {
            if (isset($data['user_type']) && trim($data['user_type']) == '') {
                $this->error[] = 'kiểu người dùng không được bỏ trống';
            }
            if (isset($data['full_name']) && trim($data['full_name']) == '') {
                $this->error[] = 'họ tên không được bỏ trống';
            }
            if (isset($data['user_name']) && trim($data['user_name']) == '') {
                $this->error[] = 'tên đăng nhập không được bỏ trống';
            } else {
                /*$userexits = $this->modelobj->getinforuserbykey(strtoupper($data['user_name']),'user_name');
                if(isset($userexits->user_code) && $id != $userexits->user_code){
                    $this->error[] = 'tên đăng nhập đã tồn tại trên hệ thống';
                }else{
                    $data['user_name'] = strtoupper($data['user_name']);
                }*/
            }

        }
        return true;
    }

    /*********************************************************************************************************
     * Danh mục tổ chức: template email
     *********************************************************************************************************/
    public function indexTemplateEmail()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Quản lý Template Email';

        $search['p_temp_code'] = addslashes(Request::get('p_temp_code', ''));
        $search['p_product_code'] = addslashes(Request::get('p_product_code', ''));
        $search['p_temp_type'] = addslashes(Request::get('p_temp_type', 'EMAIL'));
        $search['p_pack_code'] = addslashes(Request::get('p_pack_code', ''));
        $search['p_org_code'] = addslashes(Request::get('p_org_code', ''));
        $search['p_is_active'] = addslashes(Request::get('p_is_active', STATUS_INT_MOT));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $page_no = (int)Request::get('page_no', 1);
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchTemplates($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndexTemplates', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    public function ajaxGetAction()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW, PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $result = [];
        $success = STATUS_INT_KHONG;
        $message = 'Có lỗi thao tác';
        $arrAjax = ['success' => $success, 'html' => $result, 'message' => $message];
        if (trim($functionAction) != '') {
            $result = $this->$functionAction($dataRequest);
            if (is_array($result)) {
                $arrAjax = array_merge($arrAjax, $result);
            }
        }
        return Response::json($arrAjax);
    }

    private function _funcGetDetailTemplates($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
        $divShowInfor = isset($request['divShow']) ? json_decode($request['divShow']) : 'content-page-right';

        $objectId = isset($dataInput->objectId) ? $dataInput->objectId : 0;
        $inforItem = isset($dataInput->dataItem) ? $dataInput->dataItem : false;

        $contentDataTemplate = isset($inforItem->DATA) ? $inforItem->DATA : '';
        $contentHeaderTemplate = isset($inforItem->HEADER) ? $inforItem->HEADER : '';
        $contentFooterTemplate = isset($inforItem->FOOTER) ? $inforItem->FOOTER : '';

        if (isset($inforItem->TEMP_ID)) {
            //bỏ mấy đối tượng CLOB trong DB
            unset($inforItem->HEADER);
            unset($inforItem->FOOTER);
            unset($inforItem->DATA);
        }

        $optionStatusEdit = FunctionLib::getOption($this->arrStatus, isset($dataItem->IS_ACTIVE) ? $dataItem->IS_ACTIVE : '');
        $this->dataOutItem = [
            'dataItem' => $inforItem,
            'contentDataTemplate' => $contentDataTemplate,
            'contentHeaderTemplate' => $contentHeaderTemplate,
            'contentFooterTemplate' => $contentFooterTemplate,
            'optionStatusEdit' => $optionStatusEdit,
        ];

        $this->_outDataView($request, (array)$inforItem);
        $html = View::make($this->templateRoot . 'component._popupEditTemplate')
            ->with(array_merge([
                'obj_id' => $objectId,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $arrAjax = array('success' => 1, 'loadPage' => 0, 'viewHtml' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    private function _funcPostDetailTemplates($request)
    {
        $dataForm = $request;
        $typeTabAction = $dataForm['typeTabAction'];
        $active = (int)$dataForm['ACTION_FORM'];
        if ($this->_validFormDataRelation($typeTabAction, $active, $dataForm) && empty($this->error)) {
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /*********************************************************************************************************
     * Danh mục tổ chức: template config
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Quản lý Template Config';

        $search['p_is_active'] = addslashes(Request::get('p_is_active', STATUS_INT_MOT));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['p_product_code'] = addslashes(Request::get('p_product_code', ''));
        $search['p_org_code'] = addslashes(Request::get('p_org_code', ''));
        $search['p_pack_code'] = addslashes(Request::get('p_pack_code', ''));
        $page_no = (int)Request::get('page_no', 1);
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchTemplateConfig($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    private function _getInfoItem($request)
    {
        $objectId = $request['objectId'] ?? '';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : [];
        $data = $dataOther = $dataOtherSMS = [];
        if (trim($objectId) != '' || trim($objectId) != '0') {
            $dataSearch = $this->modelObj->getTemplateConfig(['p_temp_id' => $objectId]);

            //lay dư liệu tab default
            if (isset($dataSearch['Data']) && !empty($dataSearch['Data'])) {
                $isCopy = isset($dataInput->isCopy) ? $dataInput->isCopy : STATUS_INT_KHONG;
                $data = isset($dataSearch['Data'][0][0]) ? $dataSearch['Data'][0][0] : [];//temp_config

                //thêm mới hoặc sửa
                if ($isCopy == STATUS_INT_MOT) {
                    $objectId = STATUS_INT_KHONG;
                    $data->TEMP_ID = STATUS_INT_KHONG;
                    $data->ORG_CODE = '';
                    $data->TEMP_CODE = '';
                    $data->PRODUCT_CODE = '';
                    $data->DESCRIPTION = $data->DESCRIPTION . '-Copy';
                } else {
                    $dataOtherSMS = isset($dataSearch['Data'][1]) ? $dataSearch['Data'][1] : [];//temp_sms
                    $dataOther = isset($dataSearch['Data'][2]) ? $dataSearch['Data'][2] : [];//temp
                }

                $this->dataOutItem = [
                    'actionEdit' => 0, //0: thêm mới, 1: edit
                    'formNameOther' => $this->tabOtherItem1,
                    'isCopy' => $isCopy,
                    'dataOther' => $dataOther,
                    'dataOtherSMS' => $dataOtherSMS,
                    'typeTab' => $this->tabOtherItem1,
                    'obj_id' => 0,
                    'objectId' => $objectId,
                    'divShowId' => $this->tabOtherItem1,
                ];
            }
        }
        $this->_outDataView($request, (array)$data);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'data' => $data,
                'objectTemplateID' => isset($data->TEMP_ID) ? $data->TEMP_ID : 0,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        $id = (isset($dataForm['objectId']) && trim($dataForm['objectId']) != '') ? (int)$dataForm['objectId'] : 0;
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }

        if ($this->_validFormData($id, $dataForm) && empty($this->error)) {
            $result = $this->modelObj->updateTemplateConfig($dataForm, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                //EDIT: lấy lại dữ liệu đã cập nhật để hiển thị lại
                if ($id > 0) {
                    $request = $dataForm;
                    $request['formName'] = $dataForm['formName'];
                    $this->_outDataView($request, $dataForm);
                    $html = View::make($this->templateRoot . 'component._detailFormItem')
                        ->with(array_merge([
                            'data' => (object)$dataForm,
                            'objectId' => $id,
                        ], $this->dataOutCommon))->render();
                    $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
                    $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
                } //ADD: thêm mới thì load lại dư liệu để nhập các thông tin khác
                else {
                    $item_code = isset($result['Data'][0]->TEMP_ID) ? $result['Data'][0]->TEMP_ID : '';
                    $dataForm['TEMP_ID'] = $item_code;
                    $request['objectId'] = $item_code;
                    $request['divShowInfor'] = 'divDetailItem';
                    $request['dataInput'] = json_encode(['item' => $dataForm]);
                    $arrAjax = $this->_getInfoItem($request);
                }
                return Response::json($arrAjax);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /*********************************************************************************************************
     * Get danh sách các temp detail
     *********************************************************************************************************/
    private function _ajaxGetListTempDetail($request)
    {
        $objectId = isset($request['objectId']) ? $request['objectId'] : 0;
        $dataListTempDetail = $this->modelObj->getListTemplateDetail(['p_temp_id' => $objectId]);
        $templateOut = $this->templateRoot . 'component._listTemplateDetail';

        $this->_outDataView($request, []);
        $html = View::make($templateOut)
            ->with(array_merge([
                'data' => isset($dataListTempDetail['Data'][0]) ? $dataListTempDetail['Data'][0] : [],
                'TEMP_ID' => $objectId
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    private function _ajaxGetItemTempDetail($request)
    {
        $objectId = isset($request['objectId']) ? $request['objectId'] : 0;//temp_id
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $itemTempDetai = isset($dataInput['DataItemTempDetail']) ? $dataInput['DataItemTempDetail'] : [];

        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = isset($dataInput['itemId']) ? $dataInput['itemId'] : '';

        $actionEdit = !empty($itemTempDetai) ? STATUS_INT_MOT : STATUS_INT_KHONG;
        $templateOut = $this->templateRoot . 'component._formTemplateDetail';

        $this->_outDataView($request, (array)$itemTempDetai);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataOther' => $itemTempDetai,
                'temp_id' => $objectId, //id template
                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $objectId,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _ajaxGetItemOther($request)
    {
        $data = $inforItem = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;

        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $templateOut = $this->templateRoot . 'component._formApiDatabases';
        //data chính
        $dataTemplateConfig = isset($arrKey['DataTemplateConfig']) ? (object)$arrKey['DataTemplateConfig'] : false;
        $dataTemplate = isset($arrKey['DataTemplate']) ? (object)$arrKey['DataTemplate'] : false;
        $dataTemplateSMS = isset($arrKey['DataTemplateSMS']) ? (object)$arrKey['DataTemplateSMS'] : false;

        $obj_id = isset($dataTemplateConfig->TEMP_ID) ? $dataTemplateConfig->TEMP_ID : 0;
        switch ($typeTab) {
            case $this->tabOtherItem1:
                $contentDataTemplate = $contentHeaderTemplate = $contentFooterTemplate = '';
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = $dataTemplate;
                    $contentDataTemplate = isset($inforItem->DATA) ? $inforItem->DATA : '';
                    $contentHeaderTemplate = isset($inforItem->HEADER) ? $inforItem->HEADER : '';
                    $contentFooterTemplate = isset($inforItem->FOOTER) ? $inforItem->FOOTER : '';
                    //bỏ mấy đối tượng CLOB trong DB
                    unset($inforItem->HEADER);
                    unset($inforItem->FOOTER);
                    unset($inforItem->DATA);
                    $templateOut = $this->templateRoot . 'component._formTemplate';
                } else {//get list danh sách item other
                    $dataTemConf = $this->modelObj->getTemplateConfig(['p_temp_id' => $obj_id]);
                    $inforItem = isset($dataTemConf['Data'][2]) ? $dataTemConf['Data'][2] : [];
                    $templateOut = $this->templateRoot . 'component._listTemplate';
                }
                $actionEdit = (trim($itemId) != '') ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $this->dataOutItem = [
                    'contentDataTemplate' => $contentDataTemplate,
                    'contentHeaderTemplate' => $contentHeaderTemplate,
                    'contentFooterTemplate' => $contentFooterTemplate,];
                break;
            case $this->tabOtherItem2:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = $dataTemplateSMS;
                    $templateOut = $this->templateRoot . 'component._formTemplateSMS';
                } else {//get list danh sách item other
                    $dataTemConf = $this->modelObj->getTemplateConfig(['p_temp_id' => $obj_id]);
                    $inforItem = isset($dataTemConf['Data'][1]) ? $dataTemConf['Data'][1] : [];
                    $templateOut = $this->templateRoot . 'component._listTemplateSMS';
                }
                $actionEdit = (trim($itemId) != '') ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $this->dataOutItem = [];
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$dataTemplate);
        $html = View::make($templateOut)
            ->with(array_merge([
                'data' => $dataTemplateConfig,
                'dataOther' => $inforItem,
                'dataOtherSMS' => $inforItem,
                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $obj_id,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            $success = STATUS_INT_MOT;
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }

    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        if (empty($dataRequest)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        //check form with file upload
        $typeTabAction = isset($dataRequest['typeTabAction']) ? $dataRequest['typeTabAction'] : $dataForm['typeTabAction'];
        $dataForm = isset($dataRequest['typeTabAction']) ? $dataRequest : $dataForm;
        $active = (int)$dataForm['ACTION_FORM'];

        if ($this->_validFormDataRelation($typeTabAction, $active, $dataForm) && empty($this->error)) {
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        } else {
            return Response::json(returnError($this->error));
        }
    }

    private function _updateDataRelation($dataForm, $typeTabAction)
    {
        $active = (int)$dataForm['ACTION_FORM'];
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
            case $this->tabOtherItem4:
                $ext_file = 'html';
                $folder = FOLDER_FILE_CREATE_ORDER;

                if (isset($_FILES['inputFileHeader']) && count($_FILES['inputFileHeader']) > 0 && $_FILES['inputFileHeader']['name'] != '') {
                    $fileNameHeader = app(Upload::class)->uploadFileHdi('inputFileHeader', $folder, $ext_file);
                    if (trim($fileNameHeader) != '') {
                        $pathFileUpload = getDirFile($fileNameHeader);
                        $contentHeader = file_get_contents($pathFileUpload);
                        $dataForm['HEADER'] = base64_encode($contentHeader);
                        app(Upload::class)->removeFile($folder, $fileNameHeader);
                    } else {
                        return Response::json(returnError(viewLanguage('Upfile Header không đính dạng: ' . $ext_file)));
                    }
                }
                if (isset($_FILES['inputFileFooter']) && count($_FILES['inputFileFooter']) > 0 && $_FILES['inputFileFooter']['name'] != '') {
                    $fileNameFooter = app(Upload::class)->uploadFileHdi('inputFileFooter', $folder, $ext_file);
                    if (trim($fileNameFooter) != '') {
                        $pathFileUpload2 = getDirFile($fileNameFooter);
                        $contentFooter = file_get_contents($pathFileUpload2);
                        $dataForm['FOOTER'] = base64_encode($contentFooter);
                        app(Upload::class)->removeFile($folder, $fileNameFooter);
                    } else {
                        return Response::json(returnError(viewLanguage('Upfile Footer không đính dạng: ' . $ext_file)));
                    }
                }
                if (isset($_FILES['inputFileData']) && count($_FILES['inputFileData']) > 0 && $_FILES['inputFileData']['name'] != '') {
                    $fileNameData = app(Upload::class)->uploadFileHdi('inputFileData', $folder, $ext_file);
                    if (trim($fileNameData) != '') {
                        $pathFileUpload3 = getDirFile($fileNameData);
                        $contentData = file_get_contents($pathFileUpload3);
                        $dataForm['DATA'] = base64_encode($contentData);
                        app(Upload::class)->removeFile($folder, $fileNameData);
                    } else {
                        return Response::json(returnError(viewLanguage('Upfile Data không đính dạng: ' . $ext_file)));
                    }
                }

                $result = $this->modelObj->updateTemplate($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem2:
                $result = $this->modelObj->updateTemplateSMS($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem3:
                $result = $this->modelObj->updateTemplateDetail($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            default:
                break;
        }

        if ($result['Success'] == STATUS_INT_MOT) {
            if ($typeTabAction == $this->tabOtherItem3 || $typeTabAction == $this->tabOtherItem4) {
                //cập nhật temp_detail
                $arrAjax = array('success' => 1, 'loadPage' => 1, 'message' => 'Successfully', 'divShowInfor' => '', 'html' => '');
                return $arrAjax;
            } else {
                //lấy lại dữ liệu vừa sửa
                $dataInput['type'] = isset($dataForm['typeTabAction']) ? $dataForm['typeTabAction'] : '';
                $dataInput['isDetail'] = STATUS_INT_KHONG;
                $dataInput['itemOther'] = $dataForm;
                $dataInput['arrKey']['DataTemplateConfig'] = isset($dataForm['dataTemplateConfig']) ? json_decode($dataForm['dataTemplateConfig']) : [];
                $requestLoad['dataInput'] = json_encode($dataInput);
                $requestLoad['objectId'] = isset($dataForm['TEMP_CONFIG']) ? $dataForm['TEMP_CONFIG'] : 0;
                $requestLoad['divShowId'] = isset($dataForm['divShowIdAction']) ? $dataForm['divShowIdAction'] : '';
                $requestLoad['formName'] = $dataForm['formName'];

                $html = $this->_ajaxGetItemOther($requestLoad);
                $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);
            }
            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }

    private function _validFormDataRelation($typeTabAction = '', $active = STATUS_INT_KHONG, &$data = array())
    {
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                if (!empty($data)) {
                    if (isset($data['BIRTHDAY']) && trim($data['BIRTHDAY']) == '') {
                        $this->error[] = 'Ngày sinh không được bỏ trống';
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

}
