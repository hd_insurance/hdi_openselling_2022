<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\SystemCore;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\SystemCore;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Services\ActionExcel;
use App\Services\ImportExcel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class StorageSynchController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrProject = array();
    private $arrDefineCode = array();

    private $templateRoot = DIR_PRO_SYSTEM.'/SystemCore/'. '.storageSynch.';
    private $routerIndex = 'storageSynch.index';
    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new SystemCore();
        $this->arrDefineCode = [];
    }

    private function _outDataView($request, $data)
    {
        $this->arrStatus = CGlobal::$arrStatus;
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);
        $optionProjectCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProject, isset($data['PROJECT_CODE']) ? $data['PROJECT_CODE'] : '');
        $optionSearchDefineCode = FunctionLib::getOption([DEFINE_ALL => '---Chọn---'] +$this->arrDefineCode, isset($data['s_define_code']) ? $data['s_define_code'] : DEFINE_ALL);

        $formId = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = $request['objectId'] ?? 0;
        return $this->dataOutCommon = [
            'optionSearchDefineCode' => $optionSearchDefineCode,
            'optionStatus' => $optionStatus,
            'arrStatus' => $this->arrStatus,
            'optionProjectCode' => $optionProjectCode,
            'arrProject' => $this->arrProject,

            'urlIndex' => URL::route('storageSynch.index'),
            'urlGetItem' => URL::route('storageSynch.ajaxGetItem'),
            'urlPostItem' => URL::route('storageSynch.ajaxPostItem'),
            'urlActionFunction' => URL::route('storageSynch.ajaxActionFunction'),
            'urlDeleteItem' => '',

            'form_id' => $formId,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
        ];
    }

    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Dữ liệu đồng bộ golive';
        $limit = CGlobal::number_show_500;
        $page_no = (int)Request::get('page_no', 1);
        $export_excel = (int)Request::get('export_excel', 0);
        $search['page_no'] = $page_no;
        $search['p_limit'] = $limit;
        $search['s_search'] = trim(addslashes(Request::get('s_search', '')));
        $search['p_str_id_search'] = trim(addslashes(Request::get('str_id_search', '')));

        $dataList = [];
        $total = 0;
        $result = $this->modelObj->searchList($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        if ($export_excel == STATUS_INT_MOT) {
            $file_name = 'File export data';
            $type_export = ActionExcel::EXPORT_DATA_GOLIVE;
            $this->actionExcel = new ActionExcel();
            $dataExcel = ['data' => $dataList, 'total' => $total, 'file_name' => $file_name];
            $this->actionExcel->exportExcel($dataExcel,$type_export);
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'total' => $total,
            'search' => $search,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD,PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $oject_id = $request['objectId'] ?? 0;
        $data = $arrSelectMenuCheck = [];
        $is_copy = STATUS_INT_KHONG;
        if ($oject_id > 0) {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $data = isset($dataInput->item) ? $dataInput->item : false;
            $is_copy = isset($dataInput->is_copy) ? $dataInput->is_copy : STATUS_INT_KHONG;
            $arrSelectMenuCheck = (isset($data->MENU_CODE) && trim($data->MENU_CODE) != '') ? explode(',', $data->MENU_CODE) : [];
        }

        $this->_outDataView($request, (array)$data);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'data' => $data,
                'is_copy' => $is_copy,
                'arrSelectMenuCheck' => $arrSelectMenuCheck,
            ], $this->dataOutCommon))->render();
        $arrAjax = array('success' => 1, 'html' => $html);
        return Response::json($arrAjax);
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD,PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];
        $id = (int)$dataForm['objectId'] ?? 0;
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }

        if ($this->_validFormData($dataForm) && empty($this->error)) {
            $dataForm['ID'] = ($id > 0) ? $id : '';// ID tự tăng
            $result = $this->modelObj->editItem($dataForm, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                return Response::json(['loadPage'=>STATUS_INT_MOT,'success'=>1]);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    public function ajaxActionFunction()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW, PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }

        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $message = 'Có lỗi thao tác';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            if (is_array($html)) {
                return Response::json($html);
            } else {
                $success = STATUS_INT_MOT;
                $message = 'Thực hiện thành công';
            }
        }
        $arrAjax = array('success' => $success, 'html' => $html, 'message' => $message);
        return Response::json($arrAjax);
    }

    /*********************************************************************************************************
     * Function Đẩy dữ liệu vào bảng tạm
     *********************************************************************************************************/
    private function _pushAddDataSynch($request){
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;

        $dataSyn = isset($dataInput['dataSyn']) ? $dataInput['dataSyn'] : '';
        $tableSyn = (int)isset($dataInput['tableSyn']) ? $dataInput['tableSyn'] : '';
        $keySyn = (int)isset($dataInput['keySyn']) ? $dataInput['keySyn'] : '';

        $this->modelObj->editDataSynch($dataSyn,$tableSyn,$keySyn);
        return 1;
    }

    private function _openPopupImportExcel($request){
        $this->_outDataView($_POST, $request);
        $html = View::make($this->templateRoot . 'component.popupImportExcel')
            ->with(array_merge([
                'data' => [],
            ], $this->dataOutCommon))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    private function _actionImportExcel($request){
        if (!isset($_FILES['inputFileExcel']) || empty($_FILES['inputFileExcel'])) {
            return Response::json(returnError(viewLanguage('Chưa chọn file upload để import data')));
        }
        $arrAjax = array('success' => STATUS_INT_KHONG,'isLoading' => STATUS_INT_KHONG, 'message' => 'Có lỗi import');
        $importExcel = new ImportExcel();
        $dataExcelImport = $importExcel->importExcelDataGolive($request, $_FILES);
        if (isset($dataExcelImport['dataExcel']) && !empty($dataExcelImport['dataExcel'])) {
            $result = $this->modelObj->importDataSynch($dataExcelImport['dataExcel']);
            if (isset($result['Data'][0]->KEY_OUT)) {
                $arrAjax = array('success' => STATUS_INT_MOT,'isLoading' => STATUS_INT_MOT);
            }
        } else {
            $arrAjax['message'] = 'Không có file hoặc dữ liệu không hợp lệ';
        }
        return $arrAjax;
    }

    private function _deleteListData($request){
        $strId = isset($request['strId'])?$request['strId']:'';
        $arrAjax = array('success' => STATUS_INT_KHONG, 'message' => 'Có lỗi thao tác xóa dữ liệu');
        if(trim($strId) != ''){
            $result = $this->modelObj->deleteDataSynch($strId);
            if(isset($result['Data'][0]->KEY_OUT)){
                $arrAjax = array('success' => STATUS_INT_MOT,'message' => 'Đã thực hiện thành công.');
            }
        }
        return Response::json($arrAjax);
    }
    /*********************************************************************************************************
     * Function Đẩy dữ liệu từ bảng tạm vào các bảng liên quan
     *********************************************************************************************************/
    private function _synchDataGolive($request){
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $keySyn = (int)isset($dataInput['keySyn']) ? $dataInput['keySyn'] : '';
        $this->modelObj->pushDataSynch($keySyn);
        return 1;
    }

    private function _validFormData($data = array())
    {
        if (!empty($data)) {
            if (isset($data['depart_name']) && trim($data['depart_name']) == '') {
                $this->error[] = 'Tên depart không được bỏ trống';
            }
            if (isset($data['depart_alias']) && trim($data['depart_alias']) == '') {
                $this->error[] = 'Tên viết tắt không được bỏ trống';
            }
        }
        return true;
    }

}
