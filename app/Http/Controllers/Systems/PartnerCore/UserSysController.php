<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\PartnerCore;

use App\Http\Controllers\BaseAdminController;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Models\System\PartnerCore;
use App\Models\System\ProductCore;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class UserSysController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;
    private $modelProduct = false;

    private $arrStatus = array();
    private $arrPartnerConfig = array();
        private $arrEnvCode = array();

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';

    private $templateRoot = DIR_PRO_SYSTEM . '/PartnerCore/' . 'userSys.';
    private $routerIndex = 'userPartnerSys.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new PartnerCore();
        $this->modelProduct = new ProductCore();

        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
        $this->arrEnvCode = $this->getArrOptionTypeDefine(DEFINE_ENVIROMENT_CODE);
        $this->arrPartnerConfig = $this->modelObj->getOptionPartnerConfig();

    }

    private function _outDataView($request, $data)
    {
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['ISACTIVE']) ? $data['ISACTIVE'] : STATUS_INT_MOT);
        $optionPartnerConfig = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrPartnerConfig, isset($data['PARTNER_CODE']) ? $data['PARTNER_CODE'] : '');
        $optionEnvCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrEnvCode, isset($data['ENVIROMENT_CODE']) ? $data['ENVIROMENT_CODE'] : 'DEV');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = $request['objectId'] ?? 0;
        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionPartnerConfig' => $optionPartnerConfig,
            'optionEnvCode' => $optionEnvCode,

            'arrStatus' => $this->arrStatus,
            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('userPartnerSys.ajaxGetItem'),
            'urlPostItem' => URL::route('userPartnerSys.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('userPartnerSys.ajaxGetData'),
            'urlActionOtherItem' => URL::route('userPartnerSys.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,
        ];
    }

    private function _validFormData($data = array())
    {
        if (!empty($data)) {
            if (isset($data['depart_name']) && trim($data['depart_name']) == '') {
                $this->error[] = 'Tên depart không được bỏ trống';
            }
            if (isset($data['depart_alias']) && trim($data['depart_alias']) == '') {
                $this->error[] = 'Tên viết tắt không được bỏ trống';
            }
        }
        return true;
    }

    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'User Partner';
        $limit = CGlobal::number_show_10;
        $page_no = (int)Request::get('page_no', 1);
        $search['ISACTIVE'] = addslashes(Request::get('ISACTIVE', STATUS_INT_MOT));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['p_is_active'] = $search['ISACTIVE'];
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $result = $this->modelObj->searchUserPartner($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'total' => $total,
            'search' => $search,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    public function _getInfoItem($request)
    {
        $oject_id = $request['objectId'] ?? 0;
        $data = $listPartnerUser = [];
        $is_copy = STATUS_INT_KHONG;
        if ($oject_id > 0) {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $data = isset($dataInput->item) ? $dataInput->item : false;
            $is_copy = isset($dataInput->is_copy) ? $dataInput->is_copy : STATUS_INT_KHONG;

            $partner_code = isset($dataInput->item) ? $dataInput->item->PARTNER_CODE : '';
            $user_code = isset($dataInput->item) ? $dataInput->item->USER_CODE : '';
            $inforPartner = $this->modelObj->getInforPartnerConfig($partner_code);
            if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                $partner = $inforPartner['Data'];
                $partnerAssign = isset($partner[2]) ? $partner[2] : false;//Pack assign
                if(!empty($partnerAssign)){
                    foreach ($partnerAssign as $k =>$partnerA){
                        if($partnerA->USER_CODE == $user_code){
                            $listPartnerUser[] = $partnerA;
                        }
                    }
                }
            }
        }
        $this->dataOutItem = [
            'actionEdit' => isset($dataPrimary->API_CODE) ? STATUS_INT_MOT : STATUS_INT_KHONG, //0: thêm mới, 1: edit
            'formNameOther' => $this->tabOtherItem1,
            'dataOther' => $data,
            'listPartnerUser' => $listPartnerUser,
            'typeTab' => $this->tabOtherItem1,
            'obj_id' => $oject_id,
            'divShowId' => $this->tabOtherItem1,
        ];

        $this->_outDataView($request, (array)$data);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'dataPrimary' => $data,
                'is_copy' => $is_copy,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        $id = (int)$dataForm['objectId'] ?? 0;
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        if(isset($dataForm['PARTNER_CODE']) && trim($dataForm['PARTNER_CODE']) == ''){
            return Response::json(returnError(viewLanguage('Chưa chọn PARTNER_CODE')));
        }
        if ($this->_validFormData($dataForm) && empty($this->error)) {
            $dataForm['GID'] = ($id > 0) ? $id : '';// ID tự tăng
            $result = $this->modelObj->editPartnerUser($dataForm, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                return Response::json(['loadPage' => STATUS_INT_MOT, 'success' => 1]);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _ajaxGetItemOther($request)
    {
        $data = $inforItem = $listPartnerSecret = $listPartnerUser = $listPartnerGroupApi = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = (int)isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $obj_id = $request['objectId'];
        $templateOut = $this->templateRoot . 'component._formPartnerAssign';
        //data chính
        $dataPrimary = isset($arrKey['dataPrimary']) ? (object)$arrKey['dataPrimary'] : false;
        $partner_code = isset($dataPrimary->PARTNER_CODE)? $dataPrimary->PARTNER_CODE:'';
        //myDebug($request);
        switch ($typeTab) {
            //SYS_USER_PARTNER
            case $this->tabOtherItem1:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $templateOut = $this->templateRoot . 'component._formPartnerAssign';

                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforPartnerConfig($partner_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listPartnerUser = isset($partner[2]) ? $partner[2] : false;//SYS_USER_PARTNER
                    }
                    $templateOut = $this->templateRoot . 'component._listPartnerAssign';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$inforItem);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'dataOther' => $inforItem,
                'listPartnerSecret' => $listPartnerSecret,
                'listPartnerUser' => $listPartnerUser,
                'listPartnerGroupApi' => $listPartnerGroupApi,

                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $obj_id,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    private function _updateDataRelation($dataForm, $typeTabAction)
    {
        $active = (int)$dataForm['ACTION_FORM'];
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                $result = $this->modelObj->editPartnerUser($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            default:
                break;
        }

        if ($result['Success'] == STATUS_INT_MOT) {
            //lấy lại dữ liệu vừa sửa
            $dataInput['type'] = $dataForm['typeTabAction'];
            $dataInput['isDetail'] = STATUS_INT_KHONG;
            $dataInput['itemOther'] = $dataForm;
            $dataInput['arrKey']['dataPrimary'] = json_decode($dataForm['dataPrimary']);//data cha
            $requestLoad['dataInput'] = json_encode($dataInput);

            $requestLoad['objectId'] = $dataForm['objectId'];
            $requestLoad['divShowId'] = $dataForm['divShowIdAction'];
            $requestLoad['formName'] = $dataForm['formName'];

            $html = $this->_ajaxGetItemOther($requestLoad);
            $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);

            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            $success = STATUS_INT_MOT;
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }

    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        if (empty($dataRequest)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        //check form with file upload
        $typeTabAction = isset($dataRequest['typeTabAction']) ? $dataRequest['typeTabAction'] : $dataForm['typeTabAction'];
        $dataForm = isset($dataRequest['typeTabAction']) ? $dataRequest : $dataForm;
        $active = (int)$dataForm['ACTION_FORM'];

        if ($this->_validFormDataRelation($typeTabAction, $active, $dataForm) && empty($this->error)) {
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        } else {
            return Response::json(returnError($this->error));
        }
    }

    private function _validFormDataRelation($typeTabAction = '', $active = STATUS_INT_KHONG, &$data = array())
    {
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                if (!empty($data)) {
                    if (isset($data['BIRTHDAY']) && trim($data['BIRTHDAY']) == '') {
                        $this->error[] = 'Ngày sinh không được bỏ trống';
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }
}
