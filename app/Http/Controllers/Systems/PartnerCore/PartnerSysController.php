<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\PartnerCore;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\Organization;
use App\Models\System\PartnerCore;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class PartnerSysController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrTrueFalse = array();
    private $arrUserPartner = array();
    private $arrStatus = array();
    private $arrRegion = array();
    private $arrTypeCode = array();
    private $arrTypePrice = array();
    private $arrEnvCode = array();
    private $arrOrg = array();
    private $arrCategory = array();
    private $arrGroupCodeApi = array();
    private $arrChannelProduct = array();

    private $templateRoot = DIR_PRO_SYSTEM . '/PartnerCore/' . '.partnerSys.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $routerIndex = 'partnerSys.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new PartnerCore();
        $this->arrChannelProduct = $this->getArrOptionTypeDefine(DEFINE_CHANNEL_PRODUCT);
        $this->arrRegion = $this->getArrOptionTypeDefine(DEFINE_PHAM_VI_DIA_LY);
        $this->arrTypeCode = $this->getArrOptionTypeDefine(DEFINE_TYPE_CODE_FEES);
        $this->arrTypePrice = $this->getArrOptionTypeDefine(DEFINE_TYPE_PRICE);

        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
        $this->arrEnvCode = $this->getArrOptionTypeDefine(DEFINE_ENVIROMENT_CODE);
        $this->arrTrueFalse = $this->getArrOptionTypeDefine(DEFINE_YES_OR_NO);
        $this->arrGroupCodeApi = $this->getArrOptionTypeDefine(DEFINE_GROUP_CODE_API);
        $this->arrOrg = app(Organization::class)->getArrOptionOrg();
        $this->arrUserPartner = $this->modelObj->getOptionUserPartner();
    }

    private function _outDataView($request, $data)
    {

        $optionOrgCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrg, isset($data['ORG_CODE']) ? $data['ORG_CODE'] : '');
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['ISACTIVE']) ? $data['ISACTIVE'] : STATUS_INT_MOT);
        $optionIsToken = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISTOKEN']) ? $data['ISTOKEN'] : STATUS_INT_MOT);
        $optionRefeshToken = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['IS_REFESH_TOKEN']) ? $data['IS_REFESH_TOKEN'] : STATUS_INT_KHONG);
        $optionAllAction = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISALL_ACTION']) ? $data['ISALL_ACTION'] : STATUS_INT_KHONG);
        $optionIsDelete = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISDELETE']) ? $data['ISDELETE'] : STATUS_INT_KHONG);
        $optionIsLimit = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISLIMIT']) ? $data['ISLIMIT'] : STATUS_INT_KHONG);
        $optionIsRequest = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISSIGNATURE_REQUEST']) ? $data['ISSIGNATURE_REQUEST'] : STATUS_INT_KHONG);
        $optionIsResponse = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISSIGNATURE_RESPONSE']) ? $data['ISSIGNATURE_RESPONSE'] : STATUS_INT_KHONG);
        $optionIsApproved = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISAPPROVED']) ? $data['ISAPPROVED'] : STATUS_INT_MOT);
        $optionIsUndoDelete = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTrueFalse, isset($data['ISUNDODELETE']) ? $data['ISUNDODELETE'] : STATUS_INT_KHONG);
        $optionEnvCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrEnvCode, isset($data['ENVIROMENT_CODE']) ? $data['ENVIROMENT_CODE'] : 'DEV');
        $optionUserPartner = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrUserPartner, isset($data['USER_CODE']) ? $data['USER_CODE'] : '');
        $optionGroupCodeApi = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrGroupCodeApi, isset($data['APIGROUP_CODE']) ? $data['APIGROUP_CODE'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionOrgCode' => $optionOrgCode,
            'optionIsToken' => $optionIsToken,
            'optionRefeshToken' => $optionRefeshToken,
            'optionAllAction' => $optionAllAction,
            'optionIsDelete' => $optionIsDelete,
            'optionIsLimit' => $optionIsLimit,
            'optionIsRequest' => $optionIsRequest,
            'optionIsResponse' => $optionIsResponse,
            'optionIsApproved' => $optionIsApproved,
            'optionIsUndoDelete' => $optionIsUndoDelete,
            'optionEnvCode' => $optionEnvCode,
            'optionUserPartner' => $optionUserPartner,
            'optionGroupCodeApi' => $optionGroupCodeApi,

            'arrStatus' => $this->arrStatus,
            'arrEnvCode' => $this->arrEnvCode,
            'arrTrueFalse' => $this->arrTrueFalse,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('partnerSys.ajaxGetItem'),
            'urlPostItem' => URL::route('partnerSys.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('partnerSys.ajaxGetData'),
            'urlActionOtherItem' => URL::route('partnerSys.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',
            'urlDeleteItem' => '',
            'urlAjaxChangePass' => '',
        ];
    }

    private function _validformdata($id = 0, &$data = array())
    {
        if (!empty($data)) {
            if (isset($data['user_type']) && trim($data['user_type']) == '') {
                $this->error[] = 'kiểu người dùng không được bỏ trống';
            }
            if (isset($data['full_name']) && trim($data['full_name']) == '') {
                $this->error[] = 'họ tên không được bỏ trống';
            }
            if (isset($data['user_name']) && trim($data['user_name']) == '') {
                $this->error[] = 'tên đăng nhập không được bỏ trống';
            } else {

            }
        }
        return true;
    }

    /*********************************************************************************************************
     * Index
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Partner Config';
        $page_no = (int)Request::get('page_no', 1);

        $search['ISACTIVE'] = addslashes(Request::get('ISACTIVE', ''));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['p_status'] = $search['ISACTIVE'];
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchPartnerConfig($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    private function _getInfoItem($request)
    {
        $objectId = $request['objectId'] ?? '';
        $dataPrimary = $listPartnerSecret = $listPartnerUser = $listPartnerGroupApi = $dataOther = [];
        if (trim($objectId) != '' || trim($objectId) != '0') {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $item_code = isset($dataInput->item) ? $dataInput->item->GID : '';
            $partner_code = isset($dataInput->item) ? $dataInput->item->PARTNER_CODE : '';

            $inforPartner = $this->modelObj->getInforPartnerConfig($partner_code);
            //lay dư liệu tab default
            if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                $partner = $inforPartner['Data'];
                $dataPrimary = isset($partner[0][0]) ? $partner[0][0] : false;//detail partner confign
                $listPartnerSecret = isset($partner[1]) ? $partner[1] : false;//SYS_PARTNER_SECRET
                $listPartnerUser = isset($partner[2]) ? $partner[2] : false;//SYS_USER_PARTNER
                $listPartnerGroupApi = isset($partner[3]) ? $partner[3] : false;//SYS_PARTNER_GROUP_API

                $this->dataOutItem = [
                    'actionEdit' => isset($dataPrimary->PARTNER_CODE) ? STATUS_INT_MOT : STATUS_INT_KHONG, //0: thêm mới, 1: edit
                    'formNameOther' => $this->tabOtherItem1,
                    'dataOther' => $dataOther,
                    'listPartnerSecret' => $listPartnerSecret,
                    'listPartnerUser' => $listPartnerUser,
                    'listPartnerGroupApi' => $listPartnerGroupApi,

                    'typeTab' => $this->tabOtherItem1,
                    'obj_id' => $item_code,
                    'divShowId' => $this->tabOtherItem1,
                ];
            }
        }
        $this->_outDataView($request, (array)$dataPrimary);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];
        $id = (isset($dataForm['objectId'])) ? (int)trim($dataForm['objectId']) : (int)trim($dataForm['objectId']);
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        if ($this->_validFormData($id, $dataForm) && empty($this->error)) {
            $result = $this->modelObj->editPartnerConfig($dataForm, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                //EDIT: lấy lại dữ liệu đã cập nhật để hiển thị lại
                if ($id > 0) {
                    $request = $dataForm;
                    $request['formName'] = $dataForm['formName'];
                    $this->_outDataView($request, $dataForm);
                    $html = View::make($this->templateRoot . 'component._detailFormItem')
                        ->with(array_merge([
                            'dataPrimary' => (object)$dataForm,
                            'objectId' => $id,
                        ], $this->dataOutCommon))->render();
                    $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
                    $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
                }
                //ADD: thêm mới thì load lại dư liệu để nhập các thông tin khác
                else {
                    $item_code = isset($result['Data'][0]->GID) ? 1 : 0;
                    $dataForm['GID'] = $item_code;
                    $request['objectId'] = $item_code;
                    $request['divShowInfor'] = 'divDetailItem';
                    $request['dataInput'] = json_encode(['item' => $dataForm]);
                    $arrAjax = $this->_getInfoItem($request);
                }
                return Response::json($arrAjax);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _ajaxGetItemOther($request)
    {
        $data = $inforItem = $listPartnerSecret = $listPartnerUser = $listPartnerGroupApi = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = (int)isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $obj_id = $request['objectId'];
        $templateOut = $this->templateRoot . 'component._formProductAssign';
        //data chính
        $dataPrimary = isset($arrKey['dataPrimary']) ? (object)$arrKey['dataPrimary'] : false;
        $partner_code = isset($dataPrimary->PARTNER_CODE)? $dataPrimary->PARTNER_CODE:'';
        //myDebug($request);
        switch ($typeTab) {
            //SYS_PARTNER_SECRET
            case $this->tabOtherItem1:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $templateOut = $this->templateRoot . 'component._formPartnerSecret';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforPartnerConfig($partner_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listPartnerSecret = isset($partner[1]) ? $partner[1] : false;//$partner secret
                    }
                    $templateOut = $this->templateRoot . 'component._listPartnerSecret';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionCategory'=>$optionCategory];
                break;
            //SYS_USER_PARTNER
            case $this->tabOtherItem2:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $templateOut = $this->templateRoot . 'component._formPartnerUser';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforPartnerConfig($partner_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listPartnerUser = isset($partner[2]) ? $partner[2] : false;//SYS_USER_PARTNER
                    }
                    $templateOut = $this->templateRoot . 'component._listPartnerUser';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionCategory'=>$optionCategory];
                break;
            //SYS_PARTNER_GROUP_API
            case $this->tabOtherItem3:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $templateOut = $this->templateRoot . 'component._formPartnerGroupApi';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforPartnerConfig($partner_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listPartnerGroupApi = isset($partner[3]) ? $partner[3] : false;//SYS_PARTNER_GROUP_API
                    }
                    $templateOut = $this->templateRoot . 'component._listPartnerGroupApi';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionCategory'=>$optionCategory];
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$data);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'dataOther' => $inforItem,
                'listPartnerSecret' => $listPartnerSecret,
                'listPartnerUser' => $listPartnerUser,
                'listPartnerGroupApi' => $listPartnerGroupApi,

                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $obj_id,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    private function _updateDataRelation($dataForm, $typeTabAction)
    {
        $active = (int)$dataForm['ACTION_FORM'];
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                $result = $this->modelObj->editPartnerSecret($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem2:
                $result = $this->modelObj->editPartnerUser($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem3:
                $result = $this->modelObj->editPartnerGroupApi($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            default:
                break;
        }

        if ($result['Success'] == STATUS_INT_MOT) {
            //lấy lại dữ liệu vừa sửa
            $dataInput['type'] = $dataForm['typeTabAction'];
            $dataInput['isDetail'] = STATUS_INT_KHONG;
            $dataInput['itemOther'] = $dataForm;
            $dataInput['arrKey']['dataPrimary'] = json_decode($dataForm['dataPrimary']);//data cha
            $requestLoad['dataInput'] = json_encode($dataInput);

            $requestLoad['objectId'] = $dataForm['objectId'];
            $requestLoad['divShowId'] = $dataForm['divShowIdAction'];
            $requestLoad['formName'] = $dataForm['formName'];

            $html = $this->_ajaxGetItemOther($requestLoad);
            $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);

            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            $success = STATUS_INT_MOT;
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }

    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        if (empty($dataRequest)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        //check form with file upload
        $typeTabAction = isset($dataRequest['typeTabAction']) ? $dataRequest['typeTabAction'] : $dataForm['typeTabAction'];
        $dataForm = isset($dataRequest['typeTabAction']) ? $dataRequest : $dataForm;
        $active = (int)$dataForm['ACTION_FORM'];

        if ($this->_validFormDataRelation($typeTabAction, $active, $dataForm) && empty($this->error)) {
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        } else {
            return Response::json(returnError($this->error));
        }
    }

    private function _validFormDataRelation($typeTabAction = '', $active = STATUS_INT_KHONG, &$data = array())
    {
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                if (!empty($data)) {
                    if (isset($data['BIRTHDAY']) && trim($data['BIRTHDAY']) == '') {
                        $this->error[] = 'Ngày sinh không được bỏ trống';
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

}
