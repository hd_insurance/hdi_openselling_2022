<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\ProductCore;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\CategoryCore;
use App\Models\System\Organization;
use App\Models\System\PackageCore;
use App\Models\System\ProductCore;
use App\Models\System\DatabaseConnection;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class ProductDeclarationController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrRegion = array();
    private $arrTypeCode = array();
    private $arrTypePrice = array();
    private $arrOperators = array();
    private $arrOrg = array();
    private $arrCategory = array();
    private $arrPackageCore = array();
    private $arrChannelProduct = array();
    private $arrGroupInsur = array();

    private $templateRoot = DIR_PRO_SYSTEM . '/ProductCore/' . '.productDeclaration.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $routerIndex = 'productDeclaration.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ProductCore();
        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
        $this->arrChannelProduct = $this->getArrOptionTypeDefine(DEFINE_CHANNEL_PRODUCT);
        $this->arrRegion = $this->getArrOptionTypeDefine(DEFINE_PHAM_VI_DIA_LY);
        $this->arrTypeCode = $this->getArrOptionTypeDefine(DEFINE_TYPE_CODE_FEES);
        $this->arrTypePrice = $this->getArrOptionTypeDefine(DEFINE_TYPE_PRICE);
        $this->arrOperators = $this->getArrOptionTypeDefine(DEFINE_OPERATORS);
        $this->arrGroupInsur = $this->getArrOptionTypeDefine(DEFINE_GROUP_INSUR);
        $this->arrOrg = app(Organization::class)->getArrOptionOrg();
        $this->arrPackageCore= app(PackageCore::class)->getOptionPackage();
        $this->arrCategory = app(CategoryCore::class)->getOptionCatgegory();
    }

    private function _outDataView($request, $data)
    {
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);
        $optionOrgCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrg, isset($data['ORG_CODE']) ? $data['ORG_CODE'] : '');
        $optionPackage = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrPackageCore, isset($data['PACK_CODE']) ? $data['PACK_CODE'] : '');
        $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($data['CATEGORY']) ? $data['CATEGORY'] : '');
        $optionChannelProduct = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrChannelProduct, isset($data['CHANNEL']) ? $data['CHANNEL'] : '');

        $optionTypeCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTypeCode, isset($data['TYPE_CODE']) ? $data['TYPE_CODE'] : '');
        $optionTypePrice = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrTypePrice, isset($data['VALUE_UNIT']) ? $data['VALUE_UNIT'] : 'P');
        $optionOperators = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOperators, isset($data['OPERATORS']) ? $data['OPERATORS'] : 'PLUS');

        $optionFromValue = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrRegion, isset($data['FROM_VALUE']) ? $data['FROM_VALUE'] : 'VN');
        $optionFromUnit = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrRegion, isset($data['FROM_UNIT']) ? $data['FROM_UNIT'] : '');
        $optionGroupInsur = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrGroupInsur, isset($data['GROUP_INSUR']) ? $data['GROUP_INSUR'] : 'CON_NGUOI');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionOrgCode' => $optionOrgCode,
            'optionPackage' => $optionPackage,
            'optionCategory' => $optionCategory,
            'optionChannelProduct' => $optionChannelProduct,
            'optionTypeCode' => $optionTypeCode,
            'optionFromValue' => $optionFromValue,
            'optionFromUnit' => $optionFromUnit,
            'optionTypePrice' => $optionTypePrice,
            'optionOperators' => $optionOperators,
            'optionGroupInsur' => $optionGroupInsur,
            'arrStatus' => $this->arrStatus,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('productDeclaration.ajaxGetItem'),
            'urlPostItem' => URL::route('productDeclaration.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('productDeclaration.ajaxGetData'),
            'urlActionOtherItem' => URL::route('productDeclaration.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',
            'urlDeleteItem' => '',
            'urlAjaxChangePass' => '',
        ];
    }

    private function _validformdata($id = 0, &$data = array())
    {
        if (!empty($data)) {
            if (isset($data['user_type']) && trim($data['user_type']) == '') {
                $this->error[] = 'kiểu người dùng không được bỏ trống';
            }
            if (isset($data['full_name']) && trim($data['full_name']) == '') {
                $this->error[] = 'họ tên không được bỏ trống';
            }
            if (isset($data['user_name']) && trim($data['user_name']) == '') {
                $this->error[] = 'tên đăng nhập không được bỏ trống';
            } else {

            }
        }
        return true;
    }

    /*********************************************************************************************************
     * Index
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Quản lý sản phẩm';
        $page_no = (int)Request::get('page_no', 1);

        $search['CATEGORY'] = addslashes(Request::get('CATEGORY', ''));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['p_category'] = $search['CATEGORY'];
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchProduct($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';
        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    private function _getInfoItem($request)
    {
        $objectId = $request['objectId'] ?? '';
        $dataPrimary = $listProductAssign = $listPackagesAssign = $dataOther = [];
        if (trim($objectId) != '' || trim($objectId) != '0') {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $item_code = isset($dataInput->item) ? $dataInput->item->BP_ID : '';
            $product_code = isset($dataInput->item) ? $dataInput->item->PRODUCT_CODE : '';

            $inforProduct = $this->modelObj->getInforProduct($product_code);

            //lay dư liệu tab default
            if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                $product = $inforProduct['Data'];
                $dataPrimary = isset($product[0][0]) ? $product[0][0] : false;//detail product
                $listProductAssign = isset($product[1]) ? $product[1] : false;//product assign
                $listPackagesAssign = isset($product[2]) ? $product[2] : false;//Pack assign
                $listChannelAssign = isset($product[3]) ? $product[3] : false;//Channel assign
                $listPackagesFees = isset($product[4]) ? $product[4] : false;//Pack fees

                $this->dataOutItem = [
                    'actionEdit' => isset($dataPrimary->API_CODE) ? STATUS_INT_MOT : STATUS_INT_KHONG, //0: thêm mới, 1: edit
                    'formNameOther' => $this->tabOtherItem1,
                    'dataOther' => $dataOther,
                    'listProductAssign' => $listProductAssign,
                    'listPackagesAssign' => $listPackagesAssign,
                    'listChannelAssign' => $listChannelAssign,
                    'listPackagesFees' => $listPackagesFees,
                    'typeTab' => $this->tabOtherItem1,
                    'obj_id' => $item_code,
                    'divShowId' => $this->tabOtherItem1,
                ];
            }
        }
        $this->_outDataView($request, (array)$dataPrimary);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];
        $id = (isset($dataForm['objectId'])) ? (int)trim($dataForm['objectId']) : (int)trim($dataForm['objectId']);
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        if ($this->_validFormData($id, $dataForm) && empty($this->error)) {
            $dataUpdate = [
                'BP_ID' => isset($dataForm['BP_ID']) ? $dataForm['BP_ID'] : '',
                'PRODUCT_CODE' => isset($dataForm['PRODUCT_CODE']) ? $dataForm['PRODUCT_CODE'] : '',
                'PRODUCT_NAME' => isset($dataForm['PRODUCT_NAME']) ? $dataForm['PRODUCT_NAME'] : '',
                'PRODUCT_NAME_EN' => isset($dataForm['PRODUCT_NAME_EN']) ? $dataForm['PRODUCT_NAME_EN'] : '',

                'GROUP_INSUR' => isset($dataForm['GROUP_INSUR']) ? $dataForm['GROUP_INSUR'] : '',
                'CATEGORY' => isset($dataForm['CATEGORY']) ? $dataForm['CATEGORY'] : '',

                'AGE_MIN' => isset($dataForm['AGE_MIN']) ? $dataForm['AGE_MIN'] : '',
                'AGE_MIN_UNIT' => isset($dataForm['AGE_MIN_UNIT']) ? $dataForm['AGE_MIN_UNIT'] : '',
                'AGE_MAX' => isset($dataForm['AGE_MAX']) ? $dataForm['AGE_MAX'] : '',
                'AGE_MAX_UNIT' => isset($dataForm['AGE_MAX_UNIT']) ? $dataForm['AGE_MAX_UNIT'] : '',

                'CURRENCY' => isset($dataForm['CURRENCY']) ? $dataForm['CURRENCY'] : '',
                'FEES_MIN' => isset($dataForm['FEES_MIN']) ? $dataForm['FEES_MIN'] : '',
                'FEES_DEFAULT' => isset($dataForm['FEES_DEFAULT']) ? $dataForm['FEES_DEFAULT'] : '',
                'FEES_MAX' => isset($dataForm['FEES_MAX']) ? $dataForm['FEES_MAX'] : '',

                'FEES_TYPE' => isset($dataForm['FEES_TYPE']) ? $dataForm['FEES_TYPE'] : '',
                'VAT_PERCENT' => isset($dataForm['VAT_PERCENT']) ? $dataForm['VAT_PERCENT'] : '',
                'STATUS' => isset($dataForm['STATUS']) ? $dataForm['STATUS'] : '',
            ];
            $result = $this->modelObj->editProduct($dataUpdate, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                //EDIT: lấy lại dữ liệu đã cập nhật để hiển thị lại
                if ($id > 0) {
                    $request = $dataForm;
                    $request['formName'] = $dataForm['formName'];
                    $this->_outDataView($request, $dataUpdate);
                    $html = View::make($this->templateRoot . 'component._detailFormItem')
                        ->with(array_merge([
                            'dataPrimary' => (object)$dataUpdate,
                            'objectId' => $id,
                        ], $this->dataOutCommon))->render();
                    $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
                    $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
                }
                //ADD: thêm mới thì load lại dư liệu để nhập các thông tin khác
                else {
                    $item_code = isset($result['Data'][0]->BP_ID) ? $result['Data'][0]->BP_ID : '';
                    $dataForm['BP_ID'] = $item_code;
                    $request['objectId'] = $item_code;
                    $request['divShowInfor'] = 'divDetailItem';
                    $request['dataInput'] = json_encode(['item' => $dataForm]);
                    $arrAjax = $this->_getInfoItem($request);
                }
                return Response::json($arrAjax);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _ajaxGetItemOther($request)
    {
        $data = $inforItem = $listProductAssign = $listPackagesAssign = $listChannelAssign = $listPackagesFees = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = (int)isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $obj_id = $request['objectId'];
        $templateOut = $this->templateRoot . 'component._formProductAssign';
        //data chính
        $dataPrimary = isset($arrKey['dataPrimary']) ? (object)$arrKey['dataPrimary'] : false;
        $product_code = isset($dataPrimary->PRODUCT_CODE)? $dataPrimary->PRODUCT_CODE:'';
        //myDebug($request);
        switch ($typeTab) {
            //Product Assign
            case $this->tabOtherItem1:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $templateOut = $this->templateRoot . 'component._formProductAssign';
                } else {//get list danh sách item other

                    $inforProduct = $this->modelObj->getInforProduct($product_code);
                    if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                        $product = $inforProduct['Data'];
                        $listProductAssign = isset($product[1]) ? $product[1] : false;//product assign
                    }
                    $templateOut = $this->templateRoot . 'component._listProductAssign';
                    $data = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionCategory'=>$optionCategory];
                break;

            //Package Assign
            case $this->tabOtherItem2:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $this->arrPackageCore= app(PackageCore::class)->getOptionPackage($product_code);
                    $templateOut = $this->templateRoot . 'component._formPackagesAssign';
                } else {//get list danh sách item other
                    $inforProduct = $this->modelObj->getInforProduct($product_code);
                    if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                        $product = $inforProduct['Data'];

                        $listPackagesAssign = isset($product[2]) ? $product[2] : false;//Pack assign
                    }
                    $templateOut = $this->templateRoot . 'component._listPackagesAssign';
                    $data = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionPackage = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrPackageCore, isset($dataPrimary->PACK_CODE) ? $dataPrimary->PACK_CODE : '');
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionPackage'=>$optionPackage,'optionCategory'=>$optionCategory];
                break;

            //Channel Assign
            case $this->tabOtherItem3:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $this->arrPackageCore= app(PackageCore::class)->getOptionPackage($product_code);
                    $templateOut = $this->templateRoot . 'component._formChannelAssign';
                } else {//get list danh sách item other
                    $inforProduct = $this->modelObj->getInforProduct($product_code);
                    if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                        $product = $inforProduct['Data'];
                        $listChannelAssign = isset($product[3]) ? $product[3] : false;//Channel assign
                    }
                    $templateOut = $this->templateRoot . 'component._listChannelAssign';
                    $data = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionCategory'=>$optionCategory];
                break;

            //Packages Fees
            case $this->tabOtherItem4:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $this->arrPackageCore= app(PackageCore::class)->getOptionPackage($product_code);
                    $templateOut = $this->templateRoot . 'component._formPackagesFees';
                } else {//get list danh sách item other
                    $inforProduct = $this->modelObj->getInforProduct($product_code);
                    if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                        $product = $inforProduct['Data'];
                        $listPackagesFees = isset($product[4]) ? $product[4] : false;//Pack fees
                    }
                    $templateOut = $this->templateRoot . 'component._listPackagesFees';
                    $data = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionCategory'=>$optionCategory];
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$data);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'dataOther' => $inforItem,
                'listProductAssign' => $listProductAssign,
                'listPackagesAssign' => $listPackagesAssign,
                'listChannelAssign' => $listChannelAssign,
                'listPackagesFees' => $listPackagesFees,
                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $obj_id,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }
    private function _ajaxActionOther($request)
    {
        $data = $inforItem = $listProductAssign = $listPackagesAssign = $listChannelAssign = $listPackagesFees = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $actionCode = isset($dataInput['actionCode']) ? $dataInput['actionCode'] : '';
        $itemId = (int)isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $obj_id = $request['objectId'];
        $templateOut = $this->templateRoot . 'component._formProductAssign';
        //data chính
        $dataPrimary = isset($arrKey['dataPrimary']) ? (object)$arrKey['dataPrimary'] : false;
        $product_code = isset($dataPrimary->PRODUCT_CODE)? $dataPrimary->PRODUCT_CODE:'';
        //myDebug($request);
        switch ($actionCode) {
            //Packages Fees
            case 'removePackagesFees':
                $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                $key_id = isset($inforItem->PKGF_ID)?$inforItem->PKGF_ID:'';
                $remove = $this->modelObj->removePackagesFees($key_id);

                if(isset($remove['Success']) && $remove['Success'] == 1){
                    $inforProduct = $this->modelObj->getInforProduct($product_code);
                    if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                        $product = $inforProduct['Data'];
                        $listPackagesFees = isset($product[4]) ? $product[4] : false;//Pack fees
                    }
                    $templateOut = $this->templateRoot . 'component._listPackagesFees';
                    $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                    $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                    $this->dataOutItem = ['optionCategory'=>$optionCategory];
                }
                break;
            default:
                break;
        }
        //lấy lại dữ liệu vừa sửa
        //$dataInput['type'] = $typeTab;
        $dataInput['isDetail'] = STATUS_INT_KHONG;
        $dataInput['itemOther'] = [];
        //$dataInput['arrKey']['dataPrimary'] = json_decode($dataPrimary);//data cha
        $requestLoad['dataInput'] = json_encode($dataInput);

        $requestLoad['objectId'] = $obj_id;
        $requestLoad['divShowId'] = $typeTab;
        $requestLoad['formName'] = $formNameOther;

        $html = $this->_ajaxGetItemOther($requestLoad);
        $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);

        return Response::json($arrAjax);

        /*$this->_outDataView($request, (array)$data);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'dataOther' => $inforItem,
                'listProductAssign' => $listProductAssign,
                'listPackagesAssign' => $listPackagesAssign,
                'listChannelAssign' => $listChannelAssign,
                'listPackagesFees' => $listPackagesFees,
                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $obj_id,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;*/
    }

    private function _updateDataRelation($dataForm, $typeTabAction)
    {
        $active = (int)$dataForm['ACTION_FORM'];
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                $result = $this->modelObj->editProductAssign($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem2:
                $result = $this->modelObj->editPackagesAssign($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem3:
                $result = $this->modelObj->editChannelAssign($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            case $this->tabOtherItem4:
                $result = $this->modelObj->editPackagesFees($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            default:
                break;
        }

        if ($result['Success'] == STATUS_INT_MOT) {
            //lấy lại dữ liệu vừa sửa
            $dataInput['type'] = $dataForm['typeTabAction'];
            $dataInput['isDetail'] = STATUS_INT_KHONG;
            $dataInput['itemOther'] = $dataForm;
            $dataInput['arrKey']['dataPrimary'] = json_decode($dataForm['dataPrimary']);//data cha
            $requestLoad['dataInput'] = json_encode($dataInput);

            $requestLoad['objectId'] = $dataForm['objectId'];
            $requestLoad['divShowId'] = $dataForm['divShowIdAction'];
            $requestLoad['formName'] = $dataForm['formName'];

            $html = $this->_ajaxGetItemOther($requestLoad);
            $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);

            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            $success = STATUS_INT_MOT;
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }

    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        if (empty($dataRequest)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        //check form with file upload
        $typeTabAction = isset($dataRequest['typeTabAction']) ? $dataRequest['typeTabAction'] : $dataForm['typeTabAction'];
        $dataForm = isset($dataRequest['typeTabAction']) ? $dataRequest : $dataForm;
        $active = (int)$dataForm['ACTION_FORM'];

        if ($this->_validFormDataRelation($typeTabAction, $active, $dataForm) && empty($this->error)) {
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        } else {
            return Response::json(returnError($this->error));
        }
    }

    private function _validFormDataRelation($typeTabAction = '', $active = STATUS_INT_KHONG, &$data = array())
    {
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                if (!empty($data)) {
                    if (isset($data['BIRTHDAY']) && trim($data['BIRTHDAY']) == '') {
                        $this->error[] = 'Ngày sinh không được bỏ trống';
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

}
