<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\ProductCore;

use App\Http\Controllers\BaseAdminController;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Models\System\CategoryCore;
use App\Models\System\Organization;
use App\Models\System\PackageCore;
use App\Models\System\ProductCore;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class PackageDeclarationController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;
    private $modelProduct = false;

    private $arrStatus = array();
    private $arrOrg = array();
    private $arrDiscountUnit = array();
    private $arrProject = array();
    private $arrCategory = array();
    private $arrPackageCore = array();
    private $arrProductCore = array();
    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';

    private $templateRoot = DIR_PRO_SYSTEM . '/ProductCore/' . '.packageDeclaration.';
    private $routerIndex = 'packageDeclaration.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new PackageCore();
        $this->modelProduct = new ProductCore();

        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
        $this->arrDiscountUnit = $this->getArrOptionTypeDefine(DEFINE_DISCOUNT_UNIT);
        $this->arrCategory = app(CategoryCore::class)->getOptionCatgegory();
        $this->arrProductCore = app(ProductCore::class)->getOptionProduct();
        $this->arrOrg = app(Organization::class)->getArrOptionOrg();
        $this->arrPackageCore= app(PackageCore::class)->getOptionPackage();
    }

    private function _outDataView($request, $data)
    {
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);
        $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($data['CATEGORY']) ? $data['CATEGORY'] : '');
        $optionProductCore = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProductCore, isset($data['PRODUCT_CODE']) ? $data['PRODUCT_CODE'] : '');
        $optionOrgCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrg, isset($data['ORG_CODE']) ? $data['ORG_CODE'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = $request['objectId'] ?? 0;
        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionCategory' => $optionCategory,
            'optionProductCore' => $optionProductCore,
            'optionOrgCode' => $optionOrgCode,
            'arrProject' => $this->arrProject,
            'arrDiscountUnit' => $this->arrDiscountUnit,
            'arrStatus' => $this->arrStatus,

            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('packageDeclaration.ajaxGetItem'),
            'urlPostItem' => URL::route('packageDeclaration.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('packageDeclaration.ajaxGetData'),
            'urlActionOtherItem' => URL::route('packageDeclaration.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,
        ];
    }

    private function _validFormData($data = array())
    {
        if (!empty($data)) {
            if (isset($data['depart_name']) && trim($data['depart_name']) == '') {
                $this->error[] = 'Tên depart không được bỏ trống';
            }
            if (isset($data['depart_alias']) && trim($data['depart_alias']) == '') {
                $this->error[] = 'Tên viết tắt không được bỏ trống';
            }
        }
        return true;
    }

    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Khai báo gói';
        $limit = CGlobal::number_show_10;
        $page_no = (int)Request::get('page_no', 1);
        $search['IS_ACTIVE'] = addslashes(Request::get('IS_ACTIVE', STATUS_INT_MOT));
        $search['PRODUCT_CODE'] = addslashes(Request::get('PRODUCT_CODE', ''));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['p_is_active'] = $search['IS_ACTIVE'];
        $search['p_product_code'] = $search['PRODUCT_CODE'];
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $result = $this->modelObj->searchPackage($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'total' => $total,
            'search' => $search,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    public function _getInfoItem($request)
    {
        $oject_id = $request['objectId'] ?? 0;
        $data = $listPackagesAssign = [];
        $is_copy = STATUS_INT_KHONG;
        if ($oject_id > 0) {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $data = isset($dataInput->item) ? $dataInput->item : false;
            $is_copy = isset($dataInput->is_copy) ? $dataInput->is_copy : STATUS_INT_KHONG;

            $product_code = isset($dataInput->item) ? $dataInput->item->PRODUCT_CODE : '';
            $pack_code = isset($dataInput->item) ? $dataInput->item->PACK_CODE : '';
            $inforProduct = $this->modelProduct->getInforProduct($product_code);
            if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                $product = $inforProduct['Data'];
                $packagesAssign = isset($product[2]) ? $product[2] : false;//Pack assign
                if(!empty($packagesAssign)){
                    foreach ($packagesAssign as $k =>$packA){
                        if($packA->PACK_CODE == $pack_code){
                            $listPackagesAssign[] = $packA;
                        }
                    }
                }
            }
        }
        $this->dataOutItem = [
            'actionEdit' => isset($dataPrimary->API_CODE) ? STATUS_INT_MOT : STATUS_INT_KHONG, //0: thêm mới, 1: edit
            'formNameOther' => $this->tabOtherItem1,
            'dataOther' => $data,
            'listPackagesAssign' => $listPackagesAssign,
            'typeTab' => $this->tabOtherItem1,
            'obj_id' => $oject_id,
            'divShowId' => $this->tabOtherItem1,
        ];

        $this->_outDataView($request, (array)$data);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'dataPrimary' => $data,
                'is_copy' => $is_copy,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        $id = (int)$dataForm['objectId'] ?? 0;
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        if(isset($dataForm['PRODUCT_CODE']) && trim($dataForm['PRODUCT_CODE']) != ''){
            $productAll = app(ProductCore::class)->getAllData();
            foreach ($productAll as $k=>$pr){
                if(trim($pr->PRODUCT_CODE) == trim($dataForm['PRODUCT_CODE'])){
                    $dataForm['CATEGORY'] = $pr->CATEGORY;
                }
            }
        }else{
            return Response::json(returnError(viewLanguage('Chưa chọn sản phẩm')));
        }
        if ($this->_validFormData($dataForm) && empty($this->error)) {
            $dataForm['PKG_ID'] = ($id > 0) ? $id : '';// ID tự tăng
            $result = $this->modelObj->editItem($dataForm, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                return Response::json(['loadPage' => STATUS_INT_MOT, 'success' => 1]);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /**************************************************
     * Get data Active
     * ************************************************/
    private function _functionGetData($request)
    {
        $formName = isset($request['formName']) ? $request['formName'] : 'formName';
        $titlePopup = isset($request['titlePopup']) ? $request['titlePopup'] : 'Thông tin chung';
        $objectId = isset($request['objectId']) ? (int)$request['objectId'] : STATUS_INT_KHONG;

        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $funcAction = isset($dataInput['funcAction']) ? $dataInput['funcAction'] : (isset($request['funcAction']) ? $request['funcAction'] : '');

        $htmlView = '';
        switch ($funcAction) {
            case 'getListInterestPack'://Danh sách quyền lợi
                $dataDetail = false;
                $inforPack = $dataInput['dataPack'] ? $dataInput['dataPack'] : [];
                $search['p_product_code'] = isset($inforPack['PRODUCT_CODE']) ? $inforPack['PRODUCT_CODE'] : '';
                $search['p_pack_code'] = isset($inforPack['PACK_CODE']) ? $inforPack['PACK_CODE'] : '';
                $dataBenefits = $this->modelObj->searchBenefits($search);
                $listBenefits = isset($dataBenefits['Data']['data'])?$dataBenefits['Data']['data']:[];
                $this->_outDataView($request, $dataDetail);
                $htmlView = View::make($this->templateRoot . 'component.popupInterestPack')
                    ->with(array_merge($this->dataOutCommon, [
                        'dataItem' => $dataDetail,
                        'listBenefits'=>$listBenefits,
                        'objectId' => $objectId,
                        'formNameOther' => $formName,
                        'titlePopup' => $titlePopup,
                    ]))->render();
                break;
            default:
                break;
        }
        return $htmlView;
    }

    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([ PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }
        $request = $_POST;
        $arrAjax = array('success' => 0, 'html' => '', 'msg' => '');
        $actionUpdate = 'actionUpdate';
        $dataForm = isset($request['dataForm']) ? $request['dataForm'] : [];
        $actionUpdate = isset($dataForm['actionUpdate']) ? $dataForm['actionUpdate'] : (isset($request['actionUpdate']) ? $request['actionUpdate'] : $actionUpdate);

        $typeTabAction = isset($dataForm['typeTabAction']) ? $dataForm['typeTabAction'] : '';
        //Edit các phần theo Tab 1,2,3..
        if(trim($typeTabAction) != ''){
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        }else{
            //Edit các phần popup
            switch ($actionUpdate) {
                case 'EditBenefitPack':
                    $listBenefits = isset($dataForm['listBenefits'])?json_decode($dataForm['listBenefits'],true): [];
                    $product_code = $pack_code = '';
                    $isEdit = 0;
                    if(!empty($listBenefits)){
                        foreach ($listBenefits as $kk => &$va_bef){
                            $is_check = isset($dataForm['be_code_'.$va_bef['BE_CODE']])? STATUS_INT_MOT: 0;
                            $isEdit = STATUS_INT_MOT;
                            $va_bef['IS_CHECK'] = $is_check;
                            $va_bef['UNIT'] = isset($dataForm['unit_'.$va_bef['BE_CODE']])? $dataForm['unit_'.$va_bef['BE_CODE']]: $va_bef['BE_MODE'];
                            $va_bef['BENEFIT_AMOUNT'] = isset($dataForm['benefit_amount_'.$va_bef['BE_CODE']])? $dataForm['benefit_amount_'.$va_bef['BE_CODE']]: $va_bef['BENEFIT_AMOUNT'];
                            $va_bef['FEES'] = isset($dataForm['fees_'.$va_bef['BE_CODE']])? $dataForm['fees_'.$va_bef['BE_CODE']]: $va_bef['FEES'];
                            $va_bef['PRODUCT_NAME'] = '';
                            $va_bef['PACK_NAME'] = '';
                            $va_bef['BENEFIT_NAME'] = '';
                            $product_code = $va_bef['PRODUCT_CODE'];
                            $pack_code = $va_bef['PACK_CODE'];
                        }
                    }
                    if($isEdit == STATUS_INT_MOT){
                        $dataUpdate['PRODUCT_CODE'] = $product_code;
                        $dataUpdate['PACK_CODE'] = $pack_code;
                        $dataUpdate['LIST_BENEFITS'] = $listBenefits;
                        $updateAction = $this->modelObj->editBenefits($dataUpdate);
                        $isEdit = isset($updateAction['Success'])?$updateAction['Success']:STATUS_INT_KHONG;
                    }

                    if ($isEdit > 0) {
                        $this->_outDataView($request, []);
                        $arrAjax['success'] = 1;
                        $arrAjax['html'] = '';
                        $arrAjax['loadPage'] = 1;
                        $arrAjax['divShowInfor'] = '';
                    } else {
                        $arrAjax = returnError('Không có dữ liệu cập nhật');
                    }
                    break;
                default:
                    break;
            }
            return Response::json($arrAjax);
        }
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW, PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $html = '';
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            if (is_array($html)) {
                return Response::json($html);
            } else {
                $success = STATUS_INT_MOT;
            }
        }
        $arrAjax = array('success' => $success, 'html' => $html);
        return Response::json($arrAjax);
    }

    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _ajaxGetItemOther($request)
    {
        $data = $inforItem = $listProductAssign = $listPackagesAssign = $listChannelAssign = $listPackagesFees = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = (int)isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $obj_id = $request['objectId'];
        $templateOut = $this->templateRoot . 'component._formProductAssign';
        //data chính
        $dataPrimary = isset($arrKey['dataPrimary']) ? (object)$arrKey['dataPrimary'] : false;
        $product_code = isset($dataPrimary->PRODUCT_CODE)? $dataPrimary->PRODUCT_CODE:'';
        $pack_code = isset($dataPrimary->PACK_CODE) ? $dataPrimary->PACK_CODE : '';
        switch ($typeTab) {
            //Package Assign
            case $this->tabOtherItem1:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] :false;
                    $this->arrPackageCore= app(PackageCore::class)->getOptionPackage($product_code);
                    $templateOut = $this->templateRoot . 'component._formPackagesAssign';
                } else {//get list danh sách item other
                    $inforProduct = $this->modelProduct->getInforProduct($product_code);
                    if (isset($inforProduct['Data']) && !empty($inforProduct['Data'])) {
                        $product = $inforProduct['Data'];
                        //$listPackagesAssign = isset($product[2]) ? $product[2] : false;//Pack assign
                        $packagesAssign = isset($product[2]) ? $product[2] : false;//Pack assign
                        if(!empty($packagesAssign)){
                            foreach ($packagesAssign as $k =>$packA){
                                if($packA->PACK_CODE == $pack_code){
                                    $listPackagesAssign[] = $packA;
                                }
                            }
                        }
                    }
                    $templateOut = $this->templateRoot . 'component._listPackagesAssign';
                    $data = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $optionPackage = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrPackageCore, isset($dataPrimary->PACK_CODE) ? $dataPrimary->PACK_CODE : '');
                $optionCategory = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrCategory, isset($dataPrimary->CATEGORY) ? $dataPrimary->CATEGORY : '');
                $this->dataOutItem = ['optionPackage'=>$optionPackage,'optionCategory'=>$optionCategory];
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$data);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'dataOther' => $inforItem,
                'listProductAssign' => $listProductAssign,
                'listPackagesAssign' => $listPackagesAssign,
                'listChannelAssign' => $listChannelAssign,
                'listPackagesFees' => $listPackagesFees,
                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'obj_id' => $obj_id,
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    private function _updateDataRelation($dataForm, $typeTabAction)
    {
        $active = (int)$dataForm['ACTION_FORM'];
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                $result = $this->modelProduct->editPackagesAssign($dataForm, ($active > 0) ? 'EDIT' : 'ADD');
                break;
            default:
                break;
        }
        if (isset($result['Success']) && $result['Success'] == STATUS_INT_MOT) {
            //lấy lại dữ liệu vừa sửa
            $dataInput['type'] = $dataForm['typeTabAction'];
            $dataInput['isDetail'] = STATUS_INT_KHONG;
            $dataInput['itemOther'] = $dataForm;
            $dataInput['arrKey']['dataPrimary'] = json_decode($dataForm['dataPrimary']);//data cha
            $requestLoad['dataInput'] = json_encode($dataInput);

            $requestLoad['objectId'] = $dataForm['objectId'];
            $requestLoad['divShowId'] = $dataForm['divShowIdAction'];
            $requestLoad['formName'] = $dataForm['formName'];

            $html = $this->_ajaxGetItemOther($requestLoad);
            $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);

            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }
}
