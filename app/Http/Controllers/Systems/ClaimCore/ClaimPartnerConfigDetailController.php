<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\ClaimCore;

use App\Http\Controllers\BaseAdminController;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Models\System\ClaimCore;
use App\Models\System\PackageCore;
use App\Models\System\ProductCore;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class ClaimPartnerConfigDetailController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrPackageCore = array();
    private $arrProductCore = array();

    private $templateRoot = DIR_PRO_SYSTEM.'/ClaimCore/'. '.partnerConfigDetail.';
    private $routerIndex = 'partnerConfigDetail.index';
    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ClaimCore();
        $this->arrProductCore= app(ProductCore::class)->getOptionProduct();
        $this->arrPackageCore= app(PackageCore::class)->getOptionPackage();
    }

    private function _outDataView($request, $data)
    {
        $optionProductCore = FunctionLib::getOption([DEFINE_ALL => '---Chọn---'] +$this->arrProductCore, isset($data['PRODUCT_CODE']) ? $data['PRODUCT_CODE'] : DEFINE_ALL);
        $optionPackageCore = FunctionLib::getOption([DEFINE_ALL => '---Chọn---'] +$this->arrPackageCore, isset($data['PACK_CODE']) ? $data['PACK_CODE'] : DEFINE_ALL);

        $formId = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = $request['objectId'] ?? 0;
        return $this->dataOutCommon = [
            'optionProductCore' => $optionProductCore,
            'optionPackageCore' => $optionPackageCore,

            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('partnerConfigDetail.ajaxGetItem'),
            'url_action' => URL::route('partnerConfigDetail.ajaxPostItem'),

            'form_id' => $formId,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
        ];
    }

    private function _validFormData($data = array())
    {
        if (!empty($data)) {
            if (isset($data['depart_name']) && trim($data['depart_name']) == '') {
                $this->error[] = 'Tên depart không được bỏ trống';
            }
            if (isset($data['depart_alias']) && trim($data['depart_alias']) == '') {
                $this->error[] = 'Tên viết tắt không được bỏ trống';
            }
        }
        return true;
    }

    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Config Detail bồi thường';
        $limit = CGlobal::number_show_10;
        $page_no = (int)Request::get('page_no', 1);
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $result = $this->modelObj->searchConfigDetail($search);
        //myDebug($result);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }
        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'total' => $total,
            'search' => $search,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD,PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $oject_id = $request['objectId'] ?? 0;
        $data = [];
        $is_copy = STATUS_INT_KHONG;
        if ($oject_id > 0) {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $data = isset($dataInput->item) ? $dataInput->item : false;
            $is_copy = isset($dataInput->is_copy) ? $dataInput->is_copy : STATUS_INT_KHONG;
        }

        $this->_outDataView($request, (array)$data);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'data' => $data,
                'is_copy' => $is_copy,
            ], $this->dataOutCommon))->render();
        $arrAjax = array('success' => 1, 'html' => $html);
        return Response::json($arrAjax);
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD,PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];
        $id = (int)$dataForm['objectId'] ?? 0;
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }

        if ($this->_validFormData($dataForm) && empty($this->error)) {
            $dataForm['BC_ID'] = ($id > 0) ? $id : '';// ID tự tăng
            $result = $this->modelObj->editItemConfigDetail($dataForm, ($id > 0) ? 'EDIT' : 'ADD');
            if ($result['Success'] == STATUS_INT_MOT) {
                return Response::json(['loadPage'=>STATUS_INT_MOT,'success'=>1]);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

}
