<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\ApiHdiCore;

use App\Http\Controllers\BaseAdminController;
use App\Models\System\ApiHdiCore;
use App\Models\System\DatabaseConnection;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Services\ServiceCurl;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class ActionApiController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;

    private $arrStatus = array();
    private $arrTypeData = array();
    private $arrDatabase = array();
    private $arrPartner = array();

    private $templateRoot = DIR_PRO_SYSTEM . '/ApiHdiCore/' . '.actionApi.';

    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';
    private $routerIndex = 'actionCodeApi.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new ApiHdiCore();
        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
        $this->arrTypeData = $this->getArrOptionTypeDefine(DEFINE_TYPE_DATABASE);
        $this->arrDatabase = $this->modelObj->getOptionDatabase();
        $this->arrPartner = $this->modelObj->getOptionPartner();
    }

    private function _outDataView($request, $data)
    {
        $optionDatabase = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrDatabase, isset($data['DATABASE_CODE']) ? $data['DATABASE_CODE'] : '');
        $optionPartner = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrPartner, isset($data['PARTNER_CODE']) ? $data['PARTNER_CODE'] : '');
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = (isset($request['objectId']) && trim($request['objectId']) != '0') ? 1 : 0;

        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionDatabase' => $optionDatabase,
            'optionPartner' => $optionPartner,

            'arrStatus' => $this->arrStatus,
            'arrTypeData' => $this->arrTypeData,
            'arrDatabase' => $this->arrDatabase,
            'arrPartner' => $this->arrPartner,

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,

            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('actionCodeApi.ajaxGetItem'),
            'urlPostItem' => URL::route('actionCodeApi.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('actionCodeApi.ajaxGetData'),
            'urlActionOtherItem' => URL::route('actionCodeApi.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',
            'urlDeleteItem' => '',
            'urlAjaxChangePass' => '',
        ];
    }

    private function _validformdata($id = 0, &$data = array())
    {
        if (!empty($data)) {
            $arrProcIn = $arrProcOut = [];
            foreach ($data as $key => $value){
                for ($number = 0; $number <= 20; $number++) {
                    if($key === 'KEY_IN_'.$number && trim($value) != ''){
                        $arrProcIn[$value] = $data['VALUE_IN_'.$number];
                    }
                    if($key === 'KEY_OUT_'.$number && trim($value) != ''){
                        $arrProcOut[$value] = $data['VALUE_OUT_'.$number];
                    }
                }
            }
            if(!empty($arrProcIn) && !empty($arrProcOut)){
                $data['PROC_IN'] = json_encode($arrProcIn);
                $data['PROC_OUT'] = json_encode($arrProcOut);
            }else{
                $this->error[] = 'Bạn chưa nhập input param';
            }
        }
        return true;
    }

    /*********************************************************************************************************
     * Danh mục tổ chức: APIS
     *********************************************************************************************************/
    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Quản lý Api';
        $page_no = (int)Request::get('page_no', 1);

        $search['IS_ACTIVE'] = addslashes(Request::get('IS_ACTIVE', STATUS_INT_MOT));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['p_is_active'] = $search['IS_ACTIVE'];
        $search['page_no'] = $page_no;

        $dataList = [];
        $total = 0;
        $limit = CGlobal::number_show_10;
        $result = $this->modelObj->searchActionCode($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'search' => $search,
            'total' => $total,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,

        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    private function _getInfoItem($request)
    {
        $objectId = $request['objectId'] ?? '';
        $dataPrimary = $listActionEnv = $listActionSystem = $listActionPublic = $listActionPartner = $arrProcIn = $arrProcOut = [];
        if (trim($objectId) != '' || trim($objectId) != '0') {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $item_code = isset($dataInput->item) ? $dataInput->item->ID : '';
            $action_code = isset($dataInput->item) ? $dataInput->item->ACTION_CODE : '';

            $inforPartner = $this->modelObj->getInforActionCode($action_code);

            //lay dư liệu tab default
            if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                $partner = $inforPartner['Data'];
                $dataPrimary = isset($partner[0][0]) ? $partner[0][0] : false;//OAPI_ACTION
                $listActionEnv = isset($partner[1]) ? $partner[1] : false;//OAPI_ACTION_ENV
                $listActionSystem = isset($partner[2]) ? $partner[2] : false;//OAPI_ACTION_SYSTEM
                $listActionPartner = isset($partner[3]) ? $partner[3] : false;//OAPI_PARTNER_GRANT
                $listActionPublic = isset($partner[4]) ? $partner[4] : false;//OAPI_ACTION_PUBLIC

                $arrProcIn = isset($dataPrimary->PROC_IN) ? json_decode($dataPrimary->PROC_IN) : $arrProcIn;
                $arrProcOut = isset($dataPrimary->PROC_OUT) ? json_decode($dataPrimary->PROC_OUT) : $arrProcOut;

                $this->dataOutItem = [
                    'actionEdit' => isset($dataPrimary->ACTION_CODE) ? STATUS_INT_MOT : STATUS_INT_KHONG, //0: thêm mới, 1: edit
                    'formNameOther' => $this->tabOtherItem1,
                    'dataPrimary' => $dataPrimary,
                    'listActionEnv' => $listActionEnv,
                    'listActionSystem' => $listActionSystem,
                    'listActionPartner' => $listActionPartner,
                    'listActionPublic' => $listActionPublic,

                    'typeTab' => $this->tabOtherItem1,
                    'obj_id' => $item_code,
                    'divShowId' => $this->tabOtherItem1,
                ];
            }
        }
        $this->_outDataView($request, (array)$dataPrimary);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'arrProcIn' => $arrProcIn,
                'arrProcOut' => $arrProcOut,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];
        $id = (isset($dataForm['objectId'])) ? (int)trim($dataForm['objectId']) : (int)trim($dataForm['objectId']);
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        if ($this->_validFormData($id, $dataForm) && empty($this->error)) {
            $result = $this->modelObj->editActionCode($dataForm);
            if ($result['Success'] == STATUS_INT_MOT) {
                //EDIT: lấy lại dữ liệu đã cập nhật để hiển thị lại
                if ($id > 0) {
                    $request = $dataForm;
                    $request['formName'] = $dataForm['formName'];
                    $this->_outDataView($request, $dataForm);
                    $html = View::make($this->templateRoot . 'component._detailFormItem')
                        ->with(array_merge([
                            'dataPrimary' => (object)$dataForm,
                            'objectId' => $id,
                            'arrProcIn' => (isset($dataForm['PROC_IN']) && trim($dataForm['PROC_IN']))?json_decode($dataForm['PROC_IN']):[],
                            'arrProcOut' => (isset($dataForm['PROC_OUT']) && trim($dataForm['PROC_OUT']))?json_decode($dataForm['PROC_OUT']):[],
                        ], $this->dataOutCommon))->render();
                    $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
                    $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
                } //ADD: thêm mới thì load lại dư liệu để nhập các thông tin khác
                else {
                    $action_code = isset($result['Data'][0]->ACTION_CODE) ? $result['Data'][0]->ACTION_CODE : '';
                    $dataForm['ACTION_CODE'] = $action_code;
                    $dataForm['ID'] = 1;
                    $request['objectId'] = 1;
                    $request['divShowInfor'] = 'divDetailItem';
                    $request['dataInput'] = json_encode(['item' => $dataForm]);
                    $arrAjax = $this->_getInfoItem($request);
                }
                return Response::json($arrAjax);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _ajaxGetItemOther($request)
    {
        $data = $inforItem = $listActionEnv = $listActionSystem = $listActionPartner = $listActionPublic = [];
        $formNameOther = isset($request['formName']) ? $request['formName'] : 'formName';
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $typeTab = isset($dataInput['type']) ? $dataInput['type'] : '';
        $itemId = (int)isset($dataInput['itemId']) ? $dataInput['itemId'] : '';
        $isDetail = isset($dataInput['isDetail']) ? $dataInput['isDetail'] : STATUS_INT_KHONG;
        $arrKey = isset($dataInput['arrKey']) ? $dataInput['arrKey'] : [];

        $actionEdit = STATUS_INT_KHONG;
        $templateOut = $this->templateRoot . 'component._formProductAssign';
        //data chính
        $dataPrimary = isset($arrKey['dataPrimary']) ? (object)$arrKey['dataPrimary'] : false;
        $action_code = isset($dataPrimary->ACTION_CODE) ? $dataPrimary->ACTION_CODE : '';
        //myDebug($request);
        switch ($typeTab) {
            //OAPI_ACTION_ENV
            case $this->tabOtherItem1:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] : false;
                    $templateOut = $this->templateRoot . 'component._formActionEnv';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforActionCode($action_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listActionEnv = isset($partner[1]) ? $partner[1] : false;//$partner secret
                    }
                    $templateOut = $this->templateRoot . 'component._listActionEnv';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $this->dataOutItem = [];
                break;
            //OAPI_ACTION_SYSTEM
            case $this->tabOtherItem2:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] : false;
                    $templateOut = $this->templateRoot . 'component._formActionSystem';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforActionCode($action_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listActionSystem = isset($partner[2]) ? $partner[2] : false;
                    }
                    $templateOut = $this->templateRoot . 'component._listActionSystem';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $this->dataOutItem = [];
                break;
            //OAPI_PARTNER_GRANT
            case $this->tabOtherItem3:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] : false;
                    $templateOut = $this->templateRoot . 'component._formActionPartner';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforActionCode($action_code);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listActionPartner = isset($partner[3]) ? $partner[3] : false;
                    }
                    $templateOut = $this->templateRoot . 'component._listActionPartner';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $this->dataOutItem = [];
                break;
            //OAPI_ACTION_PUBLIC
            case $this->tabOtherItem4:
                if ($isDetail == STATUS_INT_MOT) {//chi tiết item
                    $inforItem = isset($dataInput['itemInfor']) ? (object)$dataInput['itemInfor'] : false;
                    $templateOut = $this->templateRoot . 'component._formActionPublic';
                } else {//get list danh sách item other
                    $inforPartner = $this->modelObj->getInforActionCode($action_code);
                    myDebug($inforPartner);
                    if (isset($inforPartner['Data']) && !empty($inforPartner['Data'])) {
                        $partner = $inforPartner['Data'];
                        $listActionPublic = isset($partner[4]) ? $partner[4] : false;//OAPI_ACTION_PUBLIC
                    }
                    $templateOut = $this->templateRoot . 'component._listActionPublic';
                    $inforItem = (object)$dataInput['itemOther'];
                }
                $actionEdit = ($itemId > 0) ? STATUS_INT_MOT : STATUS_INT_KHONG;
                $this->dataOutItem = [];
                break;
            default:
                break;
        }
        $this->_outDataView($request, (array)$inforItem);
        $html = View::make($templateOut)
            ->with(array_merge([
                'dataPrimary' => $dataPrimary,
                'dataOther' => $inforItem,
                'listActionEnv' => $listActionEnv,
                'listActionSystem' => $listActionSystem,
                'listActionPartner' => $listActionPartner,
                'listActionPublic' => $listActionPublic,

                'actionEdit' => $actionEdit,//0: thêm mới, 1: edit
                'itemId' => $itemId,
                'formNameOther' => $formNameOther,
                'typeTab' => $typeTab,
                'divShowId' => $typeTab,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        return $html;
    }

    private function _updateDataRelation($dataForm, $typeTabAction)
    {
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                $result = $this->modelObj->editActionRelation($dataForm, 'ACTION_ENV');
                break;
            case $this->tabOtherItem2:
                $result = $this->modelObj->editActionRelation($dataForm, 'ACTION_SYSTEM');
                break;
            case $this->tabOtherItem3:
                $result = $this->modelObj->editActionRelation($dataForm, 'PARTNER_GRANT');
                break;
            case $this->tabOtherItem4:
                $result = $this->modelObj->editActionRelation($dataForm, 'ACTION_PUBLIC');
                break;
            default:
                break;
        }

        if ($result['Success'] == STATUS_INT_MOT) {
            //lấy lại dữ liệu vừa sửa
            $dataInput['type'] = $dataForm['typeTabAction'];
            $dataInput['isDetail'] = STATUS_INT_KHONG;
            $dataInput['itemOther'] = $dataForm;
            $dataInput['itemInfor'] = $dataForm;
            $dataInput['arrKey']['dataPrimary'] = json_decode($dataForm['dataPrimary']);//data cha
            $requestLoad['dataInput'] = json_encode($dataInput);

            // $requestLoad['objectId'] = $dataForm['objectId'];
            $requestLoad['divShowId'] = $dataForm['divShowIdAction'];
            $requestLoad['formName'] = $dataForm['formName'];

            $html = $this->_ajaxGetItemOther($requestLoad);
            $arrAjax = array('success' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => $html);

            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $arrAjax = array('success' => STATUS_INT_KHONG, 'loadPage' => STATUS_INT_KHONG, 'message' => 'Có lỗi thao tác!');
        $success = STATUS_INT_KHONG;
        if (trim($functionAction) != '') {
            $html = $this->$functionAction($dataRequest);
            if (is_array($html)) {
                $arrAjax = $html;
            } else {
                $success = STATUS_INT_MOT;
                $arrAjax = array('success' => $success, 'html' => $html);
            }
        }
        return Response::json($arrAjax);
    }

    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        if (empty($dataRequest)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        //check form with file upload
        $typeTabAction = isset($dataRequest['typeTabAction']) ? $dataRequest['typeTabAction'] : $dataForm['typeTabAction'];
        $dataForm = isset($dataRequest['typeTabAction']) ? $dataRequest : $dataForm;
        $active = (int)$dataForm['ACTION_FORM'];

        if ($this->_validFormDataRelation($typeTabAction, $active, $dataForm) && empty($this->error)) {
            $actionUpdate = $this->_updateDataRelation($dataForm, $typeTabAction);
            return $actionUpdate;
        } else {
            return Response::json(returnError($this->error));
        }
    }

    private function _validFormDataRelation($typeTabAction = '', $active = STATUS_INT_KHONG, &$data = array())
    {
        switch ($typeTabAction) {
            case $this->tabOtherItem1:
                if (!empty($data)) {
                    if (isset($data['BIRTHDAY']) && trim($data['BIRTHDAY']) == '') {
                        $this->error[] = 'Ngày sinh không được bỏ trống';
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

    private function _ajaxClearCacheActionCode($request)
    {
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput'], true) : false;
        $action_code = isset($dataInput['action_code']) ? trim($dataInput['action_code']) : '';
        $env_run = isset($dataInput['env_run']) ? trim($dataInput['env_run']) : 'DEV';

        $arrAjax = array('success' => STATUS_INT_KHONG, 'loadPage' => STATUS_INT_KHONG, 'message' => 'Có lỗi chưa reset được Action Code');
        if (trim($action_code) != '') {
            switch ($env_run){
                case 'DEV':
                    $url_clear = 'https://dev-newopenapi.hdinsurance.com.vn/v1/cache/refresh';//dev
                    break;
                case 'UAT':
                    $url_clear = 'https://uat-newopenapi.hdinsurance.com.vn/v1/cache/refresh';
                    break;
                case 'LIVE':
                    $url_clear = 'https://openapi.hdinsurance.com.vn/v1/cache/refresh';
                    break;
                default:
                    break;
            }

            $dataRequest['action_code'] = $action_code;
            $curl = new ServiceCurl();
            $respo = $curl->postUrl($url_clear, $dataRequest);
            //myDebug($respo);
            if (isset($respo->success) && $respo->success == STATUS_INT_MOT) {
                $templateOut = $this->templateRoot . 'component._formRefresh';
                $html = View::make($templateOut)
                    ->with(array_merge([
                        'dataCache' => $respo,
                    ], $this->dataOutCommon, $this->dataOutItem))->render();
                $arrAjax['success'] = STATUS_INT_MOT;
                $arrAjax['html'] = $html;
            } else {
                $arrAjax['message'] = isset($respo->error) ? $respo->error : 'Có lỗi xảy ra! Hãy thử lại';
            }
        }
        return $arrAjax;
    }
}
