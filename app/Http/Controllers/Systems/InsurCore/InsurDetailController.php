<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 01/2017
* @Version   : 1.0
*/

namespace App\Http\Controllers\Systems\InsurCore;

use App\Http\Controllers\BaseAdminController;
use App\Library\AdminFunction\FunctionLib;
use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Define;
use App\Library\AdminFunction\Pagging;
use App\Models\System\InsurCore;
use App\Models\System\Organization;
use App\Models\System\ProductCore;
use App\Models\System\Province;
use App\Services\ServiceCommon;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class InsurDetailController extends BaseAdminController
{
    private $error = array();
    private $dataOutCommon = array();
    private $dataOutItem = array();
    private $pageTitle = '';
    private $modelObj = false;
    private $modelProduct = false;

    private $arrStatus = array();
    private $arrStatusContract = array();
    private $arrOrg = array();
    private $arrGender = array();
    private $arrRegion = array();
    private $arrRelationship = array();

    private $arrProject = array();
    private $arrDistrict = array();
    private $arrWard = array();
    private $arrProvince = array();
    private $arrProductCore = array();
    private $tabOtherItem1 = 'tabOtherItem1';
    private $tabOtherItem2 = 'tabOtherItem2';
    private $tabOtherItem3 = 'tabOtherItem3';
    private $tabOtherItem4 = 'tabOtherItem4';

    private $templateRoot = DIR_PRO_SYSTEM . '/InsurCore/' . '.insurDetail.';
    private $routerIndex = 'insurDetail.index';

    public function __construct()
    {
        parent::__construct();
        $this->modelObj = new InsurCore();
        $this->modelProduct = new ProductCore();

        $this->arrStatus = $this->getArrOptionTypeDefine(DEFINE_STATUS);
        $this->arrStatusContract = $this->getArrOptionTypeDefine(DEFINE_CONTRACT_STATUS);
        $this->arrRelationship = $this->getArrOptionTypeDefine(DEFINE_MOI_QUAN_HE);
        $this->arrGender = $this->getArrOptionTypeDefine(DEFINE_GENDER);
        $this->arrRegion = $this->getArrOptionTypeDefine(DEFINE_PHAM_VI_DIA_LY);

        $this->arrProductCore = app(ProductCore::class)->getOptionProduct();
        $this->arrOrg = app(Organization::class)->getArrOptionOrg();
        $this->arrProvince = app(Province::class)->getOptionProvince();

    }

    private function _outDataView($request, $data)
    {
        $optionStatus = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatus, isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : STATUS_INT_MOT);
        $optionStatusContract = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrStatusContract, isset($data['STATUS']) ? $data['STATUS'] : STATUS_INT_MOT);
        $optionProductCore = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProductCore, isset($data['PRODUCT_CODE']) ? $data['PRODUCT_CODE'] : PRODUCT_CODE_XCG_TNDSBB_NEW);
        $optionOrgCode = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrOrg, isset($data['ORG_CODE']) ? $data['ORG_CODE'] : '');
        $optionRelationship = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrRelationship, isset($data['RELATIONSHIP']) ? $data['RELATIONSHIP'] : '');
        $optionGender = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrGender, isset($data['GENDER']) ? $data['GENDER'] : '');
        $optionRegion = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrRegion, isset($data['REGION']) ? $data['REGION'] : '');

        $provice_code = isset($data['PROVINCE']) ? $data['PROVINCE'] : '';
        $district_code = isset($data['DISTRICT']) ? $data['DISTRICT'] : '';
        $optionProvince = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrProvince, $provice_code);
        $this->arrDistrict = trim($provice_code) != ''?app(Province::class)->getOptionDistrict($provice_code): $this->arrDistrict;
        $this->arrWard = trim($district_code) != ''?app(Province::class)->getOptionWard($district_code):$this->arrWard;
        $optionDistrict = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrDistrict, $district_code);
        $optionWard = FunctionLib::getOption(['' => '---Chọn---'] + $this->arrWard, isset($data['WARDS']) ? $data['WARDS'] : '');

        $formName = $request['formName'] ?? 'formPopup';
        $titlePopup = $request['titlePopup'] ?? 'Thông tin chung';
        $objectId = $request['objectId'] ?? 0;
        return $this->dataOutCommon = [
            'optionStatus' => $optionStatus,
            'optionStatusContract' => $optionStatusContract,

            'optionProductCore' => $optionProductCore,
            'optionOrgCode' => $optionOrgCode,
            'optionRelationship' => $optionRelationship,
            'optionGender' => $optionGender,
            'optionRegion' => $optionRegion,
            'optionProvince' => $optionProvince,
            'optionDistrict' => $optionDistrict,
            'optionWard' => $optionWard,

            'arrProject' => $this->arrProject,
            'arrStatus' => $this->arrStatus,
            'arrStatusContract' => $this->arrStatusContract,
            'arrRelationship' => $this->arrRelationship,
            'arrGender' => $this->arrGender,
            'arrProductResign' => [PRODUCT_CODE_XCG_TNDSBB_NEW, PRODUCT_CODE_XCG_TNDSBB],

            'urlIndex' => URL::route($this->routerIndex),
            'urlGetItem' => URL::route('insurDetail.ajaxGetItem'),
            'urlPostItem' => URL::route('insurDetail.ajaxPostItem'),
            'urlAjaxGetData' => URL::route('insurDetail.ajaxGetData'),
            'urlActionOtherItem' => URL::route('insurDetail.ajaxUpdateRelation'),
            'functionAction' => '_ajaxGetItemOther',
            'urlServiceFile' => Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/',

            'formName' => $formName,
            'title_popup' => $titlePopup,
            'objectId' => $objectId,
            'tabOtherItem1' => $this->tabOtherItem1,
            'tabOtherItem2' => $this->tabOtherItem2,
            'tabOtherItem3' => $this->tabOtherItem3,
            'tabOtherItem4' => $this->tabOtherItem4,
        ];
    }

    private function _validFormData($data = array())
    {
        if (!empty($data)) {
            if (isset($data['depart_name']) && trim($data['depart_name']) == '') {
                $this->error[] = 'Tên depart không được bỏ trống';
            }
            if (isset($data['depart_alias']) && trim($data['depart_alias']) == '') {
                $this->error[] = 'Tên viết tắt không được bỏ trống';
            }
        }
        return true;
    }

    public function index()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW])) {
            return Redirect::route('admin.dashboard', array('error' => Define::ERROR_PERMISSION));
        }
        $this->pageTitle = CGlobal::$pageAdminTitle = 'Danh sách đơn bảo hiểm';
        $limit = CGlobal::number_show_10;
        $page_no = (int)Request::get('page_no', 1);
        $search['p_from_date'] = trim(addslashes(Request::get('p_from_date', '')));
        $search['p_to_date'] = trim(addslashes(Request::get('p_to_date', '')));
        $search['p_from_date'] = ($search['p_from_date'] != '') ? $search['p_from_date'] : date('d/m/Y', strtotime(Carbon::now()->startOfMonth()));
        $search['p_to_date'] = ($search['p_to_date'] != '') ? $search['p_to_date'] : date('d/m/Y', strtotime(Carbon::tomorrow()));
        $search['p_keyword'] = addslashes(Request::get('p_keyword', ''));
        $search['page_no'] = $page_no;

        $search['PRODUCT_CODE'] = addslashes(Request::get('PRODUCT_CODE', ''));
        $search['PRODUCT_CODE'] = (trim($search['PRODUCT_CODE']) != '')?$search['PRODUCT_CODE']: PRODUCT_CODE_XCG_TNDSBB_NEW;
        $search['p_product_code'] = $search['PRODUCT_CODE'];

        $search['STATUS'] = addslashes(Request::get('STATUS', ''));
        $search['p_is_active'] = $search['STATUS'];

        $dataList = [];
        $total = 0;
        $result = $this->modelObj->searchInsurDetail($search);

        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? $dataList;
            $total = $result['Data']['total'] ?? $total;
        }

        $paging = $total > 0 ? Pagging::getNewPager(3, $page_no, $total, $limit, $search) : '';

        $this->_outDataView($_GET, $search);
        return view($this->templateRoot . 'viewIndex', array_merge([
            'data' => $dataList,
            'total' => $total,
            'search' => $search,
            'stt' => ($page_no - 1) * $limit,
            'paging' => $paging,
            'pageTitle' => $this->pageTitle,
        ], $this->dataOutCommon));
    }

    public function ajaxGetItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $request = $_GET;
        $arrAjax = $this->_getInfoItem($request);
        return Response::json($arrAjax);
    }

    public function _getInfoItem($request)
    {
        $oject_id = $request['objectId'] ?? 13;//13 edit
        $data = $extendInsur = $listCertificate = [];

        if ($oject_id > 0) {
            $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
            $dataItem = isset($dataInput->item) ? $dataInput->item : false;

            $requestSearch['p_product_code'] = isset($dataItem->PRODUCT_CODE) ? $dataItem->PRODUCT_CODE : '';
            $requestSearch['p_certificate_no'] = isset($dataItem->CERTIFICATE_NO) ? $dataItem->CERTIFICATE_NO : '';
            $requestSearch['p_category'] = isset($dataItem->CATEGORY) ? $dataItem->CATEGORY : '';
            $requestSearch['p_ref_id'] = isset($dataItem->REF_ID) ? $dataItem->REF_ID : '';
            $requestSearch['p_is_edit'] = isset($dataItem->IS_EDIT) ? $dataItem->IS_EDIT : STATUS_INT_KHONG;//0: chưa sửa, 1: đã sửa

            $inforDetail = $this->modelObj->getInforInsurDetail($requestSearch);
            if (isset($inforDetail['Data']) && !empty($inforDetail['Data'])) {
                $insurDetal = $inforDetail['Data'];
                $data = isset($insurDetal[0][0]) ? $insurDetal[0][0] : false;//detail
                $extendInsur = isset($insurDetal[1][0]) ? $insurDetal[1][0] : false;//certificate
            }
        }
        $this->dataOutItem = [
            'actionEdit' => STATUS_INT_MOT, //0: thêm mới, 1: edit
            'formNameOther' => $this->tabOtherItem1,
            'dataOther' => [],
            'extendInsur' => $extendInsur,
            'typeTab' => $this->tabOtherItem1,
            'objectId' => $oject_id,
            'divShowId' => $this->tabOtherItem1,
        ];

        $this->_outDataView($request, (array)$data);
        $html = View::make($this->templateRoot . 'component.popupDetail')
            ->with(array_merge([
                'dataPrimary' => $data,
            ], $this->dataOutCommon, $this->dataOutItem))->render();
        $divShowInfor = isset($request['divShowInfor']) ? $request['divShowInfor'] : 'formShowEditSuccess';//div show infor item
        $arrAjax = array('success' => 1, 'html' => $html, 'divShowInfor' => $divShowInfor);
        return $arrAjax;
    }

    public function ajaxPostItem()
    {
        if (!$this->checkMultiPermiss([PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(viewLanguage('Bạn không có quyền thao tác.')));
        }
        $dataRequest = $_POST;
        $dataForm = $dataRequest['dataForm'] ?? [];

        $id = (int)$dataForm['objectId'] ?? 0;
        if (empty($dataForm)) {
            return Response::json(returnError(viewLanguage('Dữ liệu đầu vào không đúng')));
        }
        if ($this->_validFormData($dataForm) && empty($this->error)) {
            $result = $this->modelObj->editInsurDetail($dataForm, 'EDIT_DETAIL');
            if ($result['Success'] == STATUS_INT_MOT) {
                return Response::json(['loadPage' => STATUS_INT_MOT, 'success' => 1]);
            } else {
                return Response::json(returnError($result['Message']));
            }
        } else {
            return Response::json(returnError($this->error));
        }
    }

    /**************************************************
     * Get data Active
     * ************************************************/
    public function ajaxUpdateRelation()
    {
        if (!$this->checkMultiPermiss([ PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }
        $request = $_POST;
        $arrAjax = array('success' => 0, 'html' => '', 'msg' => '');
        $actionUpdate = 'actionUpdate';
        $dataForm = isset($request['dataForm']) ? $request['dataForm'] : [];
        $actionUpdate = isset($dataForm['actionUpdate']) ? $dataForm['actionUpdate'] : (isset($request['actionUpdate']) ? $request['actionUpdate'] : $actionUpdate);

        $edit = $this->_updateDataRelation($dataForm, $actionUpdate);
        return $edit;
    }

    public function ajaxGetData()
    {
        if (!$this->checkMultiPermiss([PERMISS_VIEW, PERMISS_ADD, PERMISS_EDIT], $this->routerIndex)) {
            return Response::json(returnError(1));
        }
        $dataRequest = $_POST;
        $functionAction = $dataRequest['functionAction'] ?? '';
        $result = [];
        $success = STATUS_INT_KHONG;
        $message = 'Có lỗi thao tác';
        $arrAjax = ['success' => $success, 'html' => $result, 'message' => $message];
        if (trim($functionAction) != '') {
            $result = $this->$functionAction($dataRequest);
            if (is_array($result)) {
                $arrAjax = array_merge($arrAjax,$result);
            }
        }
        return Response::json($arrAjax);
    }

    /*********************************************************************************************************
     * Function ký số lại
     *********************************************************************************************************/
    private function _resignCertificate($request){
        $dataInput = isset($request['dataInput']) ? json_decode($request['dataInput']) : false;
        $dataDetail = isset($dataInput->item) ? $dataInput->item: [];
        $arrAjax = ['success' => STATUS_INT_KHONG, 'message' => 'Không thành công! Ký số lỗi.'];
        if(isset($dataDetail->CERTIFICATE_NO) && trim($dataDetail->CERTIFICATE_NO) != '' && isset($dataDetail->PRODUCT_CODE) && trim($dataDetail->PRODUCT_CODE) != ''){
            $dataRequest['CERTIFICATE_NO'] = $dataDetail->CERTIFICATE_NO;
            $dataRequest['PRODUCT_CODE'] = $dataDetail->PRODUCT_CODE;

            $service = new ServiceCommon();
            $arrAjax = $service->resignCertificate($dataRequest);
        }
        return $arrAjax;
    }
    /*********************************************************************************************************
     * Các quan hệ của APIS tab
     *********************************************************************************************************/
    private function _updateDataRelation($dataForm, $actionUpdate)
    {
        $result = returnError('Không đúng thao tác! Hãy thử lại');
        switch ($actionUpdate) {
            case 'updateVehicleInfo':
                $result = $this->modelObj->editVehicleInfo($dataForm, 'EDIT_VEHICLE');
                break;
            default:
                break;
        }
        if (isset($result['Success']) && $result['Success'] == STATUS_INT_MOT) {
            //lấy lại dữ liệu vừa sửa
            $dataInput['type'] = $dataForm['typeTabAction'];
            $dataInput['isDetail'] = STATUS_INT_KHONG;
            $dataInput['itemOther'] = $dataForm;
            $dataInput['arrKey']['dataPrimary'] = json_decode($dataForm['dataPrimary']);//data cha
            $requestLoad['dataInput'] = json_encode($dataInput);

            $requestLoad['objectId'] = $dataForm['objectId'];
            $requestLoad['divShowId'] = $dataForm['divShowIdAction'];
            $requestLoad['formName'] = $dataForm['formName'];

            $arrAjax = array('success' => 1,'loadPage' => 1, 'message' => 'Successfully', 'divShowInfor' => $requestLoad['divShowId'], 'html' => '');

            return Response::json($arrAjax);
        } else {
            return Response::json(returnError($result['Message']));
        }
    }
}
