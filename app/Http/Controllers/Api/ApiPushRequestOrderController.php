<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseApiController;
use App\Services\ServiceCommon;
use Illuminate\Http\Request;


class ApiPushRequestOrderController extends BaseApiController{
    private $hdiService = false;
	public function __construct(){
		parent::__construct();
        $this->hdiService = new ServiceCommon();
	}

	public function pushOrder(Request $request){
	    if(isset($request['DataRequest']) && !empty($request['DataRequest'])){
	        $dataRequest = $request['DataRequest'];
            myDebug($dataRequest);
        }else{
	        return $this->returnResultError([],'Data emply!');
        }
    }
}
