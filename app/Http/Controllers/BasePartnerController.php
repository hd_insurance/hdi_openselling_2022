<?php
/*
* @Created by: QuynhTM
* @Author    : QuynhTM
* @Date      : 10/2021
* @Version   : 1.0
*/

namespace App\Http\Controllers;

use App\Library\AdminFunction\CGlobal;
use View;

class BasePartnerController extends Controller{

	public function __construct(){

        View::share('colorWithTab', CGlobal::$colorWithTab);
        View::share('tab_top', CGlobal::selling);
    }

}  