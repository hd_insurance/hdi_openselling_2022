<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\Partner;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;
use Illuminate\Support\Facades\Config;

class PartnerModel extends ModelService
{
    /*********************************************************************************************************
     * Thanh toán hợp đồng
     *********************************************************************************************************/

    public function checkTokenPartnerCreatOrder($dataInput = array())
    {
        $this->setUserAction();
        $dataRequestDefault = $this->dataRequestDefault;
        $dataRequestDefault["token"] = (isset($dataInput['token'])) ? $dataInput['token'] : '';;

        $dataRequest['Data'] = $dataRequestDefault;
        $dataRequest['Action'] = ['ActionCode' => ACTION_CHECK_TOKEN_PARTNER_CREAT_ORDER];

        $param_url = 'OpenApi/hdi/v1/token/valid';
        $resultApi = $this->postApiUrl($dataRequest,$param_url);
        $responce = $this->setDataResponce($resultApi,$dataRequest);
        $partner = [];
        if(isset($responce['Success']) && $responce['Success'] == STATUS_INT_MOT){
            $partner = $responce['Data'];
        }
        return $partner;
    }
}
