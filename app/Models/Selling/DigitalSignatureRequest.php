<?php
/**
 * QuynhTM
 * 13/03/2020
 */

namespace App\Models\Selling;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;
use Illuminate\Support\Facades\Config;

class DigitalSignatureRequest extends ModelService
{
    static $arrKeyRequest = [
        'CUSTOMER_NAME' => 'Tên khách hàng',
        'CUSTOMER_ADDRESS' => 'Địa chỉ',
        'VEHICLE_NUMBER_PLATE' => 'Biển kiểm soát',
        'VEHICLE_CHASSIS_NO' => 'Số khung',
        'VEHICLE_ENGINE_NO' => 'Số máy',
        'VEHICLE_MFG' => 'Năm sản xuất',
        'CUSTOMER_PHONE' => 'Số điện thoại',
        'CUSTOMER_EMAIL' => 'Email',
        //'CUSTOMER_INFO' => 'Thông tin khách hàng',
    ];
    static $arrKeyMergerDB = [
        'CUSTOMER_NAME' => 'NAME',
        'CUSTOMER_ADDRESS' => 'ADDRESS',
        'CUSTOMER_PHONE' => 'PHONE',
        'CUSTOMER_EMAIL' => 'EMAIL',
        //'CUSTOMER_INFO' => 'CUSTOMER_INFO',
        //B_VEHICLE_INFO
        'VEHICLE_NUMBER_PLATE' => 'NUMBER_PLATE',
        'VEHICLE_CHASSIS_NO' => 'CHASSIS_NO',
        'VEHICLE_ENGINE_NO' => 'ENGINE_NO',
        'VEHICLE_MFG' => 'MFG',
    ];

    static $arrIsRun = [0 => 'Mới nhận yêu cầu', 1 => 'Thao tác thủ công', 2 => 'Tự động ký số'];
    static $arrStatus = [0 => 'Chưa xử lý', 1 => 'Đã xử lý', 2 => 'Chờ duyệt', 3 => 'Từ chối xử lý', 4 => 'Xử lý lỗi'];

    const STATUS_EMAIL_TNYC_SDBS = 'TNYC_SDBS';
    const STATUS_EMAIL_TCYC_SDBS = 'TCYC_SDBS';
    const STATUS_EMAIL_DYYC_SDBS = 'DYYC_SDBS';

    /*********************************************************************************************************
     * Đồng bộ data core
     *********************************************************************************************************/
    public function searchList($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) != '') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) != '') ? $dataRequest['p_from_date'] : '',
            'R3' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) != '') ? $dataRequest['p_to_date'] : '',
            'R4' => (isset($dataRequest['p_status']) && trim($dataRequest['p_status']) != '') ? $dataRequest['p_status'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) != '') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) != '') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) != '') ? $dataRequest['p_keyword'] : '',
            'R8' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) != '') ? $dataRequest['p_org_code'] : '',
            'R9' => (isset($dataRequest['p_is_run']) && trim($dataRequest['p_is_run']) != '') ? $dataRequest['p_is_run'] : '',
            'R10' => (isset($dataRequest['p_is_send_email']) && trim($dataRequest['p_is_send_email']) != '') ? $dataRequest['p_is_send_email'] : '',
        ];
        $requestDefault["p_business"] = json_encode($arrParam, false);
        $paramRequest['Data'] = $requestDefault;
        $paramRequest['Action'] = [
            'ActionCode' => ACTION_SEARCH_INSUR_REQUEST,
        ];
        $resultApi = $this->postApiHD($paramRequest);
        return $this->setDataPaging($resultApi, $dataRequest);
    }

    /**
     * @param array $dataRequest
     * @param string $action: EDIT_REQUEST: chỉ cập nhật bảng request,
     *                      EDIT dùng cho ký số, add data vào bảng tạm
     * @return array
     */
    public function editRequest($dataRequest = array(), $action = 'EDIT')
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;

        $requestDefault["p_action"] = $action;
        $requestDefault["p_data_form"] = json_encode($dataRequest, false);
        $paramRequest['Data'] = $requestDefault;
        $paramRequest['Action'] = [
            'ActionCode' => ACTION_UPDATE_INSUR_REQUEST,
        ];
        $resultApi = $this->postApiHD($paramRequest);
        return $this->setDataResponce($resultApi, $dataRequest);
    }

    public function pushRequest($dataRequest = array())
    {
        $this->setUserAction();
        $arrParamRequest = isset($dataRequest['p_data_request']) ? $dataRequest['p_data_request'] : [];
        $arrParamFile = isset($dataRequest['p_file_attach']) ? $dataRequest['p_file_attach'] : [];

        $request["org_code"] = (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) != '') ? $dataRequest['p_org_code'] : '';
        $request["certificate_no"] = (isset($dataRequest['p_certificate_code']) && trim($dataRequest['p_certificate_code']) != '') ? $dataRequest['p_certificate_code'] : '';
        $request["note"] = (isset($dataRequest['p_note']) && trim($dataRequest['p_note']) != '') ? $dataRequest['p_note'] : '';
        $request["contact_email"] = (isset($dataRequest['p_contact_email']) && trim($dataRequest['p_contact_email']) != '') ? $dataRequest['p_contact_email'] : '';
        $request["contact_phone"] = (isset($dataRequest['p_contact_phone']) && trim($dataRequest['p_contact_phone']) != '') ? $dataRequest['p_contact_phone'] : '';
        $request["data_insur"] = json_encode($arrParamRequest, false);
        $request["file_attach"] = json_encode($arrParamFile, false);

        $requestDefault = $this->dataRequestDefault;
        $requestDefault['p_data_request'] = json_encode($request, false);

        $paramRequest['Data'] = $requestDefault;
        $paramRequest['Action'] = [
            'ActionCode' => ACTION_PUSH_INSUR_REQUEST,
        ];
        $resultApi = $this->postApiHD($paramRequest);
        myDebug($requestDefault, false);
        myDebug($resultApi);
        return $this->setDataPaging($resultApi, $dataRequest);
    }

    public function getDataByRequestCode($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault['p_request_code'] = (isset($dataRequest['p_request_code']) && trim($dataRequest['p_request_code']) != '') ? $dataRequest['p_request_code'] : '';

        $paramRequest['Data'] = $requestDefault;
        $paramRequest['Action'] = [
            'ActionCode' => ACTION_SEARCH_BY_REQUEST_CODE,
        ];
        myDebug($paramRequest,false);
        $resultApi = $this->postApiHD($paramRequest);
        return $resultApi;
    }
}
