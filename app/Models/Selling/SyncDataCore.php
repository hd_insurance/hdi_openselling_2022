<?php
/**
 * QuynhTM
 * 13/03/2020
 */

namespace App\Models\Selling;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;
use Illuminate\Support\Facades\Config;

class SyncDataCore extends ModelService
{
    /*********************************************************************************************************
     * Đồng bộ data core
     *********************************************************************************************************/
    public function searchDataSync($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',//chương trình
            'R2' => (isset($dataRequest['p_contract_no']) && trim($dataRequest['p_contract_no']) !='') ? $dataRequest['p_contract_no'] : '',//sản phẩm
            'R3' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) !='') ? $dataRequest['p_from_date'] :'',//số PLHĐ
            'R4' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) !='') ? $dataRequest['p_to_date'] : '',//người được bảo hiểm
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
        ];
        $requestDefault["p_business"] = json_encode($arrParam, false);
        $paramRequest['Data'] = $requestDefault;
        $paramRequest['Action'] = [
            'ActionCode' => ACTION_SEARCH_SYNC_DATA_CORE,
        ];
        $resultApi = $this->postApiHD($paramRequest);
        return $this->setDataPaging($resultApi,$dataRequest);
    }
}
