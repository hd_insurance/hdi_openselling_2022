<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\Selling;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class ClaimHdi extends ModelService
{
    /*********************************************************************************************************
     * Thanh toán hợp đồng
     *********************************************************************************************************/
    public $table = TABLE_GIFT_CONFIG_CODE;

    public function searchClaimHdi($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        //arr parram
        $arrParam = $this->buildParam($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            'ActionCode' => ACTION_SEARCH_CLAIM_HDI,
        ];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function getListFilesClaim($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $requestDefault["p_product_code"] = (isset($dataRequest['p_product_code'])) ? $dataRequest['p_product_code'] : '';

        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            'ActionCode' => ACTION_GET_LIST_FILES_CLAIM,
        ];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function getParamSearch($dataForm = [], $p_org_code = '', $p_product_code = '')
    {
        $page_no = (isset($dataForm['page_no']) && trim($dataForm['page_no']) != '') ? $dataForm['page_no'] : (int)Request::get('page_no', 1);
        $p_status = (isset($dataForm['p_status']) && trim($dataForm['p_status']) != '') ? $dataForm['p_status'] : '';
        $p_org_code_form = (isset($dataForm['p_org_code']) && trim($dataForm['p_org_code']) != '') ? $dataForm['p_org_code'] : trim(addslashes(Request::get('p_org_code', '')));
        $p_product_code_form = (isset($dataForm['p_product_code']) && trim($dataForm['p_product_code']) != '') ? $dataForm['p_product_code'] : trim(addslashes(Request::get('p_product_code', '')));

        $search['p_org_code'] = trim($p_org_code) != '' ? $p_org_code : $p_org_code_form;
        $search['p_product_code'] = trim($p_product_code) != '' ? $p_product_code : $p_product_code_form;
        $search["p_channel"] = (isset($dataForm['p_channel']) && trim($dataForm['p_channel']) != '') ? $dataForm['p_channel'] : trim(addslashes(Request::get('p_channel', '')));
        $search["p_str_status"] = (isset($dataForm['p_str_status']) && trim($dataForm['p_str_status']) != '') ? $dataForm['p_str_status'] : $p_status;
        $search['p_status'] = (trim($search["p_str_status"]) != '') ? implode(';', explode(',', trim($search["p_str_status"]))) : '';
        $search['p_type'] = 'LIST';

        //tìm kiếm nâng cao: A6,A7,A8,A9,A10,A15,A16
        $search["p_key_search"] = (isset($dataForm['p_key_search']) && trim($dataForm['p_key_search']) != '') ? $dataForm['p_key_search'] : trim(addslashes(Request::get('p_key_search', '')));
        $search["p_ins_name"] = isset($dataForm['p_ins_name'])? trim($dataForm["p_ins_name"]):'';//A6
        $search["p_certificate_no"] = isset($dataForm['p_certificate_no'])?trim($dataForm["p_certificate_no"]):'';//A9
        $search["p_claim_code"] = isset($dataForm['p_claim_code'])?trim($dataForm["p_claim_code"]):'';//A12

        $search["p_req_name"] = $search["p_key_search"];//A7
        $search["p_cardid"] = $search["p_key_search"];//A8
        $search["p_phone"] = $search["p_key_search"];//A10
        $search["p_flight_no"] = $search["p_key_search"];//A15
        $search["p_booking_id"] = $search["p_key_search"];//A16

        $search["p_from_date"] = (isset($dataForm['p_from_date']) && trim($dataForm['p_from_date']) != '') ? $dataForm['p_from_date'] : trim(addslashes(Request::get('p_from_date', '')));
        $search["p_to_date"] = (isset($dataForm['p_to_date']) && trim($dataForm['p_to_date']) != '') ? $dataForm['p_to_date'] : trim(addslashes(Request::get('p_to_date', '')));

        $submit = (isset($dataForm['submit']) && trim($dataForm['submit']) != '') ? $dataForm['submit'] : (int)Request::get('submit', 1);
        $search['page_no'] = ($submit == STATUS_INT_MOT) ? $page_no : STATUS_INT_KHONG;

        return $search;
    }

    public function buildParam($dataRequest = [])
    {
        //từ khóa: A6,A7,A8,A9,A10,A15,A16
        $arrParam = [
            'A1' => (isset($dataRequest['p_product_code'])) ? $dataRequest['p_product_code'] : PRODUCT_CODE_BAY_AT,//PRODUCT_CODE
            'A2' => (isset($dataRequest['p_channel'])) ? $dataRequest['p_channel'] : '',//CHANNEL
            'A3' => (isset($dataRequest['p_status'])) ? $dataRequest['p_status'] : '',//STATE
            'A4' => (isset($dataRequest['p_from_date'])) ? $dataRequest['p_from_date'] : '',//FROM_DATE
            'A5' => (isset($dataRequest['p_to_date'])) ? $dataRequest['p_to_date'] : '',//TO_DATE
            'A6' => (isset($dataRequest['p_ins_name'])) ? $dataRequest['p_ins_name'] : '',//INS_NAME
            'A7' => (isset($dataRequest['p_req_name'])) ? $dataRequest['p_req_name'] : '',//REQ_NAME
            'A8' => (isset($dataRequest['p_cardid'])) ? $dataRequest['p_cardid'] : '',//CARDID
            'A9' => (isset($dataRequest['p_certificate_no'])) ? $dataRequest['p_certificate_no'] : '',//CERTIFICATE_NO
            'A10' => (isset($dataRequest['p_phone'])) ? $dataRequest['p_phone'] : '',//PHONE

            'A11' => (isset($dataRequest['p_type'])) ? $dataRequest['p_type'] : 'LIST',//TYPE:  LIST: Để lấy cho HDI, LIST_AIR: để lấy cho màn hình VJ, DETAIL: chi tiết

            'A12' => (isset($dataRequest['p_claim_code'])) ? $dataRequest['p_claim_code'] : '',//CLAIM_CODE
            'A13' => (isset($dataRequest['p_month'])) ? $dataRequest['p_month'] : '',//MONTH
            'A14' => (isset($dataRequest['p_year'])) ? $dataRequest['p_year'] : '',//YEAR
            'A15' => (isset($dataRequest['p_flight_no'])) ? $dataRequest['p_flight_no'] : '',//FLIGHT_NO: Nếu nhập số hiệu chuyến bay
            'A16' => (isset($dataRequest['p_booking_id'])) ? $dataRequest['p_booking_id'] : '',//BOOKING_ID: Nếu nhập mã đặt chỗ
            'A17' => (isset($dataRequest['page_no'])) ? $dataRequest['page_no'] : 1,//pageing
        ];
        return $arrParam;
    }

    public function updateChangeProcess($dataInput = array())
    {
        $this->setUserAction();
        $dataRequestDefault = $this->dataRequestDefault;
        $dataRequestDefault['p_org_code'] = (isset($dataInput['p_org_code'])) ? $dataInput['p_org_code'] : '';
        $dataRequestDefault['p_prodcode'] = (isset($dataInput['p_prodcode'])) ? $dataInput['p_prodcode'] : '';
        $dataRequestDefault['p_claimtrans'] = (isset($dataInput['p_claim_code'])) ? $dataInput['p_claim_code'] : '';
        $dataRequestDefault['p_workid'] = (isset($dataInput['p_claim_status'])) ? $dataInput['p_claim_status'] : '';
        $dataRequestDefault['p_staffcode'] = (isset($dataInput['p_staffcode'])) ? $dataInput['p_staffcode'] : '';
        $dataRequestDefault['p_staffname'] = (isset($dataInput['p_staffname'])) ? $dataInput['p_staffname'] : '';
        $dataRequestDefault['p_content'] = (isset($dataInput['p_content'])) ? $dataInput['p_content'] : '';
        $dataRequestDefault['p_pay_date'] = (isset($dataInput['p_pay_date'])) ? $dataInput['p_pay_date'] : '';

        $arrClaimAndFiles = [];
        if (isset($dataInput['p_pay_claim']) && !empty($dataInput['p_pay_claim'])) {
            $arrClaimAndFiles['pay_claim'] = $dataInput['p_pay_claim'];
        }
        if (isset($dataInput['p_file_attach']) && !empty($dataInput['p_file_attach'])) {
            $arrClaimAndFiles['file_attach'] = $dataInput['p_file_attach'];
        }
        $dataRequestDefault['p_o_business'] = !empty($arrClaimAndFiles) ? $arrClaimAndFiles : '';
        $dataRequest['Data'] = $dataRequestDefault;
        $dataRequest['Action'] = ['ActionCode' => ACTION_CHANGE_PROCESS];

        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function sendMailClaim($dataInput = array())
    {
        $dataRequest['Data'] = $dataInput;
        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            "ActionCode" => "HDI_EMAIL_JSON"];
        $param_url = Config::get('config.MAIL_SERVICE');
        $resultApi = $this->postApiUrl($dataRequest, $param_url);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function updateContactOrFilesAttack($type = 1, $dataInput = array())
    {
        $this->setUserAction();
        $dataRequestDefault = $this->dataRequestDefault;
        $dataRequestDefault['p_org_code'] = (isset($dataInput['ORG_CODE'])) ? $dataInput['ORG_CODE'] : '';
        $dataRequestDefault['p_prodcode'] = (isset($dataInput['PRODUCT_CODE'])) ? $dataInput['PRODUCT_CODE'] : '';

        $dataObject['channel'] = (isset($dataInput['CHANNEL'])) ? $dataInput['CHANNEL'] : '';
        $dataObject['claim_code'] = (isset($dataInput['CLAIM_CODE'])) ? $dataInput['CLAIM_CODE'] : '';
        $dataObject['type'] = (isset($dataInput['RELATIONSHIP'])) ? $dataInput['RELATIONSHIP'] : '';

        if ($type == 1) {//update thông tin người liên hệ
            $claim_declare = []; //data update contact
            $claim_declare['name'] = (isset($dataInput['NAME'])) ? $dataInput['NAME'] : '';
            $claim_declare['relation'] = (isset($dataInput['RELATIONSHIP'])) ? $dataInput['RELATIONSHIP'] : '';
            $claim_declare['type'] = (isset($dataInput['RELATIONSHIP'])) ? $dataInput['RELATIONSHIP'] : '';
            $claim_declare['gender'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $claim_declare['idcard'] = (isset($dataInput['IDCARD'])) ? $dataInput['IDCARD'] : '';
            $claim_declare['idcard_d'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $claim_declare['idcard_p'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $claim_declare['email'] = (isset($dataInput['EMAIL'])) ? $dataInput['EMAIL'] : '';
            $claim_declare['phone'] = (isset($dataInput['PHONE'])) ? $dataInput['PHONE'] : '';
            $claim_declare['dob'] = (isset($dataInput['DOB'])) ? $dataInput['DOB'] : '';
            $claim_declare['address'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $claim_declare['wards'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $claim_declare['district'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $claim_declare['province'] = (isset($dataInput['xxxx'])) ? $dataInput['xxxx'] : '';
            $dataObject['claim']['claim_declare'] = $claim_declare;
        }

        if ($type == 2) {//update thông tin file upload
            $dataObject['claim']['file_attach'] = (isset($dataInput['ARR_FILES'])) ? $dataInput['ARR_FILES'] : [];;
        }

        //$dataRequestDefault['p_o_business'] = json_encode($dataObject);
        $dataRequestDefault['p_o_business'] = $dataObject;
        $dataRequest['Data'] = $dataRequestDefault;
        $dataRequest['Action'] = [
            'ActionCode' => ACTION_UPDATE_CONTACT_FILES_CLAIM,
        ];
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function getListDocument($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $requestDefault["p_product_code"] = (isset($dataRequest['p_product_code'])) ? $dataRequest['p_product_code'] : '';
        $requestDefault["p_language"] = 'VN';

        $requestApi['Action'] = [
            'ActionCode' => ACTION_GET_LIST_DOCUMENT_CLAIM,
        ];
        $requestApi['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($requestApi);
        return $this->setDataResponce($resultApi,$requestApi);
    }

    public function getHistoryUpdateClaim($p_claim_code = '', $p_product_code = '', $p_org_code = '')
    {
        $search['p_org_code'] = $p_org_code;
        $search['p_product_code'] = $p_product_code;
        $search['p_claim_code'] = $p_claim_code;
        $search['p_type'] = 'LIST_CHANGE';
        $dataGet = $this->searchClaimHdi($search);
        $historyUpdate = [];

        if (isset($dataGet['Data'][0]) && !empty($dataGet['Data'][0])) {
            foreach ($dataGet['Data'][0] as $key => $histo) {
                $arrDate = explode(' ', $histo->CREATE_DATE);
                $hour = $arrDate[0];
                $date = $arrDate[1];
                $historyUpdate[$date][$hour]['CREATE_BY'] = isset($histo->CREATE_BY) ? $histo->CREATE_BY : '';
                $historyUpdate[$date][$hour]['HISTORY_UPDATE'][] = $histo;
            }

        }
        return $historyUpdate;
    }
}
