<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\Selling;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;
use Illuminate\Support\Facades\Config;

class ExtenActionHdi extends ModelService
{
    /*********************************************************************************************************
     * Cấp đơn theo lô
     *********************************************************************************************************/
    public function searchDataOrder($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';

        //arr parram
        $arrParam = $this->_buildParamSearch($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        $dataRequest['Action'] = [
            'ActionCode' => ACTION_SEARCH_LIST_CREATE_ORDER,
        ];
        $dataRequest['Data'] = $requestDefault;

        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataPaging($resultApi, $dataRequest);
    }

    public function removeDataOrder($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';

        //arr parram
        $arrParam = [
            'R1' => (isset($dataRequest['programme_id']) && trim($dataRequest['programme_id']) != '') ? trim($dataRequest['programme_id']) : '',//chương trình
            'R2' => (isset($dataRequest['product_id']) && trim($dataRequest['product_id']) != '') ? trim($dataRequest['product_id']) : '',//sản phẩm
            'R3' => (isset($dataRequest['contract_no']) && trim($dataRequest['contract_no']) != '') ? trim($dataRequest['contract_no']) : '',//số PLHĐ
        ];
        $requestDefault["p_business"] = json_encode($arrParam, false);
        $dataRequest['Action'] = [
            'ActionCode' => ACTION_REMOVE_LIST_CREATE_ORDER,
        ];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi, $dataRequest);
    }

    public function genCodeFromExcel($dataRequest = array())
    {
        $this->setUserAction();
        $param = $this->dataRequestDefault;
        $param["p_org_seller"] = (isset($dataRequest['p_org_seller'])) ? trim($dataRequest['p_org_seller']) : '';
        $param["p_product"] = (isset($dataRequest['p_product'])) ? trim($dataRequest['p_product']) : '';
        $param["p_business"] = (isset($dataRequest['dataExcel'])) ? json_encode($dataRequest['dataExcel'], false) : '';

        $requestDefault['Data'] = $param;
        $requestDefault['Action'] = [
            'ActionCode' => ACTION_GEN_CERTIFICATE_FIRST,
        ];

        $resultApi = $this->postApiHD($requestDefault);
        return $this->setDataResponce($resultApi, $dataRequest);
    }

    public function searchGenCode($dataRequest = array())
    {
        $this->setUserAction();
        $param = $this->dataRequestDefault;
        $param["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $param["p_product_code"] = (isset($dataRequest['p_product_code'])) ? $dataRequest['p_product_code'] : '';
        $arrParam = [
            'R1' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) != '') ? trim($dataRequest['p_from_date']) : '',
            'R2' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) != '') ? trim($dataRequest['p_to_date']) : '',
            'R3' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) != '') ? trim($dataRequest['page_no']) : 1,//số PLHĐ
        ];
        $param["p_search"] = json_encode($arrParam, false);

        $requestDefault['Data'] = $param;
        $requestDefault['Action'] = [
            'ActionCode' => ACTION_SEARCH_CERTIFICATE_FIRST,
        ];
        $resultApi = $this->postApiHD($requestDefault);
        return $this->setDataPaging($resultApi, $requestDefault);
    }

    public function genTemplateEmailOrder($dataInput = array())
    {
        $this->setUserAction();

        $dataRequestDefault = $this->dataRequestDefault;
        $dataRequestDefault["ID"] = (isset($dataInput['programme_id'])) ? trim($dataInput['programme_id']) : '';;
        $dataRequest['Data'] = $dataRequestDefault;
        $dataRequest['Action'] = ['ActionCode' => 'KHOA_HAM'];

        $param_url = 'hdi/service/impTemplate';
        $resultApi = $this->postApiUrl($dataRequest, $param_url);
        return $this->checkReturnData($resultApi);
    }

    private function _buildParamSearch($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_search_programme_id']) && trim($dataRequest['p_search_programme_id']) != '') ? trim($dataRequest['p_search_programme_id']) : '',//chương trình
            'R2' => (isset($dataRequest['p_search_product_id']) && trim($dataRequest['p_search_product_id']) != '') ? trim($dataRequest['p_search_product_id']) : '',//sản phẩm
            'R3' => (isset($dataRequest['p_search_contract_no']) && trim($dataRequest['p_search_contract_no']) != '') ? trim($dataRequest['p_search_contract_no']) : '',//số PLHĐ
            'R4' => (isset($dataRequest['p_search_certificate_no']) && trim($dataRequest['p_search_certificate_no']) != '') ? trim($dataRequest['p_search_certificate_no']) : '',//người được bảo hiểm
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) != '') ? trim($dataRequest['page_no']) : 1,//pageing
            'R6' => (isset($dataRequest['p_search_user_bh']) && trim($dataRequest['p_search_user_bh']) != '') ? trim($dataRequest['p_search_user_bh']) : '',
            'R7' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) != '') ? trim($dataRequest['p_from_date']) : '',
            'R8' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) != '') ? trim($dataRequest['p_to_date']) : '',
        ];
        return $arrParam;
    }

    public function getDataTabCreateOrder($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? trim($dataRequest['p_org_code']) : '';
        $requestDefault["p_progid"] = (isset($dataRequest['p_progid'])) ? trim($dataRequest['p_progid']) : '';
        return $this->searchAllDataResponce($requestDefault, ACTION_GET_INFOR_PROGRAM_ALL);
    }

    public function getDetailProgrammeId($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? trim($dataRequest['p_org_code']) : '';
        $requestDefault["p_programme_id"] = (isset($dataRequest['programme_id'])) ? trim($dataRequest['programme_id']) : '';

        return $this->searchDataOne($requestDefault, ACTION_GET_INFOR_PROGRAM_DETAILS);
    }

    public function updateProgramme($dataInput, $action = 'ADD')
    {
        $arrParam = [
            'R1' => (isset($dataInput['p_progid'])) ? trim($dataInput['p_progid']) : 0,
            'R2' => (isset($dataInput['p_programme_name'])) ? trim($dataInput['p_programme_name']) : '',
            'R3' => (isset($dataInput['p_org_buyer'])) ? trim($dataInput['p_org_buyer']) : '',

            'R4' => (isset($dataInput['p_product'])) ? trim($dataInput['p_product']) : '',
            'R5' => (isset($dataInput['p_struct_code'])) ? trim($dataInput['p_struct_code']) : '',
            'R6' => (isset($dataInput['p_contract_no'])) ? trim($dataInput['p_contract_no']) : '',

            'R7' => (isset($dataInput['p_effective_date'])) ? trim($dataInput['p_effective_date']) : '',
            'R8' => (isset($dataInput['p_expiration_date'])) ? trim($dataInput['p_expiration_date']) : '',
            'R9' => (isset($dataInput['p_temp_email'])) ? trim($dataInput['p_temp_email']) : '',

            'R10' => (isset($dataInput['file_id_contract'])) ? trim($dataInput['file_id_contract']) : '',//dùng cho update
            'R11' => (isset($dataInput['p_certificate_temp'])) ? trim($dataInput['p_certificate_temp']) : '',
            'R12' => (isset($dataInput['p_email_subject'])) ? trim($dataInput['p_email_subject']) : '',
            'R13' => (isset($dataInput['p_cer_env'])) ? trim($dataInput['p_cer_env']) : '',
        ];
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_org_code"] = isset($dataInput['org_code_user_action']) ? $dataInput['org_code_user_action'] : 'HDI';
            $requestDefault["p_action"] = $action;
            $requestDefault["p_pack_obj"] = (isset($dataInput['p_package_json'])) ? json_encode($dataInput['p_package_json'], false) : '';
            $requestDefault["p_prog_obj"] = json_encode($arrParam, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_PROGRAM_DETAILS];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi, $dataRequest);
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
    }

    /*********************************************************************************************************
     * Cấp ký số
     *********************************************************************************************************/
    public function searchDigitallySigned($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = Config::get('config.API_PARENT_CODE');
        $requestDefault["p_temp_code"] = (isset($dataRequest['p_temp_code'])) ? trim($dataRequest['p_temp_code']) : 'CSSK_SVC';
        $requestDefault["p_from_date"] = (isset($dataRequest['p_from_date'])) ? trim($dataRequest['p_from_date']) : '';
        $requestDefault["p_to_date"] = (isset($dataRequest['p_to_date'])) ? trim($dataRequest['p_to_date']) : '';
        $requestDefault["p_file_code"] = (isset($dataRequest['p_file_code'])) ? trim($dataRequest['p_file_code']) : '';
        $requestDefault["p_page"] = (isset($dataRequest['page_no'])) ? trim($dataRequest['page_no']) : STATUS_INT_MOT;

        $dataRequest['Action'] = [
            'ParentCode' => Config::get('config.API_PARENT_CODE'),
            'UserName' => Config::get('config.API_USER_NAME'),
            'Secret' => Config::get('config.API_SECRET'),
            'ActionCode' => ACTION_SEARCH_DIGITALLY_SIGNED,
        ];
        $dataRequest['Data'] = $requestDefault;

        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataPaging($resultApi, $dataRequest);
    }

    public function createDigitallySigned($dataInput = array())
    {
        if (empty($dataInput))
            return false;
        try {
            $this->setUserAction();

            $dataRequestDefault["USER_NAME"] = $this->dataRequestDefault['p_username'];// $this->dataRequestDefault['p_username'];
            $dataRequestDefault["TEMP_CODE"] = 'CSSK_SVC';
            $dataRequestDefault["TYPE_FILE"] = 'BASE64';
            $dataRequestDefault["EXT_FILE"] = 'pdf';
            $dataRequestDefault["FILE"] = $dataInput['FILE'];//base64
            $dataRequestDefault["LOCATION"] = "Hà Nội";
            $dataRequestDefault["REASON"] = "Cấp giấy chứng nhận Bảo hiểm";
            $dataRequestDefault["TypeResponse"] = 'BASE64';
            $dataRequestDefault["TextNote"] = '';
            $dataRequestDefault["Alert"] = '';
            $dataRequestDefault["TEXT_CA"] = !empty($dataInput['TEXT_CA']) ? trim($dataInput['TEXT_CA']) : 'CÔNG TY TNHH BẢO HIỂM HD';//điểm neo ký số
            $dataRequestDefault["FILE_REFF"] = $dataInput['FILE_REFF'];//aaa
            $dataRequestDefault["FILE_CODE"] = '';
            $dataRequestDefault["MAIL_BUYER"] = '';
            $dataRequestDefault["MAIL_INSURED"] = '';
            $dataRequestDefault["MAIL_BENEFICIARY"] = '';
            $dataRequestDefault["MAIL_USER"] = '';
            $dataRequestDefault["MAIL_CC"] = '';
            $dataRequestDefault["MAIL_BCC"] = '';
            $dataRequestDefault["DATA_EXT"] = '';

            $dataRequest['Data'] = $dataRequestDefault;
            $dataRequest['Action'] = [
                'ParentCode' => Config::get('config.API_PARENT_CODE'),
                'UserName' => Config::get('config.API_USER_NAME'),
                'Secret' => Config::get('config.API_SECRET'),
                'ActionCode' => ACTION_CREATE_DIGITALLY_SIGNED,
            ];
            $param_url = 'hdi/service/sign';
            $resultApi = $this->postApiUrl($dataRequest, $param_url);

            /*if(Config::get('config.ENVIRONMENT') == 'DEV'){
                $url = 'https://test-apipayment.hdinsurance.com.vn/hdi/service/sign';
                $resultApi = $this->postApiByUrl($dataRequest, $url);
            }
            else{
                $param_url = 'hdi/service/sign';
                $resultApi = $this->postApiUrl($dataRequest, $param_url);
            }*/
            return $this->setDataResponce($resultApi, $dataRequest);
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
    }

    /*********************************************************************************************************
     * Cấp ký số
     *********************************************************************************************************/
    public function searchDigitallyCentech($dataRequest = array())
    {
        $requestDefault["p_action"] = isset($dataRequest['p_action']) ? trim($dataRequest['p_action']) : 'GET';
        $requestDefault["p_search"] = isset($dataRequest['p_search']) ? trim($dataRequest['p_search']) : '1';
        $requestDefault["p_json"] = isset($dataRequest['p_json']) ? json_encode($dataRequest['p_json'], false) : '1';
        $dataRequest['Action'] = [
            'ActionCode' => ACTION_DIGITALLY_CENTECH,
        ];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataPaging($resultApi, $dataRequest);
    }

    /*********************************************************************************************************
     * Đồng bộ data core
     *********************************************************************************************************/
    public function searchDataSync($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) != '') ? trim($dataRequest['p_product_code']) : '',//chương trình
            'R2' => (isset($dataRequest['p_contract_no']) && trim($dataRequest['p_contract_no']) != '') ? trim($dataRequest['p_contract_no']) : '',//sản phẩm
            'R3' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) != '') ? trim($dataRequest['p_from_date']) : '',//số PLHĐ
            'R4' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) != '') ? trim($dataRequest['p_to_date']) : '',//người được bảo hiểm
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) != '') ? trim($dataRequest['page_no']) : 1,//pageing
        ];
        $requestDefault["p_business"] = json_encode($arrParam, false);
        $paramRequest['Data'] = $requestDefault;
        $paramRequest['Action'] = [
            'ActionCode' => ACTION_SEARCH_SYNC_DATA_CORE,
        ];
        $resultApi = $this->postApiHD($paramRequest);
        return $this->setDataPaging($resultApi, $paramRequest);
    }
}
