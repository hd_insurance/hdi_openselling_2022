<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Services\ModelService;

class SystemCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: SYS_TEMP_STORAGE_SYNCH
     *********************************************************************************************************/
    public $table = TABLE_SYS_TEMP_STORAGE_SYNCH;

    public function searchList($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->buildParamInsurDetail($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_DEVOPS_SYNCH_DATA_SEARCH_LIST);
    }
    private function buildParamInsurDetail($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_str_id_search']) && trim($dataRequest['p_str_id_search']) !='') ? $dataRequest['p_str_id_search'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }

    public function editDataSynch($dataInfor = [], $tableAction = '', $keyId = '')
    {
        if(empty($dataInfor) || empty($tableAction) || empty($keyId))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_table_action"] = $tableAction;
            $requestDefault["p_ref_id"] = $keyId;
            $requestDefault["p_info_synch"] = json_encode($dataInfor, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_SYNCH_DATA_EDIT_ITEM];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function pushDataSynch($keyId = '')
    {
        if(empty($keyId))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_key"] = $keyId;

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_SYNCH_DATA_BY_KEY];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function importDataSynch($dataImport = [])
    {
        if(empty($dataImport))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $dataJson = [];
            foreach ($dataImport as $k=> $arrInfor){
                $dataJson[] = $arrInfor;
            }

            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = 0;
            $requestDefault["p_data_excel"] = json_encode(['DATA'=>$dataJson],false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_SYNCH_DATA_WITH_IMPORT_EXCEL];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function deleteDataSynch($strId = '')
    {
        if(empty($strId))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_str_id"] = $strId;

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_SYNCH_DELETE_DATA_BY_KEY];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
}
