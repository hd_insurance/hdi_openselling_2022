<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class PackageCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: TABLE_B_PACKAGES
     *********************************************************************************************************/
    public $table = TABLE_B_PACKAGES;
    public function searchPackage($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = $this->buildParamPackage($dataRequest);

        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_SEARCH_LIST_PACKAGE_CORE);
    }
    private function buildParamPackage($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }



    public function editItem($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_PACKAGE_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function getAllData(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_PACKAGE_CORE);
        if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_10000;
            $data = $this->searchPackage($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_PACKAGE_CORE, $dataList);
                }
            }
        }
        return $dataList;
    }

    public function getOptionPackage($product_code = ''){
        $dataList = $this->getAllData();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if(trim($product_code) != ''){
                    if($item->IS_ACTIVE == STATUS_INT_MOT && $item->PRODUCT_CODE == $product_code){
                        $option[$item->PACK_CODE] = $item->PACK_NAME;
                    }
                }else{
                    if($item->IS_ACTIVE == STATUS_INT_MOT){
                        $option[$item->PACK_CODE] = $item->PACK_NAME;
                    }
                }
            }
        }
        return $option;
    }
    public function removeCache($data = [])
    {
        Memcache::forgetCache(Memcache::CACHE_ALL_PACKAGE_CORE);
    }

    /*********************************************************************************************************
     * Danh sách quyền lợi theo gói, theo sản phẩm
     *********************************************************************************************************/
    public function searchBenefits($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = $this->buildParamBenefits($dataRequest);

        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_SEARCH_PACKAGE_BENEFITS);
    }
    private function buildParamBenefits($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing

        ];
        return $arrParam;
    }
    public function editBenefits($dataInput, $action = 'ADD')
    {   if(empty($dataInput))
        return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_product_code"] = $dataInput['PRODUCT_CODE'];
            $requestDefault["p_pack_code"] = $dataInput['PACK_CODE'];
            $requestDefault["p_data_form"] = json_encode($dataInput['LIST_BENEFITS'], false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_EDIT_PACKAGE_BENEFITS];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
}
