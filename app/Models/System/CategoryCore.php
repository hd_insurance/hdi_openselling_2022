<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class CategoryCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: TABLE_B_CATEGORY
     *********************************************************************************************************/
    public $table = TABLE_B_CATEGORY;
    public function searchCategory($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = $this->buildParamCategory($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        return $this->searchDataCommon($requestDefault, ACTION_SEARCH_LIST_CATEGORY_CORE);
    }
    private function buildParamCategory($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }

    public function getAllData(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_CATEGORY_CORE);
        if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_10000;
            $data = $this->searchCategory($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_CATEGORY_CORE, $dataList);
                }
            }
        }
        return $dataList;
    }

    public function getOptionCatgegory(){
        $dataList = $this->getAllData();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if($item->IS_ACTIVE == STATUS_INT_MOT){
                    $option[$item->CATEGORY] = '[' . $item->CATEGORY . '] ' . $item->CATEGORY_NAME;
                }
            }
        }
        return $option;
    }
    public function editItem($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_CATEGORY_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function removeCache($data = [])
    {
        Memcache::forgetCache(Memcache::CACHE_ALL_CATEGORY_CORE);
    }
}
