<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Services\ModelService;

class InsurCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: TABLE_B_CONTRACTS_PRODUCTS
     *********************************************************************************************************/
    public $table = TABLE_B_INSUR_DETAILS;
    public $tableVehicleInfo = TABLE_B_VEHICLE_INFO;

    public function searchInsurDetail($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->buildParamInsurDetail($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_SYSTEM_SEARCH_INSUR);
    }
    private function buildParamInsurDetail($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) !='') ? $dataRequest['p_from_date'] : '',
            'R3' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) !='') ? $dataRequest['p_to_date'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }

    public function getInforInsurDetail($dataSearch = [])
    {
        if(empty($dataSearch))
            return $this->returnStatusError('Không có data');

        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;

        $arrParam = $this->buildParamInforDetail($dataSearch);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        $dataRequest['Action'] = ['ActionCode' => ACTION_SYSTEM_GET_INFOR_INSUR];
        $dataRequest['Data'] = $requestDefault;
        //myDebug($dataRequest);
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }
    private function buildParamInforDetail($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_certificate_no']) && trim($dataRequest['p_certificate_no']) !='') ? $dataRequest['p_certificate_no'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_ref_id']) && trim($dataRequest['p_ref_id']) !='') ? $dataRequest['p_ref_id'] : '',
            'R5' => (isset($dataRequest['p_is_edit']) && trim($dataRequest['p_is_edit']) !='') ? $dataRequest['p_is_edit'] : 0,
        ];
        return $arrParam;
    }

    public function editInsurDetail($dataInput, $action = 'EDIT')
    {
        /*
         *  [DETAIL_CODE] => DD344F13649329DFE0530100007FC396
            [CONTRACT_CODE] => DD344F13649129DFE0530100007FC396
            [CERTIFICATE_NO] =>
            [CATEGORY] => XE
            [REF_ID] => DD344F13649229DFE0530100007FC396
            [PRODUCT_CODE] => XCG_VCX_NEW
            [PRODUCT_FEES] => 0
            [PACK_CODE] => GOI_1
            [PACK_FEES] => 10181818
            [CUS_CODE] =>
            [RELATIONSHIP] => BAN_THAN
            [NAME] => NGUYỄN 1982
            [DOB] =>
            [GENDER] =>
            [TYPE] => CN
            [PROVINCE] => 11
            [DISTRICT] =>
            [WARDS] =>
            [ADDRESS] => Tổ 1 Trà Bá Pleiku Gialai Lai
            [IDCARD] =>
            [IDCARD_D] =>
            [IDCARD_P] =>
            [EMAIL] => info@vifo.vn
            [PHONE] => 0915332614
            [FAX] =>
            [TAXCODE] =>
            [REGION] => VN
            [ADDITIONAL_FEES] => 0
            [EFFECTIVE_DATE] => 2022-04-23T00:00:00
            [YEAR] => 2022
            [MONTH] => 4
            [EFFECTIVE_NUM] => 20220423
            [EXPIRATION_DATE] => 2023-04-23T00:00:00
            [EXPIRATION_NUM] => 20230423
            [AMOUNT] => 10181818
            [DISCOUNT] => 0
            [DISCOUNT_UNIT] => P
            [TOTAL_DISCOUNT] => 0
            [VAT] => 1018182
            [TOTAL_AMOUNT] => 11200000
            [DATE_SIGN] =>
            [IS_CER] =>
            [TYPE_SEND] =>
            [MEMBER_CODE] =>
            [STRUCT_CODE] => D02
            [ORG_CODE] => HDI
            [SELLER_CODE] => ADMIN_VIFO
            [ORG_SELLER] => VIFO
            [CONTRACT_TYPE] => G
            [PARENT_CODE] =>
            [STATUS] => WAIT_CONFIRM
            [CREATE_DATE] => 2022-04-22T11:49:06
            [CREATE_BY] => ADMIN_VIFO
            [MODIFIED_DATE] => 2022-04-22T11:49:06
            [MODIFIED_BY] => ADMIN_VIFO
            [TEXT_SEARCH] => dd344f13649329dfe0530100007fc396dd344f13649129dfe0530100007fc396nguyen1982info@vifo.vn0915332614
            [DURATION_PAYMENT] => ALL
            [MONTH_EXP] => 4
            [YEAR_EXP] => 2023
            [MONTH_CREATE] =>
            [YEAR_CREATE] =>
            [CREATE_NUM] =>
            [STAFF_CODE] =>
            [RELATIONSHIP_STAFF] =>
            [STAFF_REF] =>
            [NATIONALITY] => VNM
            [TOTAL] => 217714
            [PAGE_SIZE] => 10
            [PAGE_CURRENT] => 1
         * */
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table, $dataUpdate);
        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_SYSTEM_ACTION_RESIGN_INSUR];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editVehicleInfo($dataInput, $action = 'EDIT')
    {
        /*
         *  VEH_CODE
            VEHICLE_VALUE
            VEHICLE_USE
            VEHICLE_TYPE
            VEHICLE_REGIS
            VEHICLE_GROUP
            WEIGH

            SEAT_NO
            REGIS_YEAR
            REGIS_NUM
            REGIS_MONTH
            REF_VEHICLE_VALUE
            NUMBER_PLATE
            NONE_NUMBER_PLATE
            MODIFIED_DATE
            MODIFIED_BY
            MODEL
            MFG
            ENGINE_NO
            CREATE_DATE
            CREATE_BY
            CHASSIS_NO
            CAPACITY
            BRAND
         * */
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->tableVehicleInfo, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            //gán key chính vào data để thủ tục dùng
            $dataUpdate['PRODUCT_CODE'] = isset($dataInput['PRODUCT_CODE'])?$dataInput['PRODUCT_CODE']:'';
            $dataUpdate['CONTRACT_CODE'] = isset($dataInput['CONTRACT_CODE'])?$dataInput['CONTRACT_CODE']:'';
            $dataUpdate['CERTIFICATE_NO'] = isset($dataInput['CERTIFICATE_NO'])?$dataInput['CERTIFICATE_NO']:'';

            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_SYSTEM_ACTION_RESIGN_INSUR];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function updateFileSigned($dataUpdate, $action = 'UDP_FILE_SIGNED')
    {   if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_SYSTEM_ACTION_RESIGN_INSUR];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
    public function getDataResignCertificate($dataUpdate, $action = 'RESIGN_INSUR')
    {   if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_SYSTEM_ACTION_RESIGN_INSUR];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function resignCertificate($dataInput = array(),$param_url = '')
    {
        $resultApi = $this->postUrl($param_url, $dataInput);
        /*
         * stdClass Object
            (
                [Success] => 1
                [Error] =>
                [ErrorMessage] =>
                [Data] => Array
                    (
                        [0] => stdClass Object
                            (
                                [Success] => 1
                                [MESSAGE] => OK
                                [FILE_ID] => 7007916f33824ff09f515fcb433dafac
                                [FILE_URL] => https://dev-hyperservices.hdinsurance.com.vn/f/7007916f33824ff09f515fcb433dafac
                                [FILE_BASE64] =>
                                [FILE_REF] => S22V01ELM9LM
                                [USER_NAME] => HDI_RESIGN
                                [REF_TYPE] => CER
                                [dtSignBack] => 2023-05-13T12:05:00
                            )

                    )

                [Signature] =>
            )
        */
        return $this->setDataResponce($resultApi,$dataInput);
    }

}
