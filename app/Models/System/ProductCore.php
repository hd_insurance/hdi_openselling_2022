<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class ProductCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: TABLE_B_CONTRACTS_PRODUCTS
     *********************************************************************************************************/
    public $table = TABLE_B_CONTRACTS_PRODUCTS;
    public $table_product_assign = TABLE_B_PRODUCT_ASSIGN;
    public $table_packages_assign = TABLE_B_PACKAGES_ASSIGN;
    public $table_packages_fees = TABLE_B_PACKAGES_FEES;
    public $table_channel_assign = TABLE_B_PRODUCT_ASSIGN_CHANNEL;
    public function searchProduct($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        //arr parram
        $arrParam = $this->buildParamProduct($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_SEARCH_LIST_PRODUCT_CORE);
    }
    private function buildParamProduct($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }

    public function getAllData(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_PRODUCT_CORE);
        if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_10000;
            $data = $this->searchProduct($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_PRODUCT_CORE, $dataList);
                }
            }
        }
        return $dataList;
    }

    public function getOptionProduct(){
        $dataList = $this->getAllData();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if($item->STATUS == 'APPROVED'){
                    $option[$item->PRODUCT_CODE] = '[' . $item->PRODUCT_CODE . '] ' . $item->PRODUCT_NAME;
                }
            }
        }
        return $option;
    }
    public function getInforProduct($product_code = '')
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_product_code"] = $product_code;

        $dataRequest['Action'] = ['ActionCode' => ACTION_GET_INFOR_PRODUCT_CORE];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function editProduct($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_org_code"] = 'HDI';
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_PRODUCT_CORE];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editProductAssign($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_product_assign, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_PRODUCT_ASSIGN_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editPackagesAssign($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_packages_assign, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_PACKAGE_ASSIGN_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
    public function editPackagesFees($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_packages_fees, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_PACKAGE_FEES_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function removePackagesFees($key_id = '')
    {
        if(empty($key_id))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_key"] = $key_id;

            $dataRequest['Action'] = ['ActionCode' => ACTION_REMOVE_PACKAGE_FEES_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editChannelAssign($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CONTRACTS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_channel_assign, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_UPDATE_CHANNEL_ASSIGN_CORE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function removeCache($data = [])
    {
        Memcache::forgetCache(Memcache::CACHE_ALL_PRODUCT_CORE);
    }

    /**************************************************************************************
     * DATABASES
     **************************************************************************************/

}
