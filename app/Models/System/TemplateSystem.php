<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class TemplateSystem extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: SYS_APIS
     *********************************************************************************************************/
    public $table_temp_config = TABLE_MD_TEMP_CONFIG;
    public $table_template = TABLE_MD_TEMPLATES;
    public $table_template_sms = TABLE_MD_TEMPLATES_SMS;
    public $table_template_detail = TABLE_MD_TEMPLATE_DETAIL;

    public function searchTemplateConfig($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';

        //arr parram
        $arrParam = $this->buildParamTemplateConfig($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        return $this->searchDataCommon($requestDefault, ACTION_SEARCH_TEMPLATE_CONFIG);
    }

    private function buildParamTemplateConfig($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }

    public function searchTemplates($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->buildParamTemplates($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_SEARCH_TEMPLATE);
    }

    private function buildParamTemplates($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_temp_code']) && trim($dataRequest['p_temp_code']) !='') ? $dataRequest['p_temp_code'] : '',
            'R2' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R3' => (isset($dataRequest['p_temp_type']) && trim($dataRequest['p_temp_type']) !='') ? $dataRequest['p_temp_type'] : '',
            'R4' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R5' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R6' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R7' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R8' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }

    public function getTemplateConfig($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $requestDefault["p_temp_id"] = (isset($dataRequest['p_temp_id'])) ? $dataRequest['p_temp_id'] : '';

        $dataRequest['Action'] = ['ActionCode' => ACTION_GET_TEMPLATE_CONFIG];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function getListTemplateDetail($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';
        $requestDefault["p_temp_id"] = (isset($dataRequest['p_temp_id'])) ? $dataRequest['p_temp_id'] : '';

        $dataRequest['Action'] = ['ActionCode' => ACTION_GET_TEMPLATE_DETAIL_BY_TEMP_ID];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function updateTemplateConfig($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPEN_MEDIA);
        $this->mergeFieldInputWithTable($dataInput, $this->table_temp_config, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_org_code"] = 'HDI';
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_EDIT_TEMPLATE_CONFIG];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function updateTemplate($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPEN_MEDIA);
        $this->mergeFieldInputWithTable($dataInput, $this->table_template, $dataUpdate);
        if(empty($dataUpdate))
            return returnError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_org_code"] = 'HDI';
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_EDIT_TEMPLATE];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function updateTemplateSMS($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPEN_MEDIA);
        $this->mergeFieldInputWithTable($dataInput, $this->table_template_sms, $dataUpdate);
        if(empty($dataUpdate))
            return returnError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_org_code"] = 'HDI';
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_EDIT_TEMPLATE_SMS];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function updateTemplateDetail($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPEN_MEDIA);
        $this->mergeFieldInputWithTable($dataInput, $this->table_template_detail, $dataUpdate);
        if(empty($dataUpdate))
            return returnError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_org_code"] = 'HDI';
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_EDIT_TEMPLATE_DETAIL];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

}
