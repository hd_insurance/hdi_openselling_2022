<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class PartnerCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: TABLE_SYS_PARTNER_CONFIG
     *********************************************************************************************************/
    public $table = TABLE_SYS_PARTNER_CONFIG;
    public $table_secret = TABLE_SYS_PARTNER_SECRET;
    public $table_user = TABLE_SYS_USER_PARTNER;
    public $table_group_api = TABLE_SYS_PARTNER_GROUP_API;

    public function searchPartnerConfig($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->buildParamPartnerConfig($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_DEVOPS_SEARCH_PARTNER_CONFIG);
    }
    private function buildParamPartnerConfig($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_partner_code']) && trim($dataRequest['p_partner_code']) !='') ? $dataRequest['p_partner_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function getAllData(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_PARTNER_CORE);
        //if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_10000;
            $data = $this->searchPartnerConfig($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_PARTNER_CORE, $dataList);
                }
            }
        //}
        return $dataList;
    }

    public function getOptionPartnerConfig(){
        $dataList = $this->getAllData();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if($item->ISACTIVE == STATUS_INT_MOT){
                    $option[$item->PARTNER_CODE] = '[' . $item->PARTNER_CODE . '] ' . $item->PARTNER_NAME;
                }
            }
        }
        return $option;
    }

    public function searchUserPartner($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->buildParamUserPartner($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_DEVOPS_SEARCH_USER_PARTNER);
    }
    private function buildParamUserPartner($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_partner_code']) && trim($dataRequest['p_partner_code']) !='') ? $dataRequest['p_partner_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function getAllDataUser(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_USER_PARTNER);
        //if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_10000;
            $data = $this->searchUserPartner($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_USER_PARTNER, $dataList);
                }
            }
        //}
        return $dataList;
    }

    public function getOptionUserPartner(){
        $dataList = $this->getAllDataUser();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if($item->ISACTIVE == STATUS_INT_MOT){
                    $option[$item->USER_CODE] = $item->USER_CODE;
                }
            }
        }
        return $option;
    }

    public function getInforPartnerConfig($product_code = '')
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_partner_code"] = $product_code;

        $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_GET_INFOR_PARTNER_CONFIG];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    public function editPartnerConfig($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_DEVOPS);
        $this->mergeFieldInputWithTable($dataInput, $this->table, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_EDIT_PARTNER_CONFIG];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editPartnerSecret($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_DEVOPS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_secret, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_EDIT_PARTNER_SECRET];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editPartnerUser($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_DEVOPS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_user, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_EDIT_PARTNER_USER];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function editPartnerGroupApi($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_DEVOPS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_group_api, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_EDIT_PARTNER_GROUP_API];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function removeCache($data = [])
    {
        Memcache::forgetCache(Memcache::CACHE_ALL_PARTNER_CORE);
        Memcache::forgetCache(Memcache::CACHE_ALL_USER_PARTNER);
    }

    /**************************************************************************************
     * DATABASES
     **************************************************************************************/

}
