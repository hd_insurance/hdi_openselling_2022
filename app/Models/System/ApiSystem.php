<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class ApiSystem extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: SYS_APIS
     *********************************************************************************************************/
    public $table = TABLE_SYS_ACTION_API;
    public $table_api_databases = TABLE_SYS_API_DATABASE;
    public $table_api_group = TABLE_SYS_API_GROUP;

    public function searchApi($dataRequest = array())
    {
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_keyword"] = (isset($dataRequest['p_keyword'])) ? $dataRequest['p_keyword'] : '';
        $requestDefault["p_is_active"] = (isset($dataRequest['p_is_active'])) ? $dataRequest['p_is_active'] : '';
        $requestDefault["p_page"] = (isset($dataRequest['page_no'])) ? $dataRequest['page_no'] : STATUS_INT_MOT;

        return $this->searchDataCommon($requestDefault, ACTION_DEVOPS_SEARCH_APIS);
    }

    //một item hoặc danh sách các item chung api_code
    public function getApiByKey($key_code = '')
    {
        if (trim($key_code) == '')
            return false;
        try {
            //$keyCache = Memcache::CACHE_APIS_BY_KEY;
            //$data = (trim($keyCache) != '') ? Memcache::getCache($keyCache . $key_code) : false;
            $data = false;
            if (!$data) {
                $dataRequestDefault = $this->dataRequestDefault;
                $dataRequestDefault["p_key"] = $key_code;

                $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_GET_APIS_BY_KEY];
                $dataRequest['Data'] = $dataRequestDefault;
                $resultApi = $this->postApiHD($dataRequest);

                $dataGet = $this->setDataResponce($resultApi,$dataRequest);
                $data = (isset($dataGet['Data'][0]) && !empty($dataGet['Data'][0])) ? $dataGet['Data'][0] : false;

                /*if ($data && trim($keyCache) != '') {
                    Memcache::putCache($keyCache . $key_code, $data);
                }*/
            }
            return $data;
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
    }

    public function editApi($dataInput, $action = 'ADD')
    {
        $this->setUserSchemaDB(SCHEMA_OPEN_API);
        if (trim($action) == '' || empty($dataInput))
            return $this->returnStatusError();
        try {
            $this->setUserAction();
            $dataRequestDefault = $this->dataRequestDefault;
            $dataRequestDefault["P_ACTION"] = $action;
            foreach ($dataInput as $key =>$value){
                $dataRequestDefault[trim($key)] = trim($value);
            }
            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_EDIT_APIS];
            $dataRequest['Data'] = $dataRequestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache($dataInput);

            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
        return false;
    }

    public function editApiGroup($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_DEVOPS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_api_group, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_EDIT_API_GROUP];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function getAllApi()
    {
        try {
            $key_cache = Memcache::CACHE_APIS_ALL;
            $data = Memcache::getCache($key_cache);
            if (!$data) {
                $requestDefault = $this->dataRequestDefault;
                $dataGet = $this->searchDataCommon($requestDefault, ACTION_GET_APIS_ALL);
                $data = isset($dataGet['Data']['data']) ? $dataGet['Data']['data'] : false;
                if ($data) {
                    Memcache::putCache($key_cache, $data);
                }
            }
            return $data;
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
    }

    public function removeCache($data)
    {
        if (!isset($data['API_CODE']))
            return false;
        Memcache::forgetCache(Memcache::CACHE_APIS_BY_KEY . $data['GID']);
        Memcache::forgetCache(Memcache::CACHE_APIS_ALL);
        Memcache::forgetCache(Memcache::CACHE_DATABASES_ALL);
    }

    /**************************************************************************************
     * DATABASES
     **************************************************************************************/
    public function editDatabases($dataInput, $action = 'ADD')
    {
        $this->setUserSchemaDB(SCHEMA_DEVOPS);
        $item = $this->actionEditCommon($dataInput, $action, $this->table_api_databases, ACTION_DEVOPS_EDIT_DATABASES_BY_ID);
        $this->removeCacheRelation($dataInput);
        return $item;
    }

    public function getDatabasesById($gid = '')
    {
        if (trim($gid) == '')
            return false;
        try {
            $key_cache = Memcache::CACHE_DATABASES_APIS_BY_ID . $gid;
            $data = Memcache::getCache($key_cache);
            if (!$data) {
                $dataRequestDefault = $this->dataRequestDefault;
                $dataRequestDefault["p_api_code"] = $gid;
                $dataRequest['Data'] = $dataRequestDefault;
                $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_GET_DATABASES_BY_ID];

                $resultApi = $this->postApiHD($dataRequest);
                $dataGet = $this->setDataFromApi($resultApi,$dataRequest);
                $data = isset($dataGet['Data']['data'][0]) ? $dataGet['Data']['data'][0] : false;
                if ($data) {
                    Memcache::putCache($key_cache, $data);
                }
            }
            return $data;
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
    }
    public function getDatabasesByKey($api_code = '')
    {
        if (trim($api_code) == '')
            return false;
        try {
            $key_cache = Memcache::CACHE_DATABASES_APIS_BY_KEY . $api_code;
            $data = Memcache::getCache($key_cache);
            $data = false;
            if (!$data) {
                $this->setUserAction();
                $dataRequestDefault = $this->dataRequestDefault;
                $dataRequestDefault["p_api_code"] = $api_code;
                $dataRequest['Data'] = $dataRequestDefault;
                $dataRequest['Action'] = ['ActionCode' => ACTION_DEVOPS_GET_DATABASES_BY_API_CODE];
                $resultApi = $this->postApiHD($dataRequest);
                $dataGet = $this->setDataFromApi($resultApi,$dataRequest);
                $data = isset($dataGet['Data']['data']) ? $dataGet['Data']['data'] : false;
                if ($data) {
                    Memcache::putCache($key_cache, $data);
                }
            }
            return $data;
        } catch (\PDOException $e) {
            return returnError($e->getMessage());
        }
    }

    public function removeCacheRelation($data, $type = '')
    {
        if (isset($data['GID']) && isset($data['GID'])) {
            Memcache::forgetCache(Memcache::CACHE_DATABASES_APIS_BY_ID . $data['GID']);
        }
    }
}
