<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class ApiHdiCore extends ModelService
{
    public $table_partner_api = TABLE_OAPI_PARTNER;
    public $table_partner_config_api = TABLE_OAPI_PARTNER_CONFIG;
    public $table_partner_grant_api = TABLE_OAPI_PARTNER_GRANT;
    public $table_database_api = TABLE_OAPI_DB_ENV;

    public $table_action_api = TABLE_OAPI_ACTION;
    public $table_action_env_api = TABLE_OAPI_ACTION_ENV;
    public $table_action_system_api = TABLE_OAPI_ACTION_SYSTEM;
    public $table_action_public_api = TABLE_OAPI_ACTION_PUBLIC;

    /*********************************************************************************************************
     * Danh mục: TABLE_ACTION_CODE
     *********************************************************************************************************/
    public function searchActionCode($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->_buildParamActionCode($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_OPENAPI_SEARCH_ACTION_CODE);
    }
    private function _buildParamActionCode($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_partner_code']) && trim($dataRequest['p_partner_code']) !='') ? $dataRequest['p_partner_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function getInforActionCode($action_code = '')
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_action_code"] = $action_code;

        $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_GET_INFOR_ACTION_CODE];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }
    public function editActionCode($dataInput)
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPENAPI);
        $this->mergeFieldInputWithTable($dataInput, $this->table_action_api, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_EDIT_ACTION_CODE];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            //$this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
    public function editActionRelation($dataInput,$type_relation = 'ACTION_ENV')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPENAPI);
        if($type_relation == 'ACTION_ENV'){
            $this->mergeFieldInputWithTable($dataInput, $this->table_action_env_api, $dataUpdate);
        }elseif ($type_relation == 'ACTION_SYSTEM'){
            $this->mergeFieldInputWithTable($dataInput, $this->table_action_system_api, $dataUpdate);
        }elseif($type_relation == 'ACTION_PUBLIC'){
            $this->mergeFieldInputWithTable($dataInput, $this->table_action_public_api, $dataUpdate);
        }elseif($type_relation == 'PARTNER_GRANT'){
            $this->mergeFieldInputWithTable($dataInput, $this->table_partner_grant_api, $dataUpdate);
        }

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_type_relation"] = $type_relation;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_EDIT_ACTION_RELATION];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            //$this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    /*********************************************************************************************************
     * Danh mục: TABLE_SYS_PARTNER
     *********************************************************************************************************/
    public function searchPartnerApi($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->_buildParamPartnerApi($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_OPENAPI_SEARCH_PARTNER_API);
    }
    private function _buildParamPartnerApi($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_partner_code']) && trim($dataRequest['p_partner_code']) !='') ? $dataRequest['p_partner_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function editPartnerApi($dataInput)
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPENAPI);
        $this->mergeFieldInputWithTable($dataInput, $this->table_partner_api, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_EDIT_PARTNER_API];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            //$this->removeCachePartnerApi();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
    public function removeCachePartnerApi()
    {
        Memcache::forgetCache(Memcache::CACHE_ALL_PARTNER_CORE);
    }
    public function editPartnerConfig($dataInput)
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPENAPI);
        $this->mergeFieldInputWithTable($dataInput, $this->table_partner_config_api, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_EDIT_PARTNER_CONFIG];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            //$this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
    public function getInforPartnerApi($product_code = '')
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_partner_code"] = $product_code;

        $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_GET_PARTNER_API];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    /*********************************************************************************************************
     * Danh mục: TABLE_OAPI_DB_ENV
     *********************************************************************************************************/
    public function searchDatabaseApi($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        //arr parram
        $arrParam = $this->_buildParamDatabaseApi($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        return $this->searchDataCommon($requestDefault, ACTION_OPENAPI_SEARCH_DATABASE_API);
    }
    private function _buildParamDatabaseApi($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_partner_code']) && trim($dataRequest['p_partner_code']) !='') ? $dataRequest['p_partner_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_category']) && trim($dataRequest['p_category']) !='') ? $dataRequest['p_category'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_search']) && trim($dataRequest['p_search']) !='') ? $dataRequest['p_search'] : '',
        ];
        return $arrParam;
    }
    public function editDatabaseApi($dataInput)
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_OPENAPI);
        $this->mergeFieldInputWithTable($dataInput, $this->table_database_api, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_OPENAPI_EDIT_DATABASE_API];
            $dataRequest['Data'] = $requestDefault;

            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCacheDatabaseApi();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }
    public function removeCacheDatabaseApi()
    {
        //Memcache::forgetCache(Memcache::CACHE_ALL_DATABASE_CORE);
    }

    /*********************************************************************************************************
     * Danh mục: FUNCTION COMMON
     *********************************************************************************************************/
    public function getPartnerAll(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_PARTNER_CORE);
        $dataList = false;
        if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_1000;
            $data = $this->searchPartnerApi($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_PARTNER_CORE, $dataList);
                }
            }
        }
        return $dataList;
    }

    public function getOptionPartner(){
        $dataList = $this->getPartnerAll();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if($item->IS_ACTIVE == STATUS_INT_MOT){
                    $option[$item->PARTNER_CODE] = $item->PARTNER_CODE;
                }
            }
        }
        return $option;
    }

    public function getDatabaseAll(){
        $dataList = Memcache::getCache(Memcache::CACHE_ALL_DATABASE_CORE);
        $dataList = false;
        if (!$dataList) {
            $dataSearch['p_limit'] = CGlobal::number_show_1000;
            $data = $this->searchDatabaseApi($dataSearch);
            if ($data['Success'] == STATUS_INT_MOT) {
                $dataList = $data['Data']['data'] ?? [];
                if($dataList){
                    Memcache::putCache(Memcache::CACHE_ALL_DATABASE_CORE, $dataList);
                }
            }
        }
        return $dataList;
    }

    public function getOptionDatabase(){
        $dataList = $this->getDatabaseAll();
        $option = [];
        if ($dataList) {
            foreach ($dataList as $ky => $item) {
                if($item->IS_ACTIVE == STATUS_INT_MOT){
                    $option[$item->DATABASE_CODE] = $item->DATABASE_CODE;
                }
            }
        }
        return $option;
    }

}
