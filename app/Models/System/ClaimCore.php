<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\System;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;

class ClaimCore extends ModelService
{
    /*********************************************************************************************************
     * Danh mục: CLAIM_PARTNER_CONFIG
     *********************************************************************************************************/
    public $table_config = TABLE_B_CLAIMS_CLAIM_PARTNER_CONFIG;
    public $table_config_detail = TABLE_B_CLAIMS_CLAIM_PARTNER_CONFIG_DETAILS;
    public $table_email = TABLE_B_CLAIMS_CLAIM_PARTNER_EMAILS;

    public function searchConfig($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = $this->buildParamConfig($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        return $this->searchDataCommon($requestDefault, ACTION_B_CLAIMS_SEARCH_CLAIM_PARTNER_CONFIG);
    }
    private function buildParamConfig($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function editItemConfig($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CLAIMS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_config, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_B_CLAIMS_EDIT_CLAIM_PARTNER_CONFIG];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    /*********************************************************************************************************/
    public function searchConfigDetail($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = $this->buildParamConfigDetail($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        return $this->searchDataCommon($requestDefault, ACTION_B_CLAIMS_SEARCH_CLAIM_PARTNER_CONFIG_DETAIL);
    }
    private function buildParamConfigDetail($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function editItemConfigDetail($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CLAIMS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_config_detail, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_B_CLAIMS_EDIT_CLAIM_PARTNER_CONFIG_DETAIL];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    /*********************************************************************************************************/
    public function searchEmail($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $arrParam = $this->buildParamEmail($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);

        return $this->searchDataCommon($requestDefault, ACTION_B_CLAIMS_SEARCH_CLAIM_PARTNER_EMAIL);
    }
    private function buildParamEmail($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : '',
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',
            'R3' => (isset($dataRequest['p_org_code']) && trim($dataRequest['p_org_code']) !='') ? $dataRequest['p_org_code'] : '',
            'R4' => (isset($dataRequest['p_is_active']) && trim($dataRequest['p_is_active']) !='') ? $dataRequest['p_is_active'] : '',
            'R5' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R6' => (isset($dataRequest['p_limit']) && trim($dataRequest['p_limit']) !='') ? $dataRequest['p_limit'] : 0,
            'R7' => (isset($dataRequest['p_keyword']) && trim($dataRequest['p_keyword']) !='') ? $dataRequest['p_keyword'] : '',
        ];
        return $arrParam;
    }
    public function editItemEmail($dataInput, $action = 'ADD')
    {
        $dataUpdate = [];
        $this->setUserSchemaDB(SCHEMA_B_CLAIMS);
        $this->mergeFieldInputWithTable($dataInput, $this->table_email, $dataUpdate);

        if(empty($dataUpdate))
            return $this->returnStatusError('Không có data cập nhật');
        try {
            $this->setUserAction();
            $requestDefault = $this->dataRequestDefault;
            $requestDefault["p_action"] = $action;
            $requestDefault["p_data_form"] = json_encode($dataUpdate, false);

            $dataRequest['Action'] = ['ActionCode' => ACTION_B_CLAIMS_EDIT_CLAIM_PARTNER_EMAIL];
            $dataRequest['Data'] = $requestDefault;
            $resultApi = $this->postApiHD($dataRequest);
            $this->removeCache();
            return $this->setDataOneResponce($resultApi,$dataRequest);
        } catch (\PDOException $e) {
            return $this->returnStatusError($e->getMessage());
        }
    }

    public function removeCache($data = [])
    {
        //Memcache::forgetCache(Memcache::CACHE_ALL_CATEGORY_CORE);
    }
}
