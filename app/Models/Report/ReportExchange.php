<?php
/**
 * QuynhTM
 * 13/03/2022
 */

namespace App\Models\Report;

use App\Models\Selling\ClaimHdi;
use App\Library\AdminFunction\Memcache;
use App\Services\ModelService;
use Illuminate\Support\Carbon;

class ReportExchange extends ModelService
{
    /********************************************************************************************
     * báo cáo sản phẩm chi tiết
     ********************************************************************************************/
    public function searchReportExchange($dataRequest = array())
    {
        $this->setUserAction();
        $requestDefault = $this->dataRequestDefault;
        $requestDefault["p_org_code"] = (isset($dataRequest['p_org_code'])) ? $dataRequest['p_org_code'] : '';

        //arr parram
        $arrParam = $this->buildParamExchange($dataRequest);
        $requestDefault["p_business"] = json_encode($arrParam, false);
        $dataRequest['Action'] = [
            'ActionCode' => ACTION_REPORT_DATA_RECONCILIATION,
        ];
        $dataRequest['Data'] = $requestDefault;
        $resultApi = $this->postApiHD($dataRequest);
        return $this->setDataResponce($resultApi,$dataRequest);
    }

    private function buildParamExchange($dataRequest = [])
    {
        $arrParam = [
            'R1' => (isset($dataRequest['p_product_code']) && trim($dataRequest['p_product_code']) !='') ? $dataRequest['p_product_code'] : 'BAY_AT',//PRODUCT_CODE
            'R2' => (isset($dataRequest['p_pack_code']) && trim($dataRequest['p_pack_code']) !='') ? $dataRequest['p_pack_code'] : '',//PACK_CODE
            'R3' => (isset($dataRequest['p_month']) && trim($dataRequest['p_month']) !='') ? $dataRequest['p_month'] : getTimeCurrent('m'),//MONTH
            'R4' => (isset($dataRequest['p_year']) && trim($dataRequest['p_year']) !='') ? $dataRequest['p_year'] : getTimeCurrent('y'),//YEAR
            'R5' => (isset($dataRequest['is_accumulated_defaul']) && trim($dataRequest['is_accumulated_defaul']) !='') ? $dataRequest['is_accumulated_defaul'] : 1,//IS_ACCUMULATE lũy quý
            'R6' => (isset($dataRequest['page_no']) && trim($dataRequest['page_no']) !='') ? $dataRequest['page_no'] : 1,//pageing
            'R7' => (isset($dataRequest['p_from_date']) && trim($dataRequest['p_from_date']) !='') ? $dataRequest['p_from_date'] : '',//ngày bắt đầu
            'R8' => (isset($dataRequest['p_to_date']) && trim($dataRequest['p_to_date']) !='') ? $dataRequest['p_to_date'] : '',//ngày đến
            'R9' => (isset($dataRequest['p_type_date_search']) && trim($dataRequest['p_type_date_search']) !='') ? $dataRequest['p_type_date_search'] : '',//type date search
            'R10' => (isset($dataRequest['p_status']) && trim($dataRequest['p_status']) !='') ? $dataRequest['p_status'] : '',//trạng thái
        ];
        return $arrParam;
    }
}
