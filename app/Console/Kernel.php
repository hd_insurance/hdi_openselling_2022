<?php

namespace App\Console;

use App\Console\Commands\CronAutoResignRequest;
use App\Console\Commands\CronSendEmailInsurRequest;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Config;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *      nano crontab -e
     *      * * * * * php /opt/hdi_php/hdi_system_dev/artisan schedule:run 2>&1 >> /opt/hdi_php/hdi_system_dev/storage/logs/cronjob.log
     * @var array
     */
    protected $commands = [
        CronAutoResignRequest::class,
        CronSendEmailInsurRequest::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $checkRunSchedule = Config::get('config.IS_SCHEDULE',false);
        if($checkRunSchedule){
            $schedule->command('cronjob:CronSendEmailInsurRequest')->everyTwoMinutes()->withoutOverlapping();//2 phút chạy quest gửi mail nhận yêu cầu
            $schedule->command('cronjob:CronAutoResignRequest')->everyFiveMinutes()->withoutOverlapping();//5 phút cháy ký số nếu có
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
