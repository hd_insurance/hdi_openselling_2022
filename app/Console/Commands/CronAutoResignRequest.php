<?php
/**
 * QuynhTM
 */

namespace App\Console\Commands;

use App\Library\AdminFunction\CGlobal;
use App\Models\Selling\DigitalSignatureRequest;
use App\Services\ServiceCommon;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;


class CronAutoResignRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:CronAutoResignRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob tự động ký số theo yêu cầu';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function set($key, $value, $default = null)
    {
        if (property_exists($this, $key)) {
            $this->$key = $value;
        } else {
            $this->$key = $default;
        }
    }

    public function get($key, $default = null)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        } else {
            return $default;
        }
    }

    ////manager/runCronjob?job=CronAutoResignRequest
    public function handle()
    {
        $folderLog = FOLDER_LOG_RESIGN_CRONJOB . getIntDateM_Y();
        $fileName = 'resignCertificateAuto' . getIntDateD_M_Y() . '.log';

        $this->modelObj = new DigitalSignatureRequest();
        $page_no = STATUS_INT_MOT;
        $dataList = $resultUpdate =[];

        $search['page_no'] = $page_no;
        //$search['p_from_date'] = date('d/m/Y', strtotime(Carbon::now()->startOfMonth()));
        $search['p_from_date'] = date('d/m/Y', strtotime(Carbon::yesterday()));
        $search['p_to_date'] = date('d/m/Y', strtotime(Carbon::tomorrow()));
        $search['p_is_run'] = STATUS_INT_KHONG;
        $search['p_status'] = STATUS_INT_KHONG;

        $result = $this->modelObj->searchList($search);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? [];
        }

        debugLog('DATA YEU CAU KY SO', $fileName, $folderLog);
        debugLog($dataList, $fileName, $folderLog);
        $urlServiceFile = Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/';
        if (!empty($dataList)) {
            $now = time();
            $fieldChange = DigitalSignatureRequest::$arrKeyMergerDB;
            $arrKeyUpdateCase2 = ['CUSTOMER_PHONE', 'CUSTOMER_EMAIL'];

            foreach ($dataList as $ky => $dataRequest) {
                $effective_date = !empty($dataRequest->EFFECTIVE_DATE) ? strtotime($dataRequest->EFFECTIVE_DATE) : 0;
                //$expiration_date = !empty($dataRequest->EXPIRATION_DATE) ? strtotime($dataRequest->EXPIRATION_DATE) : 0;
                $dataChange = (isset($dataRequest->DATA_CHANGE) && !empty($dataRequest->DATA_CHANGE)) ? json_decode($dataRequest->DATA_CHANGE) : [];
                $changeDetail = isset($dataRequest->CHANGE_DETAIL) ? json_decode($dataRequest->CHANGE_DETAIL, true) : [];
                $changeExtend = isset($dataRequest->CHANGE_EXTEND) ? json_decode($dataRequest->CHANGE_EXTEND, true) : [];

                //data tam
                $temChange = [];
                $updateRequest = [];
                $checkCase2 = STATUS_INT_KHONG;
                $changeKeys = [];
                $changeKeysHandMade = STATUS_INT_KHONG;

                //check trước ngày hiệu lực: cập nhật all data request
                if ($effective_date > $now) {
                    foreach ($dataChange as $keyChange => $objData) {
                        if (in_array($keyChange, array_keys($fieldChange)) && trim($objData->VALUE_NEW) != '') {
                            if (isset($objData->VALUE_NEW) && trim($objData->VALUE_NEW) != '') {
                                $temChange[$keyChange] = $objData;
                                $changeKeys[] = $keyChange;
                            }
                        }
                    }
                } //chỉ cập nhật: số điện thoại và Emai của KH
                else {
                    foreach ($dataChange as $keyChange => $objData) {
                        if (in_array($keyChange, $arrKeyUpdateCase2)) {
                            if (isset($objData->VALUE_NEW) && trim($objData->VALUE_NEW) != '') {
                                $temChange[$keyChange] = $objData;
                                $changeKeys[] = $keyChange;
                                $changeKeysHandMade = STATUS_INT_MOT;
                            }
                        }else{
                            if (isset($objData->VALUE_NEW) && trim($objData->VALUE_NEW) != '') {
                                $checkCase2 = STATUS_INT_MOT;
                            }
                        }
                    }
                }

                //nếu có nhiều hơn dữ liệu cập nhật Email và Phone thì để duyệt bằng tay
                if($checkCase2 == STATUS_INT_MOT){
                    $temChange = $changeKeys = [];
                }

                if (!empty($temChange) && !empty($changeKeys)) {
                    $fieldChange = DigitalSignatureRequest::$arrKeyMergerDB;
                    foreach ($temChange as $kys_field => $infor_chage) {
                        if (isset($fieldChange[$kys_field])) {
                            $field_db = $fieldChange[$kys_field];
                            //if (isset($changeDetail[$field_db])) {
                                $changeDetail[$field_db] = $infor_chage->VALUE_NEW;
                            //}
                            //if (isset($changeExtend[$field_db])) {
                                $changeExtend[$field_db] = $infor_chage->VALUE_NEW;
                            //}
                        }
                    }
                    $updateRequest['PRODUCT_CODE'] = isset($dataRequest->PRODUCT_CODE) ? $dataRequest->PRODUCT_CODE : '';
                    $updateRequest['REQUEST_CODE'] = isset($dataRequest->REQUEST_CODE) ? $dataRequest->REQUEST_CODE : '';
                    $updateRequest['FILE_ATTACH'] = isset($dataRequest->FILE_ATTACH) ? $dataRequest->FILE_ATTACH : '';
                    $updateRequest['CHANGE_DETAIL'] = json_encode($changeDetail);
                    $updateRequest['CHANGE_EXTEND'] = json_encode($changeExtend);
                    $updateRequest['CHANGE_KEYS'] = join(';', $changeKeys);
                    $updateRequest['USER_ACTION'] = 'AUTO';
                    $updateRequest['IS_RUN'] = STATUS_INT_HAI;//tự động
                    $updateRequest['STATUS'] = STATUS_INT_MOT;//đã xử lý
                    $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_KHONG;//đã xử lý

                    //cập nhật Case 2 Auto: //chỉ cập nhật: số điện thoại và Emai của KH
                    if ($changeKeysHandMade == STATUS_INT_MOT) {
                        $updateRequest['NOTE_REPLY'] = 'Ký số cập nhật Phone và Email của Khách hàng';
                        $resultUpdate = $this->modelObj->editRequest($updateRequest);
                    } else {
                        $updateRequest['NOTE_REPLY'] = 'Ký số cập nhật GCN trước ngày hiệu lực';
                        $resultUpdate = $this->modelObj->editRequest($updateRequest);
                    }
                    debugLog('DATA UPDATE REQUEST', $fileName, $folderLog);
                    debugLog($updateRequest, $fileName, $folderLog);
                }

                //cập nhật thành công và gửi mail
                if (isset($resultUpdate['Success']) && $resultUpdate['Success'] == STATUS_INT_MOT) {
                    $dataResign['CERTIFICATE_NO'] = $dataRequest->CERTIFICATE_NO;
                    $dataResign['PRODUCT_CODE'] = $dataRequest->PRODUCT_CODE;
                    $service = new ServiceCommon();
                    $resign = $service->resignCertificate($dataResign);

                    debugLog('DATA KY SO CERTIFICATE_NO=' . $dataRequest->CERTIFICATE_NO . ' VA PRODUCT_CODE=' . $dataRequest->PRODUCT_CODE, $fileName, $folderLog);
                    debugLog($resign, $fileName, $folderLog);

                    //bien hien thị email
                    $email_to = isset($changeDetail['EMAIL']) ? $changeDetail['EMAIL'] : '';
                    $name_customer = isset($changeDetail['NAME']) ? $changeDetail['NAME'] : '';
                    $file_code = isset($dataRequest->FILE_CODE) ? $dataRequest->FILE_CODE : '';
                    $file_code_new = (isset($resign['file_signed']) && trim($resign['file_signed']) != '') ? trim($resign['file_signed']): $file_code;
                    $urlFile = (trim($file_code_new) != '') ? $urlServiceFile . $file_code_new : '';
                    $email_cc = (isset($dataRequest->CONTACT_EMAIL) && trim($dataRequest->CONTACT_EMAIL) != '') ? trim($dataRequest->CONTACT_EMAIL) : '';

                    //gửi mail ký số thành công
                    $dataSendMail = [
                        "NAME_EN" => $name_customer,
                        "NAME" => $name_customer,
                        "CERTIFICATE_NO" => $dataRequest->CERTIFICATE_NO,
                        "URL_FILE" => $urlFile,
                    ];
                    if (trim($email_to) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => DigitalSignatureRequest::STATUS_EMAIL_DYYC_SDBS,
                            "PRODUCT_CODE" => DigitalSignatureRequest::STATUS_EMAIL_DYYC_SDBS,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $email_to,//$emailSend,
                            "CC" => $email_cc,
                            "BCC" => CGlobal::mail_test];
                        $sendEmail = app(ServiceCommon::class)->sendMailWithTemplate($dataSendMail);
                    }

                    if (isset($resign['file_signed']) && trim($resign['file_signed']) == '') {
                        $updateRequest['STATUS'] = STATUS_INT_BON;//xử lý lỗi
                        $updateRequest['CHANGE_KEYS'] = '';
                    }
                    if (isset($sendEmail->Success) && $sendEmail->Success == 1) {
                        $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_MOT;
                    }
                    $this->modelObj->editRequest($updateRequest, 'EDIT_REQUEST');
                }
                //DATA không có gì để cập nhật, ký số lại, chờ phê duyệt bằng tay, không gửi ,mail
                else{
                    $updateRequest['PRODUCT_CODE'] = isset($dataRequest->PRODUCT_CODE) ? $dataRequest->PRODUCT_CODE : '';
                    $updateRequest['REQUEST_CODE'] = isset($dataRequest->REQUEST_CODE) ? $dataRequest->REQUEST_CODE : '';
                    $updateRequest['FILE_ATTACH'] = isset($dataRequest->FILE_ATTACH) ? $dataRequest->FILE_ATTACH : '';
                    $updateRequest['CHANGE_DETAIL'] = json_encode($changeDetail);
                    $updateRequest['CHANGE_EXTEND'] = json_encode($changeExtend);
                    $updateRequest['CHANGE_KEYS'] = '';
                    $updateRequest['USER_ACTION'] = 'AUTO';
                    $updateRequest['IS_RUN'] = STATUS_INT_HAI;//tự động
                    $updateRequest['STATUS'] = STATUS_INT_HAI;//Chờ duyệt
                    $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_MOT;//đã xử lý
                    $updateRequest['NOTE_REPLY'] = 'Yêu cầu bổ sung chờ HDI duyệt.';
                    $this->modelObj->editRequest($updateRequest,'EDIT_REQUEST');

                    debugLog('DATA REQUEST KHONG DU DK KY SO', $fileName, $folderLog);
                    debugLog($dataRequest, $fileName, $folderLog);
                }
            }
            endLog($fileName, $folderLog);
        } else {
            debugLog('KHONG CO YEU CAU DE KY SO TU DONG!', $fileName, $folderLog);
            endLog($fileName, $folderLog);
        }

    }
}

?>
