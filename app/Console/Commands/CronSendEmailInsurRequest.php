<?php
/**
 * QuynhTM
 */

namespace App\Console\Commands;

use App\Library\AdminFunction\CGlobal;
use App\Library\AdminFunction\Upload;
use App\Models\Selling\DigitalSignatureRequest;
use App\Services\ServiceCommon;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;


class CronSendEmailInsurRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:CronSendEmailInsurRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob tự động gửi mail thông báo đã nhận yêu cầu ký số';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function set($key, $value, $default = null)
    {
        if (property_exists($this, $key)) {
            $this->$key = $value;
        } else {
            $this->$key = $default;
        }
    }

    public function get($key, $default = null)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        } else {
            return $default;
        }
    }

    ////manager/runCronjob?job=CronSendEmailInsurRequest
    public function handle()
    {
        $folderLog = FOLDER_LOG_RESIGN_CRONJOB . getIntDateM_Y();
        $fileName = 'sendMailInsurRequest' . getIntDateD_M_Y() . '.log';

        $this->modelObj = new DigitalSignatureRequest();
        $page_no = STATUS_INT_MOT;
        $dataList = [];

        $search['page_no'] = $page_no;
        $search['p_from_date'] = date('d/m/Y', strtotime(Carbon::yesterday()));
        $search['p_to_date'] = date('d/m/Y', strtotime(Carbon::tomorrow()));
        $search['p_is_send_email'] = STATUS_INT_KHONG;
        $search['p_status'] = STATUS_INT_KHONG;

        //test
        //$search['p_is_send_email'] = 13;
        //$search['p_status'] = 13;
        //$search['p_from_date'] = date('d/m/Y', strtotime(Carbon::now()->startOfMonth()));

        $result = $this->modelObj->searchList($search);
        myDebug('$result',false);
        myDebug($result,false);
        if ($result['Success'] == STATUS_INT_MOT) {
            $dataList = $result['Data']['data'] ?? [];
        }

        $urlServiceFile = Config::get('config.URL_HYPERSERVICES_' . Config::get('config.ENVIRONMENT')) . 'f/';
        if (!empty($dataList)) {
            $arrEmail = [];
            $arrUpdateRequest = [];
            foreach ($dataList as $ky => $dataRequest) {
                $request_code = isset($dataRequest->REQUEST_CODE) ? $dataRequest->REQUEST_CODE : '';
                $certificate_no = isset($dataRequest->CERTIFICATE_NO) ? $dataRequest->CERTIFICATE_NO : '';
                $product_code = isset($dataRequest->PRODUCT_CODE) ? $dataRequest->PRODUCT_CODE : '';
                $changeDetail = isset($dataRequest->CHANGE_DETAIL) ? json_decode($dataRequest->CHANGE_DETAIL, true) : [];
                $changeExtend = isset($dataRequest->CHANGE_EXTEND) ? json_decode($dataRequest->CHANGE_EXTEND, true) : [];
                $arrImage = isset($dataRequest->FILE_ATTACH) ? json_decode($dataRequest->FILE_ATTACH, true) : [];

                //lấy ảnh upload
                if(!empty($arrImage)){
                    foreach ($arrImage as $kim=>&$inforImage){
                        if(isset($inforImage['file_base64']) && trim($inforImage['file_base64']) != ''){
                            $folder = FOLDER_FILE_REQUEST;
                            $fix_name = $dataRequest->REQUEST_CODE;
                            $nameFile = isset($inforImage['file_name'])? $fix_name.'-'.$inforImage['file_name'].'.'.$inforImage['file_extension']: $fix_name.'-'.'file request '.time().'.png';
                            $pathFileUpload = app(Upload::class)->saveFileWithUrl(trim($inforImage['file_base64']), $nameFile, $folder);

                            $file_id = app(ServiceCommon::class)->moveFileToServerStore($pathFileUpload, false);
                            $inforImage['file_id'] = $file_id;
                            if (is_file($pathFileUpload)) {
                                unlink($pathFileUpload);
                            }
                        }
                    }
                }

                //bien hien thị email
                $email_to = isset($changeDetail['EMAIL']) ? $changeDetail['EMAIL'] : '';
                $name_customer = isset($changeDetail['NAME']) ? $changeDetail['NAME'] : '';
                $file_code = isset($dataRequest->FILE_CODE) ? $dataRequest->FILE_CODE : '';
                $urlFile = (trim($file_code) != '') ? $urlServiceFile.$file_code : '';
                $email_cc = (isset($dataRequest->CONTACT_EMAIL) && trim($dataRequest->CONTACT_EMAIL) != '')? trim($dataRequest->CONTACT_EMAIL):'';

                $arrEmail[$email_to] = [
                    'CERTIFICATE_NO' => $certificate_no,
                    'NAME_CUSTOMER' => $name_customer,
                    'URL_FILE' => $urlFile,
                    'EMAIL_CC' => $email_cc,
                    'PRODUCT_CODE' => $product_code,
                    'REQUEST_CODE' => $request_code,
                ];

                $updateRequest['PRODUCT_CODE'] = $product_code;
                $updateRequest['REQUEST_CODE'] = isset($dataRequest->REQUEST_CODE) ? $dataRequest->REQUEST_CODE : '';
                $updateRequest['CHANGE_DETAIL'] = json_encode($changeDetail);
                $updateRequest['CHANGE_EXTEND'] = json_encode($changeExtend);
                $updateRequest['FILE_ATTACH'] = json_encode($arrImage, false);
                $updateRequest['CHANGE_KEYS'] = '';
                $updateRequest['USER_ACTION'] = 'AUTO';
                $updateRequest['IS_RUN'] = STATUS_INT_KHONG;
                $updateRequest['STATUS'] = STATUS_INT_KHONG;
                $updateRequest['IS_SEND_EMAIL'] = STATUS_INT_KHONG;//Chưa gửi email
                $updateRequest['NOTE_REPLY'] = 'Đã gửi mail thông báo HDI tiếp nhận yêu cầu ký số';
                $arrUpdateRequest[$email_to][$request_code] = $updateRequest;
            }

            debugLog('DATA Update MAIL', $fileName, $folderLog);
            debugLog($arrUpdateRequest, $fileName, $folderLog);

            debugLog('DATA SEND MAIL', $fileName, $folderLog);
            debugLog($arrEmail, $fileName, $folderLog);

            //gửi mai theo nhóm yêu cầu ký số
            if (!empty($arrEmail) && !empty($arrUpdateRequest)) {
                $email_cc = CGlobal::mail_test;
                foreach ($arrEmail as $email_to => $dataEmail) {
                    //NAME_EN;NAME;CERTIFICATE_NO;URL_FILE
                    $dataSendMail = [
                        "NAME_EN" => $dataEmail['NAME_CUSTOMER'],
                        "NAME" => $dataEmail['NAME_CUSTOMER'],
                        "CERTIFICATE_NO" => $dataEmail['CERTIFICATE_NO'],
                        "URL_FILE" => $dataEmail['URL_FILE'],
                    ];

                    if (trim($email_to) != '') {
                        $dataSendMail['TEMP'][] = [
                            "TEMP_CODE" => DigitalSignatureRequest::STATUS_EMAIL_TNYC_SDBS,
                            "PRODUCT_CODE" => DigitalSignatureRequest::STATUS_EMAIL_TNYC_SDBS,
                            "ORG_CODE" => ORG_HDI,
                            "TYPE" => "EMAIL",
                            "TO" => $email_to,//$emailSend,
                            "CC" => $dataEmail['EMAIL_CC'],
                            "BCC" => $email_cc];
                        $sendEmail = app(ServiceCommon::class)->sendMailWithTemplate($dataSendMail);
                        if (isset($sendEmail->Success) && $sendEmail->Success == 1) {
                            foreach ($arrUpdateRequest[$email_to] as $request_code => $updateInforRequest) {
                                $updateInforRequest['IS_SEND_EMAIL'] = STATUS_INT_MOT;
                                $this->modelObj->editRequest($updateInforRequest, 'EDIT_REQUEST');
                            }
                        }
                    }
                }
            }
            endLog($fileName, $folderLog);
        } else {
            debugLog('KHONG CO YEU CAU DE GUI MAIL!', $fileName, $folderLog);
            endLog($fileName, $folderLog);
        }

    }
}

?>
