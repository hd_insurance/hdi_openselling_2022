<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 03/2020
* @Version   : 1.0
*/
/*********************************************************************************************************
 * Router các thành phần Portal
 * *******************************************************************************************************
 */

const ModulePortal = DIR_PRO_PORTAL;

Route::group(array('prefix' => 'portal'), function () {
    Route::group(array('prefix' => 'pushData'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'pushData.index', 'uses' => ModulePortal . '\PushData\PushDataController@index'));
        Route::get('ajaxGetItem', array('as' => 'pushData.ajaxGetItem', 'uses' => ModulePortal . '\PushData\PushDataController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'pushData.ajaxPostItem', 'uses' => ModulePortal . '\PushData\PushDataController@ajaxPostItem'));
        Route::post('ajaxActionFunction', array('as' => 'pushData.ajaxActionFunction', 'uses' => ModulePortal . '\PushData\PushDataController@ajaxActionFunction'));
    });

});

