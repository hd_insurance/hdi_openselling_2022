<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(array('prefix' => 'HdiService/Api', 'before' => ''), function () {
    Route::post('pushRequestOrder', array('as' => 'api.pushRequestOrder', 'uses' => DIR_PRO_API . '\ApiPushRequestOrderController@pushOrder'));
});
