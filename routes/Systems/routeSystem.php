<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 03/2020
* @Version   : 1.0
*/
/*********************************************************************************************************
 * Router các thành phần DMS Portal
 * *******************************************************************************************************
 */

const ModuleSystem = DIR_PRO_SYSTEM;

Route::group(array('prefix' => 'system'), function () {

    /* Quản lý Danh mục tổ chức */
    Route::group(array('prefix' => 'organization'), function () {
        Route::match(['GET', 'POST'],'indexOrganization', array('as' => 'organization.indexOrganization', 'uses' => ModuleSystem . '\OrganizationController@indexOrganization'));
        Route::get('ajaxGetOrganization', array('as' => 'organization.ajaxGetOrganization', 'uses' => ModuleSystem . '\OrganizationController@ajaxGetOrganization'));
        Route::post('ajaxPostOrganization', array('as' => 'organization.ajaxPostOrganization', 'uses' => ModuleSystem . '\OrganizationController@ajaxPostOrganization'));
        Route::post('ajaxDeleteOrganization', array('as' => 'organization.ajaxDeleteOrganization', 'uses' => ModuleSystem . '\OrganizationController@ajaxDeleteOrganization'));
        Route::post('ajaxGetData', array('as' => 'organization.ajaxGetData', 'uses' => ModuleSystem . '\OrganizationController@ajaxGetData'));
        //thêm sửa xóa Org other
        Route::post('ajaxUpdateOrgRelation', array('as' => 'organization.ajaxUpdateOrgRelation', 'uses' => ModuleSystem . '\OrganizationController@ajaxUpdateOrgRelation'));
        Route::post('ajaxDeleteOrgRelation', array('as' => 'organization.ajaxDeleteOrgRelation', 'uses' => ModuleSystem . '\OrganizationController@ajaxDeleteOrgRelation'));
    });

    /* Quản lý User hệ thống */
    Route::group(array('prefix' => 'user'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'userSystem.indexUser', 'uses' => ModuleSystem . '\UserSystemController@indexUser'));
        Route::get('ajaxGetUser', array('as' => 'userSystem.ajaxGetUser', 'uses' => ModuleSystem . '\UserSystemController@ajaxGetUser'));
        Route::post('ajaxPostUser', array('as' => 'userSystem.ajaxPostUser', 'uses' => ModuleSystem . '\UserSystemController@ajaxPostUser'));
        Route::post('ajaxDeleteUser', array('as' => 'userSystem.ajaxDeleteUser', 'uses' => ModuleSystem . '\UserSystemController@ajaxDeleteUser'));
        Route::post('ajaxGetData', array('as' => 'userSystem.ajaxGetData', 'uses' => ModuleSystem . '\UserSystemController@ajaxGetData'));
        //thêm sửa xóa tab other của user
        Route::post('ajaxUpdateUserRelation', array('as' => 'userSystem.ajaxUpdateUserRelation', 'uses' => ModuleSystem . '\UserSystemController@ajaxUpdateUserRelation'));

        //setting product user
        Route::get('ajaxGetProductWithUser', array('as' => 'userSystem.ajaxGetProductWithUser', 'uses' => ModuleSystem . '\UserSystemController@ajaxGetProductWithUser'));
        Route::post('ajaxPostProductWithUser', array('as' => 'userSystem.ajaxPostProductWithUser', 'uses' => ModuleSystem . '\UserSystemController@ajaxPostProductWithUser'));

        //profile
        Route::get('profile/{id}/{name}.html', array('as' => 'userSystem.userProfile', 'uses' => ModuleSystem . '\UserSystemController@getProfile'));
        Route::post('profile/{id}/{name}.html', array('as' => 'userSystem.userProfile', 'uses' => ModuleSystem . '\UserSystemController@postProfile'));
        //change pass
        Route::get('ajaxGetChangePass', array('as' => 'userSystem.ajaxGetChangePass', 'uses' => ModuleSystem . '\UserSystemController@ajaxGetChangePass'));
        Route::post('ajaxPostChangePass', array('as' => 'userSystem.ajaxPostChangePass', 'uses' => ModuleSystem . '\UserSystemController@ajaxPostChangePass'));
    });

    // Phòng ban
    Route::group(array('prefix' => 'depart'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'depart.index', 'uses' => ModuleSystem . '\DepartController@index'));
        Route::get('ajaxGetDepart', array('as' => 'depart.ajaxGetDepart', 'uses' => ModuleSystem . '\DepartController@ajaxGetDepart'));
        Route::post('ajaxPostDepart', array('as' => 'depart.ajaxPostDepart', 'uses' => ModuleSystem . '\DepartController@ajaxPostDepart'));
        Route::post('ajaxDeleteDepart', array('as' => 'depart.ajaxDeleteDepart', 'uses' => ModuleSystem . '\DepartController@ajaxDeleteDepart'));
        Route::post('ajaxGetData', array('as' => 'depart.ajaxGetData', 'uses' => ModuleSystem . '\DepartController@ajaxGetData'));
        //chuyển đổi phòng ban
        Route::post('ajaxPostMoveDepartOfStaff', array('as' => 'depart.ajaxPostMoveDepartOfStaff', 'uses' => ModuleSystem . '\DepartController@ajaxPostMoveDepartOfStaff'));
    });

    // banks
    Route::group(array('prefix' => 'banks'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'banks.index', 'uses' => ModuleSystem . '\BankController@index'));
        Route::get('ajaxGetItem', array('as' => 'banks.ajaxGetItem', 'uses' => ModuleSystem . '\BankController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'banks.ajaxPostItem', 'uses' => ModuleSystem . '\BankController@ajaxPostItem'));
        Route::post('ajaxDeleteItem', array('as' => 'banks.ajaxDeleteItem', 'uses' => ModuleSystem . '\BankController@ajaxDeleteItem'));
    });

    /* Quản lý menu system */
    Route::group(array('prefix' => 'systemMenu'), function () {
        Route::match(['GET', 'POST'], 'indexMenu', array('as' => 'menu.indexMenu', 'uses' => ModuleSystem . '\MenuSystemController@indexMenu'));
        Route::get('ajaxGetMenu', array('as' => 'menu.ajaxGetMenu', 'uses' => ModuleSystem . '\MenuSystemController@ajaxGetMenu'));
        Route::post('ajaxPostMenu', array('as' => 'menu.ajaxPostMenu', 'uses' => ModuleSystem . '\MenuSystemController@ajaxPostMenu'));
        Route::post('ajaxDeleteMenu', array('as' => 'menu.ajaxDeleteMenu', 'uses' => ModuleSystem . '\MenuSystemController@ajaxDeleteMenu'));
    });

    /* Quản lý group menu */
    Route::group(array('prefix' => 'menuGroup'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'menuGroup.index', 'uses' => ModuleSystem . '\MenuGroupController@index'));
        Route::get('ajaxGetGroupMenu', array('as' => 'menuGroup.ajaxGetGroupMenu', 'uses' => ModuleSystem . '\MenuGroupController@ajaxGetGroupMenu'));
        Route::post('ajaxPostGroupMenu', array('as' => 'menuGroup.ajaxPostGroupMenu', 'uses' => ModuleSystem . '\MenuGroupController@ajaxPostGroupMenu'));
        Route::post('ajaxDeleteGroupMenu', array('as' => 'menuGroup.ajaxDeleteGroupMenu', 'uses' => ModuleSystem . '\MenuGroupController@ajaxDeleteGroupMenu'));
        Route::post('ajaxGetData', array('as' => 'menuGroup.ajaxGetData', 'uses' => ModuleSystem . '\MenuGroupController@ajaxGetData'));
        //thêm sửa xóa tab other
        Route::post('ajaxUpdateRelation', array('as' => 'menuGroup.ajaxUpdateRelation', 'uses' => ModuleSystem . '\MenuGroupController@ajaxUpdateRelation'));
        Route::post('ajaxGetListMenuPermission', array('as' => 'menuGroup.ajaxGetListMenuPermission', 'uses' => ModuleSystem . '\MenuGroupController@ajaxGetListMenuPermission'));
    });

    /* Quản lý Type defines */
    Route::group(array('prefix' => 'typeDefines'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'typeDefines.index', 'uses' => ModuleSystem . '\TypeDefinesController@index'));
        Route::get('ajaxGetItem', array('as' => 'typeDefines.ajaxGetItem', 'uses' => ModuleSystem . '\TypeDefinesController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'typeDefines.ajaxPostItem', 'uses' => ModuleSystem . '\TypeDefinesController@ajaxPostItem'));
    });

    /* Quản lý đơn vị hành chính: tỉnh thành quận huyện*/
    Route::group(array('prefix' => 'provincial'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'provincial.index', 'uses' => ModuleSystem . '\ProvincialController@index'));
        Route::get('ajaxGetItem', array('as' => 'provincial.ajaxGetItem', 'uses' => ModuleSystem . '\ProvincialController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'provincial.ajaxPostItem', 'uses' => ModuleSystem . '\ProvincialController@ajaxPostItem'));
        Route::post('ajaxDeleteItem', array('as' => 'provincial.ajaxDeleteItem', 'uses' => ModuleSystem . '\ProvincialController@ajaxDeleteItem'));
        Route::post('ajaxGetData', array('as' => 'provincial.ajaxGetData', 'uses' => ModuleSystem . '\ProvincialController@ajaxGetData'));
        //thêm sửa xóa tab other của user
        Route::post('ajaxUpdateItemRelation', array('as' => 'provincial.ajaxUpdateItemRelation', 'uses' => ModuleSystem . '\UserSystemController@ajaxUpdateItemRelation'));
    });

    /*********************************************************************************************
     * Setting PartnerGroupApi
     *********************************************************************************************/
    Route::group(array('prefix' => 'storageSynch'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'storageSynch.index', 'uses' => ModuleSystem . '\SystemCore\StorageSynchController@index'));
        Route::get('ajaxGetItem', array('as' => 'storageSynch.ajaxGetItem', 'uses' => ModuleSystem . '\SystemCore\StorageSynchController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'storageSynch.ajaxPostItem', 'uses' => ModuleSystem . '\SystemCore\StorageSynchController@ajaxPostItem'));
        Route::post('ajaxActionFunction', array('as' => 'storageSynch.ajaxActionFunction', 'uses' => ModuleSystem . '\SystemCore\StorageSynchController@ajaxActionFunction'));
    });
    /*********************************************************************************************
     * Setting insur core
     *********************************************************************************************/
    /* Quản lý insurCore */
    Route::group(array('prefix' => 'insurDetail'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'insurDetail.index', 'uses' => ModuleSystem . '\InsurCore\InsurDetailController@index'));
        Route::get('ajaxGetItem', array('as' => 'insurDetail.ajaxGetItem', 'uses' => ModuleSystem . '\InsurCore\InsurDetailController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'insurDetail.ajaxPostItem', 'uses' => ModuleSystem . '\InsurCore\InsurDetailController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'insurDetail.ajaxGetData', 'uses' => ModuleSystem . '\InsurCore\InsurDetailController@ajaxGetData'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'insurDetail.ajaxUpdateRelation', 'uses' => ModuleSystem . '\InsurCore\InsurDetailController@ajaxUpdateRelation'));
    });


    /*********************************************************************************************
     * Setting product core
     *********************************************************************************************/
    /* Quản lý sản phẩm */
    Route::group(array('prefix' => 'productDeclaration'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'productDeclaration.index', 'uses' => ModuleSystem . '\ProductCore\ProductDeclarationController@index'));
        Route::get('ajaxGetItem', array('as' => 'productDeclaration.ajaxGetItem', 'uses' => ModuleSystem . '\ProductCore\ProductDeclarationController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'productDeclaration.ajaxPostItem', 'uses' => ModuleSystem . '\ProductCore\ProductDeclarationController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'productDeclaration.ajaxGetData', 'uses' => ModuleSystem . '\ProductCore\ProductDeclarationController@ajaxGetData'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'productDeclaration.ajaxUpdateRelation', 'uses' => ModuleSystem . '\ProductCore\ProductDeclarationController@ajaxUpdateRelation'));
    });

    /* Quản lý PackageDeclaration */
    Route::group(array('prefix' => 'packageDeclaration'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'packageDeclaration.index', 'uses' => ModuleSystem . '\ProductCore\PackageDeclarationController@index'));
        Route::get('ajaxGetItem', array('as' => 'packageDeclaration.ajaxGetItem', 'uses' => ModuleSystem . '\ProductCore\PackageDeclarationController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'packageDeclaration.ajaxPostItem', 'uses' => ModuleSystem . '\ProductCore\PackageDeclarationController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'packageDeclaration.ajaxGetData', 'uses' => ModuleSystem . '\ProductCore\PackageDeclarationController@ajaxGetData'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'packageDeclaration.ajaxUpdateRelation', 'uses' => ModuleSystem . '\ProductCore\PackageDeclarationController@ajaxUpdateRelation'));
    });

    /* Quản lý CategoryDeclaration */
    Route::group(array('prefix' => 'categoryDeclaration'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'categoryDeclaration.index', 'uses' => ModuleSystem . '\ProductCore\CategoryDeclarationController@index'));
        Route::get('ajaxGetItem', array('as' => 'categoryDeclaration.ajaxGetItem', 'uses' => ModuleSystem . '\ProductCore\CategoryDeclarationController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'categoryDeclaration.ajaxPostItem', 'uses' => ModuleSystem . '\ProductCore\CategoryDeclarationController@ajaxPostItem'));
    });

    /* Quản lý ClaimPartnerConfigController */
    Route::group(array('prefix' => 'partnerConfig'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'partnerConfig.index', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerConfigController@index'));
        Route::get('ajaxGetItem', array('as' => 'partnerConfig.ajaxGetItem', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerConfigController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'partnerConfig.ajaxPostItem', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerConfigController@ajaxPostItem'));
    });
    /* Quản lý ClaimPartnerConfigDetailController */
    Route::group(array('prefix' => 'partnerConfigDetail'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'partnerConfigDetail.index', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerConfigDetailController@index'));
        Route::get('ajaxGetItem', array('as' => 'partnerConfigDetail.ajaxGetItem', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerConfigDetailController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'partnerConfigDetail.ajaxPostItem', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerConfigDetailController@ajaxPostItem'));
    });
    /* Quản lý ClaimPartnerEmailController */
    Route::group(array('prefix' => 'partnerEmail'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'partnerEmail.index', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerEmailController@index'));
        Route::get('ajaxGetItem', array('as' => 'partnerEmail.ajaxGetItem', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerEmailController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'partnerEmail.ajaxPostItem', 'uses' => ModuleSystem . '\ClaimCore\ClaimPartnerEmailController@ajaxPostItem'));
    });
    /*********************************************************************************************
     * Partner DEVOPS
     *********************************************************************************************/

    Route::group(array('prefix' => 'partnerSys'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'partnerSys.index', 'uses' => ModuleSystem . '\PartnerCore\PartnerSysController@index'));
        Route::get('ajaxGetItem', array('as' => 'partnerSys.ajaxGetItem', 'uses' => ModuleSystem . '\PartnerCore\PartnerSysController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'partnerSys.ajaxPostItem', 'uses' => ModuleSystem . '\PartnerCore\PartnerSysController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'partnerSys.ajaxGetData', 'uses' => ModuleSystem . '\PartnerCore\PartnerSysController@ajaxGetData'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'partnerSys.ajaxUpdateRelation', 'uses' => ModuleSystem . '\PartnerCore\PartnerSysController@ajaxUpdateRelation'));
    });

    Route::group(array('prefix' => 'userPartnerSys'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'userPartnerSys.index', 'uses' => ModuleSystem . '\PartnerCore\UserSysController@index'));
        Route::get('ajaxGetItem', array('as' => 'userPartnerSys.ajaxGetItem', 'uses' => ModuleSystem . '\PartnerCore\UserSysController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'userPartnerSys.ajaxPostItem', 'uses' => ModuleSystem . '\PartnerCore\UserSysController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'userPartnerSys.ajaxGetData', 'uses' => ModuleSystem . '\PartnerCore\UserSysController@ajaxGetData'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'userPartnerSys.ajaxUpdateRelation', 'uses' => ModuleSystem . '\PartnerCore\UserSysController@ajaxUpdateRelation'));
    });
});

//Quản lý API CORE MỚI
Route::group(array('prefix' => 'sysApiCore'), function () {

    Route::group(array('prefix' => 'partnerApi'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'partnerApi.index', 'uses' => ModuleSystem . '\ApiHdiCore\PartnerApiController@index'));
        Route::get('ajaxGetItem', array('as' => 'partnerApi.ajaxGetItem', 'uses' => ModuleSystem . '\ApiHdiCore\PartnerApiController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'partnerApi.ajaxPostItem', 'uses' => ModuleSystem . '\ApiHdiCore\PartnerApiController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'partnerApi.ajaxGetData', 'uses' => ModuleSystem . '\ApiHdiCore\PartnerApiController@ajaxGetData'));
        //thêm tab other của partner
        Route::post('ajaxUpdateRelation', array('as' => 'partnerApi.ajaxUpdateRelation', 'uses' => ModuleSystem . '\ApiHdiCore\PartnerApiController@ajaxUpdateRelation'));
    });

    Route::group(array('prefix' => 'actionCodeApi'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'actionCodeApi.index', 'uses' => ModuleSystem . '\ApiHdiCore\ActionApiController@index'));
        Route::get('ajaxGetItem', array('as' => 'actionCodeApi.ajaxGetItem', 'uses' => ModuleSystem . '\ApiHdiCore\ActionApiController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'actionCodeApi.ajaxPostItem', 'uses' => ModuleSystem . '\ApiHdiCore\ActionApiController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'actionCodeApi.ajaxGetData', 'uses' => ModuleSystem . '\ApiHdiCore\ActionApiController@ajaxGetData'));
        //thêm tab other của action_code
        Route::post('ajaxUpdateRelation', array('as' => 'actionCodeApi.ajaxUpdateRelation', 'uses' => ModuleSystem . '\ApiHdiCore\ActionApiController@ajaxUpdateRelation'));
    });

    Route::group(array('prefix' => 'databaseApi'), function () {
        Route::match(['GET', 'POST'], 'index', array('as' => 'databaseApi.index', 'uses' => ModuleSystem . '\ApiHdiCore\DatabaseApiController@index'));
        Route::get('ajaxGetItem', array('as' => 'databaseApi.ajaxGetItem', 'uses' => ModuleSystem . '\ApiHdiCore\DatabaseApiController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'databaseApi.ajaxPostItem', 'uses' => ModuleSystem . '\ApiHdiCore\DatabaseApiController@ajaxPostItem'));
    });
});


Route::group(array('prefix' => 'systemApi'), function () {

    /* Quản lý Api hệ thống */
    Route::group(array('prefix' => 'apiSystem'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'apiSystem.index', 'uses' => ModuleSystem . '\ApiSystemController@index'));
        Route::get('ajaxGetItem', array('as' => 'apiSystem.ajaxGetItem', 'uses' => ModuleSystem . '\ApiSystemController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'apiSystem.ajaxPostItem', 'uses' => ModuleSystem . '\ApiSystemController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'apiSystem.ajaxGetData', 'uses' => ModuleSystem . '\ApiSystemController@ajaxGetData'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'apiSystem.ajaxUpdateRelation', 'uses' => ModuleSystem . '\ApiSystemController@ajaxUpdateRelation'));
    });

    /* Quản lý databases */
    Route::group(array('prefix' => 'databaseConnection'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'databaseConnection.index', 'uses' => ModuleSystem . '\DatabaseConnectionController@index'));
        Route::get('ajaxGetItem', array('as' => 'databaseConnection.ajaxGetItem', 'uses' => ModuleSystem . '\DatabaseConnectionController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'databaseConnection.ajaxPostItem', 'uses' => ModuleSystem . '\DatabaseConnectionController@ajaxPostItem'));
    });

    /* Quản lý template config */
    Route::group(array('prefix' => 'templateConfig'), function () {
        Route::match(['GET', 'POST'],'index', array('as' => 'templateConfig.index', 'uses' => ModuleSystem . '\TemplateSystemController@index'));
        Route::match(['GET', 'POST'],'indexTemplateEmail', array('as' => 'templateConfig.indexTemplateEmail', 'uses' => ModuleSystem . '\TemplateSystemController@indexTemplateEmail'));
        Route::get('ajaxGetItem', array('as' => 'templateConfig.ajaxGetItem', 'uses' => ModuleSystem . '\TemplateSystemController@ajaxGetItem'));
        Route::post('ajaxPostItem', array('as' => 'templateConfig.ajaxPostItem', 'uses' => ModuleSystem . '\TemplateSystemController@ajaxPostItem'));
        Route::post('ajaxGetData', array('as' => 'templateConfig.ajaxGetData', 'uses' => ModuleSystem . '\TemplateSystemController@ajaxGetData'));
        Route::post('ajaxGetAction', array('as' => 'templateConfig.ajaxGetAction', 'uses' => ModuleSystem . '\TemplateSystemController@ajaxGetAction'));
        //thêm tab other của api
        Route::post('ajaxUpdateRelation', array('as' => 'templateConfig.ajaxUpdateRelation', 'uses' => ModuleSystem . '\TemplateSystemController@ajaxUpdateRelation'));
    });

});
