<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 03/2022
* @Version   : 1.0
*/

use Illuminate\Support\Facades\Route;

const Admin = "Admin";
const ControllerFunc = "";

//Quan tri CMS cho admin
Route::get('/{url?}', array('as' => 'admin.login', 'uses' => Admin . '\AdminLoginController@getLogin'));
Route::post('/{url?}', array('as' => 'admin.login', 'uses' => Admin . '\AdminLoginController@postLogin'));

Route::group(array('prefix' => 'manager', 'before' => ''), function () {
    require __DIR__ . '/'.DIR_PRO_SYSTEM.'/admin.php';
    require __DIR__ . '/'.DIR_PRO_SYSTEM.'/routeSystem.php';
    require __DIR__ . '/'.DIR_PRO_PORTAL.'/routePortal.php';

    require __DIR__ . '/'.DIR_PRO_SELLING.'/routeSelling.php';
    require __DIR__ . '/'.DIR_PRO_SELLING.'/routeReport.php';
});

require __DIR__ . '/'.DIR_PRO_SELLING.'/routePartner.php';// đối tác
require __DIR__ . '/api.php';

Route::group(array('prefix' => 'ajaxCommon'), function () {
    Route::post('buildOption', array('as' => 'ajaxCommon.buildOption', 'uses' => ControllerFunc.'AjaxCommonController@getOptionCommon'));
});

Route::group(array('prefix' => 'ajax', 'before' => ''), function () {
    Route::post('uploadImage', array('as' => 'ajax.uploadImage', 'uses' => ControllerFunc.'AjaxUploadController@uploadImage'));
    Route::post('removeImageCommon', array('as' => 'ajax.removeImageCommon', 'uses' => ControllerFunc.'AjaxUploadController@removeImageCommon'));
    Route::post('getImageContentCommon', array('as' => 'ajax.getImageContentCommon', 'uses' => ControllerFunc.'AjaxUploadController@getImageContentCommon'));
    Route::get('sendEmail', array('as' => 'ajax.sendEmail', 'uses' => ControllerFunc.'AjaxUploadController@sendEmail'));
    Route::match(['GET', 'POST'], 'ajax-get-product-other-site', array('as' => 'ajax.getProductFromOther', 'uses' => ControllerFunc.'AjaxUploadController@getProductFromOtherSite'));
});

Route::get('sentmail/mail', array('as' => 'admin.mail', 'uses' => ControllerFunc.'MailSendController@sentEmail'));
Route::get('sentmail/sentEmailInsmart', array('as' => 'hdi.sentEmailInsmart', 'uses' => ControllerFunc.'MailSendController@sentEmailInsmart'));

