<?php
/*
* @Created by: QuynhTM
* @Author    : manhquynh1984@gmail.com
* @Date      : 03/2020
* @Version   : 1.0
*/
/*********************************************************************************************************
 * Router Partner
 * *******************************************************************************************************
 */

const ModulePartner = DIR_PRO_PARTNER_HDI;

Route::group(array('prefix' => 'partner'), function () {
    Route::get('pagePartnerError', array('as' => 'partner.pagePartnerError', 'uses' => ModulePartner . '\CreateOrderController@pagePartnerError'));

    //CreateOrderController: partner/indexCreaterOrder
    //Tạo đơn bảo hiểm cho đối tác
    Route::match(['GET', 'POST'],'indexCreaterOrder', array('as' => 'partner.indexCreaterOrder', 'uses' => ModulePartner . '\CreateOrderController@indexCreaterOrder'));

});


